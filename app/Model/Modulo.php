<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

App::uses("AppModel", "Model");

class Modulo extends AppModel {

    public $useTable     = "acos";
    public $actsAs       = array('Tree');
    public $displayField = "alias"; 

    public $belongsTo = array(
        "Parent" => array(
                "className"  => "Modulo",
                "foreignKey" => "parent_id",
                "conditions" => "",
                "fields"     => "",
                "order"      => ""
        ),
    );

    var $hasMany = array(
        "Permisos" => array(
                "className" => "Permiso",
                "foreignKey" => "aco_id",
                "dependent" => true,
                "conditions" => "",
                "fields" => "",
                "order" => "",
                "limit" => "",
                "offset" => "",
                "exclusive" => "",
                "finderQuery" => "",
                "counterQuery" => ""
        ),
    );

    public function isUploadedFile($params) {
    	$val = array_shift($params);
    	if ((isset($val["error"]) && $val["error"] == 0) ||
        	(!empty( $val["tmp_name"]) && $val["tmp_name"] != "none")
    	) {
        	return is_uploaded_file($val["tmp_name"]);
    	}
    	return false;
    }
    
    function beforeSave($options = null) {
 	  parent::beforeSave($options);
    }   
}
