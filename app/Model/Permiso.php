<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses("AppModel", "Model");

class Permiso extends AppModel {
    	
    public $useTable     = "aros_acos";
    public $displayField = "id";

    public $belongsTo = array(
            "Permitido" => array(
                    "className"  => "Permitido",
                    "foreignKey" => "aro_id",
                    "conditions" => "",
                    "fields"     => "",
                    "order"      => ""
            ),
            "Modulo" => array(
                    "className"  => "Modulo",
                    "foreignKey" => "aco_id",
                    "conditions" => "",
                    "fields"     => "",
                    "order"      => ""
            ),
    );
    
    public function isUploadedFile($params) {
    	$val = array_shift($params);
    	if ((isset($val["error"]) && $val["error"] == 0) || (!empty( $val["tmp_name"]) && $val["tmp_name"] != "none")) {
            return is_uploaded_file($val["tmp_name"]);
    	}
    	return false;
    }
    
    function beforeSave($options = null) {
        parent::beforeSave($options);
        if(isset($this->data['Permiso']['permitido'])){
            $permitido = 1;
        }else{
            $permitido = -1;
        }
        $this->data['Permiso']['create_'] = $permitido;
        $this->data['Permiso']['read_']   = $permitido;
        $this->data['Permiso']['update_'] = $permitido;
        $this->data['Permiso']['delete_'] = $permitido;
    }   
}
