<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

App::uses("AppModel", "Model");

class Mensaje extends AppModel {

    public $displayField = "id";

    public $belongsTo = array(
        'Origen' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'origen',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
        ),
        'Destino' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'destino',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
        ),
        'Propietario' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'propietario',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
        ),
        'Usuario' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'usuario',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
        )
    );

    function beforeSave($options = null) {
        parent::beforeSave($options);  
    }   
}
