<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

App::uses("AppModel", "Model");

class Grupo extends AppModel {

    public $displayField = "nombre";
    public $actsAs       = array('Acl' => array('type' => 'requester'));

    var $hasMany = array(
        "Usuario" => array(
            "className"    => "Usuario",
            "foreignKey"   => "grupo_id",
            "dependent"    => true,
            "conditions"   => "",
            "fields"       => "",
            "order"        => "",
            "limit"        => "",
            "offset"       => "",
            "exclusive"    => "",
            "finderQuery"  => "",
            "counterQuery" => ""
        ),
    );

    public function isUploadedFile($params) {
    	$val = array_shift($params);
    	if ((isset($val["error"]) && $val["error"] == 0) ||
        	(!empty( $val["tmp_name"]) && $val["tmp_name"] != "none")
    	) {
        	return is_uploaded_file($val["tmp_name"]);
    	}
    	return false;
    }
    
    function beforeSave($options = null) {
 	  parent::beforeSave($options); 
    }
    
    public function parentNode() {
        return null;
    }   
}
