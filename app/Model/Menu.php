<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
App::uses('AppModel', 'Model');

class Menu extends AppModel {

    public $displayField = 'titulo';

    var $belongsTo = array(
        'MenuParent' => array(
            'className'  => 'Menu',
            'foreignKey' => 'menu_id',
            'conditions' => '',
            'fields'     => '',
            'order'      => ''
        )
    );
    
    function afterSave($created, $options = null){
        parent::afterSave($created, $options = null);
        if($created==TRUE){
            App::uses('Modulo', 'Model');
            $modulo    = new Modulo();
            $lastId    = $this->getLastInsertID();
            $parentid  = $modulo->find('first',array('conditions'=>array('Modulo.alias'=>'permisosmenus')));
            $moduloReg = array('Modulo'=>array('id'=>null,'parent_id'=>$parentid['Modulo']['id'],'model'=>'Menu','foreign_key'=>$lastId,'alias'=>null));
            $modulo->save($moduloReg);
        }
    }
    
    public function beforeDelete($cascade = true) {
        parent::beforeDelete($cascade);
        App::uses('Modulo', 'Model');
        $modulo=new Modulo();
        $modulo->deleteAll(array('Modulo.model'=>'Menu','Modulo.foreign_key'=> $this->id),true);
    }
    
}