<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

App::uses('AppModel', 'Model');

class Condicion extends AppModel {

    public $belongsTo = array(
        'Filtro' => array(
                'className' => 'Filtro',
                'foreignKey' => 'filtro_id',
                'conditions' => '',
                'fields' => '',
                'order' => ''
        )
    );
}
