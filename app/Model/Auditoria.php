<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

App::uses("AppModel", "Model");

class Auditoria extends AppModel {

    public $displayField = "id";

    public function isUploadedFile($params) {
    	$val = array_shift($params);
    	if ((isset($val["error"]) && $val["error"] == 0) ||
        	(!empty( $val["tmp_name"]) && $val["tmp_name"] != "none")
    	) {
            return is_uploaded_file($val["tmp_name"]);
    	}
    	return false;
    }
    
    public function getGeneralStats(){
        $this->useAfterFind = false;
        $auditorias = array();
        if($this->isOracleDataSource()){
            $auditorias = $this->find('all',array(
                'group'  => "TO_CHAR(created,'YYYY-MM-DD HH24') || ':00:00'",
                'fields' => array(
                    "TO_CHAR(created,'YYYY-MM-DD HH24') || ':00:00' \"Auditoria.created\"",
                    "COUNT( created ) action"
            )));
        }else{
            $auditorias = $this->find('all',array(
                'group'  => 'date_format(created,"%Y-%m-%d %H")',
                'fields' => array(
                    ' COUNT( created ) as action', 
                    'created'
            )));
        }
        return $auditorias;
    }
    
}