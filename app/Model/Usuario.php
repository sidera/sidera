<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

App::uses('AppModel', 'Model');

class Usuario extends AppModel {

    public $displayField  = 'nombre';
    public $actsAs       = array('Acl' => array('type' => 'requester'));

    public $belongsTo = array(
            'Grupo' => array(
                    'className'  => 'Grupo',
                    'foreignKey' => 'grupo_id',
                    'conditions' => '',
                    'fields'     => '',
                    'order'      => ''
            )
    );
    
    public $validate = array(
		'username' => array(
			'alphaNumeric' => array(
				'rule' => 'alphaNumeric',
				'required' => true,
				'message' => 'Solo letras y n�meros'
			)
		),
    );

    public function isUploadedFile($params) {
        $val = array_shift($params);
        if ((isset($val['error']) && $val['error'] == 0) || (!empty( $val['tmp_name']) && $val['tmp_name'] != 'none') ) {
            return is_uploaded_file($val['tmp_name']);
        }
        return false;
    }

    function beforeSave($options = null) {
        parent::beforeSave($options);
    }

    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['Usuario']['grupo_id'])) {
            $grupoId = $this->data['Usuario']['grupo_id'];
        } else {
            $grupoId = $this->field('grupo_id');
        }
        if (!$grupoId) {
            return null;
        } else {
            return array('Grupo' => array('id' => $grupoId));
        }
    }
 
    function beforeFind($queryData) {
        parent::beforeFind($queryData);
    }
    
}
