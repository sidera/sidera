<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    
   var $inCharset  = CODIFICACION_FILES;
   var $outCharset = CODIFICACION_BD;
   var $ds = null;
   
   
   function beforeValidate($options = null) {
       $this->ds = ConnectionManager::getDataSource($this->useDbConfig)->config;
       $this->recodeToSaveData($this->data[$this->name]);
       
       if(isset($this->data['Usuario']['password'])){
           $this->data['Usuario']['password'] = AuthComponent::password($this->data['Usuario']['password']);
       }else{
           unset($this->data['Usuario']['password']);
       }
       return true;
    }
      
   function beforeSave($options = null) {
       $this->ds = ConnectionManager::getDataSource($this->useDbConfig)->config;
       $this->recodeToSaveData($this->data[$this->name]);
       
       if(isset($this->data['Usuario']['password'])){
           $this->data['Usuario']['password'] = AuthComponent::password($this->data['Usuario']['password']);
       }else{
           unset($this->data['Usuario']['password']);
       }
       return true;
    }
      
    function recodeToSaveData(&$datos){
        foreach( $datos as $key => $value){
          if (!isset($datos[$key]) || $datos[$key]=="") continue;
          $typeValue="unknown";
          
          if( preg_match("/^[0-9]{1,3}(?:\.?[0-9]{3})*(?:\,[0-9]{2})?$/",$value) == 1){
              if( $this->isSqlServerDataSource() ) {
                  if($datos[$key]=='NULL')
                  $datos[$key] = NULL;
                  
              }elseif($this->isOracleDataSource()){
              	$datos[$key] = str_replace(".", "", $value);
                
              }else{
              	$datos[$key] = str_replace(".", "", $value);
              	$datos[$key] = str_replace(",", ".", $datos[$key]);
              }
              $typeValue="float";
              
          }elseif ($this->IsDateToSave($value,$key)){
              $datos[$key] = $this->converToSaveDate($value,$key);
               if( $this->isSqlServerDataSource() ) {
                  if($datos[$key]=='NULL')
                  $datos[$key] = NULL;
              }
              $typeValue="date";
              
          }elseif ($this->IsTimeToSave($value,$key)){
              if( $this->isOracleDataSource() ) {
                  $datos[$key] = $this->converToSaveTime($value,$key);
              }
              $typeValue="time";
              
          }elseif(is_array($value)){
              $datos[$key] = $this->recodeToSaveData($value); 
              $typeValue="array";
              
          }else{
              $datos[$key]=  iconv($this->inCharset, $this->outCharset.'//TRANSLIT', $datos[$key]);
          }
        }
       return $datos;  
    }
    
    function converToSaveDate($date, $key) {
        $fieldsValue = split("[/.-.:.' ']", $date);
        list($day, $month, $year, $hour, $minute, $second) = $fieldsValue;
        $formattedDate = "";
        if ($this->isSqlServerDataSource()) {
            $formattedDate=$date;
        } else {
            if ($this->isOracleDataSource()) {
                if ($hour != '') {
                    $formattedDate = $year . "/" . $month . "/" . $day . " " . $hour . ":" . $minute . ":" . $second;
                } else {
                    $formattedDate = $year . "/" . $month . "/" . $day;
                }
            } else {
                if (isset($hour)) {
                    $formattedDate = $year . "/" . $month . "/" . $day . " " . $hour . ":" . $minute . ":" . $second;
                } else {
                    $formattedDate = $year . "/" . $month . "/" . $day;
                }
            }
        }
        return $formattedDate;
    }
  
    function converToShowDate($date){
        $dia     = substr($date, 8, 2);
        $mes     = substr($date, 5, 2);
        $anio    = substr($date, 0, 4);
        $hora    = substr($date, 11, 2);
        $minuto  = substr($date, 14, 2);
        $segundo = substr($date, 17, 2);
        $formattedDate = "";
        if( $hora && $minuto && $segundo ){ 
            if (($hora!='00')||($minuto!='00')||($segundo!='00')){
                $formattedDate = $dia."/".$mes."/".$anio."  ".$hora.":".$minuto.":".$segundo;
            }else{
                $formattedDate = $dia."/".$mes."/".$anio;
            }
        }else{
            $formattedDate = $dia."/".$mes."/".$anio;
        }
        return $formattedDate;
    }
    
    function converToShowTime($date,$key){
        $onlyTime = substr($date,11,8);
        return $onlyTime;
    }

    function converToSaveTime($time,$key){
        $date="1970/01/01 ".$time;
        return $date;
    }
  
   function IsDateToSave($date,$key){
        $fieldsValue = split( "[/.-.:.' ']", $date );
        if(isset($fieldsValue[0]) && isset($fieldsValue[1]) && isset($fieldsValue[2])){
            list($dd,$mm,$yy) = $fieldsValue;
            if ($dd!="" && $mm!="" && $yy!=""){
                return checkdate(intval($mm),  intval($dd),intval($yy));
            }
        }
        return false;
   }
   
   function IsTimeToSave($time,$key){
        if (!is_numeric($time) && date('H:i:s', strtotime($time)) == $time){
           return true;
        }
        return false;
   }  
   
   function IsTimeToShow($date,$key){
       $onlyDate = substr($date,0,10);
       $onlyTime = substr($date,11,8);
       if ($onlyDate == '1970-01-01'){ 
            if (date('H:i:s', strtotime($onlyTime)) == $onlyTime){
               return true;
            }
            return false;
       }
   }  
       
    function IsDateToShow($date,$key){
        $fieldsValue = split( "[-/: ]", $date );        
        if( isset($fieldsValue[4]) && $fieldsValue[4] !== "" ){
            if (date('Y-m-d', strtotime($date)) == $date || date('Y/m/d', strtotime($date)) == $date){
                return true;
            }
        }else{ 
            if (date('Y-m-d h:i:s', strtotime($date)) == $date || date('Y/m/d h:i:s', strtotime($date)) == $date){
                return true;
            }
        }
        return false;
    }

    function checkDateTime($date) {
        if (date('Y-m-d H:i:s', strtotime($date)) == $date) {
            return true;
        } 
        return false;
    }

    function isOracleDataSource(){
        $this->ds = ConnectionManager::getDataSource($this->useDbConfig)->config;    
        if($this->ds['datasource']=='Database/Oracle'){
            return true;
        }
        return false;
    }
    
    function isSqlServerDataSource(){
        $this->ds = ConnectionManager::getDataSource($this->useDbConfig)->config;    
        if($this->ds['datasource']=='Database/Sqlserver' || $this->ds['datasource']=='Database/Sybase'){
            return true;
        }
        return false;
    }
    
     function noSpecialCharecter($check){
        $value = array_values($check);
        $value = $value[0];
        $aguja = array("[", "]", "`", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "|", "+", "\\","\"","'", "=", "?", "�", ";", ":", "'", "<", ">", "{", "}", "[", ")");
        if($this->strposa($value,$aguja)){
            return false;
        }
        else{
            return true;
        }
    }
    function strposa($haystack, $needle, $offset = 0) {
        if (!is_array($needle))
            $needle = array($needle);
        foreach ($needle as $query) {
            if (strpos($haystack, $query, $offset) !== false)
                return true; // stop on first true result
        }
        return false;
    }
}