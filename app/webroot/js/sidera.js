$(document).ready(function() {
    $("#bg-loading").ajaxStart(function(){$(this).show();});
    $("#bg-loading").ajaxComplete(function(){$(this).hide();});
    $("#loading").ajaxStart(function(){$(this).show();});
    $("#loading").ajaxStop(function(){$(this).hide();});

    $(".noResponseAction").click( 
        function (event) {
            event.preventDefault();
            $.post($(this).attr('href'));
        }
    );
    
    $.ajaxSetup({
       error:function(x,e){
           if(x.status==0){
               //alert('No tiene conexion!!\n Compruebe si tiene acceso a la Red.');
           }else if(x.status==404){
                alert('URL no encontrada.');
           }else if(x.status==500){
               alert('Error Interno del Servidor.');
           //}else if(e=='parsererror'){
           //    alert('Error.\nParsing JSON Request failed.');
           }else if(e=='timeout'){
               alert('Finalizado tiempo de espera.');
           }else if(x.status==403){
               location.reload();
           }else {
               alert('Error desconocido.\n'+x.responseText);
               alert('Error desconocido.\n'+x.status);
               location.reload();
           }
       }
   });
    

});


//Funcion para que se mantega la misma diracci�n y deshabilita el retorno a otra pagina con back.
history.pushState(null, null, window.location.pathname);
window.addEventListener('popstate', function(event) {
history.pushState(null, null, window.location.pathname);
});
     
//Para elemento listaConsulta dentro de pesta�as
$('a[data-toggle="tab"]').on( "shown", function(event, ui) {
	var table = $.fn.dataTable.fnTables(true);
	if( table.length > 0 ) {
		$(table).dataTable().fnAdjustColumnSizing();
	}
});