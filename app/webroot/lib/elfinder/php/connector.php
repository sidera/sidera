<?php

error_reporting(0); // Set E_ALL for debuging

include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderConnector.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinder.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeDriver.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeLocalFileSystem.class.php';
// Required for MySQL storage connector
// include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeMySQL.class.php';
// Required for FTP connector support
// include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeFTP.class.php';


/**
 * Simple function to demonstrate how to control file access using "accessControl" callback.
 * This method will disable accessing files/folders starting from  '.' (dot)
 *
 * @param  string  $attr  attribute name (read|write|locked|hidden)
 * @param  string  $path  file path relative to volume root directory started with directory separator
 * @return bool|null
 **/
function access($attr, $path, $data, $volume) {
	return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
		? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
		:  null;                                    // else elFinder decide it itself
}
$directorio=$_GET["directorio"];
$alias=$_GET["alias"];
$path=$_GET["path"];
$url=$_GET["url"];
$pattern=$_GET["pattern"];
$read=$_GET["read"];
$write=$_GET["write"];
$rm=$_GET["rm"];
$hidden=$_GET["hidden"];
$locked=$_GET["locked"];

//die(print_r($directorio));
$opts = array(
	// 'debug' => true,
	'roots' => array(
		array(
                    'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
                    'path'          => $path, //'../../../../AppModulos/webroot/ApplicationFiles/Contenido/'.$directorio,         // path to files (REQUIRED)
                    'URL'           => $url, //'http://'.$_SERVER['HTTP_HOST'].'/gestionproyectos/app/AppModulos/webroot/ApplicationFiles/Contenido/'.$directorio, // URL to files (REQUIRED)
                    'alias'         => $alias,
                    'accessControl' => 'access',             // disable and hide dot starting files (OPTIONAL)
                    'uploadMaxSize' => '25M',
                    'disabled' => array('extract'),
                    'uploadAllow' => array(
                        'application/pdf',
                        'application/vnd.ms-excel',
                        'application/vnd.ms-powerpoint',
                        'application/vnd.oasis.opendocument.text',
                        'application/vnd.ms-powerpoint',
                        'application/vnd.ms-word',
                        'application/xml',
                        'application/x-gzip',
                        'application/x-bzip2',
                        'application/zip',
                        'application/x-rar',
                        'application/x-tar',
                        'application/x-7z-compressed',
                        'image/x-ms-bmp',
                        'image/jpeg',
                        'image/gif',
                        'image/png',
                        'image/tiff',
                        'image/x-targa',
                        'image/vnd.adobe.photoshop',
                        'image/xbm',
                        'image/pxm',
                        'text/html',
                        'text/css',
                        'text/rtf',
                        'text/rtfd',
                        'text/xml',
                        'text/x-sql',
                        'text/plain',
                        'text/x-comma-separated-values'
                        ),
                    'uploadDeny'  => array('all'),
                    'uploadOrder' => 'deny,allow',
                    'attributes' => array(
                        array( // hide readmes
                            'pattern'   => $pattern, //You can also set permissions for file types by adding, for example, .jpg inside pattern.
                            'read'      => $read,
                            'write'     => $write,
                            'rm'        => $rm,
                            'hidden'    => $hidden,
                            'locked'    => $locked
                        ),
                        array( // hide readmes
                            'pattern'   => '/.tmb|.quarantine/', //You can also set permissions for file types by adding, for example, .jpg inside pattern.
                            'read'      => true,
                            'write'     => true,
                            'rm'        => false,
                            'hidden'    => true,
                            'locked'    => false
                        )
                    )
                )
	)
);

// run elFinder
$connector = new elFinderConnector(new elFinder($opts));
$connector->run();

