<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
$archivo='//(cofiguracion dinamica)Parametros de la aplicacion
define("APLICACION", "'.iconv("UTF-8","ISO-8859-1",$opciones["Configuracion"]["aplicacion"]).'"); 
define("VERSION_MIAPP", "'.iconv("UTF-8","ISO-8859-1",$opciones["Configuracion"]["version_miapp"]).'");
define("ORGANISMO", "'.iconv("UTF-8","ISO-8859-1",$opciones["Configuracion"]["organismo"]).'"); 
define("CONSEJERIA", "'.iconv("UTF-8","ISO-8859-1",$opciones["Configuracion"]["consejeria"]).'");
define("DEBUG", "0");
define("MODO_MANTENIMIENTO", "'.$opciones["Configuracion"]["modo_mantenimiento"].'");
define("SUBNAVBAR","'.$opciones["Configuracion"]["subnavbar"].'");
define("AUDITORIA_ACCESO", "'.$opciones["Configuracion"]["auditoria_acceso"].'");
define("REPORT_UNIT", "'.iconv("UTF-8","ISO-8859-1",$opciones["Configuracion"]["report_unit"]).'");
define("REPORT_SERVER", "'.$opciones["Configuracion"]["report_server"].'");
define("DEFAULT_LOGIN_JASPER","'.$opciones["Configuracion"]["jasper_login"].'");
define("DEFAULT_PASSWORD_JASPER","'.$opciones["Configuracion"]["jasper_password"].'");
define("MOSTRAR_MENUSSISTEMA", "'.$opciones["Configuracion"]["mostrar_menus_sistema"].'");

//(cofiguracion dinamica)Codificacion de caracteres
define("CODIFICACION_BD", "'.$opciones["Configuracion"]["codificacion_bd"].'");
define("CODIFICACION_FILES", "'.$opciones["Configuracion"]["codificacion_files"].'");
define("INICIO", "'.$opciones["Configuracion"]["inicio"].'");

//(cofiguracion dinamica)Configuracion redireccion
define("PROTOCOLO","'.$opciones["Configuracion"]["protocolo"].'");             // "https" para preproduccion y produccion
define("RUTA_PROYECTO",   "'.$opciones["Configuracion"]["ruta_proyecto"].'"); // "00/miapp" para desarrollo, preproduccion y produccion 

//(configuracion dinamica)Correo Electronico
define("EMAIL_HOST","'.$opciones["Configuracion"]["email_host"].'");
define("EMAIL_PORT",'.$opciones["Configuracion"]["email_port"].');
define("EMAIL_USER","'.$opciones["Configuracion"]["email_user"].'");
define("EMAIL_PASSWORD","'.$opciones["Configuracion"]["email_password"].'");
define("EMAIL_TRANSPORT","'.$opciones["Configuracion"]["email_transport"].'");
define("EMAIL_FROM","'.$opciones["Configuracion"]["email_from"].'");
define("EMAIL_FORMAT","'.$opciones["Configuracion"]["email_format"].'");
define("EMAIL_TLS",'.$opciones["Configuracion"]["email_tls"].');

require_once "appModulosConstants.php";';
?>
