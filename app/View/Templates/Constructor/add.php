<?php
require  'Elementos'.DS.'estructuraPagina.php';
$vista="add";
$archivo=$cabeceraAdd;

foreach ($propiedadesModelo as $campo) {
    $nombreCampo = $campo["name"];
    $tipoCampo   = $campo["type"];
    $adminteNulos   = $campo["null"];
    $tama�o = $campo['length'];
    if ($tama�o) {
        if (isset($propiedad["precision"])) {//Oracle
            $entero = $propiedad['precision'] - $propiedad['scale'];
            $decimal = $propiedad['scale'];
        } else {
            $campoFloat = explode(",", $tama�o);
            $entero = $campoFloat[0] - $campoFloat[1];
            $decimal = ($campoFloat[1] == null) ? 0 : $campoFloat[1];
            ($decimal == 0) ? $maxlength = $entero : $maxlength = $entero + $decimal +1;
        }
    }   
    if($adminteNulos=="NO") $required="required"; else $required="";
    if($nombreCampo=="id")continue;
    
    $pattern='';
    if($tipoCampo=="integer") $pattern='pattern="<?=VALID_NUMERICO?>" title="s�lo n�meros enteros"';
    if($tipoCampo=="float")   $pattern='pattern="<?=VALID_DECIMAL?>" title="s�lo n�meros enteros o con '.$decimal.' decimales"';
    
    include 'Elementos'.DS.'elementosTiposAdd.php';

    $comienzoCampo = substr($nombreCampo,0,5);
    if ($comienzoCampo == 'bool_') $tipoCampo="boolean";
    if ($comienzoCampo == 'time_') $tipoCampo="time";
    $terminacionCampo = substr($nombreCampo,-3,strlen($nombreCampo));

    switch ($tipoCampo) {
        case "date":
                    $archivo.=$tipoDate;
                break;
        case (($tipoCampo=="datetime") || ($tipoCampo=="timestamp")):
                    $archivo.=$tipoDateTime;
                break;
        case ("time"):
                    $archivo.=$tipoTime;
                break;
        case "boolean":
                $archivo.=$tipoBoolean;
                break;
        default:
                if($terminacionCampo == "_id"){
                    $archivo.=$tipoCombo;
                }else{
                    $archivo.=$tipoSimple;
                }
                break;
    }
    $archivo.='
            ';
}

$archivo.=$pieAdd.$scriptAdd;