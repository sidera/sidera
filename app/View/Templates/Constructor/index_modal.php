<?php
require 'Elementos'.DS.'estructuraPagina.php';
$archivo=$cabeceraIndex;

                foreach ($propiedadesModelo as $campo) {
                    $nombreCampo = $campo["name"];
                    include 'Elementos'.DS.'elementosTiposIndex.php';
                    $terminacionCampo = substr($nombreCampo,-3,strlen($nombreCampo));
                    if($terminacionCampo != "_id"){
                        $archivo.=$headerIndexCampoSimple.'
                            ';
                    }else{
                        $archivo.=$headerIndexCampoAjeno.'
                            ';
                    }    
                }        
           		$archivo.='<!--nuevosCamposCabecera-->
                    </tr>
                </thead>
                <tbody class="griddata" id="'.$modelo.'GridData">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?php echo $registro["'.$modelo.'"]["id"];?>" class="gradeA" title="Haga clic para editar el registro"> 
                        <td><i value="<?=$registro["'.$modelo.'"]["id"]?>" class="checkCell fa fa-lg fa-square-o"></i></td>
                        ';
                         foreach ($propiedadesModelo as $campo) {
                            $nombreCampo = $campo["name"];
                            $tipoCampo   = $campo["type"];
                            $comienzoCampo = substr($nombreCampo,0,5);
                            $modeloAjeno      = ucwords(substr($nombreCampo,0,-3));
                            //Esto lo hago para decirle el numero de decimales que antes siempre mostraba 2
                            if ($campo["type"] == "float") {
                                $decimales = 0;
                                if (isset($campo["length"])) {
                                    $decimales = explode(',', $campo["length"]);
                                    $decimales = $decimales['1'];
                                    if($decimales=="" || $decimales==null)
                                        $decimales = 0;
                                }
                            } 
                            include 'Elementos'.DS.'elementosTiposIndex.php';
                                    
                            if ($comienzoCampo == 'bool_') $tipoCampo="boolean";
                            if ($comienzoCampo == 'time_') $tipoCampo="time";
                            
                            $terminacionCampo = substr($nombreCampo,$nombreCampo-3,strlen($nombreCampo));

                            if($terminacionCampo!="_id"){
                                switch ($tipoCampo) {
                                    case "boolean":
                                                    $archivo.=$tipoBoolean;
                                        break;
                                    case "float":    
                                                    $archivo.=$tipoFloat;
                                        break;   
                                    case "date":
                                                    $archivo.=$tipoDate;
                                        break;
                                    case (($tipoCampo=="datetime")||($tipoCampo=="timestamp")):  
                                                    $archivo.=$tipoDateTime;
                                        break;
                                    case ($tipoCampo=="time"):
                                                    $archivo.=$tipoTime;
                                        break;
                                    case ($tipoCampo=="integer" || $tipoCampo=="biginteger"):
                                                    $archivo.=$tipoInteger;
                                        break;
                                    default:
                                        $archivo.=$tipoSimple;
                                }
                            }else{
                                $archivo         .= $tipoAjeno;
                            }
                            $archivo.='
            ';                
                            }
$archivo.=$pieIndex.$scriptIndexModal;