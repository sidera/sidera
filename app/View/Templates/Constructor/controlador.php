<?php
$archivo='<?php
App::uses("AppController", "Controller");

class '.ucwords($controlador).'Controller extends AppController {

    var    $layout      = "ajax";
    public $modelo      = "'.$modelo.'";
    public $controlador = "'.$controlador.'";
';

$archivo.= '
    public function beforeFilter(){
        parent::beforeFilter();
        $this->modelo        = "'.$modelo.'";
        $this->controlador   = "'.$controlador.'";
        $this->modelosAjenos = array(';
        if($opciones['Constructor']['relaciones']['pertenecea']['numRelaciones']>0){
            for($i = 1; $i <= $opciones['Constructor']['relaciones']['pertenecea']['numRelaciones']; $i++){
                $modeloPerteneceA      = ucwords($opciones['Constructor']['relaciones']['pertenecea'][$i]);
                $controladorPerteneceA = strtolower(Inflector::pluralize($modeloPerteneceA));
                $campoClaveAjena       = strtolower($modeloPerteneceA)."_id";
                if($opciones['Constructor']['relaciones']['pertenecea']['check'][$i]){
                    $controladores = array_push($controladores,$controladorPerteneceA);
                    if($i == 1){
                        $archivo .= '"'.$modeloPerteneceA.'"';
                    }else{
                        $archivo .= ',"'.$modeloPerteneceA.'"';
                    }
                }	
            }
        }
        $archivo.=');
        $this->modelosAjenosRelacionados = array();    
    }
    
    public function beforeRender(){
        parent::beforeRender();
        $this->set("modelo",$this->modelo);
        $this->set("controlador",$this->controlador);
    }
';

 $archivo.='
    public function index() {
        $this->obtenerDisplayfields();
        $this->paginate["conditions"] = $this->comprobarFiltroActivo();
        $this->paginate["conditions"] = $this->comprobarBusquedaActiva();
        $this->paginate["limit"] = $this->comprobarPaginacion();
        $this->paginate["order"] = array("'.$modelo.'.id" => "DESC");
        //$this->auditoria->auditar($this);
        $registros = $this->paginate();
        $this->set(compact("registros"));
    }
';

$archivo.='
    public function add() {
        $this->obtenerDatosModelosAjenos();
        $this->checkModal();
        $this->set(compact("Add"));
    }
    
    public function create(){
        $this->'.$modelo.'->create();
        if ($this->'.$modelo.'->save($this->request->data)) {
            $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
            $configModulo = $this->Esquema->leerConfigSession($modelo);
            if($this->isAuditoriaActiva($configModulo)){
                $this->auditoria->auditar($this);
            }
        }  else {
            foreach ($this->$modelo->validationErrors as $key => $value) {
                $mensajeError.=$value[0];
            }
            $this->Session->setFlash(MSG_SAVEDERROR." ".$mensajeError, ERROR);
        }
        if($this->request->data["formType"]== "modal") {
            $this->autoRender = false;
        } else {
            $this->redirectSidera("'.$controlador.'", "index");
        }    
    }
';
	
$archivo.='
    public function edit($id = null) {
        $registro = $this->'.$modelo.'->read(null, $id);
        $this->obtenerDatosModelosAjenos();
        $this->Esquema->obtenerDatosModelosAjenosRelacionados($id,$this);
        $this->checkModal();
        $this->set(compact("Edit","registro"));
    }
    
    public function update($id = null){
        $this->'.$modelo.'->id = $id;
        //$this->oldData = $this->'.$modelo.'->read();
        if ($this->'.$modelo.'->save($this->request->data)) {
            $this->Session->setFlash(MSG_EDITSUCCESS, SUCCESS);
            //$this->auditoria->historico($this);
            //$this->auditoria->auditar($this);
        }else{
            foreach ($this->$modelo->validationErrors as $key => $value) {
                $mensajeError.=$value[0];
            }
            $this->Session->setFlash(MSG_SAVEDERROR." ".$mensajeError, ERROR);
        }
            
        if ($this->request->data["formType"] == "modal") {
            $this->autoRender = false;
        } else {
            if ($this->Session->check("page['.$modelo.']") == TRUE) {
                $this->redirectSidera("'.$controlador.'", "index/page:" . $this->Session->read("page['.$modelo.']"));
            } else {
                $this->redirectSidera("'.$controlador.'", "index");
            }
        } 
    }

    public function delete($id = null) {
        $this->autoRender = false;
        $datos            = $this->request->data["id"];
        $exploded_id      = explode(",", $datos);
        foreach ($exploded_id as $id) {
            $this->'.$modelo.'->id=$id;
            //$this->oldData = $this->'.$modelo.'->read();
            if (!$this->'.$modelo.'->exists()) {
                $this->Session->setFlash(MSG_DELETENOTICE, NOTICE);
            }
            //$this->auditoria->historico($this);
            //$this->auditoria->auditar($this);
            $this->'.$modelo.'->delete(); 
        }
        $this->Session->setFlash(MSG_DELETESUCCESS, SUCCESS);  
        if ($this->request->data["formType"] != "modal") {
            $this->redirectSidera("'.$controlador.'", "index");
        }           
    }
';

if($opciones['Constructor']['general']["crearIndexModal"]==ACTIVO){
    

$archivo.='    
    public function indexModal($id_relacionado,$campo_relacionado,$id_subgrid) {
        $this->comprobarPaginacion();
        
        $this->Esquema->comprobarSessionModeloYML($this->modelo);
        $configModulo = $this->Esquema->leerConfigSession($this->modelo);
        $camposModelo = $configModulo["general"]["campos"];
     
        $fieldsForSearch = $this->Esquema->obtenerCamposABuscarEnIndex($this,$configModulo,$camposModelo,$this->modelo);
        $this->comprobarFiltroActivo();
        $this->comprobarBusquedaActiva($fieldsForSearch);
        
        //En este apartado le metemos el campo con el que esta relacionado el modelo actual
        $this->paginate["conditions"][$this->modelo.\'.\'.$campo_relacionado]=$id_relacionado;
        $this->paginate["limit"] = $configModulo["general"]["config"]["registrosPagina"];
        $this->paginate["order"] = array("$this->modelo.id" => "DESC");
        
        if($this->isAuditoriaActiva($configModulo,"view")){
            $this->auditoria->auditar($this);
        }
        $configModulo["general"]["botones"]["btconfigurar"]     = $this->Session->read("CofigurarModulos");
        $configModulo["general"]["botones"]["botonSincronizar"] = $this->Session->read("CofigurarModulos");
        
        $registros = $this->paginate();
        $this->set(compact("registros","configModulo","id_subgrid","id_relacionado","campo_relacionado"));
    }
    
    public function cargarFormModal($id_relacionado,$campo_relacionado,$id_subgrid,$id_edit){
        $this->set(compact("id_relacionado","campo_relacionado","id_subgrid","id_edit")); 
    }';
}
$archivo.='
}';