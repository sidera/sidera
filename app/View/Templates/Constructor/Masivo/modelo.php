<?php
$db      = ConnectionManager::getDataSource($obj->conexion);
$source  = $db->config['datasource'];
$archivo = '<?php
App::uses("AppModel", "Model");

class '.$obj->modeloACrear.' extends AppModel {

    var $useDbConfig     = "'.$obj->conexion.'";
    public $displayField = "'."id".'";

';
if(count($obj->relacionesPerteneceA)>0){
	$archivo.='
        //Relaciones tipo : Un '.$modelo.' pertenece a un/una "Modelo"	
	public $belongsTo = array(
	';        
        foreach ($obj->relacionesPerteneceA as $relacion) {
            $modeloPerteneceA = ucwords($relacion["modelo"]);
            $campoClaveAjena  = $relacion["campo"];            
            $archivo.='	"'.$modeloPerteneceA.'" => array(
			"className"  => "'.$modeloPerteneceA.'",
			"foreignKey" => "'.$campoClaveAjena.'",
			"conditions" => "",
			"fields"     => "",
			"order"      => ""
		),
	';
        }
        $archivo.=');

	';
}       

if(count($obj->relacionesTieneMuchos)>0){
    $archivo.=' 
        //Relaciones tipo : Un '.$modelo.' tiene muchos/muchas "Modelos"	
	var $hasMany = array(
	';
    foreach ($obj->relacionesTieneMuchos as $relacion) {
        $modeloTieneMuchos=ucwords($relacion["modelo"]);
        $campoClaveAjena=$relacion["principal"]."_id";
        $archivo.=' 	"'.$modeloTieneMuchos.'" => array(
				"className"    => "'.$modeloTieneMuchos.'",
				"foreignKey"   => "'.$campoClaveAjena.'",
				"dependent"    => false,
				"conditions"   => "",
				"fields"       => "",
				"order"        => "",
				"limit"        => "",
				"offset"       => "",
				"exclusive"    => "",
				"finderQuery"  => "",
				"counterQuery" => ""
			),
        ';
       }
    $archivo.=');
        
	';
}

$archivo.='    
 	function beforeSave($options = null) {
 	  ';
 	
        $archivo.='parent::beforeSave($options); 
      
    }   
}';