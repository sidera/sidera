<?php
$archivo='
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php if($configModulo["general"]["botones"]["btNuevo"])            echo $this->element("ElementsVista".DS."Index".DS."botonNuevo"); ?>
        <?php if($configModulo["general"]["botones"]["btEditar"])           echo $this->element("ElementsVista".DS."Index".DS."botonEditar"); ?>
        <?php if($configModulo["general"]["botones"]["btEliminar"])         echo $this->element("ElementsVista".DS."Index".DS."botonEliminar"); ?>
        <?php if($configModulo["general"]["botones"]["btFiltrar"])          echo $this->element("ElementsVista".DS."Index".DS."botonFiltrar"); ?>
        <?php if($configModulo["general"]["botones"]["btImportar"])         echo $this->element("ElementsVista".DS."Index".DS."botonImportar"); ?>
        <?php if($configModulo["general"]["botones"]["btExportar"])         echo $this->element("ElementsVista".DS."Index".DS."botonExportar"); ?>
        <?php if($configModulo["general"]["botones"]["btconfigurar"])       echo $this->element("ElementsVista".DS."Index".DS."botonConfigurar"); ?>
        <?php if($configModulo["general"]["botones"]["botonSincronizar"])   echo $this->element("ElementsVista".DS."Index".DS."botonSincronizar"); ?>   
        <?php if($configModulo["general"]["acciones"]["accionEditar"])      echo $this->element("ElementsVista".DS."Index".DS."accionEditar"); ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."accionOrdenarColumnasGrid"); ?>
        <?php if($configModulo["general"]["acciones"]["accionChekearTodos"]) echo $this->element("ElementsVista".DS."Index".DS."accionCheckearTodos"); ?>
      </div>
      <div class="btn-group pull-right right span4 input-xxlarge">
        <?php if($configModulo["general"]["botones"]["btBuscar"]) echo $this->element("ElementsVista".DS."Index".DS."searchForm",array("model"=>"$controlador"));?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered">
        <div>
            <div class="box-header span4">
                <i class="fa fa-list"></i> <?php echo ($configModulo["general"]["config"]["etiquetaModulo"])?$configModulo["general"]["config"]["etiquetaModulo"]:$modelo;?>
            </div>
            <div class="groupPagination span6 pull-right">
                <?php echo $this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>$modelo));?>
                <?php echo $this->element("ElementsVista".DS."Index".DS."registrosPorPagina");?>
            </div>
        </div>
        <div role="grid" class="dataTables_wrapper" id="DataTables_Table_0_wrapper">
            <table class="table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="ui-state-default notdraggable check" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                            <i class="fa fa-check"></i>
                        </th>
                       ';
                        foreach ($propiedadesModelo as $campo) {
                            $archivo.='	<?php $'.$campo[name].'="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"'.$campo[name].'","$configModulo"=>$configModulo));?><?php $'.$campo[name].'="fin";?>
			     ';
                        }
			
           $archivo.='    <?php $espacioFuturosCampos=true; ?>
                	</tr>
                </thead>
                <tbody class="griddata">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?php echo $registro[$modelo]["id"];?>" class="gradeA" title="Haga clic para editar el registro"> 
                        <td class="checkCell">
                            <input type="checkbox" value="<?php echo $registro[$modelo]["id"];?>" class="itemSelector" title="Seleccione los elementos que desee eliminar">
                        </td>
                        ';
                         foreach ($propiedadesModelo as $campo) {
                            $nombreCampo=$campo[name];
                            $comienzoCampo=substr($nombreCampo,0,5);
                            if ($comienzoCampo=='bool_') $campo[type]="boolean";
			    $terminacionCampo=substr($nombreCampo,$nombreCampo-3,strlen($nombreCampo));
			            	if($terminacionCampo<>"_id"){
			            		if($campo[type]=="boolean"){
                                                    $archivo.='<td class="'.$nombreCampo.'"><?php echo $this->Form->input("",array("type" => "checkbox","disabled"=>"true",checked =>$registro[$modelo]["'.$nombreCampo.'"])); ?></td>
						';
                                                }elseif($campo[type]=="float"){
                                                    $archivo.='<td class="'.$nombreCampo.'"><?php if(!is_null($registro[$modelo]["'.$nombreCampo.'"])) echo $this->Number->format($registro[$modelo]["'.$nombreCampo.'"], array(
                                                                    "places"    => 2,
                                                                    "before"    => false,
                                                                    "decimals"  => ".",
                                                                    "thousands" => ","
                                                                    )); ?></td>
						';
                                                }elseif($campo["type"]=="date"){
                                                    $archivo.='<td class="'.$nombreCampo.'"><?php if(!is_null($registro[$modelo]["'.$nombreCampo.'"])) echo $this->Time->format("d/m/Y",$registro[$modelo]["'.$nombreCampo.'"]); ?></td>
						';
                                                }elseif($campo["type"]=="datetime" || $campo["type"]=="timestamp"){
                                                    $archivo.='<td class="'.$nombreCampo.'"><?php  if(!is_null($registro[$modelo]["'.$nombreCampo.'"]))echo $this->Time->format("d/m/Y h:i:s",$registro[$modelo]["'.$nombreCampo.'"]); ?></td>
						';
                                                }elseif($campo["type"]=="time"){
                                                    $archivo.='<td class="'.$nombreCampo.'"><?php  if(!is_null($registro[$modelo]["'.$nombreCampo.'"]))echo $this->Time->format("H:i:s",$registro[$modelo]["'.$nombreCampo.'"]); ?></td>
						';
                                                }else{
                                                    $archivo.='<td class="'.$nombreCampo.'"><?php echo h($registro[$modelo]["'.$nombreCampo.'"]); ?></td>
						';
                                                }
                                            }else{
                                                $modeloAjeno      = ucwords(substr($nombreCampo,0,-3));
                                                $controladorAjeno = strtolower(Inflector::pluralize($modeloAjeno));
                                                $archivo         .= '<td class="'.$nombreCampo.'">
			                	<?php echo h($registro["'.$modeloAjeno.'"][$'.$nombreCampo.'DisplayField]); ?></td>
						';
                                            }
                            }
                            $archivo.='
                        <?php $espacioFuturosCampos=true; ?>
                        </tr>
                     <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" id="myScript">
$(document).ready(function() { 
    $(":checkbox").uniform();    
    <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); ?>
    <?php echo $this->element("ElementsVista".DS."Index".DS."accionMostrarEnIndex"); ?>
});
</script>';
