<?php
$archivo='
<?php echo $this->Form->create("$modelo",array("url" => array("controller" => $controlador, "action" => "create")));?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsVista".DS."Add".DS."botonGuardarAdd"); ?>
        <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <i class="fa fa-pencil"></i> Nuevo Registro - <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __($modelo);?>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="padded">
                    <div class="section-title <?php if($configModulo["general"]["columnas"]["columna1"]=="") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna1"]; ?> </div>
                    <div class="vpadded input-xlarge" id="columna1">
                        ';
             foreach ($propiedadesModelo as $campo) {
                $nombreCampo = $campo["name"];
                $tipoCampo   = $campo["type"];
                $archivo    .='
                    	<?php $'.$nombreCampo.'="inicio";?>';
                $comienzoCampo = substr($nombreCampo,0,5);
                if ($comienzoCampo == 'bool_') $tipoCampo="boolean";
                if ($comienzoCampo == 'time_') $tipoCampo="time";
                $terminacionCampo=substr($nombreCampo,$nombreCampo-3,strlen($nombreCampo));
                switch ($tipoCampo) {
                    case "date":
                        $archivo.='
	                        <div id="'.$nombreCampo.'Datepicker" class="input-prepend date <?php echo $configModulo["add"]["campos"]["'.$nombreCampo.'"]["orden"];?>"  title="Doble click para la eliminar" >
	                            <label><?php echo $configModulo["add"]["campos"]["'.$nombreCampo.'"]["etiqueta"];?></label>
	                            <button class="btn light-blue inverse date-button add-on datetimepicker-button">
	                                <i class="fa fa-calendar"></i>
	                            </button>
	                            <input type="text" ondblclick="$(this).val(\'\')" readonly=""  class="input-small center datepicker"  name="data['.$modelo.']['.$nombreCampo.']" >
	                        </div>
                        ';
                        break;
                    case (($tipoCampo=="datetime") || ($tipoCampo=="timestamp")):
                        $archivo.='
	                        <div  id="'.$nombreCampo.'DateTimepicker" class="input-prepend <?php echo $configModulo["add"]["campos"]["'.$nombreCampo.'"]["orden"];?>" title="Doble click para la eliminar">
	                            <label><?php echo $configModulo["add"]["campos"]["'.$nombreCampo.'"]["etiqueta"];?></label>
	                            <button type="button"  class="btn light-blue inverse date-button add-on datetimepicker-button">
	                                <i class="fa fa-calendar"></i>
	                            </button>
	                            <input type="text"  ondblclick="$(this).val(\'\')" readonly="readonly" class="input-small center datepicker"  name="data['.$modelo.']['.$nombreCampo.']"/>							
	                        </div>
                        ';
                        break;
                    case ("time"):
                        $archivo.='
	                        <div  id="'.$nombreCampo.'Timepicker" class="input-prepend <?php echo $configModulo["add"]["campos"]["'.$nombreCampo.'"]["orden"];?>" title="Doble click para la eliminar">
	                            <label><?php echo $configModulo["add"]["campos"]["'.$nombreCampo.'"]["etiqueta"];?></label>
	                            <button type="button"  class="btn light-blue inverse date-button add-on datetimepicker-button">
	                                <i class="fa fa-time"></i>
	                            </button>
	                            <input type="text" ondblclick="$(this).val(\'\')" readonly="readonly" class="input-small center datepicker"  name="data['.$modelo.']['.$nombreCampo.']"/>							
	                        </div>
                        ';
                        break;
                    case "boolean":
                        $archivo.='
	                        <?php 
                        		echo $this->Form->input("'.$nombreCampo.'",array(
		                            "before" => "<label>",
		                            "after"  => "".$configModulo["add"]["campos"]["'.$nombreCampo.'"]["etiqueta"]."</label>",
		                            "type"   => "checkbox",    
		                            "id"     => "'.$nombreCampo.'",
		                            "label"  => false,
		                            "class"  => "input-xlarge",
		                            "placeholder" => "Escriba el '.$nombreCampo.' ...",
		                            "div"    => array("class"=>" ".$configModulo["add"]["campos"]["'.$nombreCampo.'"]["orden"]."")
	                            ));
		                    ?>
                         ';
                        break;
                    default:
                        if($terminacionCampo<>"_id"){
                            $archivo.='
                            <?php 
                            	echo $this->Form->input("'.$nombreCampo.'",array(
	                                "id"     => "'.$nombreCampo.'",
	                                "label"  => $configModulo["add"]["campos"]["'.$nombreCampo.'"]["etiqueta"],
	                                "class"  => "input-xlarge",
	                                "type"   => "text",
	                                "placeholder" => "Escriba el '.$nombreCampo.' ...",
	                                "div"    => array("class"=>" ".$configModulo["add"]["campos"]["'.$nombreCampo.'"]["orden"]."")
                                ));?>
                         ';
                        }else{
                            $archivo.='
                            <?php 
                            	echo $this->Form->input("'.$nombreCampo.'",array(
	                                "id"     => "'.$nombreCampo.'",
	                                "label"  => $configModulo["add"]["campos"]["'.$nombreCampo.'"]["etiqueta"],
	                                "class"  => "input-xlarge chzn-select",
	                                "empty"  => "Seleccione una opci�n...",
	                                "placeholder" => "Escriba el '.$nombreCampo.' ...",
	                                "div"    => array("class"=>" ".$configModulo["add"]["campos"]["'.$nombreCampo.'"]["orden"]."")
                                ));?>
                         ';
                        }
                        break;
                }
                $archivo.='
                		 <?php $'.$nombreCampo.'="fin";?>
                		 ';
            }        
           $archivo.='
                         <?php $espacioFuturosCampos=true; ?>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="padded">
                    <div class="section-title <?php if($configModulo["general"]["columnas"]["columna2"]=="hide") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna2"]; ?> </div>
                    <div class="vpadded input-xlarge" id="columna2"></div>
                </div>
            </div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
    <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); ?>
    <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); ?>
    <?php if($configModulo["general"]["acciones"]["accionOrdenarCampos"]) echo $this->element("ElementsVista".DS."accionOrdenarCamposFormularios"); ?>
    <?php echo $this->element("ElementsVista".DS."Add".DS."accionMostrarEnAdd"); ?>
    <?php $espacioFuturosControles=true; ?>
}); 
</script>';