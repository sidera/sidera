<?php
$archivo='<?php
App::uses("AppController", "Controller");

class '.$obj->controladorACrear.'Controller extends AppController {
    var $layout      = "ajax";
';

$controladores = array();
if(count($obj->relacionesPerteneceA)>0){
    foreach ($obj->relacionesPerteneceA as $relacion) {
        $modeloPerteneceA = ucwords($relacion["modelo"]);
        $controladores[]  = '"'.$modeloPerteneceA.'"';
    } 
}
$modelosA = implode(",",$controladores);      
$archivo .= '
    public function beforeFilter(){
        parent::beforeFilter();
        $this->modelo        = "'.$obj->modeloACrear.'";
        $this->controlador   = "'.strtolower($obj->controladorACrear).'";
        $this->modelosAjenos = array('.$modelosA.');
        $this->modelosAjenosRelacionados = array();
    }
';        
$archivo.='
    public function index() {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        $this->comprobarPaginacion();
        $this->Esquema->comprobarSessionModeloYML($modelo);
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $camposModelo = $configModulo["general"]["campos"];
     
        $fieldsForSearch = $this->Esquema->obtenerCamposABuscarEnIndex($this,$configModulo,$camposModelo,$modelo);
        $this->comprobarFiltroActivo();
        $this->comprobarBusquedaActiva($fieldsForSearch);
        
        $this->paginate["limit"] = $configModulo["general"]["config"]["registrosPagina"];
        $this->paginate["order"] = array("$modelo.id" => "DESC");
        
        if($this->isAuditoriaActiva($configModulo,"view")){
            $this->auditoria->auditar($this);
        }
        $configModulo["general"]["botones"]["btconfigurar"]     = $this->Session->read("CofigurarModulos");
        $configModulo["general"]["botones"]["botonSincronizar"] = $this->Session->read("CofigurarModulos");
        
        $registros = $this->paginate();
        $this->set(compact("registros","modelo","controlador","configModulo"));
    }

    public function add() {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        $schema       = $this->$modelo->schema();
        $propiedades  = $this->Esquema->propiedadesModelo($schema); 
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $configModulo = $this->Esquema->identificarCamposObligatorios($configModulo,$propiedades,"add");
        $this->Esquema->obtenerDatosModelosAjenos($this);
        $accion       = "Add";
        $this->checkModal();
        $this->set(compact("modelo","propiedades","controlador","accion","configModulo"));
    }
	
    public function create(){
        $modelo      = $this->modelo;
        $controlador = $this->controlador;

        $this->$modelo->create();            
        if ($this->$modelo->save($this->request->data)) {
            $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
            $configModulo = $this->Esquema->leerConfigSession($modelo);
            if($this->isAuditoriaActiva($configModulo)){
                $this->auditoria->auditar($this);
            }
        } else {
            $this->Session->setFlash(MSG_SAVEDERROR, ERROR);
        }
        $this->redirectSidera($controlador,"index");   
    }

    public function edit($id = null) {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;        
        $accion       = "Edit";  
        $schema       = $this->$modelo->schema(true);
        $propiedades  = $this->Esquema->propiedadesModelo($schema);  
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $configModulo = $this->Esquema->identificarCamposObligatorios($configModulo,$propiedades,"edit");
        
        $this->request->data = $this->$modelo->read(null, $id);
        
        $this->Esquema->obtenerDatosModelosAjenos($this);
        $this->Esquema->obtenerDatosModelosAjenosRelacionados($id,$this);

        if($this->isAuditoriaActiva($configModulo)){
            $this->auditoria->auditar($this);
        }
        $this->set(compact("modelo","propiedades","controlador","accion","configModulo"));
    }
    
    public function update($id = null){
        $modelo            = $this->modelo;
        $controlador       = $this->controlador;
        $this->$modelo->id = $id;
        
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        if($this->isAuditoriaActiva($configModulo)){
            $this->auditoria->auditar($this);
        }
             
        if($this->isHistoricoActivo($configModulo)){
                $this->oldData = $this->$modelo->read();
        }
        
        if ($this->$modelo->save($this->request->data)) {
            $this->Session->setFlash(MSG_EDITSUCCESS, SUCCESS);
            if($this->isHistoricoActivo($configModulo)){
                $this->auditoria->historico($this);
            }
        } else {
            $this->Session->setFlash(MSG_EDITERROR, ERROR);
        }
        if($this->Session->check("page[$this->modelo]")==TRUE){    
            $this->redirectSidera($controlador,"index/page:".$this->Session->read("page[$this->modelo]"));   
        }else{
            $this->redirectSidera($controlador,"index");        
        }
    }

    public function delete($id = null) {
        $this->autoRender = false;
        $modelo           = $this->modelo;
        $controlador      = $this->controlador;
        $datos            = $this->request->data["id"];
        $exploded_id      = explode(",", $datos);
        $configModulo     = $this->Esquema->leerConfigSession($modelo);
        
        foreach ($exploded_id as $id) {
            $this->$modelo->id=$id;
            if (!$this->$modelo->exists()) {
                $this->Session->setFlash(MSG_DELETENOTICE, NOTICE);
            }
            if($this->isAuditoriaActiva($configModulo)){
                $this->auditoria->auditar($this);
            }
            if($this->isHistoricoActivo($configModulo)){
                $this->auditoria->historico($this);
            }
            $this->$modelo->delete(); 
        }
        $this->Session->setFlash(MSG_DELETESUCCESS, SUCCESS);  
        $this->redirectSidera($controlador,"index");        
    }
}';