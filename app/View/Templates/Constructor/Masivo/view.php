<?php
$archivo='
<?php echo $this->Form->create("$modelo");?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); //Bloque para mensajes con setFlash ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
       	<?php if($configModulo["general"]["botones"]["btNuevo"]) echo $this->element("ElementsVista".DS."Index".DS."botonNuevo"); //Elemento con funcionalidad de boton de Nuevo ?>
        <?php if($configModulo["general"]["acciones"]["accionEditar"]) echo $this->element("ElementsVista".DS."Index".DS."botonEditar"); //Elemento con funcionalidad de boton de Editar ?>
        <?php if($configModulo["general"]["botones"]["btEliminar"]) echo $this->element("ElementsVista".DS."botonEliminarView"); //Elemento con funcionalidad de boton de Eliminar (propio de la vista)?>
        <?php echo $this->element("ElementsVista".DS."botonCancelar"); //Elemento con funcionalidad de boton de Cancelar ?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <i class="fa fa-eye-open"></i> Consultar Registro - <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __($modelo);?>
        </div>
        <div class="row-fluid">
        	<div class="span6">
				<div class="padded">
					<div class="section-title <?php if($configModulo["general"]["columnas"]["columna1"]=="") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna1"]; ?> </div>
					<div class="vpadded" id="columna1">
        				'; 
	       foreach ($propiedadesModelo as $campo) {
		$nombreCampo=$campo[name];
                $tipoCampo=$campo[type];
                $archivo.='
                    <?php $'.$nombreCampo.'="inicio";?>';
                $comienzoCampo=substr($nombreCampo,0,5);
                if ($comienzoCampo=='bool_') $tipoCampo="boolean";
				$terminacionCampo=substr($nombreCampo,$nombreCampo-3,strlen($nombreCampo));
            	if($terminacionCampo<>"_id"){
					if($tipoCampo=="boolean"){
						$archivo.='
					<div class="<?php echo $configModulo["view"]["campos"]["'.$nombreCampo.'"]["orden"];?>">	
						<label><?php echo $configModulo["view"]["campos"]["'.$nombreCampo.'"]["etiqueta"];?></label>
				        <div class="well well-small fieldView" rel="tooltip left" title="Haga clic para editar">
				            <?php echo $this->Form->input("",array("type" => "checkbox","id"=>"'.$nombreCampo.'","label"=>false,checked =>$registro[$modelo]["'.$nombreCampo.'"])); ?>
				        </div>
			        </div>
					';
					}else{
						$archivo.='
					<div class="<?php echo $configModulo["view"]["campos"]["'.$nombreCampo.'"]["orden"];?>">	
						<label><?php echo $configModulo["view"]["campos"]["'.$nombreCampo.'"]["etiqueta"];?></label>
				        <div class="well well-small fieldView" rel="tooltip left" title="Haga clic para editar">
				            <?php echo h($registro[$modelo]["'.$nombreCampo.'"]);?>
				        </div>
			        </div>
					';
					}	
					
				}else{
					$modeloAjeno = ucwords(substr($nombreCampo,0,-3));
					$archivo.='
					<div class="<?php echo $configModulo["view"]["campos"]["'.$nombreCampo.'"]["orden"];?>">	
						<label><?php echo $configModulo["view"]["campos"]["'.$nombreCampo.'"]["etiqueta"];?></label>
				        <div class="well well-small fieldView" rel="tooltip left" title="Haga clic para editar">
				            <?php echo h($registro['.$modeloAjeno.'][$'.$nombreCampo.'DisplayField]);?>
				        </div>
			        </div>
					';
				}
                $archivo.='<?php $'.$nombreCampo.'="fin";?>';
			}        
           $archivo.='
                    <?php $espacioFuturosCampos=true; ?>
           			
                    </div>
				</div>
			</div>
			<div class="span6">
				<div class="padded">
					<div class="section-title <?php if($configModulo["general"]["columnas"]["columna2"]=="hide") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna2"]; ?> </div>
					<div class="vpadded" id="columna2">
					</div>
				</div>
			</div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
	<?php echo $this->element("ElementsVista".DS."javascriptRequerido"); //Elemento con el javascript requerido para esta vista ?>
    <?php echo $this->element("ElementsVista".DS."accionOrdenarCamposFormularios"); //Elemento con funcionalidad de ordenar campos del formulario ?>
}); 
</script>';
?>