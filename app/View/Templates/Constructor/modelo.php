<?php
$db      = ConnectionManager::getDataSource($opciones['Constructor']['conexion']);
$source  = $db->config['datasource'];
$archivo = '<?php
App::uses("AppModel", "Model");

class '.$modelo.' extends AppModel {
    
    var $useDbConfig     = "'.$opciones['Constructor']['conexion'].'";
    public $displayField = "'.$opciones['Constructor']['general']['displayfield'].'";

';

$archivo .='public $validate = array(';
foreach ($propiedadesModelo as $key => $value) {
    $archivo.='"' . $value['name'] . '"=> array(';

    switch ($value['type']) {
        case "integer":
            if (isset($value['Length2']) || isset($value['length'])) {
                if (isset($value['length'])) {
                    $maximo_integer = $value['length'];
                }
                if (isset($value['Length2'])) {
                    $maximo_integer = $value['Length2'];
                }
            } else {
                $maximo_integer = 10;
            }
            include 'Elementos' . DS . 'elementosValidateModel.php';
            if (($maximo_integer == 11) || ($maximo_integer == 10)) {
                if (($value['null'] == "NO") && ($value['name'] != "id")) {
                    $archivo.=$tipo_integer_obligatorio;
                } else {
                    $archivo.=$tipo_integer;
                }
            } else {
                if (($value['null'] == "NO") && ($value['name'] != "id")) {
                    $archivo.=$tipo_integer_obligatorio_delimitado;
                } else {
                    $archivo.=$tipo_integer_delimitado;
                }
            }
            break;
        case "biginteger":
            if (isset($value['Length2']) || isset($value['length'])) {
                if (isset($value['length'])) {
                    $maximo_big_integer = $value['length'];
                }
                if (isset($value['Length2'])) {
                    $maximo_big_integer = $value['Length2'];
                }
            } else {
                $maximo_big_integer = 19;
            }
            include 'Elementos' . DS . 'elementosValidateModel.php';
            if ($value['null'] == "NO") {
                $archivo.= $tipo_biginteger_obligatorio;
            } else {
                $archivo.= $tipo_biginteger;
            }
            break;
        case "boolean":
            $archivo.= $tipo_booleano;
            break;
        case "string":
            if ($value['length'] > 0) {
                $maximo_caracteres = $value['length'];
            } else {
                $maximo_caracteres = 255;
            }
            include 'Elementos' . DS . 'elementosValidateModel.php';
            if ($value['null'] == "NO") {
                $archivo.= $tipo_string_obligatorio;
            } else {
                $archivo.= $tipo_string;
            }
            break;
        case "date":
            if ($source=="Database/Sqlserver" || $source=="Database/Sybase") {
                $tipo_formato_fecha = "dmy";
            } else {
                $tipo_formato_fecha = "ymd";
            }
            include 'Elementos' . DS . 'elementosValidateModel.php';
            if ($value['null'] == "NO") {
                $archivo.= $tipo_date_obligatorio;
            } else {
                $archivo.= $tipo_date;
            }
            break;
        case "datetime":
            if ($source=="Database/Sqlserver" || $source=="Database/Sybase") {
                $tipo_formato_fecha = "dmy";
            } else {
                $tipo_formato_fecha = "ymd";
            }
            include 'Elementos' . DS . 'elementosValidateModel.php';
            if ($value['null'] == "NO") {
                $archivo.= $tipo_datetime_obligatorio;
            } else {
                $archivo.= $tipo_datetime;
            }
            break;
        case "float":
            //Este control lo pongo cuando es Mysql y no viene dado el length cuando es un float 
            if (isset($value['Length2']) || isset($value['length'])) {
                $campoFloat = explode(",", $value['length']);
                if (isset($value['Length2'])) {
                    $maximo = $value['Length2'];
                } else {
                    $maximo = $campoFloat[0];
                }
                if (($campoFloat[1] == null) || ($campoFloat[1] == 0)) {
                    $decimal = 0;
                } else {
                    $decimal = $campoFloat[1];
                }
            } else {
                $decimal = 0;
            }

            //si no tiene decimales lo trataremos como un entero normal
            if ($decimal == 0) {
                if (isset($value['Length2']) || isset($value['length'])) {
                    if (isset($value['length'])) {
                        $maximo_integer = $value['length'];
                    }
                    if (isset($value['Length2'])) {
                        $maximo_integer = $value['Length2'];
                        if($maximo_integer>23)$maximo_integer=12;
                    }
                } else {
                    $maximo_integer = 10;
                }
                include 'Elementos' . DS . 'elementosValidateModel.php';
                if (($maximo_integer == 11) || ($maximo_integer == 10)) {
                    if (($value['null'] == "NO") && ($value['name'] != "id")) {
                        $archivo.=$tipo_integer_obligatorio;
                    } else {
                        $archivo.=$tipo_integer;
                    }
                } else {
                    if (($value['null'] == "NO") && ($value['name'] != "id")) {
                        $archivo.=$tipo_integer_obligatorio_delimitado;
                    } else {
                        $archivo.=$tipo_integer_delimitado;
                    }
                }
            } else {
                //aumentamos en uno maximo para tener en cuenta el "." del decimal
                $maximo++;
                include 'Elementos' . DS . 'elementosValidateModel.php';
                if ($value['null'] == "NO") {
                    $archivo.= $tipo_float_obligatorio;
                } else {
                    $archivo.= $tipo_float;
                }
            }
            break;
        case "text":
            if ($value['null'] == "NO") {
                $archivo.= $tipo_text_obligatorio;
            } else {
                $archivo.= $tipo_text;
            }
            break;
        case "time":
            if ($value['null'] == "NO") {
                $archivo.= $tipo_time_obligatorio;
            } else {
                $archivo.= $tipo_time;
            }
            break;
        default:
            $archivo.= "ha habido algun campo que no tiene un tipo definido concreto";
            break;
    }
}
$archivo.=');';

if($opciones['Constructor']['relaciones']['pertenecea']['numRelaciones']>0){
    $archivo.=' 
    //Relaciones tipo : Un '.$modelo.' pertenece a un/una "Modelo"	
    public $belongsTo = array(
    	';
    for($i = 1; $i <= $opciones['Constructor']['relaciones']['pertenecea']['numRelaciones']; $i++){
        $modeloPerteneceA = ucwords($opciones['Constructor']['relaciones']['pertenecea'][$i]);
        $campoClaveAjena  = strtolower($modeloPerteneceA)."_id";
        if($opciones['Constructor']['relaciones']['pertenecea']['check'][$i]){ 
            $archivo.='
		"'.$modeloPerteneceA.'" => array(
			"className"  => "'.$modeloPerteneceA.'",
			"foreignKey" => "'.$campoClaveAjena.'",
			"conditions" => "",
			"fields"     => "",
			"order"      => ""
		),
        ';
        }
    }
$archivo.='
	);
	';
}
if($opciones['Constructor']['relaciones']['tienemuchos']['numRelaciones']>0){
    $archivo.=' 
    //Relaciones tipo : Un '.$modelo.' tiene muchos/muchas "Modelos"	
    var $hasMany = array(
    	';
    for($i = 1; $i <= $opciones['Constructor']['relaciones']['tienemuchos']['numRelaciones']; $i++){
        $modeloTieneMuchos = ucwords($opciones['Constructor']['relaciones']['tienemuchos'][$i]);
        $campoClaveAjena   = strtolower($modelo)."_id";
        if($opciones['Constructor']['relaciones']['tienemuchos']['check'][$i]){ 
            $archivo.='
		"'.$modeloTieneMuchos.'" => array(
			"className"    => "'.$modeloTieneMuchos.'",
			"foreignKey"   => "'.$campoClaveAjena.'",
			"dependent"    => false,
			"conditions"   => "",
			"fields"       => "",
			"order"        => "",
			"limit"        => "",
			"offset"       => "",
			"exclusive"    => "",
			"finderQuery"  => "",
			"counterQuery" => ""
		),
        ';
        }
    }
$archivo.='
	);';
}

$archivo.='    
 	 function beforeValidate($options = null) {
 	  ';
         $archivo.='return(parent::beforeValidate($options));
    }';

$archivo.='    
 	function beforeSave($options = null) {
 	  ';
        $archivo.='parent::beforeSave($options);
    }
}';