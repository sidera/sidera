<?php
$tipo_integer =         '"numero" => array(
                                      "rule" => "numeric",
                                      "required" => false,
                                      "allowEmpty" => true,
                                      "message" => "Solo se permiten numeros",
                                      ),
                                      "limiteentero" => array(
                                      "rule" =>array("range", -2147483648, 2147483648),
                                      "required" => false,
                                      "allowEmpty" => true,
                                      "message" => "Solo se permitenumeros entre este rango -2147483648 y 2147483648",
                                      ),
                        ),
                        ';

$tipo_integer_delimitado =         '"numero" => array(
                                      "rule" => "numeric",
                                      "required" => false,
                                      "allowEmpty" => true,
                                      "message" => "Solo se permiten numeros",
                                      ),
                                      "largomaximo" => array(
                                            "rule" => array("maxLength", '.$maximo_integer.'),
                                            "required" => false,
                                            "allowEmpty" => true,
                                            "message" => "Solo se permiten un maximo de '.$maximo_integer.' numeros para el big integer",
                                      ),
                        ),
                        ';

$tipo_biginteger =      '"bignumero" => array(
                                      "rule" => "numeric",
                                      "required" => false,
                                      "allowEmpty" => true,
                                      "message" => "Solo se permiten numeros",
                                      ),
                                      "largomaximo" => array(
                                            "rule" => array("maxLength", '.$maximo_big_integer.'),
                                            "required" => false,
                                            "allowEmpty" => true,
                                            "message" => "Solo se permiten un maximo de '.$maximo_big_integer.' numeros para el big integer",
                                      ),
                        ),
                        ';

$tipo_booleano =        '"booleano" => array(
                                          "rule" =>  array("boolean"),
                                          "required" => false,
                                          "allowEmpty" => true,
                                          "message" => "Solo se permiten valores boolean. 0 o 1",
                                          ),
                        
                        ),
                        ';
$tipo_date =            '"fechadate" => array(
                                        "rule" => array("date", "'.$tipo_formato_fecha.'"),
                                        "required" => false,
                                        "allowEmpty" => true,
                                        "message" => "Solo se permiten fecahs con el formato dmy o ymd",
                                        ),
                        ),
                        ';
$tipo_datetime =        '"fechadatetime" => array(
                                            "rule" => array("datetime", "'.$tipo_formato_fecha.'"),
                                            "required" => false,
                                            "allowEmpty" => true,
                                            "message" => "Solo se permiten fechas con el formato dmy H:i:s o ymd H:i:s",
                                            ),
                        ),
                        ';
$tipo_time =            '"time" => array(
                                            "rule" => "time",
                                            "required" => false,
                                            "allowEmpty" => true,
                                            "message" => "Solo se permiten tiempos con el formato H:i:s",
                                            ),
                        ),
                        ';


$tipo_text =            '"text" => array(
                                          "rule" => "noSpecialCharecter",
                                          "required" => false,
                                          "allowEmpty" => true,
                                          "message" => "Solo se admiten los caracteres permitidos",
                                         ), 
                                          "largomaximo" => array(
                                            "rule" => array("maxLength", 1000),
                                            "required" => false,
                                            "allowEmpty" => true,
                                            "message" => "Solo se admiten 1000 caracteres como maximo",
                                          ),
                        ),
                        ';

$tipo_string = '"string" => array(
                                  "rule" => "noSpecialCharecter",
                                  "required" => false,
                                  "allowEmpty" => true,
                                  "message" => "Solo se admiten los caracteres permitidos",
                                  ),
                                  "largomaximo" => array(
                                  "rule" => array("maxLength",' . $maximo_caracteres . ' ),
                                  "required" => false,
                                  "allowEmpty" => true,
                                  "message" => "No se puede exceder el maximo de caracteres",
                                  ),   
                        ),
                        ';

$tipo_float = '"decimal" => array(
                                        "rule" => array("decimal",' . $decimal . '),
                                        "required" => false,
                                        "allowEmpty" => true,    
                                        "message" => "Solo se permiten numeros y el numero de decimales correcto",
                                       ),
                                        "largomaximo" => array(
                                        "rule" => array("maxLength", ' . $maximo . '),
                                        "required" => false,
                                        "allowEmpty" => true,
                                        "message" => "No se pueden exceder el numero de caracteres maximos",
                                         ),
                        ),
                        ';

$tipo_integer_obligatorio =         '"numero" => array(
                                      "rule" => "numeric",
                                      "required" => true,
                                      "allowEmpty" => false,
                                      "message" => "Solo se permiten numeros",
                                      ),
                                      "limiteentero" => array(
                                      "rule" =>array("range", -2147483648, 2147483648),
                                      "required" => true,
                                      "allowEmpty" => false,
                                      "message" => "Solo se permitenumeros entre este rango -2147483648 y 2147483648",
                                      ),
                        ),
                        ';

$tipo_integer_obligatorio_delimitado =         '"numero" => array(
                                      "rule" => "numeric",
                                      "required" => true,
                                      "allowEmpty" => false,
                                      "message" => "Solo se permiten numeros",
                                      ),
                                      "largomaximo" => array(
                                            "rule" => array("maxLength", '.$maximo_integer.'),
                                            "required" => true,
                                            "allowEmpty" => false,
                                            "message" => "Solo se permiten un maximo de '.$maximo_integer.' numeros para el big integer",
                                      ),
                        ),
                        ';

$tipo_biginteger_obligatorio =      '"bignumero" => array(
                                      "rule" => "numeric",
                                      "required" => true,
                                      "allowEmpty" => false,
                                      "message" => "Solo se permiten numeros",
                                      ),
                                      "largomaximo" => array(
                                            "rule" => array("maxLength", '.$maximo_big_integer.'),
                                            "required" => true,
                                            "allowEmpty" => false,
                                            "message" => "Solo se permiten un maximo de '.$maximo_big_integer.' numeros para el big integer",
                                      ),
                        ),
                        ';
$tipo_date_obligatorio =            '"fechadate" => array(
                                        "rule" => array("date", "'.$tipo_formato_fecha.'"),
                                        "required" => true,
                                        "allowEmpty" => false,
                                        "message" => "Solo se permiten fecahs con el formato dmy o ymd",
                                        ),
                        ),
                        ';
$tipo_datetime_obligatorio =        '"fechadatetime" => array(
                                            "rule" => array("datetime", "'.$tipo_formato_fecha.'"),
                                            "required" => true,
                                            "allowEmpty" => false,
                                            "message" => "Solo se permiten fechas con el formato dmy H:i:s o ymd H:i:s",
                                            ),
                        ),
                        ';
$tipo_time_obligatorio =            '"time" => array(
                                            "rule" => "time",
                                            "required" => true,
                                             "allowEmpty" => false,
                                            "message" => "Solo se permiten tiempos con el formato H:i:s",
                                            ),
                        ),
                        ';


$tipo_text_obligatorio =            '"text" => array(
                                          "rule" => "noSpecialCharecter",
                                          "required" => true,
                                          "allowEmpty" => false,
                                          "message" => "Solo se admiten los caracteres permitidos",
                                         ), 
                                          "largomaximo" => array(
                                            "rule" => array("maxLength", 1000),
                                            "required" => true,
                                            "allowEmpty" => false,
                                            "message" => "Solo se admiten 1000 caracteres como maximo",
                                          ),
                        ),
                        ';

$tipo_string_obligatorio = '"string" => array(
                                              "rule" => "noSpecialCharecter",
                                              "required" => true,
                                              "allowEmpty" => false,
                                              "message" => "Solo se admiten los caracteres permitidos",
                                              ),
                                              "largomaximo" => array(
                                              "rule" => array("maxLength",' . $maximo_caracteres . ' ),
                                              "required" => true,
                                              "allowEmpty" => false,
                                              "message" => "No se puede exceder el maximo de caracteres",
                                              ),   
                        ),
                        ';
$tipo_float_obligatorio = '"decimal" => array(
                                        "rule" => array("decimal",' . $decimal . '),
                                        "required" => true,
                                        "allowEmpty" => false,    
                                        "message" => "Solo se permiten numeros y el numero de decimales correcto",
                                       ),
                                        "largomaximo" => array(
                                        "rule" => array("maxLength", ' . $maximo . '),
                                        "required" => true,
                                        "allowEmpty" => false,
                                        "message" => "No se pueden exceder el numero de caracteres maximos",
                                         ),
                        ),
                        ';
