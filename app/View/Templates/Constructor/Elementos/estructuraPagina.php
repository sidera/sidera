<?php
$cabeceraAdd='<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a href="#"><i class="fa fa-cog fa-lg  "></i></a></li>
            <?php echo $this->element("ElementsVista".DS."Add".DS."botonGuardarAdd"); ?>
            <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
        </ul>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6">
            <i id="iconoModulo" class="fa fa-pencil fa-lg"> </i> 
            <span id="etiquetaModulo"> Nuevo registro '.$opciones["Constructor"]["etiqueta"].'</span> 
        </div><!--columnaCabecera-->
    </div><!--cabeceraModulo--> 
    
    <div id="cuerpoModulo" class="container-fluid">
        <form action="/sidera/'.$controlador.'/create/" id="'.$modelo.'AddForm" method="post" accept-charset="iso-8859-1">
            <div id="fila1" class="row-fluid">
                <div id="columna1" class="col-md-6 padded">
';

$pieAdd='
                    <!--nuevosCampos-->
                </div><!--columna1-->
            </div><!--fila1-->  
        </form>
    </div><!--cuerpoModulo-->
    
    <div id="pieModulo" class="row-fluid hide">
        <div id="columnaPie" class="col-md-12 ">
        </div><!--columnaPie-->
    </div><!--pieModulo--> 
</div><!--modulo-->';

$scriptAdd='
<script type="text/javascript">
$(document).ready(function() {
    <?php echo $this->element("ElementsVista".DS."Add".DS."addJs"); ?>
<<<<<<< HEAD
       
    //<!--espacioFuturosControles-->
=======
    <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); ?>
    <?php echo $this->element("ElementsVista".DS."autoValidationTexto"); ?>
    <?php $espacioFuturosControles=true; ?>
>>>>>>> 0487d4bcd6ca10e719e29a6fb360d1fb4b806aee
}); 
</script>';

$cabeceraEdit='<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a href="#"><i class="fa fa-cog fa-lg  "></i></a></li>
            <?php echo $this->element("ElementsVista".DS."Edit".DS."botonGuardarEdit"); ?>
            <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
        </ul>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6">
            <i id="iconoModulo" class="fa fa-edit fa-lg"> </i> 
            <span id="etiquetaModulo"> Modificar registro '.$opciones["Constructor"]["etiqueta"].'</span> 
        </div><!--columnaCabecera-->
    </div><!--cabeceraModulo--> 
    
    <div id="cuerpoModulo" class="container-fluid">
        <form action="/sidera/'.$controlador.'/update/<?=$registro["'.$modelo.'"]["id"]?>" id="'.$modelo.'EditForm" method="post" accept-charset="iso-8859-1">
            <div id="fila1" class="row-fluid">
                <div id="columna1" class="col-md-6 padded">
';

$pieEdit='
                    <!--nuevosCampos-->
                </div><!--columna1-->
            </div><!--fila1-->  
        </form>
    </div><!--cuerpoModulo-->
    
    <div id="pieModulo" class="row-fluid hide">
        <div id="columnaPie" class="col-md-12 ">
        </div><!--columnaPie-->
    </div><!--pieModulo-->  
</div><!--modulo-->';

$scriptEdit='
<script type="text/javascript">
$(document).ready(function() {
    <?php echo $this->element("ElementsVista".DS."Edit".DS."editJs"); ?>
<<<<<<< HEAD
       
    //<!--espacioFuturosControles-->
=======
    <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); ?>
    <?php echo $this->element("ElementsVista".DS."autoValidationTexto"); ?>
    <?php $espacioFuturosControles=true; ?>
>>>>>>> 0487d4bcd6ca10e719e29a6fb360d1fb4b806aee
}); 
</script>';


$cabeceraIndex='<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a href="#"><i class="fa fa-cog fa-lg  "></i></a></li>
            <li><a onclick=\'$("#cuerpoApp").load("'.$controlador.'/add")\' href="#">Nuevo</a></li>
            <?=$this->element("ElementsVista".DS."Index".DS."botonEliminar")?>
            <li><a onclick=\'$("#cuerpoApp").load("'.$controlador.'/exportarMostrar")\' href="#">Exportar</a></li>
            <?=$this->element("ElementsVista".DS."Index".DS."accionEditar")?>
        </ul>
    </div>  
    <div id="columnaBusquedaBarraAccion" class="pull-right col-md-3 col-xs-8 ">
       <?=$this->element("ElementsVista".DS."Index".DS."searchForm",array("model"=>"'.$controlador.'"))?></li>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6 col-xs-6">
            <i id="iconoModulo" class=" fa fa-align-justify fa-lg"></i> 
            <span id="etiquetaModulo">'.$opciones["Constructor"]["etiqueta"].'</span> 
        </div><!--columnaCabecera-->
        <div id="columnaPaginacion" class="col-md-6">
            <?=$this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>"'.$modelo.'"))?>
        </div><!--columnaPaginacion-->
    </div><!--cabeceraModulo--> 
 
    <div id="cuerpoModulo" class="fila row-fluid">  
        <div  class="columna dataTables_wrapper" role="grid" id="DataTables_Table_0_wrapper">
            <table class="elemento table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="check"><i class="fa fa-check-square fa-lg"></i></th>
                        '; 


$pieIndex='<!--nuevosCamposCuerpo-->
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div id="pieModulo" class="row-fluid">
        <div id="columnaPie" class="col-md-8 ">
        </div><!--columnaPie-->
        
        <div id="columnaPaginacionPie" class="col-md-4 pull-right">
            <?=$this->element("ElementsVista".DS."Index".DS."registrosPorPagina")?>
        </div><!--columnaPaginacionPie-->     
    </div><!--pieModulo--> 
    
</div><!--modulo--> '; 

$scriptIndex='
<script type="text/javascript" id="myScript">
$(document).ready(function() {
    <?=$this->element("ElementsVista".DS."Index".DS."indexJs")?>
});
</script>';

$scriptIndexModal='
<script type="text/javascript" id="myScript">
$(document).ready(function() { <?php echo $this->element("ElementsVista".DS."Modal".DS."Index".DS."indexJs",array("id_subgrid"=>"$id_subgrid")); ?>});
</script>';