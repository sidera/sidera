<?php
$tipoDate = '
                    <div id="'.$nombreCampo.'Div" class="form-group">
                        <label>'.ucwords($nombreCampo).'</label>
                        <div class="input-group date" id="'.$nombreCampo.'">
                            <input type="text" class="form-control" name="data['.$modelo.']['.$nombreCampo.']" value="<?=$this->Time->format("d/m/Y",$registro["'.$modelo.'"]["'.$nombreCampo.'"])?>" '.$required.'/>
                            <i class="input-group-addon fa fa-calendar fa-lg"></i>
                        </div>
                    </div><!--'.$nombreCampo.'Div-->';

$tipoDateTime = '
                    <div id="'.$nombreCampo.'Div" class="form-group">
                        <label>'.ucwords($nombreCampo).'</label>
                        <div class="input-group datetime" id="'.$nombreCampo.'">
                            <input type="text" class="form-control" name="data['.$modelo.']['.$nombreCampo.']" value="<?=$this->Time->format("d/m/Y H:i:s",$registro["'.$modelo.'"]["'.$nombreCampo.'"])?>" '.$required.'/>
                            <i class="input-group-addon fa fa-calendar fa-lg"></i>
                        </div>
                    </div><!--'.$nombreCampo.'Div-->';

$tipoTime = '
                    <div id="'.$nombreCampo.'Div" class="form-group">
                        <label>'.ucwords($nombreCampo).'</label>
                        <div class="input-group time" id="'.$nombreCampo.'">
                            <input type="text" class="form-control" name="data['.$modelo.']['.$nombreCampo.']" value="<?=$this->Time->format("H:i:s",$registro["'.$modelo.'"]["'.$nombreCampo.'"])?>" '.$required.'/>
                            <i class="input-group-addon fa fa-clock-o fa-lg"></i>
                        </div>
                    </div><!--'.$nombreCampo.'Div-->';

$tipoBoolean = '
                    <div id="'.$nombreCampo.'Div" class="form-group">
                        <label>'.ucwords($nombreCampo).'</label>
                        <input type="hidden" name="data['.$modelo.']['.$nombreCampo.']" value="<?=$registro["'.$modelo.'"]["'.$nombreCampo.'"]?>" onclick=" $(this).val()==0 ? $(this).val(1) : $(this).val(0) "/>
                        <i class="form-control fa fa-lg fa-check-square-o <?php if (!$registro["'.$modelo.'"]["'.$nombreCampo.'"]) echo "fa-square-o"?>" onclick="$(this).toggleClass(\'fa-square-o\').siblings().trigger(\'click\');"></i>
                    </div><!--'.$nombreCampo.'Div-->';

$tipoCombo = '
                    <div id="'.$nombreCampo.'Div" class="form-group">
                        <label>'.ucwords(Inflector::pluralize(substr($nombreCampo,0,-3))).'</label>
                        <select  id="'.$nombreCampo.'" name="data['.$modelo.']['.$nombreCampo.']" class="chosen-select" '.$required.'>
                            <option value="" selected>Seleccione una opci�n...</option>
                        <?php foreach ($'.Inflector::pluralize(substr($nombreCampo,0,-3)).' as $clave => $valor) {
                            if ($clave == $registro["'.$modelo.'"]["'.$nombreCampo.'"]) $selected="selected"; else $selected="";
                            echo "<option value=\'".$clave."\' ".$selected.">".$valor."</option>";
                        }?>
                        </select>
                    </div><!--'.$nombreCampo.'Div-->';

$tipoSimple = '
                    <div id="'.$nombreCampo.'Div" class="form-group">
                        <label>'.ucwords($nombreCampo).'</label>
                        <input id="'.$nombreCampo.'" type="text" name="data['.$modelo.']['.$nombreCampo.']" class="form-control" value="<?=$registro["'.$modelo.'"]["'.$nombreCampo.'"]?>" maxlength="'.$maxlength.'" '.$pattern.' '.$required.'/>
                    </div><!--'.$nombreCampo.'Div-->';