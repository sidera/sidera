<?php
<<<<<<< HEAD
$headerIndexCampoSimple = '<th data-header="'.$modelo.'.'.$nombreCampo.'" class="orderable"><?=$this->Paginator->sort("'.$nombreCampo.'","'.ucwords($nombreCampo).'")?></th>';
$headerIndexCampoAjeno = '<th data-header="'.$modelo.'.'.$nombreCampo.'" class="orderable"><?=$this->Paginator->sort("'.$nombreCampo.'","'.ucwords(Inflector::pluralize(substr($nombreCampo,0,-3))).'")?></th>';
$tipoDate = '<td class="'.$nombreCampo.'"><?=$this->Time->format("d/m/Y",$registro["'.$modelo.'"]["'.$nombreCampo.'"])?></td>';
$tipoDateTime = '<td class="'.$nombreCampo.'"><?=$this->Time->format("d/m/Y H:i:s",$registro["'.$modelo.'"]["'.$nombreCampo.'"])?></td>';
$tipoTime = '<td class="'.$nombreCampo.'"><?=$this->Time->format("H:i:s",$registro["'.$modelo.'"]["'.$nombreCampo.'"])?></td>';
$tipoFloat = '<td class="'.$nombreCampo.'"><?=number_format($registro["'.$modelo.'"]["'.$nombreCampo.'"], '.$decimales.', ",", ".")?></td>';
$tipoInteger='<td class="'.$nombreCampo.'"><?=number_format($registro["'.$modelo.'"]["'.$nombreCampo.'"], 0, ",", ".")?></td>';
$tipoBoolean = '<td class="'.$nombreCampo.'"><i class="fa <?=($registro["Demo"]["booleano"]) ? "fa-check-square-o" : "fa-square-o" ?>"></td>';
$tipoAjeno = '<td class="'.$nombreCampo.'"><?=$registro["'.$modeloAjeno.'"][$'.$modeloAjeno.'DisplayField]?></td>';
$tipoSimple = '<td class="'.$nombreCampo.'"><?=$registro["'.$modelo.'"]["'.$nombreCampo.'"]?></td>';
=======
$headerIndex = '			<?php 
                                if($configModulo["index"]["campos"]["'.$nombreCampo.'"]["mostrar"] == ACTIVO){
                                    $colsHeader[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] = $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"'.$nombreCampo.'","$configModulo"=>$configModulo));
                                }
                            ?>';
$tipoDate = '				<?php 
                                if($configModulo["index"]["campos"]["'.$nombreCampo.'"]["mostrar"] == ACTIVO){ 
                                    $cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] =($registro[$modelo]["'.$nombreCampo.'"]=="" || $registro[$modelo]["'.$nombreCampo.'"]==null)? \'<td class="'.$nombreCampo.'"></td>\':\'<td class="'.$nombreCampo.'">\'.$this->Time->format("d/m/Y",$registro[$modelo]["'.$nombreCampo.'"]).\'</td>\';
                                }
                            ?>';
$tipoDateTime = '			<?php 
                                if($configModulo["index"]["campos"]["'.$nombreCampo.'"]["mostrar"] == ACTIVO){ 
                                    $cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] = ($registro[$modelo]["'.$nombreCampo.'"]=="" || $registro[$modelo]["'.$nombreCampo.'"]==null)? \'<td class="'.$nombreCampo.'"></td>\':\'<td class="'.$nombreCampo.'">\'.$this->Time->format("d/m/Y H:i:s",$registro[$modelo]["'.$nombreCampo.'"]).\'</td>\';
                                }
                            ?>';
$tipoTime = '				<?php 
                                if($configModulo["index"]["campos"]["'.$nombreCampo.'"]["mostrar"] == ACTIVO){ 
                                        $cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] = ($registro[$modelo]["'.$nombreCampo.'"]=="" || $registro[$modelo]["'.$nombreCampo.'"]==null)? \'<td class="'.$nombreCampo.'"></td>\':\'<td class="'.$nombreCampo.'">\'.$this->Time->format("H:i:s",$registro[$modelo]["'.$nombreCampo.'"]).\'</td>\';
                                }
                            ?>';
$tipoFloat = '				<?php 
								if($configModulo["index"]["campos"]["'.$nombreCampo.'"]["mostrar"] == ACTIVO){
                                    if($registro[$modelo]["'.$nombreCampo.'"]=="" || $registro[$modelo]["'.$nombreCampo.'"]==null){
                                          $cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] =  \'<td class="'.$nombreCampo.'"></td>\';
                                    }
                                    else{
                                    $cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] = \'<td class="'.$nombreCampo.'">\'.$this->Number->format($registro[$modelo]["'.$nombreCampo.'"], array(
                                                    "places"    => '.$decimales.',
                                                    "before"    => false,
                                                    "decimals"  => ",",
                                                    "thousands" => "."
                                                    )).\'</td>\';
                                    }
                                }
                            ?>';

$tipoInteger='
                            <?php 
                                if($configModulo["index"]["campos"]["'.$nombreCampo.'"]["mostrar"] == ACTIVO){
                                    if($registro[$modelo]["'.$nombreCampo.'"]=="" || $registro[$modelo]["'.$nombreCampo.'"]==null){
                                          $cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] =  \'<td class="'.$nombreCampo.'"></td>\';
                                    }
                                    else{
                                    $cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] = \'<td class="'.$nombreCampo.'">\'.$this->Number->format($registro[$modelo]["'.$nombreCampo.'"], array(
                                                    "places"    => 0,
                                                    "before"    => false,
                                                    "decimals"  => ",",
                                                    "thousands" => "."
                                                    )).\'</td>\';
                                    }
                                }
                            ?>';

$tipoBoolean = '			<?php 
                                if($configModulo["index"]["campos"]["'.$nombreCampo.'"]["mostrar"] == ACTIVO){ 
									$cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] = \'<td class="'.$nombreCampo.'">\'.$this->Form->input("",array("type" => "checkbox","disabled"=>"true","checked" =>$registro[$modelo]["'.$nombreCampo.'"])).\'</td>\';
                                }
                            ?>';
$tipoAjeno='
                            <?php 
                                if($configModulo["index"]["campos"]["'.$nombreCampo.'"]["mostrar"] == ACTIVO){ 
                                        $cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] = \'<td class="'.$nombreCampo.'">\'.htmlentities($registro["'.$modeloAjeno.'"][$'.$nombreCampo.'DisplayField],ENT_COMPAT | ENT_HTML401,"ISO-8859-1").\'</td>\';
                                }
                            ?>';

$tipoSimple='
                            <?php 
                                if($configModulo["index"]["campos"]["'.$nombreCampo.'"]["mostrar"] == ACTIVO){ 
                                        $cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] = \'<td class="'.$nombreCampo.'">\'.htmlentities($registro[$modelo]["'.$nombreCampo.'"],ENT_COMPAT | ENT_HTML401,"ISO-8859-1").\'</td>\';
                                }
                            ?>';
>>>>>>> 0487d4bcd6ca10e719e29a6fb360d1fb4b806aee
