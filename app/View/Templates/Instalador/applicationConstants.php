<?php
$archivo='<?php    
    
//(cofiguracion manual)Conexion DEFAULT de la aplicacion
define("DEFAULT_DATABASE",     "'.$opciones["Configuracion"]["database"].'");
define("DEFAULT_HOST",         "'.$opciones["Configuracion"]["host"].'");
define("DEFAULT_LOGIN",        "'.$opciones["Configuracion"]["login"].'");
define("DEFAULT_PASSWORD",     "'.$opciones["Configuracion"]["password"].'");
define("DEFAULT_DATASOURCE",   "'.$opciones["Configuracion"]["datasource"].'");

define("DEFAULT_PREFIX",       "");
define("DEFAULT_PERSISTENT",   false);

//(cofiguracion manual)Versiones
define("VERSION_MIAPP",        "1.0");

//(cofiguracion dinamica)Parametros de la aplicacion
define("APLICACION", "'.$opciones["Configuracion"]["aplicacion"].'"); 
define("ORGANISMO", "'.$opciones["Configuracion"]["organismo"].'"); 
define("CONSEJERIA", "'.$opciones["Configuracion"]["consejeria"].'");
define("REGISTROS_PAGINA", "10");
define("DESCRIPCION", "<p>DESCRIPCION</p>");
define("DEBUG", "0");
define("MODO_MANTENIMIENTO", "0");
define("AUDITORIA_ACCESO", "1");
define("SUBNAVBAR","0");
define("REPORT_UNIT", "RiesgosLaborales");
define("REPORT_SERVER", "172.16.7.58:8080");
define("DEFAULT_LOGIN_JASPER", "jasperadmin");
define("DEFAULT_PASSWORD_JASPER", "jasper");
define("MOSTRAR_MENUSSISTEMA", "0");

//(cofiguracion dinamica)Codificacion de caracteres
define("CODIFICACION_BD", "ISO-8859-1");
define("CODIFICACION_FILES", "UTF-8");
define("INICIO","");

//(cofiguracion dinamica)Configuracion redireccion
define("PROTOCOLO","'.$opciones["Configuracion"]["protocolo"].'");             // "https" para preproduccion y produccion
define("RUTA_PROYECTO",   "'.$opciones["Configuracion"]["ruta_proyecto"].'"); // "00/miapp" para desarrollo, preproduccion y produccion 
    
//(configuracion dinamica)Correo Electronico
define("EMAIL_HOST","legacy.gobex.es");
define("EMAIL_PORT",25);
define("EMAIL_USER","sidera");
define("EMAIL_PASSWORD","sidera");
define("EMAIL_TRANSPORT","smtp");
define("EMAIL_FROM","sidera@gobex.es");
define("EMAIL_FORMAT","smtp");
define("EMAIL_TLS",true);

    
?>
';

?>
