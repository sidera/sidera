<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

  <div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">
      <div class="error-container center">
        <div class="info-text text-transparent">
            Aplicación en Mantenimiento
        </div>
        <hr class="main">
        <div class="error-text text-transparent">
            <div class="big-icon text-inverse">
                <i class="fa fa-wrench"></i>
            </div>
        </div>
        <hr class="main">
        <div class="info-text text-transparent">
            Disculpe las molestias
        </div>
      </div>
    </div>
  </div>
</div>