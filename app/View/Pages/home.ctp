<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="box bordered">
              <div class="box inner light-gray">
                <div class="row-fluid">
                   
                    <div class="span2 center"><a class="menu-header configuracion" href="configuraciones/edit">
                      <div class="stat-box light-blue">
                        <i class="fa fa-wrench fa fa-large"></i>
                        <span class="count"></span>
                        <span class="stat-text">Configuración</span>
                      </div></a>
                    </div>

                    <div class="span2 center"><a class="menu-header mensajes" href="mensajes/recibidos">
                      <div class="stat-box dark-blue">
                        <i class="fa fa-envelope fa fa-large"></i>
                        <span class="count" id="messagesCounter"></span>
                        <span class="stat-text">Mensajes</span>
                      </div></a>
                    </div>
               
                    <div class="span2 center"><a class="menu-header logs" >
                      <div class="stat-box border-blue">
                        <i class="fa fa-warning-sign fa fa-large"></i>
                        <span class="count"></span>
                        <span class="stat-text">Logs</span>
                      </div></a>
                    </div>
                    <div class="span2 center"><a class="menu-header auditorias" href="auditorias/index">
                      <div class="stat-box text-blue">
                        <i class=" fa fa-th fa fa-large"></i>
                        <span class="count"></span>
                        <span class="stat-text">Auditorías</span>
                      </div></a>
                    </div>
                    <div class="span2 center">
                        <a class="menu-header monitor">
                            <div class="stat-box medium-blue">
                                <i class=" fa fa-bar-chart fa fa-large"></i>
                                <span class="count"></span>
                                <span class="stat-text">Monitor</span>
                            </div>
                        </a>
                    </div>
                    <div class="span2 center"><a class="menu-header eventos">
                      <div class="stat-box dark-plum">
                        <i class=" fa fa-calendar fa fa-large"></i>
                        <span class="count"></span>
                        <span class="stat-text">Eventos</span>
                      </div></a>
                    </div>
               </div>
            </div>
            <div class="row-fluid">
                <div id="leftContainer"><div class="span6"></div></div>
                <div id="rightContainer"></div>
            </div>
         </div>
    </div>
   </div>
 </div>
<script type="text/javascript" id="myScript">
$(document).ready(function() {
    
    $('#rightContainer').load("auditorias/getGeneralStats");
    $('#leftContainer').load("mensajes/getLastMessages");
   
    $(".logs").click(function(event){
        event.preventDefault();
        $('#rightContainer').load("logmessages/getErrorLogMessages");
    });
    $(".monitor").click(function(event){
        event.preventDefault();
        $('#rightContainer').load("auditorias/getGeneralStats");
    });
   
    $(".eventos").click(function(event){
        event.preventDefault();
        $('#rightContainer').load("eventos/getCalendar");
    });
     
});
</script>