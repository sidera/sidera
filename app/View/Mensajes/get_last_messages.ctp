<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="span6">
    <div class="padded">
        <div class="section-title">Mensajes</div>
        <ul class="recent-activity dashBoardMessage">
            <?php foreach($lastMessages as $lastMessage){ ?>
            <li class="event-box <?php if($lastMessage['Mensaje']['estado'] == 1) echo "newMessage";?>" 
                title="<?php if($lastMessage['Mensaje']['estado'] == 1) echo "Mensaje no le�do. ";?>Haga clic para ver el mensaje" 
                ref="<?php echo $lastMessage['Mensaje']['id'];?>">
                <div class="evento">
                    <div class="action">
                        <i class="fa fa-envelope"></i>
                        <a class="username" href="#">
                            <?php echo implode(" ", array(
                                    $lastMessage['Origen']['nombre'],
                                    $lastMessage['Origen']['apellido1'],
                                    $lastMessage['Origen']['apellido2']));?>
                        </a>
                        <div><?php echo $lastMessage['Mensaje']['asunto'];?></div>
                    </div>
                    <div class="time">
                        <i class="fa fa-time"></i>
                        <?php echo $this->Time->timeAgoInWords( $lastMessage['Mensaje']['created'], array('format' => 'F jS, Y', 'end' => '+1 year'));?>
                    </div>
                </div>
            </li>
            <?php }?>
        </ul>
    </div>
</div>
<script type="text/javascript" id="myScript">
$(document).ready(function() {
    $(".dashBoardMessage li").click(function(event){
        event.preventDefault();
        $("#cuerpoApp").load("mensajes/edit_recibidos/"+$(this).attr("ref"));
    });
});
</script>