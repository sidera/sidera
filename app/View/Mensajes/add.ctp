<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<?php echo $this->Form->create("$modelo",array("url" => array("controller" => $controlador, "action" => "enviar")));?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsMensajes".DS."botonEnviarNuevoMensaje"); ?>
        <button class="btn btn-default" id="cancelarAccion">
            <i class="fa fa-circle-arrow-left  "></i> Cancelar
        </button>      
        <?php $pagina = ($this->Session->check("page[$modelo]")==TRUE)?"/page:".$this->Session->read("page[$modelo]"):"";?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form span6">
        <div class="box-header">
           <i class="fa fa-envelope"></i> Nuevo Mensaje
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="padded">
                    <div class="section-title <?php if($configModulo["general"]["columnas"]["columna1"]=="") echo "hide" ?>"> 
                        <?php echo $configModulo["general"]["columnas"]["columna1"]; ?> </div>
                    <div class="vpadded " id="columna1">
                    <?php $id="inicio";?>
	                    <?php echo $this->Form->input("id",array(
	                            "id"     => "id",
	                            "label"  => $configModulo["add"]["campos"]["id"]["etiqueta"],
	                            "class"  => "input-xlarge",
	                            "div"    => array("class" => " ".$configModulo["add"]["campos"]["id"]["orden"]."")
	                    ));?>
                    <?php $id="fin";?>
                    <?php $destino="inicio";?>
                    	<?php echo $this->Form->input("destino",array(
	                            "id"       => "destino",
	                            "label"    => $configModulo["add"]["campos"]["destino"]["etiqueta"],
	                            "multiple" => true,    
	                            "options"  => $usuarios,  
	                            "class"    => "input-xlarge chzn-select",
	                            "data-placeholder" => "Haga click para selecionar los destinatarios",
	                            "title"    => "Haga click para selecionar los destinatarios",
	                            "div"      => array("class"=>"full-width ".$configModulo["add"]["campos"]["destino"]["orden"]."")
                        ));?>
                    <?php $destino="fin";?>
                    <?php $asunto="inicio";?>
                    	<?php echo $this->Form->input("asunto",array(
                            "id"       => "asunto",
                            "label"    => $configModulo["add"]["campos"]["asunto"]["etiqueta"],
                            "class"    => "input-xlarge",
                            "placeholder" => "Escriba el asunto ...",
                            "div"      => array("class" => "full-width ".$configModulo["add"]["campos"]["asunto"]["orden"]."")
                         ));?>
                    <?php $asunto="fin";?>
                    <?php $texto="inicio";?>
                    	<?php echo $this->Form->input("texto",array(
                            "type"     => "textarea",
                            "id"       => "texto",
                            "label"    => $configModulo["add"]["campos"]["texto"]["etiqueta"],
                            "class"    => "input-xlarge alto-fijo",
                            "placeholder" => "Escriba el texto ...",
                            "div"      => array("class" => "full-width ".$configModulo["add"]["campos"]["texto"]["orden"]."")
                        ));?>
                    <?php $texto="fin";?>
                    <?php $created="inicio";?>
                        <div class="input-prepend <?php echo $configModulo["add"]["campos"]["created"]["orden"];?>" title="Doble click para la eliminar">
                            <label><?php echo $configModulo["add"]["campos"]["created"]["etiqueta"];?></label>
                            <button type="button" id="createdButtonDatepicker" class="btn light-blue inverse date-button"><i class="fa fa-calendar"></i></button>
                            <input type="text" id="createdDatepicker" ondblclick="$(this).val('')" readonly="readonly" class="input-small center datepicker"  name="data[Mensaje][created]"/>							
                        </div>
                    <?php $created="fin";?>
                    <?php $propietario="inicio";?>
                    	<?php echo $this->Form->input("propietario",array(
                            "id"       => "propietario",
                            "value"    => $user_id,    
                            "label"    => $configModulo["add"]["campos"]["propietario"]["etiqueta"],
                            "class"    => "input-xlarge",
                            "div"      => array("class" => " ".$configModulo["add"]["campos"]["propietario"]["orden"]."")
                    	));?>
                    <?php $propietario="fin";?>                        
                    <?php $estado="inicio";?>
                    	<?php echo $this->Form->input("estado",array(
                            "before"   => "<label>",
                            "after"    => "".$configModulo["add"]["campos"]["estado"]["etiqueta"]."</label>",
                            "type"     => "checkbox",
                            "id"       => "estado",
                            "label"    => false,
                            "class"    => "input-xlarge",
                            "div"      => array("class" => " ".$configModulo["add"]["campos"]["estado"]["orden"]."")
                        ));?>
                    <?php $estado="fin";?>
                    <?php $origen="inicio";?>
                    	<?php echo $this->Form->input("origen",array(
                            "id"       => "origen",
                            "value"    => $user_id,    
                            "label"    => $configModulo["add"]["campos"]["origen"]["etiqueta"],
                            "class"    => "input-xlarge",
                            "div"      => array("class" => " ".$configModulo["add"]["campos"]["origen"]["orden"]."")
                        ));?>
                    <?php $origen="fin";?>
                    <?php $destinos_texto="inicio";?>
                    	<?php echo $this->Form->input("destinos_texto",array(
                            "id"       => "destinos_texto",
                            "label"    => $configModulo["add"]["campos"]["destinos_texto"]["etiqueta"],
                            "class"    => "input-xlarge",
                            "div"      => array("class" => " ".$configModulo["add"]["campos"]["destinos_texto"]["orden"]."")
                        ));?>
                    <?php $destinos_texto="fin";?>
                    <?php $espacioFuturosCampos=true; ?>
                    </div>
                </div>
            </div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
<?php echo $this->element("ElementsVista".DS."javascriptRequerido"); ?>
<?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); ?>
<?php if($configModulo["general"]["acciones"]["accionOrdenarCampos"]) echo $this->element("ElementsVista".DS."accionOrdenarCamposFormularios"); ?>
<?php echo $this->element("ElementsVista".DS."Add".DS."accionMostrarEnAdd"); ?>
var controlador="<?php echo $controlador;?>"; 
var modelo="<?php echo $modelo;?>";
var options = {target:"#content"};
$(".chzn-select").chosen();
$("#destino").rules("add", {required:true});
    $("#cancelarAccion").click(
            function(event){
                event.preventDefault();
                $('#cuerpoApp').load("mensajes/recibidos");
            }
        );
    });
    $('#destino').change(function(event) {
        event.preventDefault();
        $('#destinos_texto').val("");
       
        if($('#destino option:selected').val()=='0'){
            var cadenaTodos="";
            $('#destino option').each( function(index ){
                if($(this).val()!='0') cadenaTodos = cadenaTodos +$(this).text();
                
                
            });
            $('#destinos_texto').val(remplazar(cadenaTodos,'\n','; '));
            $('#destino').val('');
        }else{
            var cadena = $('#destino option:selected').text();
            $('#destinos_texto').val(remplazar(cadena,'\n','; '));
        }
    });
        
    function remplazar (texto, buscar, nuevo){
        var temp = '';
        var longitud = texto.length;
        for (j=0; j<longitud; j++) {
            if (texto[j] == buscar) temp += nuevo;
            else temp += texto[j];
        }
        return temp;
    }
</script>