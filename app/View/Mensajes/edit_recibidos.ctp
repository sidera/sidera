<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php echo $this->Form->create("$modelo",array("url" => array("controller" => $controlador, "action" => "responderRecibidos")));?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); //Bloque para mensajes con setFlash ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsMensajes".DS."botonResponderMensajeRecibido"); //Elemento con funcionalidad de boton de Responder a Mensaje Recibido ?>
        <?php echo $this->element("ElementsMensajes".DS."botonCrearNuevoMensaje"); //Elemento con funcionalidad de boton de Nuevo ?>
        <?php echo $this->element("ElementsMensajes".DS."botonMensajesRecibidos"); //Elemento con funcionalidad de boton de ir a Mensajes Recibidos ?>
        <?php echo $this->element("ElementsMensajes".DS."botonMensajesEnviados"); //Elemento con funcionalidad de boton de ir a Mensajes Enviados ?>
        <?php echo $this->element("ElementsMensajes".DS."botonEliminarMensajeRecibido"); // Elemento con funcionalidad de boton de Eliminar el Mensaje Recibido  ?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form span6">
        <div class="box-header">
           <i class="fa fa-envelope"></i> Mensaje Recibido
        </div>
        <div class="row-fluid">
        	<div class="span12">
				<div class="padded">
					<div class="section-title <?php if($configModulo["general"]["columnas"]["columna1"]=="") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna1"]; ?> </div>
					<div class="vpadded " id="columna1">
                    <p><div class="nota badge full-width">
                        <label><i class="fa fa-persona fa fa-large"></i>( Enviado por <b><?php echo $origen; ?></b> el d�a <b><?php echo substr($this->request->data['Mensaje']['created'], 0, 10);?></b> a las <b><?php echo substr($this->request->data['Mensaje']['created'], 11, 6);?></b> )</label>
                    </div></p>
                    
                    <?php $asunto="inicio";?>
							<?php echo $this->Form->input("asunto",array(
							"before"=>"<label>".$configModulo["edit"]["campos"]["asunto"]["etiqueta"]."</label>",
							"id"=>"asunto",
							"label"=>false,
                            "readonly"=>"readonly",
							"class"=>"input-xlarge readonly-blanco ",
							"placeholder"=>"Escriba el asunto ...",
							"div"=>array("class"=>"full-width ".$configModulo["edit"]["campos"]["asunto"]["orden"]."")
							));?>
					<?php $asunto="fin";?>
                    <?php $texto="inicio";?>
							<?php echo $this->Form->input("texto",array(
							"before"=>"<label>".$configModulo["edit"]["campos"]["texto"]["etiqueta"]."</label>",
							"id"=>"texto",
                            "readonly"=>"readonly",
							"label"=>false,
							"class"=>"input-xlarge alto-fijo readonly-blanco ",
							"placeholder"=>"Escriba el texto ...",
							"div"=>array("class"=>"full-width ".$configModulo["edit"]["campos"]["texto"]["orden"]."")
							));?>
					<?php $texto="fin";?>
                   
                    <?php echo $this->Form->input('responder',array(
                        'before'=>'<label>',
                        'after'=>' Responder Mensaje</label>',
                        'name' => 'data[responder]',
                        'type' => 'checkbox',
                        'label'=>false,
                        'class'=>'',
                        'checked'=>false,
                        "div"=>array("class"=>"full-width ")
                        ));?>
                    
                    <?php $created="inicio";?>
						<div class="input-prepend hide <?php echo $configModulo["edit"]["campos"]["created"]["orden"];?>" readonly="readonly" title="Doble click para la eliminar">
							<label><?php echo $configModulo["edit"]["campos"]["created"]["etiqueta"];?></label>
							<button type="button" id="createdButtonDatepicker" class="btn light-blue inverse date-button"><i class="fa fa-calendar"></i></button>
							<input type="text" value="<?php echo  $this->Time->format('d/m/Y h:i:s',$this->request->data[Mensaje][created]);?>" <?php echo $configModulo["edit"]["campos"]["created"]["orden"];?>id="createdDatepicker" ondblclick="$(this).val('')" readonly="readonly" class="input-small center datepicker"  name="data[Mensaje][created]"/>							
						</div>
					<?php $created="fin";?>
                    <?php $propietario="inicio";?>
							<?php echo $this->Form->input("propietario",array(
							"before"=>"<label>".$configModulo["edit"]["campos"]["propietario"]["etiqueta"]."</label>",
							"id"=>"propietario",
                            "value"=>$this->request->data[Mensaje][propietario],    
							"label"=>false,
							"class"=>"input-xlarge ",
							"placeholder"=>"Escriba el propietario ...",
							"div"=>array("class"=>"hide ".$configModulo["edit"]["campos"]["propietario"]["orden"]."")
							));?>
					<?php $propietario="fin";?>
                    
                    <?php $id="inicio";?>
							<?php echo $this->Form->input("id",array(
							"before"=>"<label>".$configModulo["edit"]["campos"]["id"]["etiqueta"]."</label>",
							"id"=>"id",
							"label"=>false,
                            "value"=>$this->request->data['Mensaje']['id'],    
							"class"=>"input-xlarge ",
							"placeholder"=>"Escriba el id ...",
							"div"=>array("class"=>"hide ".$configModulo["edit"]["campos"]["id"]["orden"]."")
							));?>
					<?php $id="fin";?>
                    <?php $origen="inicio";?>
                            <?php echo $this->Form->input("origen",array(
                            "before"=>"<label>".$configModulo["edit"]["campos"]["origen"]["etiqueta"]."</label>",
                            "id"=>"origen",
                            "readonly"=>"readonly",
                            "value"=>$this->request->data['Mensaje']['origen'],    
                            "label"=>false,
                            "class"=>"input-xlarge ",
                            "placeholder"=>"Escriba el origen ...",
                            "div"=>array("class"=>"hide ".$configModulo["edit"]["campos"]["origen"]["orden"]."")
                            ));?>
                    <?php $origen="fin";?>
                    <?php $destino="fin";?>
                            <?php echo $this->Form->input("destino",array(
                            "before"=>"<label>".$configModulo["edit"]["campos"]["destino"]["etiqueta"]."</label>",
                            "id"=>"destino",
                            "readonly"=>"readonly",
                            "value"=>$this->request->data['Mensaje']['destino'],    
                            "label"=>false,
                            "class"=>"input-xlarge ",
                            "placeholder"=>"Escriba el destino ...",
                            "div"=>array("class"=>"hide ".$configModulo["edit"]["campos"]["destino"]["orden"]."")
                            ));?>
                    <?php $destino="fin";?>    
                        
                    <?php $estado="inicio";?>
                    <?php echo $this->Form->input("estado",array(
                    "before"=>"<label>",
                    "after"=>"".$configModulo["edit"]["campos"]["estado"]["etiqueta"]."</label>",
                    "type"=>"checkbox",
                    "id"=>"estado",
                    "value"=>$this->request->data['Mensaje']['estado'],
                    "label"=>false,
                    "class"=>"input-xlarge ",
                    "placeholder"=>"Escriba el estado ...",
                    "div"=>array("class"=>" hide ".$configModulo["edit"]["campos"]["estado"]["orden"]."")
                    ));?>
                    <?php $estado="fin";?>

                    

                    <?php $destinos_texto="inicio";?>
                    <?php echo $this->Form->input("destinos_texto",array(
                    "before"=>"<label>".$configModulo["edit"]["campos"]["destinos_texto"]["etiqueta"]."</label>",
                    "id"=>"destinos_texto",
                    "label"=>false,
                    "class"=>"input-xlarge ",
                    "placeholder"=>"Escriba el destinos_texto ...",
                    "div"=>array("class"=>" hide ".$configModulo["edit"]["campos"]["destinos_texto"]["orden"]."")
                    ));?>
                    <?php $destinos_texto="fin";?>
            <?php $espacioFuturosCampos=true; ?>
           			</div>
				</div>
			</div>
			<div class="span0">
				<div class="padded">
					<div class="section-title <?php if($parametrosModulo["etiqueta_columna2"]=="hide") echo "hide" ?>"> <?php echo $parametrosModulo["etiqueta_columna2"]; ?> </div>
					<div class="vpadded input-xlarge" id="columna2">
					</div>
				</div>
			</div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
    <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); //Elemento con funcionalidad de validar formulario ?>
    $(":checkbox").uniform();
    var controlador="<?php echo $controlador;?>"; 
    var modelo="<?php echo $modelo;?>";
    var options = {target:"#content"};
   
    var origen=$("#origen").val();
    var destino=$("#destino").val();
    var texto=$("#texto").val();
	var asunto=$("#asunto").val();
    
    $("#MensajeResponder").click( 
        function (event) {
            event.stopPropagation();
            if($('#MensajeResponder').filter(':checked').size() == 1){
                $("#botonResponderMensajeRecibido").removeClass("disabled");
                $("#asunto").attr("readonly", false);
                $("#texto").attr("readonly", false);
                //$("#origen").val(destino);
                //$("#destino").val(origen);
                $("#asunto").val("Re:["+$("#asunto").val()+"]");
                $("#texto").val(""+$("#texto").val()+"\r---\r");
            }else{
                $("#botonResponderMensajeRecibido").addClass("disabled");
                $("#asunto").attr("readonly", true);
                $("#texto").attr("readonly", true);
                //$("#origen").val(origen);
                //$("#destino").val(destino);
                $("#asunto").val(asunto);
                $("#texto").val(texto);
            }
        }
    );
    //Comprueba si hay mensajes sin leer en la bandeja de entrada
    if ('<?php echo $mensajesNuevos;?>'==0){
        $('#spanMensajesNuevos').addClass('hide');
        $('#elementoMensajes').removeClass('notificacion-mensajes');
        $('#spanMensajesRecibidos').addClass('hide');
    }else{
        $('#spanMensajesNuevos').removeClass('hide');
        $('#elementoMensajes').addClass('notificacion-mensajes');
        $('#spanMensajesRecibidos').removeClass('hide');
        $('#spanMensajesNuevos').text('<?php echo $mensajesNuevos;?>');
        $('#spanMensajesRecibidos').text('(<?php echo $mensajesNuevos;?>)');
    }    
 });
</script>