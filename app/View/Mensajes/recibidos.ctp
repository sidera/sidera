<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php
//Autor:   
//Versi�n: 
?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); //Bloque para mensajes con setFlash ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsMensajes".DS."botonCrearNuevoMensaje"); //Elemento con funcionalidad de boton de Nuevo ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."botonFiltrar"); // Elemento con funcionalidad de boton de Filtrar ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."botonImportar"); // Elemento con funcionalidad de boton de Importar ?> 
            <?php echo $this->element("ElementsVista".DS."Index".DS."botonExportar"); // Elemento con funcionalidad de boton de Exportar ?>
            <?php echo $this->element("ElementsVista".DS."Index".DS."botonConfigurar"); // Elemento con funcionalidad de boton de Configurar ?>
        <?php echo $this->element("ElementsMensajes".DS."botonMensajesEnviados"); //Elemento con funcionalidad de boton de ir a Mensajes Enviados ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."botonEliminar"); // Elemento con funcionalidad de boton de Eliminar ?>
        <?php echo $this->element("ElementsMensajes".DS."accionEditarRecibidos"); //Elemento con funcionalidad de Editar al hacer click ?>
        <?php echo $this->element("ElementsVista".DS."accionOrdenarColumnasGrid"); //Elemento con funcionalidad de Ordenar las Columnas del Grid al hacer click ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."accionCheckearTodos"); // Elemento con funcionalidad de Checkear Todos en el grid ?>
      </div>
      <div class="btn-group pull-right right span4 input-xxlarge">
        <div class="input-append">
                    <input type="text"id="searchText"
                    <?php if(isset($searchText)) echo "value='".$searchText."' readonly='readonly'";?>>
                    </input>
                    <?php if(!isset($searchText)){ ?>
                    <button type="button" class="btn light-blue inverse" id="botonSearch" title="Haga clic para buscar. Use ',' para buscar m�s palabras"><i class="fa fa-search"></i></button>
                    <?php }else{?>
                    <button type="button" class="btn btn-warning" id="removeSearch" title="Haga clic para eliminar la b�squeda activa">
                    <i class="fa fa-remove"></i></button>
                    <?php } ?>
        </div>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered">
        <div>
            <div class="box-header span6">
                <i class="fa fa-user"></i> Mensajes Recibidos
            </div>
            <div class="groupPagination span6 pull-right">
                <?php echo $this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>$modelo));?>
                <?php echo $this->element("ElementsMensajes".DS."registrosPorPaginaRecibidos");?>
            </div>
        </div>
        <div role="grid" class="dataTables_wrapper" id="DataTables_Table_0_wrapper">
            <table class="table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="ui-state-default check" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                            <i class="fa fa-check"></i>
                        </th>
                       	<?php $id="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"id","$configModulo"=>$configModulo));?><?php $id="fin";?>
						<?php $propietario="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"propietario","$configModulo"=>$configModulo));?><?php $propietario="fin";?>
					    <?php $origen="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"origen","$configModulo"=>$configModulo));?><?php $origen="fin";?>
                        <?php $destino="inicio";?><?php //echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"destino","$configModulo"=>$configModulo));?><?php $destino="fin";?>
						<?php $asunto="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"asunto","$configModulo"=>$configModulo));?><?php $asunto="fin";?>
						<?php $texto="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"texto","$configModulo"=>$configModulo));?><?php $texto="fin";?>
						<?php $created="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"created","$configModulo"=>$configModulo));?><?php $created="fin";?>
						<?php $estado="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"estado","$configModulo"=>$configModulo));?><?php $estado="fin";?>
                        <?php $espacioFuturosCampos=true; ?>
               </tr>
                </thead>
                <tbody class="griddata">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?php echo $registro[$modelo]["id"];?>" class="<?php if($registro[$modelo]["estado"]) echo "info"?> gradeA" title="Haga clic para editar el registro"> 
                        <td class="checkCell">
                            <input type="checkbox" value="<?php echo $registro[$modelo]["id"];?>" class="itemSelector" title="Seleccione los elementos que desee eliminar">
                        </td>
                        <td class="id"><?php echo h($registro[$modelo]["id"]); ?></td>
						<td class="propietario"><?php echo h($registro["Propietario"]["username"]); ?></td>
                        <td class="origen"><?php echo h($registro["Origen"]['apellido1']." ".$registro["Origen"]['apellido2'].", ".$registro["Origen"]['nombre']); ?></td>
						 <!--<td class="destino"><?php //echo h($registro["Destino"]['apellido1']." ".$registro["Destino"]['apellido2'].", ".$registro["Destino"]['nombre']); ?></td>-->
						<td class="asunto"><?php echo h($registro[$modelo]["asunto"]); ?></td>
						<td class="texto"><?php echo h($registro[$modelo]["texto"]); ?></td>
						<td class="created"><?php echo $this->Time->format('d/m/Y h:i:s',$registro[$modelo]["created"]); ?></td>
						<td class="estado"><?php echo $this->Form->input("",array("type" => "checkbox","disabled"=>"true",checked =>$registro[$modelo]["estado"])); ?></td>
                        <?php $espacioFuturosCampos=true; ?>
                        </tr>
                     <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" id="myScript">
//<![CDATA[
$(document).ready(function() { 
    $(":checkbox").uniform();   
    <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); //Elemento con el javascript requerido para esta vista ?>
    <?php echo $this->element("ElementsVista".DS."Index".DS."accionMostrarEnIndex"); // Elemento con funcionalidad de mostrar (o no) columnas en el grid ?>    
    //Campo de busqueda personalizado
    jQuery.fn.getFilters = function(){
        var filter  = '';
        if($('#searchText').val() != '') filter = 'searchText:'+$('#searchText').val()+'/';
        
        if('<?php echo $this->Paginator->sortKey();?>' != '') filter += 'sort:<?php echo $this->Paginator->sortKey();?>/direction:<?php echo $this->Paginator->sortDir();?>/';
        if('<?php echo $this->Paginator->current();?>' != '') filter += 'page:<?php echo $this->Paginator->current();?>';
        return filter;
    }
    
    jQuery.fn.sendSearch = function(href){
        var filter  = '';
       $('#searchText').val( $('#searchText').val().trim());
       $('#searchText').val( $('#searchText').val().replace(/\//g,'-'));
       $('#searchText').val( $('#searchText').val().replace(/ /g,'|'));
       
        if(href!=null){
            $("#cuerpoApp").load(href+'/');
        }else{
            $("#cuerpoApp").load('<?php  echo strtolower($this->name);?>/<?php echo $this->request->params['action'];?>/'+$().getFilters());
        }
    }
    
    <?php if(isset($searchText)){ ?>
    $("#removeSearch").bind('click',function(event){ $("#cuerpoApp").load('<?php  echo strtolower($this->name);?>/<?php echo $this->request->params['action'];?>');});
    <?php }else{?>
    $("#botonSearch").bind('click',function(event){$().sendSearch();});
    <?php } ?>
    $(".griddata td:first-child ").click( 
    function (event) {
        event.stopPropagation();
        //colorea las filas checkeadas    
        if ($(this).children().children().children().is(':checked')){
            $(this).parent().addClass('row-selected');
        }else{
            $(this).parent().removeClass('row-selected');
        }
    }
    );    
});
//]]>
</script>