<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php echo $this->Form->create("$modelo",array("url" => array("controller" => $controlador, "action" => "update")));?>

<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsVista".DS."Edit".DS."botonGuardarEdit"); ?>
        <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <i class="fa fa-edit"></i> Editar Registro - <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __($modelo);?>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="padded">
                    <div class="section-title <?php if($configModulo["general"]["columnas"]["columna1"]=="") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna1"]; ?> </div>
                    <div class="vpadded input-xlarge" id="columna1">
                    <?php $id="inicio";?>
                    <?php echo $this->Form->input("id",array(
                            "before" => "<label>".$configModulo["edit"]["campos"]["id"]["etiqueta"]."</label>",
                            "id"     => "id",
                            "label"  => false,
                            "class"  => "input-xlarge",
                            "div"    => array("class"=>" ".$configModulo["edit"]["campos"]["id"]["orden"]."")
                            ));?>
                    <?php $id="fin";?>
                    <?php $title="inicio";?>
                    <?php echo $this->Form->input("title",array(
                            "before" => "<label>".$configModulo["edit"]["campos"]["title"]["etiqueta"]."</label>",
                            "id"     => "title",
                            "label"  => false,
                            "class"  => "input-xlarge",
                            "placeholder" => "Escriba el t�tulo ...",
                            "div"    => array("class"=>" ".$configModulo["edit"]["campos"]["title"]["orden"]."")
                            ));?>
                    <?php $title="fin";?>
                    <?php $startevent="inicio";?>
                        <div id="starteventDateTimepicker" class="input-prepend date <?php echo $configModulo["add"]["campos"]["startevent"]["orden"];?>"  title="Doble click para la eliminar" >
                            <label><?php echo $configModulo["add"]["campos"]["startevent"]["etiqueta"];?></label>
                            <button class="btn light-blue inverse date-button add-on datetimepicker-button">
                                <i class="fa fa-calendar"></i>
                            </button>
                            <input type="text" ondblclick="$(this).val('')" readonly=""  class="input-small center datepicker" value="<?php echo $this->Time->format('d/m/Y h:i:s',$this->request->data['Evento']['startevent']);?>" name="data[Evento][startevent]" >
                        </div>
                    <?php $startevent="fin";?>
                    <?php $endevent="inicio";?>
                    <div id="endeventDateTimepicker" class="input-prepend date <?php echo $configModulo["add"]["campos"]["endevent"]["orden"];?>"  title="Doble click para la eliminar" >
                        <label><?php echo $configModulo["add"]["campos"]["endevent"]["etiqueta"];?></label>
                        <button class="btn light-blue inverse date-button add-on datetimepicker-button">
                            <i class="fa fa-calendar"></i>
                        </button>
                        <input type="text" ondblclick="$(this).val('')" readonly=""  class="input-small center datepicker" value="<?php echo $this->Time->format('d/m/Y h:i:s',$this->request->data['Evento']['endevent']);?>" name="data[Evento][endevent]" >
                    </div>
                    <?php $endevent="fin";?>
                    <?php $allday="inicio";?>
                    <?php echo $this->Form->input("allday",array(
                        "before" => "<label>".$configModulo["edit"]["campos"]["allday"]["etiqueta"]."</label>",
                        "id"     => "allday",
                        "label"  => false,
                        "class"  => "input-xlarge",
                        "div"    => array("class"=>" ".$configModulo["edit"]["campos"]["allday"]["orden"]."")
                        ));?>
                    <?php $allday="fin";?>
                    <?php $description="inicio";?>
                        <?php echo $this->Form->input("description",array(
                                        "before"      => "<label>".$configModulo["add"]["campos"]["description"]["etiqueta"]."</label>",
                                        "id"          => "description",
                                        "label"       => false,
                                        "class"       => "input-xlarge",
                                        "div"         => array("class"=>" ".$configModulo["add"]["campos"]["description"]["orden"]."")
                        ));?>
                    <?php $description="fin";?>    
                    <?php $espacioFuturosCampos=true; ?>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="padded">
                <div class="section-title <?php if($configModulo["general"]["columnas"]["columna2"]=="hide") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna2"]; ?> </div>
                <div class="vpadded input-xlarge" id="columna2"></div>
            </div>
        </div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
    <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); ?>
    <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); ?>
    <?php if ($configModulo["general"]["acciones"]["accionOrdenarCampos"]) echo $this->element("ElementsVista".DS."accionOrdenarCamposFormularios"); ?>
    <?php echo $this->element("ElementsVista".DS."Edit".DS."accionMostrarEnEdit"); ?>    
}); 
</script>
