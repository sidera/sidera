<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php echo $this->Form->create("$modelo");?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
       	<?php if($configModulo["general"]["botones"]["btNuevo"]) echo $this->element("ElementsVista".DS."Index".DS."botonNuevo"); ?>
        <?php if($configModulo["general"]["acciones"]["accionEditar"]) echo $this->element("ElementsVista".DS."Index".DS."botonEditar"); ?>
        <?php if($configModulo["general"]["botones"]["btEliminar"]) echo $this->element("ElementsVista".DS."botonEliminarView"); ?>
        <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <i class="fa fa-eye-open"></i> Consultar Registro - <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __($modelo);?>
        </div>
        <div class="row-fluid">
            <div class="span6">
                <div class="padded">
                    <div class="section-title <?php if($configModulo["general"]["columnas"]["columna1"]=="") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna1"]; ?> </div>
                    <div class="vpadded" id="columna1">
                    <?php $title="inicio";?>
                    <div class="<?php echo $configModulo["view"]["campos"]["title"]["orden"];?>">	
                        <label><?php echo $configModulo["view"]["campos"]["title"]["etiqueta"];?></label>
                        <div class="well well-small fieldView" rel="tooltip left" title="Haga clic para editar">
                            <?php echo h($registro[$modelo]["title"]);?>
                        </div>
                    </div>
                    <?php $title="fin";?>
                    <?php $startevent="inicio";?>
                    <div class="<?php echo $configModulo["view"]["campos"]["startevent"]["orden"];?>">	
                        <label><?php echo $configModulo["view"]["campos"]["startevent"]["etiqueta"];?></label>
                        <div class="well well-small fieldView" rel="tooltip left" title="Haga clic para editar">
                            <?php echo h($registro[$modelo]["startevent"]);?>
                        </div>
                    </div>
                    <?php $startevent="fin";?>
                    <?php $endevent="inicio";?>
                    <div class="<?php echo $configModulo["view"]["campos"]["endevent"]["orden"];?>">	
                        <label><?php echo $configModulo["view"]["camposevent"]["end"]["etiqueta"];?></label>
                        <div class="well well-small fieldView" rel="tooltip left" title="Haga clic para editar">
                            <?php echo h($registro[$modelo]["endevent"]);?>
                        </div>
                    </div>
                    <?php $endevent="fin";?>
                    <?php $created="inicio";?>
                    <div class="<?php echo $configModulo["view"]["campos"]["created"]["orden"];?>">	
                        <label><?php echo $configModulo["view"]["campos"]["created"]["etiqueta"];?></label>
                        <div class="well well-small fieldView" rel="tooltip left" title="Haga clic para editar">
                            <?php echo h($registro[$modelo]["created"]);?>
                        </div>
                    </div>
                    <?php $created="fin";?>
                    <?php $modified="inicio";?>
                    <div class="<?php echo $configModulo["view"]["campos"]["modified"]["orden"];?>">	
                        <label><?php echo $configModulo["view"]["campos"]["modified"]["etiqueta"];?></label>
                        <div class="well well-small fieldView" rel="tooltip left" title="Haga clic para editar">
                            <?php echo h($registro[$modelo]["modified"]);?>
                        </div>
                    </div>
                    <?php $modified="fin";?>
                    <?php $allday="inicio";?>
                    <div class="<?php echo $configModulo["view"]["campos"]["allday"]["orden"];?>">	
                        <label><?php echo  $configModulo["view"]["campos"]["allday"]["etiqueta"];?></label>
                        <div class="well well-small fieldView" rel="tooltip left" title="Haga clic para editar">
                            <?php echo $this->Form->input("",array("type" => "checkbox","id"=>"allday","label"=>false,checked =>$registro[$modelo]["allday"])); ?>
                        </div>
                    </div>
                    <?php $allday="fin";?>
                    <?php $espacioFuturosCampos=true; ?>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="padded">
                    <div class="section-title <?php if($configModulo["general"]["columnas"]["columna2"]=="hide") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna2"]; ?> </div>
                    <div class="vpadded" id="columna2"></div>
                </div>
            </div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
    <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); ?>
    <?php echo $this->element("ElementsVista".DS."accionOrdenarCamposFormularios"); ?>
}); 
</script>