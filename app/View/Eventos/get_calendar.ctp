<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="span6">
    <div class="padded">
        <div class="section-title">Eventos</div>
        <div class="chart-container-calendar calendar" id="calendar" ></div>
    </div>
</div>
<script type="text/javascript" id="myScript">
$(document).ready(function() {
    $('#calendar').fullCalendar({
        header: {
            left:   'prev,next today',
            center: 'title',
            right:  'month,agendaWeek,agendaDay'
        },
        timeFormat: 'H:mm',
        editable:   false,
        events:     'eventos/getEventsForDateRange',
        droppable:  false,
        eventBackgroundColor: '#D15B47',
        eventBorderColor:'#D15B47',
        eventRender: function(event, element) { 
            element.attr('title',event.description);
        }
    });
});
</script>
