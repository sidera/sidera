<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php echo $this->Form->create("$modelo",array("url" => array("controller" => $controlador, "action" => "create")));?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages");  ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsVista".DS."Add".DS."botonGuardarAdd"); ?>
        <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <i class="fa fa-pencil"></i> Nuevo Registro - <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __("$modelo");?>
        </div>
        <div class="row-fluid">
           <div class="span6 padded">
                <div class="section-title">Datos Personales</div>
                    <?php $nombre="inicio";?>
                    <?php echo $this->Form->input("nombre",array(
                        "before" => "<label>".$configModulo["add"]["campos"]["nombre"]["etiqueta"]."</label>",
                        "id"     => "nombre",
                        "label"  => false,
                        "class"  => "input-xlarge",
                        "placeholder" => "Escriba el nombre ...",
                        "div"    => array("class"=>" ".$configModulo["add"]["campos"]["nombre"]["orden"]."")
                    ));?>
                    <?php $nombre="fin";?>
                    <?php $apellido1="inicio";?>
                    <?php echo $this->Form->input("apellido1",array(
                        "before" => "<label>".$configModulo["add"]["campos"]["apellido1"]["etiqueta"]."</label>",
                        "id"     => "apellido1",
                        "label"  => false,
                        "class"  => "input-xlarge",
                        "placeholder" => "Escriba el apellido ...",
                        "div"    => array("class"=>" ".$configModulo["add"]["campos"]["apellido1"]["orden"]."")
                    ));?>
                    <?php $apellido1="fin";?>
                    <?php $apellido2="inicio";?>
                    <?php echo $this->Form->input("apellido2",array(
                        "before" => "<label>".$configModulo["add"]["campos"]["apellido2"]["etiqueta"]."</label>",
                        "id"     => "apellido2",
                        "label"  => false,
                        "class"  => "input-xlarge",
                        "placeholder" => "Escriba el apellido ...",
                        "div"    => array("class"=>" ".$configModulo["add"]["campos"]["apellido2"]["orden"]."")
                    ));?>
                    <?php $apellido2="fin";?>
                    <?php $nif="inicio";?>
                    <?php echo $this->Form->input("nif",array(
                        "before" => "<label>".$configModulo["add"]["campos"]["nif"]["etiqueta"]."</label>",
                        "id"     => "nif",
                        "label"  => false,
                        "class"  => "input-xlarge",
                        "placeholder" => "Escriba el nif ...",
                        "div"    => array("class"=>" ".$configModulo["add"]["campos"]["nif"]["orden"]."")
                    ));?>
                    <?php $nif="fin";?>
                    <?php $email="inicio";?>
                    <?php echo $this->Form->input("email",array(
                        "before" => "<label>".$configModulo["add"]["campos"]["email"]["etiqueta"]."</label>",
                        "id"     => "email",
                        "label"  => false,
                        "class"  => "input-xlarge",
                        "placeholder" => "Escriba el email ...",
                        "div"    => array("class"=>" ".$configModulo["add"]["campos"]["email"]["orden"]."")
                    ));?>
                    <?php $email="fin";?>
           </div>
           <div class="span6 padded">
                <div class="section-title">Perfil de Usuario</div> 
                <?php $grupo_id="inicio";?>
                <?php echo $this->Form->input("grupo_id",array(
                        "before" => "<label>".$configModulo["add"]["campos"]["grupo_id"]["etiqueta"]."</label>",
                        "id"     => "grupo_id",
                        "label"  => false,
                        "class"  => "input full-width chzn-select",
                        "div"    => array("class"=>" ".$configModulo["add"]["campos"]["grupo_id"]["orden"]."")
                ));?>
                <?php $grupo_id="fin";?>
                <?php $username="inicio";?>
                <?php echo $this->Form->input("username",array(
                        "before" => "<label>".$configModulo["add"]["campos"]["username"]["etiqueta"]."</label>",
                        "id"     => "username",
                        "label"  => false,
                        "class"  => "input-xlarge",
                        "placeholder" => "Escriba el username ...",
                        "div"    => array("class"=>" ".$configModulo["add"]["campos"]["username"]["orden"]."")
                ));?>
                <?php $username="fin";?>
                <?php $password="inicio";?>
                <?php echo $this->Form->input("password",array(
                        "before" => "<label>".$configModulo["add"]["campos"]["password"]["etiqueta"]."</label>",
                        "id"     => "password",
                        "minlength"  =>5,
                        "label"  => false,
                        "class"  => "input-xlarge",
                        "div"    => array("class"=>" ".$configModulo["add"]["campos"]["password"]["orden"]."")
                ));?>
                <?php $password="fin";?>
                <?php $espacioFuturosCampos=true; ?>
           </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
    $(".chzn-select").chosen();
    var controlador="<?php echo $controlador;?>"; 
    var modelo="<?php echo $modelo;?>";  
    var options = {target:"#cuerpoApp"}; 
   <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); ?>
    <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); ?>
    <?php if($configModulo["general"]["acciones"]["accionOrdenarCampos"]) echo $this->element("ElementsVista".DS."accionOrdenarCamposFormularios"); ?>
    <?php echo $this->element("ElementsVista".DS."Add".DS."accionMostrarEnAdd"); ?>
}); 
</script>