<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<?php echo $this->Form->create("Usuario",array("url" => "updatePass"));?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <button class="btn btn-default" id="guardarPassRegistro<?php echo $modelo;?>">
        	<i class="fa fa-plus-circle  "></i> Cambiar Contraseña
        </button>
        <?php  
            //Comprobar si viene de una página
            if($this->Session->check("page[$modelo]")==TRUE){
                $pagina="/page:".$this->Session->read("page[$modelo]");
            }else{
                $pagina=""; 
            }
        ?>

      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form span6">
        <div class="box-header">
           <i class="fa fa-key"></i> Cambiar Contraseña
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="padded">
                    <div class="section-title <?php if($configModulo["general"]["columnas"]["columna1"]=="") echo "hide" ?>"> 
                        <?php echo $this->request->data['Usuario']['nombre']." ".$this->request->data['Usuario']["apellido1"]." ".$this->request->data['Usuario']["apellido1"];?> </div>
                    <div class="vpadded " id="columna1">
                        
                    <?php $username="inicio";?>
                    <?php echo $this->Form->input("username",array(
                            "before" => "<label>Usuario</label>",
                            "id"     => "username",
                            "readonly"  =>"readonly",
                            "value" => $this->request->data['Usuario']['username'],
                            "label"  => false,
                            "class"  => "input-xlarge",
                            "placeholder" => "Escriba el username ...",
                            "div"    => array("class"=>"")
                    ));?>
                    <?php $username="fin";?>
                        
                    <?php $passActual="inicio";?>
                    <?php echo $this->Form->input("passActual",array(
                            "before"   => '<label><span class="color-red">*</span> Contraseña Actual</label>',
                            "id"       => "passActual",
                            "type"  => "password",
                            "required"  =>"required",
                            "autocomplete"=>"off",
                            "minlength"  =>5,
                            "label"    => false,
                            "class"    => "input-xlarge",
                            "placeholder" => "Escriba la contraseña actual ...",
                            "div"    => array("class"=>"")
                            ));?>
                    <?php $passActual="fin";?>
                    
                    <?php $nuevaPass="inicio";?>
                    <?php echo $this->Form->input("nuevaPass",array(
                            "before"   => "<label><span class='color-red'>*</span> Nueva Contraseña</label>",
                            "id"       => "nuevaPass",
                            "type"  => "password",
                            "required"  =>"required",
                            "minlength"  =>5,
                            "value"  =>"",
                            "label"    => false,
                            "class"    => "input-xlarge",
                            "placeholder" => "Escriba la nueva contraseña ...",
                            "div"    => array("class"=>"")
                            ));?>
                    <?php $nuevaPass="fin";?>
                        
                   <?php $confirmarNuevaPass="inicio";?>
                    <?php echo $this->Form->input("confirmarNuevaPass",array(
                            "before"   => "<label><span class='color-red'>*</span> Nueva Contraseña (Confirmar)</label>",
                            "id"       => "confirmarNuevaPass",
                            "required"  =>"required",
                            "type"  => "password",
                            "minlength"  =>5,
                            "value"  =>"",
                            "autocomplete"=>"off",
                            "label"    => false,
                            "class"    => "input-xlarge",
                            "placeholder" => "Confirme la nueva contraseña ...",
                            "div"    => array("class"=>"")
                            ));?>
                    <?php $confirmarNuevaPass="fin";?>
                        
                    <?php $espacioFuturosCampos=true; ?>
                    </div>
                </div>
            </div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
<?php echo $this->element("ElementsVista".DS."javascriptRequerido"); ?>
var controlador="<?php echo $controlador;?>"; 
var modelo="<?php echo $modelo;?>";

    $("#UsuarioPassForm").validate({
    rules: {
            passActual:{
                       minlength: 5
                       },
            nuevaPass:{
                       minlength: 5
                       },
            confirmarNuevaPass:{
                       minlength: 5
                       }           
            }
    });
    
    $("#nuevaPass").rules("add", {
           maxlength: function(element) {
               if( $("#nuevaPass").val() != $("#passActual").val()) return 9; else return false;
            },
            messages: {
                maxlength:"La nueva contraseña ha de ser distinta de la contraseña actual"
            }
    });
    
    $("#confirmarNuevaPass").rules("add", {
           maxlength: function(element) {
               if( $("#confirmarNuevaPass").val() == $("#nuevaPass").val()) return 128; else return false;
            },
            messages: {
                maxlength:"Las dos contraseñas que has ingresado no concuerdan"
            }
    });
    
 	$('#UsuarioPassForm').ajaxForm({target:'#content'});
        $('#guardarPassRegistro<?php echo $modelo;?>').click(function(event){
            event.preventDefault();
            if ($("#UsuarioPassForm").valid()){
                $("#UsuarioPassForm").ajaxSubmit({success:function(data){$('#cuerpoApp').html(data);}});
            }
        });
   });   
</script>