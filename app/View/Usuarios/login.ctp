<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
    	<?php echo Configure::read('App.name');?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <?php
       
        //JQuery 
        $this->Html->script('jquery.min.js', array('inline' => false));
        $this->Html->script('jquery.form.min.js', array('inline' => false));
        
        //Bootstrap
        $this->Html->css('/app/webroot/sidera3/bootstrap/css/bootstrap.min.css', null, array('inline' => false));  
        $this->Html->script('/app/webroot/sidera3/bootstrap/js/bootstrap.min.js', array('inline' => false));
        $this->Html->css('/app/webroot/css/sidera.css', null, array('inline' => false));
        //Font-Awesome
        $this->Html->css('../lib/font-awesome-4.7.0/css/font-awesome.min.css', null, array('inline' => false));
        echo '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500">';
        
        //Extensiones
         $this->Html->meta('icon', $this->Html->url('/app/webroot/img/sidera/sidera_favicon.ico'));
     
    ?>    
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!-- there's an IE separated stylesheet with the following resets for IE -->
    <!--[if lte IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
          <script src="../javascripts/html5shiv.js" type="text/javascript"></script>
          <script src="../javascripts/excanvas.js" type="text/javascript"></script>
          <script src="../javascripts/ie_fix.js" type="text/javascript"></script>
          <link href="../stylesheets/ie_fix.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body>
    <div id="cuerpoApp" class="top-content login container ">
        <div id="flashMessagesLogin"><?php echo $this->Session->flash('auth');?></div>  	
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text">
                <h1><strong><?php echo Configure::read('App.name');?>  <?php echo VERSION_MIAPP;?></strong></h1>
                <div class="description">
                    <h4><?php echo Configure::read('App.organismo');?> </h4>
                    <p>( <?php echo Configure::read('App.consejeria');?> )</p>
                </div>
            </div>
        </div>
        <div id="form-login" class="row form-box col-md-4 col-md-offset-4 col-xs-offset-1 col-xs-10 text-left">
            <div id="form-login-cabecera" class="form-top ">
                <div class="form-top-left col-md-10 col-xs-8">
                        <h3>Acceso a la aplicación</h3>
                <p class="hidden-xs">Introduzca usuario y contraseña para acceder:</p>
                </div>
                <div class="form-top-right col-md-2 col-xs-4 icono-login">
                        <i class="fa fa-lock fa-3x"></i>
                </div>
            </div>
            <div id="form-login-cuerpo" class="form-bottom ">
                <?php echo $this->Form->create('Usuario', array('action' => 'login','name'=>'form_login','class'=>'login-form'));?>
                <div class="form-group">
                        <label class="sr-only" for="form-username">Usuario</label>
                        <input id="username" name="data[Usuario][username]" placeholder="Usuario..." class="form-username form-control" type="text">
                </div>
                <div class="form-group">
                        <label class="sr-only" for="form-password">Contraseña</label>
                        <input id="password" name="data[Usuario][password]" placeholder="Contraseña..." class="form-password form-control" type="password">
                </div>
                <?php echo $this->Form->end(array('label'=>'Acceder','class'=>'btn btn-default','id'=>'login-submit','div'=>'actions'));?>
            </div>
        </div>
    </div>
 </body>
 </html>   
 <script type="text/javascript" id="myScript">
//<![CDATA[
$(document).ready(function() { 
  $('#username').focus();  
});
//]]>
</script>   
</html>