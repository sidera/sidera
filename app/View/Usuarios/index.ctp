<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>
<div id="barraAccion" class="row-fluid actionsBar">
    <div id="columnaBotonesUsuario" class="btn-group  pull-left" >
        <?=$this->element("ElementsVista".DS."Index".DS."botonNuevo")?>
        <?=$this->element("ElementsVista".DS."Index".DS."botonEliminar")?>
        <?=$this->element("ElementsVista".DS."Index".DS."botonExportar")?>
        <?=$this->element("ElementsVista".DS."Index".DS."accionEditar")?>
    </div><!--columnaBotonesUsuario-->
    <div id="columnaBotonesAdministrador" class="btn-group col-md-4" >
        <?=$this->element("ElementsVista".DS."Index".DS."botonConfigurar")?>
        <?=$this->element("ElementsVista".DS."Index".DS."botonSincronizar")?>   
    </div><!--columnaBotonesAdministrador-->
    <div id="columnaBusquedaBarraAccion" class="pull-right col-md-2">
        <?=$this->element("ElementsVista".DS."Index".DS."searchForm",array("model"=>"usuarios"))?>
    </div><!--columnaBusquedaBarraAccion--> 
</div><!--barraAccion--> 
    
<div id="modulo" class="container-fluid form">
    
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6 col-xs-6">
            <i id="iconoModulo" class=" fa fa-align-justify fa-lg"></i> 
            <span id="etiquetaModulo">Usuarios</span> 
        </div><!--columnaCabecera-->
        <div id="columnaPaginacion" class="col-md-6 pull-right">
            <?=$this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>"Usuario"))?>
        </div><!--columnaPaginacion-->
    </div><!--cabeceraModulo--> 
 
    <div id="cuerpoModulo" class="fila row-fluid">  
        <div  class="columna dataTables_wrapper" role="grid" id="DataTables_Table_0_wrapper">
            <table class="elemento table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="check"><i class="fa fa-check-square fa-lg"></i></th>
                        <th data-header="id" class="orderable"><?=$this->Paginator->sort("id","Identificador")?></th>
                        <th data-header="username" class="orderable"><?=$this->Paginator->sort("username","Username"); ?></th>
                        <th data-header="nif" class="orderable"><?=$this->Paginator->sort("nif","Nif")?></th>
                        <th data-header="apellido1" class="orderable"><?=$this->Paginator->sort("apellido1","1er Apellido")?></th>
                        <th data-header="apellido2" class="orderable"><?=$this->Paginator->sort("apellido2","2� Apellido")?></th>
                        <th data-header="email" class="orderable"><?=$this->Paginator->sort("email","eMail")?></th>
                        <th data-header="grupo_id" class="orderable"><?=$this->Paginator->sort("grupo_id","Grupo")?></th>
                        <!--nuevosCamposCabecera-->
                	</tr>
                </thead>
                <tbody class="griddata" id="PruebaGridData">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?=$registro["Usuario"]["id"]?>" class="gradeA" title="Haga doble clic para editar el registro"> 
                        <td><i value="<?=$registro["Usuario"]["id"]?>" class="checkCell fa fa-lg fa-square-o"></i></td>
                        <td class="id"><?=$registro["Usuario"]["id"]?></td>
                        <td class="username"><?=$registro["Usuario"]["username"]?></td>
                        <td class="nif"><?=$registro["Usuario"]["nif"]?></td>
                        <td class="apellido1"><?=$registro["Usuario"]["apellido1"]?></td>
                        <td class="apellido2"><?=$registro["Usuario"]["apellido2"]?></td>
                        <td class="email"><?=$registro["Usuario"]["email"]?></td>
                        <td class="grupo_id"><?=$registro["Usuario"]["grupo_id"]?></td>
                        
                        <!--nuevosCamposCuerpo-->
                     </tr>
                     <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div id="pieModulo" class="row-fluid">
        <div id="columnaPie" class="col-md-8 ">
        </div><!--columnaPie-->
        
        <div id="columnaPaginacionPie" class="col-md-4 pull-right">
            <?=$this->element("ElementsVista".DS."Index".DS."registrosPorPagina")?>
        </div><!--columnaPaginacionPie-->     
    </div><!--pieModulo--> 
    
</div><!--modulo--> 
<script type="text/javascript" id="myScript">
$(document).ready(function() {
    <?=$this->element("ElementsVista".DS."Index".DS."indexJs")?>
});
</script>