<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php echo $this->Form->create("$modelo",array("url" => array("controller" => $controlador, "action" => "configurarModuloGuardar")));?><div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); //Bloque para mensajes con setFlash ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsConfigurarModulos".DS."botonGuardarConfig");?>  <!--Elemento con funcionalidad de boton de Guardar Edit -->
        <?php echo $this->element("ElementsVista".DS."botonCancelar");?>  <!--Elemento con funcionalidad de boton de Cancelar -->       
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <?php if($configModulo["general"]["config"]["etiquetaModulo"]) {$etiqueta=$configModulo["general"]["config"]["etiquetaModulo"];} else {$etiqueta=$modelo;}?>
           <i class="fa fa-edit"></i> Configurar M�dulo - <?php echo $etiqueta;?>
        </div>
          <div class="row-fluid">
        	<div class="span12">
				<div class="padded">
					
				<div id="panel" class="span10 padded"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab"> Par�metros Generales</a></li>
                        <li><a href="#tab6" data-toggle="tab"> Vista General</a></li>
                        <li><a href="#tab5" data-toggle="tab">Index</a></li>
                        <li><a href="#tab2" data-toggle="tab"> Add / Edit</a></li>
                        <li><a href="#tab3" data-toggle="tab"> Controles sobre Campos</a></li>
                        <li><a href="#tab4" data-toggle="tab"> Auditor�a</a></li>
                     
                    </ul>
                <div class="tab-content alto-fijo">
                    <?php echo $this->element("ElementsConfigurarModulos".DS."opcionGeneral"); ?>
                    <?php echo $this->element("ElementsConfigurarModulos".DS."opcionAddEdit");?>
                    <?php echo $this->element("ElementsConfigurarModulos".DS."opcionControlesCampos");?>
                    <?php echo $this->element("ElementsConfigurarModulos".DS."opcionAuditoria");?>
                    <?php echo $this->element("ElementsConfigurarModulos".DS."opcionIndex"); ?>
                    <?php echo $this->element("ElementsConfigurarModulos".DS."opcionVistaGeneral"); ?>
                    <?php //echo $this->element("ElementsConfigurarModulos".DS."opcionEdit"); ?>
                    <?php //echo $this->element("ElementsConfigurarModulos".DS."opcionView"); ?>
                </div><!-- tab content -->
		</div><!-- panel1 -->
		</div>
	</div>
     </div>
     </div>
</div>
<input type="hidden" value="saveForm" id="accionForm" name="data[accionForm]">
<?php echo $this->element("ElementsConfigurarModulos".DS."accionConfigurar");?>
<?php echo $this->Form->end();?>
<script type="text/javascript" id="myScript">
//<![CDATA[
$(document).ready(function() {
    var controlador="<?php echo $controlador;?>"; 
    var modelo="<?php echo modelo;?>";
    $(":checkbox").uniform();
    $(".chzn-select").chosen();
    $.post("configuraciones/borrarCache");
});
//]]>
</script>