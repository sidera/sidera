<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php echo $this->Form->create("$modelo",array("url" => array("controller" => $controlador, "action" => "create")));?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); // Bloque para mensajes con setFlash ?>
    <?php if(!isset($formType)){?><hr class="main"><?php }?>
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsVista".DS."Add".DS."botonGuardarAdd"); // Elemento con funcionalidad de boton de Guardar Add ?>
        <?php echo $this->element("ElementsVista".DS."botonCancelar");  // Elemento con funcionalidad de boton de Cancelar ?>
      </div>
    </div>
    <?php if(!isset($formType)){?><hr class="main"><?php }?>
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <i class="fa fa-pencil"></i> Nuevo Registro - <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __("$modelo");?>
        </div>
        <div class="row-fluid">
           <div class="span6 padded">
               <?php $nombre="inicio";?>
							<?php echo $this->Form->input("nombre",array(
							"before"=>"<label>".$configModulo["add"]["campos"]["nombre"]["etiqueta"]."</label>",
							"id"=>"nombre",
							"label"=>false,
							"class"=>"input-xlarge",
							"placeholder"=>"Escriba el nombre ...",
							"div"=>array("class"=>" ".$configModulo["add"]["campos"]["nombre"]["orden"]."")
							));?>
					<?php $nombre="fin";?>
                <?php $descripcion="inicio";?>
							<?php echo $this->Form->input("descripcion",array(
							"before"=>"<label>".$configModulo["add"]["campos"]["descripcion"]["etiqueta"]."</label>",
							"id"=>"descripcion",
                                                        "type"        => "textarea",   
                                                        "rows"        => 5,   
							"label"=>false,
							"class"=>"input-xlarge",
							"placeholder"=>"Escriba el nombre ...",
							"div"=>array("class"=>" ".$configModulo["add"]["campos"]["nombre"]["orden"]."")
							));?>
					<?php $descripcion="fin";?>
               
               <div class="note">
                   <i class="fa fa-exclamation-sign"></i>Puede escribir un m�ximo de 250 caracteres.
                </div>
                 <?php $espacioFuturosCampos=true; ?>
           </div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
    <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); //Elemento con el javascript requerido para esta vista ?>
    <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); //Elemento con funcionalidad de validar formulario ?>
    <?php if($configModulo["general"]["acciones"]["accionOrdenarCampos"]) echo $this->element("ElementsVista".DS."accionOrdenarCamposFormularios"); //Elemento con funcionalidad de ordenar los campos del formulario ?>
    <?php echo $this->element("ElementsVista".DS."Add".DS."accionMostrarEnAdd"); // Elemento con funcionalidad de mostrar (o no) columnas en el formulario Add ?>
}); 
</script>