<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php echo $this->Form->create("$modelo",array("url" => array("controller" => $controlador, "action" => "update")));?>
<?php echo $this->Form->input("id");?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); ?> <!--Bloque para mensajes con setFlash -->
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsVista".DS."Edit".DS."botonGuardarEdit");?>  <!--Elemento con funcionalidad de boton de Guardar Edit -->
        <?php echo $this->element("ElementsVista".DS."botonCancelar");?>  <!--Elemento con funcionalidad de boton de Cancelar -->       
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered  form">
        <div class="box-header">
           <i class="fa fa-edit"></i> Editar Registro - <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __("$modelo");?>
        </div>
        <div class="row-fluid">
           <div class="span6 padded">
                <?php $nombre="inicio";?>
							<?php echo $this->Form->input("nombre",array(
							"before"=>"<label>".$configModulo["edit"]["campos"]["nombre"]["etiqueta"]."</label>",
							"id"=>"nombre",
							"label"=>false,
							"class"=>"input-xlarge",
							"placeholder"=>"Escriba el nombre ...",
							"div"=>array("class"=>" ".$configModulo["edit"]["campos"]["nombre"]["orden"]."")
							));?>
					<?php $nombre="fin";?>
               <?php $descripcion="inicio";?>
							<?php echo $this->Form->input("descripcion",array(
							"before"=>"<label>".$configModulo["edit"]["campos"]["descripcion"]["etiqueta"]."</label>",
							"id"=>"nombre",
                                                        "label"=>false,
                                                        "type"        => "textarea",  
                                                        "rows"        => 5,     
							"class"=>"input-xlarge",
							"placeholder"=>"Escriba la descripcion ...",
							"div"=>array("class"=>" ".$configModulo["edit"]["campos"]["descripcion"]["orden"]."")
							));?>
					<?php $descripcion="fin";?>
               <?php $espacioFuturosCampos=true; ?>
               <div class="note">
                   <i class="fa fa-exclamation-sign"></i>Puede escribir un m�ximo de 250 caracteres.
                </div>
           </div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() { 
	var controlador="<?php echo $controlador;?>"; 
	var modelo="<?php echo $modelo;?>";  
    var options = {target:"#content"}; 
   <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); //Elemento con el javascript requerido para esta vista ?>
    <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); //Elemento con funcionalidad de validar formulario ?>
    <?php if ($configModulo["general"]["acciones"]["accionOrdenarCampos"]) echo $this->element("ElementsVista".DS."accionOrdenarCamposFormularios"); //Elemento con funcionalidad de ordenar los campos del formulario ?>
    <?php echo $this->element("ElementsVista".DS."Edit".DS."accionMostrarEnEdit"); // Elemento con funcionalidad de mostrar (o no) columnas en el formulario Edit ?>    
}); 
</script>
