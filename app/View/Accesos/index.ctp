<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php if($configModulo["general"]["botones"]["btFiltrar"]) echo $this->element("ElementsVista".DS."Index".DS."botonFiltrar"); ?>
        <?php if($configModulo["general"]["botones"]["btExportar"]) echo $this->element("ElementsVista".DS."Index".DS."botonExportar");  ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."accionOrdenarColumnasGrid");  ?>
        <?php if($configModulo["general"]["acciones"]["accionChekearTodos"]) echo $this->element("ElementsVista".DS."Index".DS."accionCheckearTodos");  ?>
      </div>
      <div class="btn-group pull-right right span4 input-xxlarge">
        <?php if($configModulo["general"]["botones"]["btBuscar"]) echo $this->element("ElementsVista".DS."Index".DS."searchForm",array("model"=>"$controlador"));?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered">
        <div>
            <div class="box-header span4">
                <i class="fa fa-user"></i> <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __("$modelo");?>
            </div>
            <div class="groupPagination span6 pull-right">
                <?php echo $this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>$modelo));?>
                <?php echo $this->element("ElementsVista".DS."Index".DS."registrosPorPagina");?>
            </div>
        </div>
        <div role="grid" class="dataTables_wrapper" id="DataTables_Table_0_wrapper">
            <table class="table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="ui-state-default check" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                            <i class="fa fa-check"></i>
                        </th>
                       	<?php $id="inicio";?>
                        <?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"id","$configModulo"=>$configModulo));?><?php $id="fin";?>
                        <?php $username="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"username","$configModulo"=>$configModulo));?><?php $username="fin";?>
                        <?php $clave="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"clave","$configModulo"=>$configModulo));?><?php $clave="fin";?>
                        <?php $ip="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"ip","$configModulo"=>$configModulo));?><?php $ip="fin";?>
                        <?php $exito="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"exito","$configModulo"=>$configModulo));?><?php $exito="fin";?>
                        <?php $created="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"created","$configModulo"=>$configModulo));?><?php $created="fin";?>
                        <?php $modified="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"modified","$configModulo"=>$configModulo));?><?php $modified="fin";?>
                        <?php $espacioFuturosCampos=true; ?>
                   </tr>
                </thead>
                <tbody class="griddata">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?php echo $registro[$modelo]["id"];?>" class="gradeA" title="Haga clic para editar el registro"> 
                        <td class="checkCell">
                            <input type="checkbox" value="<?php echo $registro[$modelo]["id"];?>" class="itemSelector" title="Seleccione los elementos que desee eliminar">
                        </td>
                        <td class="id"><?php echo h($registro[$modelo]["id"]); ?></td>
                        <td class="username"><?php echo h($registro[$modelo]["username"]); ?></td>
                        <td class="clave"><?php echo h($registro[$modelo]["clave"]); ?></td>
                        <td class="ip"><?php echo h($registro[$modelo]["ip"]); ?></td>
                        <td class="exito"><?php echo h($registro[$modelo]["exito"]); ?></td>
                        <td class="created"><?php echo $this->Time->format('d/m/Y h:i:s',$registro[$modelo]["created"]); ?></td>
                        <td class="modified"><?php echo $this->Time->format('d/m/Y h:i:s',$registro[$modelo]["modified"]); ?></td>						
                        <?php $espacioFuturosCampos=true; ?>
                        </tr>
                     <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" id="myScript">
$(document).ready(function() { 
    $(":checkbox").uniform();    
    <?php echo $this->element("ElementsVista".DS."javascriptRequerido");?>
    <?php echo $this->element("ElementsVista".DS."Index".DS."accionMostrarEnIndex");?>
});
</script>