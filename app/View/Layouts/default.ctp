<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo Configure::read('App.name');?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->    
       
    <?php
        //JQuery 
        $this->Html->script('jquery.min.js', array('inline' => false));
        $this->Html->script('jquery.form.min.js', array('inline' => false));
        
        //Bootstrap
        $this->Html->css('/app/webroot/sidera3/bootstrap/css/bootstrap.min.css', null, array('inline' => false));  
        //$this->Html->css('/app/webroot/sidera3/bootstrap/css/bootstrap-theme.min.css', array('inline' => false));
        $this->Html->script('/app/webroot/sidera3/bootstrap/js/bootstrap.min.js', array('inline' => false));
        
        //simplePagination
        $this->Html->css('/app/webroot/lib/simplePagination_1.6/simplePagination.css', null, array('inline' => false));    
        $this->Html->script('/app/webroot/lib/simplePagination_1.6/simplePagination.js', array('inline' => false));
        
        //Chosen
        $this->Html->css('/app/webroot/sidera3/chosen/chosen.min.css', null, array('inline' => false));    
        $this->Html->script('/app/webroot/sidera3/chosen/chosen.jquery.min.js', array('inline' => false));
        
        //DateTimePicker (requiere moment.js)
        $this->Html->script('/app/webroot/lib/bootstrap-datetimepicker/moment/moment_2.15.2.js', array('inline' => false));
        $this->Html->css('/app/webroot/lib/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css', null, array('inline' => false));  
        $this->Html->script('/app/webroot/lib/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', array('inline' => false));
         
        //sidera
        $this->Html->css('sidera.css', null, array('inline' => false));
        $this->Html->script('sidera.js', array('inline' => false));
        
        //Font-Awesome
        $this->Html->css('../lib/font-awesome-4.7.0/css/font-awesome.min.css', null, array('inline' => false));
        echo '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500">';
        
        //Application
        $this->Html->css( '/app/AppModulos/webroot/applicationStyles.css', null, array('inline' => false));
        $this->Html->script('/app/AppModulos/webroot/applicationJavascripts.js', array('inline' => false));
        
        //Bootstrap-Select bootstrap-select-1.12.1
        $this->Html->css( '/app/webroot/lib/bootstrap-select-1.12.1/dist/css/bootstrap-select.min', null, array('inline' => false));
        $this->Html->script('/app/webroot/lib/bootstrap-select-1.12.1/dist/js/bootstrap-select.min', array('inline' => false));
        
        //jQuery Validation
        //$this->Html->script('../lib/jquery-validation-1.10.0/jquery.validate.min.js', array('inline' => false));
        //$this->Html->script('../lib/jquery-validation-1.10.0/messages_es.js', array('inline' => false));
        /*
        $this->Html->css('/app/webroot/whitemin/stylesheets/application.css', null, array('inline' => false));       
        $this->Html->script('/app/webroot/whitemin/javascripts/application.js', array('inline' => false));
        $this->Html->script('jquery.form.js', array('inline' => false));
        $this->Html->script('jquery.transit.min.js', array('inline' => false));
        $this->Html->script('autoNumeric.js', array('inline' => false));
        $this->Html->script('sidera_validations.js', array('inline' => false));
        
        $this->Html->script('/app/webroot/whitemin/javascripts/flot/jquery.flot.time.js', array('inline' => false));
        $this->Html->script('/app/webroot/whitemin/javascripts/flot/jquery.flot.selection.js', array('inline' => false));
        
        $this->Html->css('../lib/datetimepicker/bootstrap-datetimepicker.min.css', null, array('inline' => false));
        $this->Html->css('../lib/datepicker/css/datepicker.css', null, array('inline' => false));
        $this->Html->css('../lib/bootstrap-duallistbox/bootstrap-duallistbox.css', null, array('inline' => false));
        $this->Html->css('../lib/dragtable/css/dragtable-default.css', null, array('inline' => false));
        $this->Html->css('../lib/jquery-ui/css/jquery-ui.css', null, array('inline' => false));
        $this->Html->css('../lib/elfinder/css/elfinder.min.css', null, array('inline' => false));
        $this->Html->css('../lib/elfinder/css/theme.css', null, array('inline' => false));  
  
       
    //    $this->Html->script('../lib/tinymce/tinymce.min', array('inline' => false));
        $this->Html->script('../lib/datetimepicker/bootstrap-datetimepicker.min.js', array('inline' => false));
        $this->Html->script('../lib/datepicker/js/bootstrap-datepicker.js', array('inline' => false));
        $this->Html->script('../lib/bootstrap-duallistbox/jquery.bootstrap-duallistbox.js', array('inline' => false));
        $this->Html->script('../lib/datepicker/js/locales/bootstrap-datepicker.es.js', array('inline' => false));
        $this->Html->script('../lib/dragtable/js/jquery.dragtable.js', array('inline' => false));
        $this->Html->script('../lib/elfinder/js/elfinder.min.js', array('inline' => false));
        $this->Html->script('../lib/elfinder/js/i18n/elfinder.es.js', array('inline' => false));
        */ 
    ?>   
    
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!-- there's an IE separated stylesheet with the following resets for IE -->
    <!--[if lte IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
          <script src="../javascripts/html5shiv.js" type="text/javascript"></script>
          <script src="../javascripts/excanvas.js" type="text/javascript"></script>
          <script src="../javascripts/ie_fix.js" type="text/javascript"></script>
          <link href="../stylesheets/ie_fix.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
    
</head>
 <body>
    <?=$this->element('header')?>
     <div id="cuerpoApp">
       <?=$this->Session->flash()?>
       <?=$this->fetch('content')?>
    </div>
    <div id="bg-loading"></div>
    <div id="loading"></div>
    <!--<footer id="pieApp" class="footer row-fluid navbar  navbar-default navbar-fixed-bottom">
        <div class="columna col-md-12">
            <span><?//=Configure::read('App.name').CONSEJERIA.ORGANISMO?></span>
        </div>
    </footer>-->
</body>
</html>