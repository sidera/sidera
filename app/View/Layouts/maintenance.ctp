<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
    	<?php echo Configure::read('App.name');?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->    
    <?php
        
        $this->Html->meta('icon', $this->Html->url('/app/webroot/img/sidera/sidera_favicon.ico'));
        $this->Html->css('/app/webroot/whitemin/stylesheets/application.css', null, array('inline' => false));       
        $this->Html->css('sidera.css', null, array('inline' => false));
        $this->Html->script('../lib/jquery-validation-1.10.0/messages_es.js', array('inline' => false));
    ?>    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!-- there's an IE separated stylesheet with the following resets for IE -->
    <!--[if lte IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
          <script src="../javascripts/html5shiv.js" type="text/javascript"></script>
          <script src="../javascripts/excanvas.js" type="text/javascript"></script>
          <script src="../javascripts/ie_fix.js" type="text/javascript"></script>
          <link href="../stylesheets/ie_fix.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body>
<?php echo $this->element('header');?>

<?php 
    echo $this->fetch('content');
?>

</body>
</html>