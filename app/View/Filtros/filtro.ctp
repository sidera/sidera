<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="box-header">
    <span class="titleFilter"><i class="icon-filter"></i> Filtros</span>
    <a class="icon-remove-circle btnFiltroClose pull-right" title='Cerrar ventana de filtros'></a>
</div>
<div class="row-fluid">
<form method="post" class="form-filtrar form-inline" action="<?php echo $nombreControlador;?>/index">
<input type="hidden" id="filtro" name="data[filtro]" value="<?php echo $filtro;?>"/>
     <div id="divCrearFiltro">
        <div class="controls control-group">
            <div>
                <select class="input-medium filtrosLista full-width chzn-select" id="listaFiltros">
                    <option value="0">Seleccione un filtro...</option>
                    <?php 
                    for ( $i = 0 ; $i < count($filtrosGuardados) ; $i ++) {
                        echo '<option value="'.$filtrosGuardados[$i][Filtro][id].'">'.$filtrosGuardados[$i][Filtro][titulo].'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="controlFilters">
                <div class="btn-group">
                    <button class="btn btn-mini dark-blue" id="cargarFiltro" title="Haga clic para cargar el filtro seleccionado" type="button">
                        <i class="icon-download icon-white"></i> Cargar</button></button>
                    <button class="btn btn-mini dark-blue" id="eliminarFiltro" title="Haga clic para eliminar el filtro seleccionado" type="button">
                        <i class="icon-trash icon-white"></i> Eliminar</button></button>
                    <button class="btn btn-mini dark-blue" id="guardarFiltro" title="Haga clic para guardar el filtro actual">
                        <i class=" icon-upload icon-white "></i> Guardar</button>
                    <button class="btn btn-mini dark-blue" id="ejecutarFiltro" title="Haga clic para ejecutar el filtro">
                        <i class="icon-play icon-white "></i> Ejecutar</button>
                    <button class="btn btn-mini dark-blue" id="reiniciarFiltro" type="button" title="Haga clic empezar de nuevo">
                        <i class="icon-refresh icon-white "></i> Reiniciar</button>
                </div>
                <div class="btn-group optionFilters pull-right" data-toggle="buttons-radio">
                    <button 
                        class = "btn btn-mini dark-blue active" 
                        id    = "todasCondiciones" 
                        type  = "button" 
                        title = "Se han de cumplir todas las condiciones">Todas</button>
                    <button 
                        class = "btn btn-mini dark-blue" 
                        id    = "algunaCondicion" 
                        type  = "button" 
                        title = "Se ha de cumplir alguna de las condiciones">Alguna</button>
                </div>
                <input type="hidden" id="numeroCondiciones" name="data[filtros][numeroCondiciones]" value="0">
                <input type="hidden" id="tipoCondicion" name="data[filtros][tipoCondicion]" value="AND">
            </div>
            
        </div>
    </div>
    <div id="filtroSelectores">
        <div id="filtroCampos">
        <?php
            echo $this->Form->input('campoCabecera',array(
                'id'      => 'campoCabecera',
                'name'    => false,
                'label'   => false,
                'options' => array($campos),
                'title'   => 'Haga clic en el campo de texto para seleccionar',
                'class'   => 'filtrosLista chzn-select',
                'data-placeholder'=>"Seleccione un campo",
            ));
        ?>
        <?php
            echo $this->Form->input('campoTipo',array(
                'label'   => false,
                'name'    => false,
                'id'      => 'campoTipo',
                'div'     => null,
                'options' => array($tipos),
                'style'   => 'display:none'
            ));
        ?>
        </div>
        <div id="filtroOperadores">
         <?php
            foreach ($tipos as $key22 => $value22) {
                $key2=$key22;
                $value2=$value22;
                break;
            }
         ?>   
        <?php
            echo $this->Form->input('comparadorCabecera',array(
                'name'    => false,
                'label'   => false,
                'options' => $operadores[$tipos[$key2]],
                'title'   => 'Haga clic en el campo de texto para seleccionar',
                'class'   => 'filtrosLista chzn-select'
            ));
        ?>
        </div>
        <div id="filtroValor">
        <?php
            echo $this->Form->input('contenidoCabecera',array(
                'name'    => false,
                'label'   => false,
                'placeholder '=>'Escriba el contenido del filtro ...',
                'title'   => 'Haga clic en el campo de texto para escribir',
                'class'   => 'filtrosInput'
            ));
        ?>
        </div>
        <div>
            <button title="Haga clic para a�adir la condici�n al filtro" class="btn light-blue inverse" id="addFiltro" type="button">
                <i class="icon-plus-sign icon-white "></i></button>
        </div>
    </div>
    <div id="filtros" class="well well-small"></div>
    </div> 
</div>

</form>
<div id='eliminarFiltroDialog' class="modal hide fade in"  aria-hidden="false">
    <div class="modal-header">
        <h3>Eliminar Filtro</h3>
    </div>
    <div class="modal-body">
    �Realmente desea eliminar este filtro?
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal" class="btn">Cancelar</button>
        <button id="okEliminarFiltro" data-dismiss="modal" class="btn btn-primary">Aceptar</button>
    </div>
</div>
<div class="modal-backdrop hide fade in" id="eliminarFiltroDialogFondo"></div>
<div id="divGuardarFiltro" class="modal hide fade in"  aria-hidden="false">
    <div class="modal-header">
        <button class="close" id="dialogButtonCancel" aria-hidden="true" data-dismiss="modal" type="button">x</button>
        <h3>Guardar Filtro</h3>
    </div>
    <div class="modal-body">
        <p>Este filtro se almacenar� en el si�stema para poder se reutilizado en posteriores ocasiones.</p>
        <p>
            <label>T�tulo:</label>
            <input type="text" id="tituloFiltro" class="input-xlarge" placeholder="Introduzca un t��tulo para este filtro..." >
        </p>
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal" class="btn">Cancelar</button>
        <button id="okGuardarFiltro" data-dismiss="modal" class="btn btn-primary">Aceptar</button>
    </div>
</div>
<div class="modal-backdrop hide fade in" id="divGuardarFiltroFondo"></div>
</div>
<script type="text/javascript">
$(document).ready(function() { 
    $(".chzn-select").chosen(); $.getScript('<?php echo Router::url('js/index.js');?>');
    <?php if($filtro=="SI"){?>
        $('#sinFiltro').removeClass('hide');
        $('#filtrar').addClass('orange');
    <?php }?>  
        $('#contenidoCabecera').keyup(function() {
        var cadena = $(this).val();
        cadena = cadena.replace(/[`~!@#$%^&*()|+\=?�;'",.<>\{\}\[\]\\]/gi,'');
        $(this).val(cadena);
      });
});
    

</script>
