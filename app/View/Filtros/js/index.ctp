<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 $(document).ready(function() {
$('.form-filtrar').ajaxForm();
    $('#ejecutarFiltro').click( 
        function (event) {
            event.preventDefault();
            if($("#filtros").children().length>0){
                $("#filtro").val("SI");
                var options = {target:'#cuerpoApp'};
                $('.form-filtrar').ajaxSubmit(options);
                $('#filter').transition({ x: '-505px' });
            }else{
                $("#filterDialog > .modal-body" ).html("Debe introducir al menos una condicion para ejecutar el filtro!!");
                $("#filterDialog").modal();
            }
        }
    );
    
    $(document).on('click',".removeFiltro",function(event){
        event.preventDefault();
        $("#div"+$(this).attr('id')).remove();
    });
    
    function cambiarComparador(cComparador,cOrden)
    {
        cComparadorClave="";
        if (cOrden=="almacenar"){
            switch(cComparador){
                case '=': cComparadorClave="eq"; break;
                case '<>': cComparadorClave="ne"; break;
                case '<': cComparadorClave="lt"; break;
                case '<=': cComparadorClave="le"; break;
                case '>': cComparadorClave="gt"; break; 
                case '>=': cComparadorClave="ge"; break;
                case 'empieza por': cComparadorClave="bw"; break;
                case 'no empieza por': cComparadorClave="bn"; break;
                case 'termina por': cComparadorClave="ew"; break;
                case 'no termina por': cComparadorClave="en"; break;
                case 'contiene': cComparadorClave="cn"; break;
                case 'no contiene': cComparadorClave="nc"; break;
                default: cComparadorClave="";
            }
        }else{
            switch(cComparador){
                case 'eq': cComparadorClave="="; break;
                case 'ne': cComparadorClave="<>"; break;
                case 'lt': cComparadorClave="<"; break;
                case 'le': cComparadorClave="<="; break;
                case 'gt': cComparadorClave=">"; break;
                case 'ge': cComparadorClave=">="; break;
                case 'bw': cComparadorClave="empieza por"; break;
                case 'bn': cComparadorClave="no empieza por"; break;
                case 'ew': cComparadorClave="termina por"; break;
                case 'en': cComparadorClave="no termina por"; break;
                case 'cn': cComparadorClave="contiene"; break;
                case 'nc': cComparadorClave="no contiene"; break;
                default: cComparadorClave="";
            }
        }
        
        return cComparadorClave;
    }
	
    function pintarCondiciones(pCampo,pCampoEtiqueta,pComparador,pContenido)
    {
        pComparadorClave=cambiarComparador(pComparador,"almacenar");
        numero= parseInt($("#numeroCondiciones").val())+1;
        $("#numeroCondiciones").val(numero);
        $("#filtros").append('<div id="div'+numero+'"></div>');
        $("#div"+numero).append('<div id="condicion'+numero+'" class="alert turquoise fade in"></div>');
        $("#condicion"+numero).append('<button class="close removeFiltro" data-dismiss="alert" type="button" id="'+numero+'">×</button>');
        $("#condicion"+numero).append('<input type="hidden" name="data[filtros][campo'+numero+']" id="campo'+numero+'" value="'+pCampo+'">');
        $("#condicion"+numero).append('<input type="hidden" name="data[filtros][comparador'+numero+']" id="comparador'+numero+'" value="'+pComparadorClave+'">');
        $("#condicion"+numero).append('<input type="hidden" name="data[filtros][contenido'+numero+']" id="contenido'+numero+'" value="'+pContenido+'" title="'+pContenido+'">');
        $("#condicion"+numero).append('<span>'+pCampoEtiqueta+' '+pComparador+' '+pContenido+'</span>');
        if (numero>1){
            numeroanterior=numero-1;
            if($("#tipoCondicion").val()=="AND"){tipo="Y";color="yellow";}else{tipo="O";color="red";}
            $("#condicion"+numeroanterior).append('<span class="row label '+color+' pull-right" ><h4>'+tipo+'</h4></span>');
        }
        $("#contenidoCabecera").val("");
        
    }
    
    $('#addFiltro').click( 
        function (event) {
            event.preventDefault();
            if ($("#contenidoCabecera").val()!=""){
                pintarCondiciones($('#campoCabecera option:selected').val(),$('#campoCabecera option:selected').text(),$('#comparadorCabecera option:selected').text(),$('#contenidoCabecera').val());
            }else{
                $("#filterDialog > .modal-body" ).html("Introduca el contenido de la condición!!");
                $("#filterDialog").modal();
            }
        }
    );
    
    $("#todasCondiciones").click( 
        function (event) {
            event.preventDefault();
            $("#tipoCondicion").val("AND");
            $(".row").replaceWith('<span class="row label yellow pull-right" ><h4>Y</h4></span>');
        }
    );
    $("#algunaCondicion").click( 
        function (event) {
            event.preventDefault();
            $("#tipoCondicion").val("OR");
            $(".row").replaceWith('<span class="row label red pull-right" ><h4>O</h4></span>');
        }
    );
    $("#reiniciarFiltro").click(
        function (event) {
            event.preventDefault();
            $("#filtros").html('');
            $("#numeroCondiciones").val("0");
            $("#contenidoCabecera").val("");
            $("#filtro").val("NO");
            $('#filter').transition({ x: '-505px' });
            $('#cuerpoApp').load($('#controlador').val()+'/'+$('#nombreAcccion').val()+'/do:clearFilter');
        }
    );
    jQuery.fn.actualizarListaComparadores = function(){
        if($('#campoCabecera option:selected').length==0){ 
            $("#campoTipo").val(0);
        }else{
            $("#campoTipo").val($('#campoCabecera option:selected').val());
        }
        tipoSeleccionado=$("#campoTipo option:selected").text();
        var operadoresString = [
           {'value':'eq','text':'='},
           {'value':'ne','text':'<>'},
           {'value':'bw','text':'empieza por'},
           {'value':'bn','text':'no empieza por'},
           {'value':'ew','text':'termina por'},
           {'value':'en','text':'no termina por'},
           {'value':'cn','text':'contiene'},
           {'value':'nc','text':'no contiene'}];
        var operadoresInteger = [
           {'value':'eq','text':'='},
           {'value':'ne','text':'<>'},
           {'value':'lt','text':'<'},
           {'value':'le','text':'<='},
           {'value':'gt','text':'>'},
           {'value':'ge','text':'>='},
           {'value':'bw','text':'empieza por'},
           {'value':'bn','text':'no empieza por'},
           {'value':'ew','text':'termina por'},
           {'value':'en','text':'no termina por'},
           {'value':'cn','text':'contiene'},
           {'value':'nc','text':'no contiene'}];
        $('#comparadorCabecera option').remove();
        $('#comparadorCabecera').html('');
        switch(tipoSeleccionado){
            case 'string': 
                $.each(operadoresString, function(i){
                    $('#comparadorCabecera').append($("<option></option>").attr("value",operadoresString[i]['value']).text(operadoresString[i]['text']));
                });
                break;
                 "mike","joe","larry"
            case 'integer': case 'number': case 'datetime': case 'float': default:
                $.each(operadoresInteger, function(i){
                    $('#comparadorCabecera').append($("<option></option>").attr("value",operadoresInteger[i]['value']).text(operadoresInteger[i]['text']));
                });
                break;
        }
    };
    $("#campoCabecera").change( 
        function (event) {
            $().actualizarListaComparadores();
             $('#comparadorCabecera').trigger("liszt:updated");
        }
    );
    
    $(".btn-inverse").click( 
        function (event) {
            $("#div"+$(this).attr('id')).remove();
            if($('#filtros').children().length == 0){ 
                //$("#filtro").val("NO");
            }
        }
    );
    $("#cargarFiltro").click( 
        function (event) {
            event.preventDefault();
            if ($('#listaFiltros').val()!="0"){
                $.get('filtros/leerCondicionesFiltro/'+$('#listaFiltros').val(),
                    function(data){
                        var cond = jQuery.parseJSON(data);
                        if(cond[0].Filtro.tipo_condicion=="AND"){ $("#todasCondiciones").click();}
                        else { $("#algunaCondicion").click();}
                        for(var i=0;i< cond.length;i++){
                            pComparadorClave=cambiarComparador(cond[i].Condicion.comparador,"mostrar");
                            pintarCondiciones(cond[i].Condicion.campo,$('#campoCabecera option[value=' +cond[i].Condicion.campo+']').text(),pComparadorClave,cond[i].Condicion.contenido);
                        }
                    }
                );
            }else{
                $("#filterDialog > .modal-body" ).html("Seleccione el filtro a cargar!!");
                $("#filterDialog").modal();
            }
        }
    );
    $("#guardarFiltro").click( 
        function (event) {
            event.preventDefault();
            if($("#filtros").children().length>0){
                $("#divGuardarFiltro").modal();                
            }else{
                $("#filterDialog > .modal-body" ).html("Debe introducir al menos una condición para guardar el filtro!!");
                $("#filterDialog").modal();
            }
        }
    );
    $("#okGuardarFiltro").click(
        function (event) {
            event.preventDefault();
            var i=0;
            var j=0;
            parametros="data[Filtro][modelo]="+$('#modelo').val()+"&data[Filtro][titulo]="+$('#tituloFiltro').val()+"&data[Filtro][tipo_condicion]="+$('#tipoCondicion').val();
            for(i=0;i<$("#numeroCondiciones").val();i++){
                j=i+1;
                campo=$("#"+"campo"+j).val();
                comparador=$("#"+"comparador"+j).val();
                contenido=$("#"+"contenido"+j).val();
                if(campo){
                    parametros=parametros+"&data[Condicion]["+i+"][campo]="+campo+"&data[Condicion]["+i+"][comparador]="+comparador+"&data[Condicion]["+i+"][contenido]="+contenido;
                }
             }
             $.ajax({
                type :"post",
                url : "filtros/add/",
                data:parametros,
                async:false 
            });
            $(".popover-content").load("filtros/filtro/"+$('#modelo').val());
        }
    );

    $("#eliminarFiltro").click( 
        function (event) {
            event.preventDefault();
            if ($('#listaFiltros').val()!="0"){
                if(!$(this).hasClass('disabled')){
                    $("#eliminarFiltroDialog").modal();
                }
            }else{
                $("#filterDialog > .modal-body" ).html("Seleccione el filtro a eliminar!!");
                $("#filterDialog").modal();
            }
        }
    );
    $("#okEliminarFiltro").click(
        function (event) {
            event.preventDefault();
            var idFiltro=$('#listaFiltros').val();
            $('#listaFiltros option[value="'+idFiltro+'"]').remove();
            $.post("filtros/delete/",{'id':idFiltro});
            $("#filter").load("filtros/filtro/"+$('#modelo').val());
        }
    );
    
    
});