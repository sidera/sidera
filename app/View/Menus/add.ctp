<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a href="#"><i class="fa fa-cog fa-lg  "></i></a></li>
            <?php echo $this->element("ElementsVista".DS."Add".DS."botonGuardarAdd"); ?>
            <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
        </ul>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6">
            <i id="iconoModulo" class="fa fa-edit fa-lg"> </i> 
            <span id="etiquetaModulo"> Modificar Men� </span> 
        </div><!--columnaCabecera-->
    </div><!--cabeceraModulo--> 
    
    <div id="cuerpoModulo" class="container-fluid">
        <form action="/sidera/menus/create/<?=$registro["Menu"]["id"]?>" id="MenuAddForm" method="post" accept-charset="iso-8859-1">
            <div id="fila1" class="row-fluid">
                <div id="columna1" class="col-md-6 padded">

                    <div id="tituloDiv" class="form-group">
                        <label>T�tulo</label>
                        <input id="titulo" type="text" name="data[Menu][titulo]" class="form-control">
                    </div><!--tituloDiv-->
            
                    <div id="menu_idDiv" class="form-group">
                        <label>Men� Padre</label>
                        <select  id="MenuMenuId" name="data[Menu][menu_id]" class="chosen-select">
                            <option value=""> > </option>
                        <?php foreach ($menusPadre as $clave => $valor) {
                            echo "<option value='".$clave."' ".$selected.">".$valor."</option>";
                        }?>
                        </select>
                    </div><!--menu_idDiv-->
                    
                    <div id="controladorDiv" class="form-group">
                        <label>Controlador</label>
                        <input id="controlador" type="text" name="data[Menu][controlador]" class="form-control">
                    </div><!--controladorDiv-->
                    
                    <div id="accionDiv" class="form-group">
                        <label>Acci�n</label>
                        <input id="accion" type="text" name="data[Menu][accion]" class="form-control">
                    </div><!--accionDiv-->
                    
                    
                    <div id="ordenDiv" class="form-group">
                        <label>Orden</label>
                        <input id="orden" type="number" name="data[Menu][orden]" class="form-control" value="1">
                     </div><!--ordenDiv-->
                    
                    <div id="estadoDiv" class="form-group">
                        <label>Men� Activo</label>
                        <input type="hidden" name="data[Menu][estado]" value="Activo" onclick=" $(this).val()=='Inactivo' ? $(this).val('Activo') : $(this).val('Inactivo') "/>
                        <i class="form-control fa fa-lg fa-check-square-o" onclick="$(this).toggleClass('fa-square-o').siblings().trigger('click');"></i>
                    </div><!--estadoDiv-->
                    
                    <input id="MenuRuta" type="hidden" name="data[Menu][ruta]"/>
                        
                    <!--nuevosCampos-->
                </div><!--columna1-->
            </div><!--fila1-->  
        </form>
    </div><!--cuerpoModulo-->
    
    <div id="pieModulo" class="row-fluid hide">
        <div id="columnaPie" class="col-md-12 ">
        </div><!--columnaPie-->
    </div><!--pieModulo--> 
</div><!--modulo-->
<script type="text/javascript">
$(document).ready(function() {
    <?php echo $this->element("ElementsVista".DS."Add".DS."addJs"); ?>
    var options = {target:"#content"}; 
    var tablasSistema = new Array();
    $('#MenuAddForm').ajaxForm(options);
    $('#addMenu').click(function(event){
        event.preventDefault();
        existeTitulo = $.inArray($('#titulo').val(), tablasSistema);
        if( existeTitulo > -1 ){
                var mensajeErrorTitulo ='<label id="mensajeErrorTitulo" class="error"> T�tulo de men� reservado para el sistema, por favor, elija otro.</label>'
                if ($('#mensajeErrorTitulo')) $('#mensajeErrorTitulo').remove();
                jQuery(mensajeErrorTitulo).appendTo('.divTitulo');
        }else{
            if ($('#mensajeErrorTitulo')) $('#mensajeErrorTitulo').remove();
            textoTroceado = $("#MenuMenuId option:selected").text().split(">");
            numeroNiveles = textoTroceado.length;
            if((numeroNiveles>2)&&($("#MenuControlador").val().length==0)){
                var mensajeErrorControlador ='<label id="mensajeErrorControlador" class="error"> Complete el campo controlador.</label>'
                if ($('#mensajeErrorControlador')) $('#mensajeErrorControlador').remove();
                jQuery(mensajeErrorControlador).appendTo('.divControlador');
            }else{
                $('#MenuRuta').val($('#MenuMenuId option:selected').text()+">"+$('#titulo').val());
                $('#MenuAddForm').ajaxSubmit({async:false});	
                $('#cuerpoApp').load('menus/index');
            }
        }
    });
      
    $('#MenuMenuId').change(function() {
        $('#MenuRuta').val($('#MenuMenuId option:selected').text()+">"+$('#MenuTitulo').val());
    });
    
    $('#MenuTitulo').keyup(function (e) { 
        $('#MenuRuta').val($('#MenuMenuId option:selected').text()+">"+$('#MenuTitulo').val());
    });
    //<!--espacioFuturosControles-->
}); 
</script>
