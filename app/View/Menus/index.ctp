<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."Index".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a href="#"><i class="fa fa-cog fa-lg  "></i></a></li>
            <li><a onclick='$("#cuerpoApp").load("menus/add")'>Nuevo</a></li>
            <?=$this->element("ElementsVista".DS."Index".DS."botonEliminar")?>
            <li><a onclick='$("#cuerpoApp").load("menus/exportarMostrar")'>Exportar</a></li>
        </ul>
    </div>  
    <div id="columnaBusquedaBarraAccion" class="pull-right col-md-3 col-xs-8 ">
       <?=$this->element("ElementsVista".DS."Index".DS."searchForm",array("model"=>"menus"))?></li>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6">
            <i id="iconoModulo" class=" fa fa-align-justify fa-lg"></i> 
            <span id="etiquetaModulo"></span> 
        </div><!--columnaCabecera-->
        <div id="columnaPaginacion" class="col-md-6 pull-right">
            <?=$this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>"Menu"))?>
        </div><!--columnaPaginacion-->
    </div><!--cabeceraModulo--> 
 
    <div id="cuerpoModulo" class="fila row-fluid">  
        <div  class="columna dataTables_wrapper" role="grid" id="DataTables_Table_0_wrapper">
            <table class="elemento table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="check"><i class="fa fa-check-square fa-lg"></i></th>
                        <th data-header="id" class="orderable"><?=$this->Paginator->sort("id","Id")?></th>
                        <th data-header="titulo" class="orderable"><?=$this->Paginator->sort("titulo","T�tulo")?></th>
                        <th data-header="menu_id" class="orderable"><?=$this->Paginator->sort("menu_id","Men�")?></th>
                        <th data-header="estado" class="orderable"><?=$this->Paginator->sort("estado","Estado")?></th>
                        <th data-header="controlador" class="orderable"><?=$this->Paginator->sort("controlador","Controlador")?></th>
                        <th data-header="accion" class="orderable"><?=$this->Paginator->sort("accion","Acci�n")?></th>
                        <th data-header="orden" class="orderable"><?=$this->Paginator->sort("orden","Orden")?></th>
                        <th data-header="ruta" class="orderable"><?=$this->Paginator->sort("ruta","Ruta")?></th>                           
                        <!--nuevosCamposCabecera-->
                    </tr>
                </thead>
                <tbody class="griddata" id="MenuGridData">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?php echo $registro["Menu"]["id"];?>" class="gradeA" title="Haga clic para editar el registro"> 
                        <td><i value="<?=$registro["Menu"]["id"]?>" class="checkCell fa fa-lg fa-square-o"></i></td>
                        <td class="id"><?=number_format($registro["Menu"]["id"], 0, ",", ".")?></td>
                        <td class="titulo"><?=$registro["Menu"]["titulo"]?></td>
                        <td class="menu_id"><?=$registro["MenuParent"]["titulo"]?></td>
                        <td class="estado"><?=$registro["Menu"]["estado"]?></td>
                        <td class="controlador"><?=$registro["Menu"]["controlador"]?></td>
                        <td class="accion"><?=$registro["Menu"]["accion"]?></td>
                        <td class="orden"><?=$registro["Menu"]["orden"]?></td>
                        <td class="ruta"><?=$registro["Menu"]["ruta"]?></td>
                        <!--nuevosCamposCuerpo-->
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div id="pieModulo" class="row-fluid">
        <div id="columnaPie" class="col-md-8 ">
        </div><!--columnaPie-->
        
        <div id="columnaPaginacionPie" class="col-md-4 pull-right">
            <?=$this->element("ElementsVista".DS."Index".DS."registrosPorPagina")?>
        </div><!--columnaPaginacionPie-->     
    </div><!--pieModulo--> 
    
</div><!--modulo--> 
<script type="text/javascript" id="myScript">
$(document).ready(function() {
    <?=$this->element("ElementsVista".DS."Index".DS."indexJs")?>
});
</script>
