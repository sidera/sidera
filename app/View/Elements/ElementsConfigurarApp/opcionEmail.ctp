<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div class="tab-pane" id="opcionEmail">
    <div class="col-md-5">   
        <legend class="alert-info" id="labelDB">Parámetros de Correo Electrónico</legend>
        <div class="form-group">
            <label>Servidor</label>
            <input type="text" class="form-control" name="data[Configuracion][email_host]" value="<?=EMAIL_HOST?>" maxlength="256"  placeholder="smtp.sidera.es"/>
        </div> 
        
        <div class="form-group">
            <label>Puerto</label>
            <input type="text" class="form-control"  name="data[Configuracion][email_port]" value="<?=EMAIL_PORT?>" maxlength="256"  placeholder="25"/>
        </div> 
        
        <div class="form-group">
            <label>Transporte</label>
            <div class="input-group radio-form">
                <input name="data[Configuracion][email_transport]" id="email_transport" value="<?=EMAIL_TRANSPORT?>" type="hidden" aria-describedby="spanCodificacionBD">
                <i class="form-control fa fa-lg fa-circle-o <?php if (EMAIL_TRANSPORT=='smtp') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('smtp');"></i>
                <span class="input-group-addon" id="spanCodificacionBD"> Smtp </span>
                <i class="form-control fa fa-lg fa-circle-o <?php if (EMAIL_TRANSPORT=='mail') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('mail');"></i>
                <span class="input-group-addon" id="spanCodificacionBD">  Mail </span>
                <i class="form-control fa fa-lg fa-circle-o <?php if (EMAIL_TRANSPORT=='debug') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('debug');"></i>
                <span class="input-group-addon" id="spanCodificacionBD">  Debug </span>
            </div>
        </div>
        
        <div class="form-group">
            <label>Formato</label>
            <div class="input-group radio-form">
                <input name="data[Configuracion][email_format]" id="email_transport" value="<?=EMAIL_FORMAT?>" type="hidden" aria-describedby="spanEmailFormat">
                <i class="form-control fa fa-lg fa-circle-o <?php if (EMAIL_FORMAT=='html') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('html');"></i>
                <span class="input-group-addon" id="spanEmailFormat"> HTML </span>
                <i class="form-control fa fa-lg fa-circle-o <?php if (EMAIL_FORMAT=='texto') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('texto');"></i>
                <span class="input-group-addon" id="spanEmailFormat">  Texto </span>
            </div>
        </div>  
        
        <div class="form-group">
            <label>TLS</label>
            <input type="hidden" id="modo_mantenimiento" name="data[Configuracion][email_tls]" value="<?=EMAIL_TLS?>" onclick=" $(this).val()==0 ? $(this).val(1) : $(this).val(0) "/>
            <i class="form-control fa fa-lg fa-check-square-o <?php if (!EMAIL_TLS) echo "fa-square-o"?>" onclick="$(this).toggleClass('fa-square-o').siblings().trigger('click');"></i>
        </div>
    </div> 
    
    <div class="col-md-6 col-md-offset-1">   
        <legend class="alert-info" id="labelDB">Cuenta de Usuario</legend>
        <div class="form-group">
            <label>Usuario</label>
            <input type="text" class="form-control" name="data[Configuracion][email_user]" value="<?=EMAIL_USER?>" maxlength="256"  placeholder="nombre.apellido"/>
        </div> 
        
        <div class="form-group">
            <label>Contraseña</label>
            <input type="text" class="form-control" name="data[Configuracion][email_password]" value="<?=EMAIL_PASSWORD?>" maxlength="256"  placeholder="******" />
        </div> 
        
        <div class="form-group">
            <label>Desde</label>
            <input type="text" class="form-control" name="data[Configuracion][email_from]" value="<?=EMAIL_FROM?>" maxlength="256"  placeholder="origen@sidera.es" />
        </div> 
        
    </div> 
</div>      
	
		
	
<script type="text/javascript">
$(document).ready(function() {
	$("#transport").val('<?php echo EMAIL_TRANSPORT;?>');
	$("#format").val('<?php echo EMAIL_FORMAT;?>');
	$("#tls").val('<?php echo EMAIL_TLS;?>');
 });
</script>