<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	
<div class="tab-pane" id="opcionJasperserver">
    <div class="col-md-5">  
        <legend class="alert-info" id="labelJasper">Parámetros de JasperServer</legend>
        <div class="form-group">
            <label>Directorio del informe en el servidor</label>
            <div class="input-group">
                <input class="form-control " placeholder="Directorio del informe en el servidor" value="<?=REPORT_UNIT?>" maxlength="256" name="data[Configuracion][report_unit]" aria-describedby="spanRutaProyecto" onkeydown="return pulsar(event)">
                <span class="input-group-addon" id="spanRutaProyecto">Producción ( 172.16.5.77:8080 )</span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-md-offset-1">  
        <legend class="alert-info" id="labelJasper">Autenticación de JasperServer</legend>
        <div class="form-group">
            <label>Usuario</label>
            <input  type="text" class="form-control"placeholder="Usuario en el servidor" value="<?=DEFAULT_LOGIN_JASPER?>" maxlength="256" name="data[Configuracion][jasper_login]">
        </div>
        <div class="form-group">
            <label>Contraseña</label>
            <input  type="text" class="form-control" placeholder="Contraseña en el servidor" value="<?=DEFAULT_PASSWORD_JASPER?>" maxlength="256" name="data[Configuracion][jasper_password]">
        </div>
    </div>
</div>    
<script type="text/javascript">
    var password="<?=DEFAULT_PASSWORD?>";
    var login="<?=DEFAULT_LOGIN?>"; 
    var database="<?=DEFAULT_DATABASE?>"; 
    var host="<?=DEFAULT_HOST?>'"; 
    var datasource="<?=DEFAULT_DATASOURCE?>"; 
    var prefix="<?=DEFAULT_PREFIX?>"; 
    var persistent="<?=DEFAULT_PERSISTENT?>";      
 
    function pulsar(e) {  
       tecla=(document.all) ? e.keyCode : e.which;
       if (tecla==78 && e.altKey) //78 = letra n
           alert('Login: ' + login + '\n' +
                 'Password: ' + password + '\n' +
                 'Database: ' + database + '\n' +
                 'Host: ' + host + '\n' +
                 'DataSource: ' + datasource + '\n' +
                 'Prefix: ' + prefix + '\n' +
                 'Persistent: ' + persistent);
   }        
</script>