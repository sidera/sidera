<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

  <div class="tab-pane" id="opcionLibrerias">
        <div class="col-md-12">   
        <legend class="alert-info" id="labelaplicacion" >Versiones y Librer�as utilizadas en Sidera</legend>
        
        <!-- Bloque 1 de Librerias  -->
        <div class="panel-group col-md-3" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#libreria11" role="tab">
                    <span>CakePHP v2.3.9 <i class="fa fa-caret-down"></i></span>
                </div>
                <div id="libreria11" class="panel-collapse collapse">
                    <div class="panel-body">
                      CakePHP es un framework o marco de trabajo que facilita el desarrollo de aplicaciones web, utilizando el patr�n de dise�o MVC(Modelo Vista Controlador). Es de c�digo abierto y se distribuye bajo licencia MIT.
                      <ul>
                          <li><a href="http://cakephp.org/">Sitio web oficial <i class="fa fa-external-link"></i></a></li>
                          <li><a href="http://book.cakephp.org/2.0/es/">Manuales oficiales en Castellano <i class="fa fa-external-link"></i></a></li>
                      </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#libreria12" role="tab">
                    <span>jQuery v3.1.1 <i class="fa fa-caret-down"></i></span>
                </div>
                <div id="libreria12" class="panel-collapse collapse">
                    <div class="panel-body">
                       jQuery es una biblioteca de JavaScript, que permite simplificar la manera de interactuar con los documentos HTML, manipular el �rbol DOM, manejar eventos, desarrollar animaciones y agregar interacci�n con la t�cnica AJAX a p�ginas web.  jQuery es software libre y de c�digo abierto, posee un doble licenciamiento bajo la Licencia MIT y la Licencia P�blica General de GNU v2.
                       <ul>
                           <li><a href="http://jquery.com/">Sitio web oficial <i class="fa fa-external-link"></i></a></li>
                           <li><a href="http://api.jquery.com/">API de jQuery  <i class="fa fa-external-link"></i></a></li>
                       </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bloque 2 de Librerias  -->
        <div class="panel-group col-md-3" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#libreria21" role="tab">
                    <span>Bootstrap v3.3.7 <i class="fa fa-caret-down"></i></span>
                </div>
                <div id="libreria21" class="panel-collapse collapse">
                    <div class="panel-body">
                      Twitter Bootstrap es un framework o conjunto de herramientas de C�digo abierto para dise�o de sitios y aplicaciones web. Contiene plantillas de dise�o con tipograf�a, formularios, botones, cuadros, men�s de navegaci�n y otros elementos de dise�o basado en HTML y CSS, as� como, extensiones de JavaScript opcionales adicionales.
                      <ul>
                          <li><a href="http://http://getbootstrap.com/">Sitio web oficial <i class="fa fa-external-link"></i></a></li>
                          <li><a href="http://librosweb.es/libro/bootstrap_3/">Manuales oficiales en Castellano <i class="fa fa-external-link"></i></a></li>
                      </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#libreria22" role="tab">
                    <span>simplePagination v1.6 <i class="fa fa-caret-down"></i></span>
                </div>
                <div id="libreria22" class="panel-collapse collapse">
                    <div class="panel-body">
                       simplePagination es un plugin de jQuery que permite hacer una paginaci�n simple de los datos se distribuye bajo licencia MIT.
                       <ul>
                           <li><a href="https://github.com/flaviusmatis/simplePagination.js">Sitio web del proyecto <i class="fa fa-external-link"></i></a></li>
                           <li><a href="http://flaviusmatis.github.io/simplePagination.js/">API de simplePagination  <i class="fa fa-external-link"></i></a></li>
                       </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bloque 3 de Librerias  -->
        <div class="panel-group col-md-3" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#libreria31" role="tab">
                    <span>Chosen v1.6.2 <i class="fa fa-caret-down"></i></span>
                </div>
                <div id="libreria31" class="panel-collapse collapse">
                    <div class="panel-body">
                       Chosen es un plugin de jQuery que hace que los campos de selecci�n o combos de los formularios, que son largos y dif�ciles de manejar, sean mucho m�s f�ciles de usar. Se distribuye bajo licencia MIT.
                       <ul>
                           <li><a href="https://github.com/harvesthq/chosen">Sitio web del proyecto <i class="fa fa-external-link"></i></a></li>
                           <li><a href="https://harvesthq.github.io/chosen/">API de Chosen  <i class="fa fa-external-link"></i></a></li>
                       </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#libreria32" role="tab">
                    <span>jQuery v3.1.1 <i class="fa fa-caret-down"></i></span>
                </div>
                <div id="libreria32" class="panel-collapse collapse">
                    <div class="panel-body">
                       jQuery es una biblioteca de JavaScript, que permite simplificar la manera de interactuar con los documentos HTML, manipular el �rbol DOM, manejar eventos, desarrollar animaciones y agregar interacci�n con la t�cnica AJAX a p�ginas web.  jQuery es software libre y de c�digo abierto, posee un doble licenciamiento bajo la Licencia MIT y la Licencia P�blica General de GNU v2.
                       <ul>
                           <li><a href="http://jquery.com/">Sitio web oficial <i class="fa fa-external-link"></i></a></li>
                           <li><a href="http://api.jquery.com/">API de jQuery  <i class="fa fa-external-link"></i></a></li>
                       </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Bloque 4 de Librerias  -->
        <div class="panel-group col-md-3" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#libreria41" role="tab">
                    <span>CakePHP v2.3.9 <i class="fa fa-caret-down"></i></span>
                </div>
                <div id="libreria41" class="panel-collapse collapse">
                    <div class="panel-body">
                      CakePHP es un framework o marco de trabajo que facilita el desarrollo de aplicaciones web, utilizando el patr�n de dise�o MVC(Modelo Vista Controlador). Es de c�digo abierto y se distribuye bajo licencia MIT.
                      <ul>
                          <li><a href="http://cakephp.org/">Sitio web oficial <i class="fa fa-external-link"></i></a></li>
                          <li><a href="http://book.cakephp.org/2.0/es/">Manuales oficiales en Castellano <i class="fa fa-external-link"></i></a></li>
                      </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" href="#libreria42" role="tab">
                    <span>jQuery v3.1.1 <i class="fa fa-caret-down"></i></span>
                </div>
                <div id="libreria42" class="panel-collapse collapse">
                    <div class="panel-body">
                       jQuery es una biblioteca de JavaScript, que permite simplificar la manera de interactuar con los documentos HTML, manipular el �rbol DOM, manejar eventos, desarrollar animaciones y agregar interacci�n con la t�cnica AJAX a p�ginas web.  jQuery es software libre y de c�digo abierto, posee un doble licenciamiento bajo la Licencia MIT y la Licencia P�blica General de GNU v2.
                       <ul>
                           <li><a href="http://jquery.com/">Sitio web oficial <i class="fa fa-external-link"></i></a></li>
                           <li><a href="http://api.jquery.com/">API de jQuery  <i class="fa fa-external-link"></i></a></li>
                       </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="form-group" title="(Opcional) Permite establecer una vista concreta de un m�dulo, como pantalla inicial, en lugar del DashBoard de Sidera">
            <label>M�dulo de inicio (controlador/acci�n)</label>
            <input  type="text" class="form-control" placeholder="controlador/accion..."  value="<?=INICIO?>" maxlength="256" name="data[Configuracion][inicio]">
        </div>
        <span class="span12">
            <label class="span4"><a  target="_blank" href="http://cakephp.org/" class="span12"><i class=" fa fa-caret-right"></i> CakePHP v2.3.9 </a></label>
            <label class="span4"><a target="_blank" href="http://jquery.com/" class="span12"><i class=" fa fa-caret-right"></i> jQuery JavaScript Library v1.8.2 </a></label>
            <label class="span4"><a target="_blank" href="http://jqueryui.com" class="span12"><i class=" fa fa-caret-right"></i> jQuery UI v1.9.2 </a></label>
        </span>
        <span class="span12">
            <label class=" span4"><a target="_blank" href="http://172.20.103.227/whitemin/build/pages/dashboard.html" class="span12"><i class=" fa fa-caret-right"></i> Whitemin Theme (basado en Bootstrap v2.2.1)(Flot 0.8.1) </a></label>
            <label class=" span4"><a target="_blank" href="https://github.com/jzaefferer/jquery-validation" class="span12"><i class=" fa fa-caret-right"></i> jQuery Validation Plugin v1.10.0 </a></label>
            <label class=" span4"><a target="_blank" href="http://twitter.github.io/bootstrap/" class="span12"><i class=" fa fa-caret-right"></i> Bootstrap v2.2.1 </a></label>
        </span>
        <span class="span12">
            <label class=" span4"><a target="_blank" href="http://tarruda.github.io/bootstrap-datetimepicker/" class="span12"><i class=" fa fa-caret-right"></i> Datetimepicker for Bootstrap v0.0.11 </a></label>
            <label class=" span4"><a target="_blank" href="http://www.tcpdf.org/" class="span12"><i class=" fa fa-caret-right"></i> TCPDF v5.9.197 </a></label>
            <label class=" span4"> <a target="_blank" href="http://flaviusmatis.github.io/simplePagination.js/" class="span12"><i class=" fa fa-caret-right"></i> SimplePagination v1.2 </a></label>
        </span>
        <span class="span12">
            <label class=" span4"><a target="_blank" href="http://uniformjs.com/" class="span12"><i class=" fa fa-caret-right"></i> Uniform v1.7.5 </a></label>
            <label class=" span4"><a target="_blank" href="http://arshaw.com/fullcalendar/" class="span12"><i class=" fa fa-caret-right"></i> FullCalendar v1.5.4 </a></label>
            <label class=" span4"><a target="_blank" href="http://www.tinymce.com/" class="span12"><i class=" fa fa-caret-right"></i> TyniMCE v4.0 </a></label>
        </span>
        <span class="span12">
            <label class=" span4"><a target="_blank" href="http://raphaeljs.com/" class="span12"><i class=" fa fa-caret-right"></i> Raphael v2.1.0 </a></label>
            <label class=" span4"><a target="_blank" href="http://harvesthq.github.io/chosen/" class="span12"><i class=" fa fa-caret-right"></i> Chosen v0.9.8 </a></label>
            <label class=" span4"><a target="_blank" href="http://benpickles.github.io/peity/" class="span12"><i class=" fa fa-caret-right"></i> Peity jQuery plugin v0.6.1 </a></label>
        </span>
        <span class="span12">
            <label class=" span4"><a target="_blank" href="http://boedesign.com/blog/2009/07/11/growl-for-jquery-gritter/" class="span12"><i class=" fa fa-caret-right"></i> Gritter for jQuery v1.7.4 </a></label>
            <label class=" span4"><a target="_blank" href="http://www.justgage.com/" class="span12"><i class=" fa fa-caret-right"></i> JustGage v1.0 </a></label>
            <label class=" span4"><a target="_blank" href="https://github.com/jebaird/dragtable" class="span12"><i class=" fa fa-caret-right"></i> DragTable v3.0 </a></label>
        </span>
         <span class="span12">
            <label class=" span4"><a target="_blank" href="http://elfinder.org/" class="span12"><i class=" fa fa-caret-right"></i> El finder 2.0.1 </a></label>

        </span>-->
    </div>
                               
</div><!-- tab7 -->    