<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

  <div class="tab-pane active" id="opcionParametrosApp">
    <div class="col-md-5">   
        <legend class="alert-info" id="labelaplicacion" >Parámetros de la Aplicación</legend>
        <div class="form-group">
            <label>Nombre de la aplicación</label>
            <input  type="text" class="form-control" placeholder="Nombre de la aplicación..."  value="<?=APLICACION?>" maxlength="256" name="data[Configuracion][aplicacion]">
        </div>
        <div class="form-group">
            <label>Versión de la aplicación</label>
            <input  type="text" class="form-control" placeholder="Versión de la aplicación..."  value="<?=VERSION_MIAPP?>" maxlength="256" name="data[Configuracion][version_miapp]">
        </div>
        <div class="form-group">
            <label>Nombre del organismo</label>
            <input  type="text" class="form-control" placeholder="Organismo..." value="<?=ORGANISMO?>" maxlength="256" name="data[Configuracion][organismo]">
        </div>
        
        <div class="form-group">
            <label>Nombre de la consejería</label>
            <input  type="text" class="form-control" placeholder="Consejería..." value="<?=CONSEJERIA?>" maxlength="256" name="data[Configuracion][consejeria]">
        </div>
    </div>
    <div class="col-md-6 col-md-offset-1">   
        <legend class="alert-info" id="labelDB">Parámetros de Conexión de Servidores</legend>
        <div class="form-group">
            <label>Tipo de protocolo</label>
            <div class="input-group radio-form">
                <input name="data[Configuracion][protocolo]" id="<?=PROTOCOLO?>" value="http" type="hidden" aria-describedby="spanProtocolo">
                <i class="form-control fa fa-lg fa-circle-o <?php if (PROTOCOLO=='http') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('http');"></i>
                <span class="input-group-addon" id="spanProtocolo"> http <i class="fa fa-caret-right"></i> ( Localhost y Desarrollo) </span>
                <i class="form-control fa fa-lg fa-circle-o <?php if (PROTOCOLO=='https') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('https');"></i>
                <span class="input-group-addon" id="spanProtocolo">  https <i class="fa fa-caret-right"></i> ( Preproducción y Producción) </span>
            </div>
        </div>   
        
        <div class="form-group">
            <label>Ruta del proyecto</label>
            <div class="input-group">
                <input class="form-control " value="<?=RUTA_PROYECTO?>" placeholder="Ruta del Proyecto..." aria-describedby="spanRutaProyecto" type="text" maxlength="256" name="data[Configuracion][ruta_proyecto]">
                <span class="input-group-addon" id="spanRutaProyecto">MiApp <i class="fa fa-caret-right"></i> ( Localhost ) </span>
                <span class="input-group-addon" id="spanRutaProyecto"> 00/MiApp <i class="fa fa-caret-right"></i> ( Desarrollo, Preproducción y Producción )</span>
            </div>
        </div>
    </div>  
</div><!-- tab1 -->
