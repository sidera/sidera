<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

<li>
    <a id="guardarEditRegistro" href="#">Guardar</a>
</li>
<script type="text/javascript">
	var options = {target:'#content'}; 
 	$('#<?php echo $modelo;?>EditForm').ajaxForm(options);
    $('#guardarEditRegistro').click(function(event){
        event.preventDefault();
        //if ($("#<?php echo $modelo;?>EditForm").valid()){
            // $("#<?php echo $modelo;?>EditForm").ajaxSubmit();
            $("#<?php echo $modelo;?>EditForm").ajaxSubmit({success:function(data){ location.reload();}});
            
              
        //}
    });
</script>
