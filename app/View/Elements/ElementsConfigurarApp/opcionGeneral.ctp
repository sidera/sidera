<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

<div class="tab-pane" id="opcionGeneral">
    <div class="col-md-5">   
        <legend class="alert-info" id="labelDB">Administraci�n del Sistema</legend>
        <div class="form-group" title="Cambia el estado de la aplicaci�n a modo mantemiento. S�lo puede acceder el administrador del sistema.">
            <label>Modo mantenimiento</label>
            <input type="hidden" id="modo_mantenimiento" name="data[Configuracion][modo_mantenimiento]" value="<?=MODO_MANTENIMIENTO?>" onclick=" $(this).val()==0 ? $(this).val(1) : $(this).val(0) "/>
            <i class="form-control fa fa-lg fa-check-square-o <?php if (!MODO_MANTENIMIENTO) echo "fa-square-o"?>" onclick="$(this).toggleClass('fa-square-o').siblings().trigger('click');"></i>
        </div>
        
        <div class="form-group" title="Permite registrar todos los accesos e intentos de acceso al sistema">
            <label>Auditor�a de acceso</label>
            <input type="hidden" id="auditoria_acceso" name="data[Configuracion][auditoria_acceso]" value="<?=AUDITORIA_ACCESO?>" onclick=" $(this).val()==0 ? $(this).val(1) : $(this).val(0) "/>
            <i class="form-control fa fa-lg fa-check-square-o <?php if (!AUDITORIA_ACCESO) echo "fa-square-o"?>" onclick="$(this).toggleClass('fa-square-o').siblings().trigger('click');"></i>
        </div>
        
        <div class="form-group" title="Permite administrar los men�s del sistema en el m�dulo Men�s.">
            <label>Gestionar men�s del sistema.</label>
            <input type="hidden" id="mostrar_menus_sistema" name="data[Configuracion][mostrar_menus_sistema]" value="<?=MOSTRAR_MENUSSISTEMA?>" onclick=" $(this).val()==0 ? $(this).val(1) : $(this).val(0) "/>
            <i class="form-control fa fa-lg fa-check-square-o <?php if (!MOSTRAR_MENUSSISTEMA) echo "fa-square-o"?>" onclick="$(this).toggleClass('fa-square-o').siblings().trigger('click');"></i>
        </div>
    </div>
    <div class="col-md-6 col-md-offset-1 ">   
        <legend class="alert-info" id="labelaplicacion" >Configuraci�n del Sistema</legend>
        <div class="form-group" title="(Opcional) Permite establecer una vista concreta de un m�dulo, como pantalla inicial, en lugar del DashBoard de Sidera">
            <label>Pantalla de inicio (controlador/acci�n)</label>
            <input  type="text" class="form-control" placeholder="controlador/accion..."  value="<?=INICIO?>" maxlength="256" name="data[Configuracion][inicio]">
        </div>
        
        <div id="subnavbar" class="form-group" title="(Opcional)Muestra una barra de informaci�n/navegaci�n por debajo de la caberea de la aplicacion">
            <label>Barra de informaci�n/navegaci�n adicional</label>
            <input type="hidden" name="data[Configuracion][subnavbar]" value="<?=SUBNAVBAR?>" onclick=" $(this).val()==0 ? $(this).val(1) : $(this).val(0) "/>
            <i class="form-control fa fa-lg fa-check-square-o <?php if (!SUBNAVBAR) echo "fa-square-o"?>" onclick="$(this).toggleClass('fa-square-o').siblings().trigger('click');"></i>
        </div>
    </div> 
    
</div>