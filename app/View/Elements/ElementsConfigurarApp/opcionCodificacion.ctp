<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

<div class="tab-pane" id="opcionCodificacion">
      <div class="col-md-5">   
        <legend class="alert-info" id="labelDB">Parámetros de Codificación</legend>
        <div class="form-group">
            <label>Codificación de BD</label>
            <div class="input-group radio-form">
                <input name="data[Configuracion][codificacion_bd]" id="codificacion_bd" value="<?=CODIFICACION_BD?>" type="hidden" aria-describedby="spanCodificacionBD">
                <i class="form-control fa fa-lg fa-circle-o <?php if (CODIFICACION_BD=='ISO-8859-1') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('ISO-8859-1');"></i>
                <span class="input-group-addon" id="spanCodificacionBD"> ISO-8859-1 </span>
                <i class="form-control fa fa-lg fa-circle-o <?php if (CODIFICACION_BD=='UTF-8') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('UTF-8');"></i>
                <span class="input-group-addon" id="spanCodificacionBD">  UTF-8 </span>
            </div>
        </div>  
        
        <div class="form-group">
            <label>Codificación de archivos</label>
            <div class="input-group radio-form">
                <input name="data[Configuracion][codificacion_files]" id="codificacion_files" value="<?=CODIFICACION_FILES?>" type="hidden" aria-describedby="spanCodificacionArchivos">
                <i class="form-control fa fa-lg fa-circle-o <?php if (CODIFICACION_FILES=='ISO-8859-1') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('ISO-8859-1');"></i>
                <span class="input-group-addon" id="spanCodificacionArchivos"> ISO-8859-1 </span>
                <i class="form-control fa fa-lg fa-circle-o <?php if (CODIFICACION_FILES=='UTF-8') echo "fa-dot-circle-o"?>" onclick="$(this).addClass('fa-dot-circle-o').siblings('i').removeClass('fa-dot-circle-o').siblings('input').val('UTF-8');"></i>
                <span class="input-group-addon" id="spanCodificacionArchivos">  UTF-8 </span>
            </div>
        </div>
    </div>
</div>