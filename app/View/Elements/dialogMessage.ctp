<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div class="modal ventana-combo-modal hide fade <?php if(isset($type)) echo $type;?>" tabindex="-1" role="dialog" id="<?php echo $dialogId;?>" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo $header;?></h3>
    </div>
    <div class="modal-body" id="dialogBody<?php echo $dialogId;?>">
        <?php echo $body;?>
    </div>
    <div class="modal-footer">
        <?php if(!isset($cancelButton) || (isset($cancelButton) && $cancelButton!=null)){ ?>
        <button type="button" class="btn btn-default" id="dialogMessageCancel<?php echo $dialogId;?>">
            <?php if(isset($cancelLabel)){ echo $cancelLabel; }else{ echo 'Cancelar';}?>
        </button>
        <?php } ?>
        <?php if(!isset($okButton) || (isset($okButton) && $okButton!=null)){ ?>
        <button type="button" class="btn btn-small dark-blue " id="dialogMessageOk<?php echo $dialogId;?>">
        <?php if(isset($actionLabel)){ echo $actionLabel; }else{ echo 'Aceptar';}?>
        </button>
        <?php } ?>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {  
    <?php if(isset($launcherId)){ ?>
            
    $("#<?php echo $launcherId;?>").click(
        function (event) {
            $('#<?php echo $dialogId;?>').modal({keyboard: false});
            $('.main').remove();
        }
    );
    <?php } ?>
    $("#dialogMessageCancel<?php echo $dialogId;?>").on("click", 
        function (event) {
            $('#<?php echo $dialogId;?>').modal('hide');
        }
    );
    <?php if(isset($okButton)){ ?>
    $("#dialogMessageOk<?php echo $dialogId;?>").click(
        function (event) {
            event.preventDefault();
            <?php if(isset($action)): ?>
            $("#cuerpoApp").load('<?php echo $action;?>');
            <?php endif;?>
            <?php if(isset($customAction)): ?>
            $().<?php echo $customAction;?>();
            <?php endif;?>
            $('#<?php echo $dialogId;?>').modal('hide');
        }
    );
    <?php } ?>
    <?php if(isset($url)) { ?>
    $("#dialogBody<?php echo $dialogId;?>").load('<?php echo $url;?>/formType:modal');
    <?php } ?>
    
    jQuery.fn.extend({
        dialogCallback<?php echo $modelo;?>: function() {
            <?php if(isset($callback)) { ?>
            $('#cuerpoApp').<?php echo $callback;?>();
            <?php } ?>
        }
    });
    
     <?php if(isset($urlEvent) && isset($launcherSelector)  && isset($dialogId) ) { ?>
           $("<?php echo $launcherSelector;?>").<?php echo $launcherEvent;?>(
                function (event) {
                    $('#<?php echo $dialogId;?>').modal({keyboard: false});
                    $('.main').remove();
                    $("#dialogBody<?php echo $dialogId;?>").load('<?php echo $urlEvent;?>',{ <?php echo $urlparameters ?>});
                }
            );
    <?php } ?>
    
})

</script>
