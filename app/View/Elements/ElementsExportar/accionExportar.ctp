<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <script type="text/javascript">
 	

<?php  
$nombresModelos=array_keys($modelos);
foreach ($nombresModelos as $modelo): ?> 

//Acciones sobre los checkbox de selecci�n m�ltiple
$(".<?php echo $modelo;?>th").bind("click", 
        function (event) {
            event.stopPropagation();
            
            if ($('.<?php echo $modelo;?>td').is(':checked')){
           	 	$('.<?php echo $modelo;?>td').removeAttr('checked');
           	 	$('.<?php echo $modelo;?>td').parent().removeClass('checked');
                        
                        $("li[id*=<?php echo $modelo;?>-]").each(function(index) {
                                      $(this).remove();
                        });
            }else{//checkear

            	$('.<?php echo $modelo;?>td').attr('checked',true);
                $('.<?php echo $modelo;?>td').parent().addClass('checked');
                
                $('.<?php echo $modelo;?>td').each(function(index) {
                                         
                
            
                
                 var $li = $('<li  id="'+$(this).attr('id')+'" class="ui-state-default elementoListaExportar">'+
                        '<i class="fa fa-resize-vertical iconoElementoExportar"/>'+
                        '<div class="btn-group exportar" data-toggle="buttons-radio" id="'+$(this).attr('id')+ '"name='+$(this).attr('id')+'>'+
                                '<button id="'+$(this).attr('id')+ '"class="botonExportar ascendente badge badge-izquierda btn btn-mini dark-blue" type="button" value="ASC">ASC'+
                                '<button id="'+$(this).attr('id')+ '"class="botonExportar descendente badge badge-derecha btn btn-mini light-blue inverse" type="button" value="DESC">DESC'+
                        '</div>'+
                       
                        '<?php echo $modelo;?>'+'.'+$(this).val()+                                              
                        '<input type="hidden" class="inputExportar" value='+$(this).attr("id")+' name='+$(this).attr("id")+'> ' +                  
                        '<input type="hidden" class="inputExportarO" value="ASC" name="data['+$(this).attr('id')+']"'+'> ' +  
                    '</li>');   
                          
                      $($li).appendTo($('.ordenCampos')); 
                 });
            	
                          
                
            }
        }
    );
  <?php endforeach; ?>     


 //Acciones sobre los checkbox de selecci�n individual
    $(':checkbox').each(function(){
      $(this).click(function(e){
          if($(this).attr('checked')){
                  //var $li = $('<li  id="'+$(this).attr('id')+'" class="ui-state-default elementoListaExportar">'+$(this).attr('class').substring(0, $(this).attr('class').length-2)+'.'+$(this).val()+'</li>');  
                   var $li = $('<li  id="'+$(this).attr('id')+'" class="ui-state-default elementoListaExportar">'+
                        '<i class="fa fa-resize-vertical iconoElementoExportar"/>'+
                        '<div class="btn-group exportar" data-toggle="buttons-radio" id="'+$(this).attr('id')+ '"name='+$(this).attr('id')+'>'+
                                '<button id="'+$(this).attr('id')+ '"class="botonExportar ascendente badge badge-izquierda btn btn-mini dark-blue" type="button" value="ASC">ASC'+
                                '<button id="'+$(this).attr('id')+ '"class="botonExportar descendente badge badge-derecha btn btn-mini light-blue inverse" type="button" value="DESC">DESC'+
                        '</div>'+
                       
                        $(this).attr('class').substring(0, $(this).attr('class').length-2)+'.'+$(this).val()+                                              
                        '<input type="hidden" class="inputExportar" value='+$(this).attr("id")+' name='+$(this).attr("id")+'> ' +                  
                        '<input type="hidden" class="inputExportarO" value="ASC" name="data['+$(this).attr('id')+']"'+'> ' +  
                    '</li>');   
                  $($li).appendTo($('.ordenCampos')); 
         }else{
             $('li#'+$(this).attr('id')).remove();
          }    
      });
 });
 
$(document).ready(function() {
  //Elecci�n de tipo de orden
  $(".botonExportar").on("click", 
        function (event) {
            
         $(this).addClass("dark-blue")
                 .removeClass("light-blue")
                 .removeClass("inverse");
                                          
          if ($(this).hasClass('ascendente')){
              $("#"+$(this).attr('id')+' [class~=descendente]').addClass("light-blue")
                                                           .addClass("inverse")
                                                           .removeClass("dark-blue");                                                            
          }else{
                $("#"+$(this).attr('id')+' [class~=ascendente]').addClass("light-blue")
                                                           .addClass("inverse")
                                                           .removeClass("dark-blue");  
          }    
                 
            $("#"+$(this).attr('id')+".elementoListaExportar>.inputExportarO").val($(this).attr('value'));
        
        
        }
    );
}); 
</script>

