<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
$('#filtrar').click(function (event) {
    event.stopPropagation();
    $('#filter').show().transition({ x:'300px' });    
});
$('.btnFiltroClose').on('click',function(event){
    if($('#icono-boton-ocultar').hasClass('oculto')){
         $('#filter').transition({ x:'-323px' }).hide();
    }else{ 
         $('#filter').transition({ x:'-504px' }).hide();
    }
});