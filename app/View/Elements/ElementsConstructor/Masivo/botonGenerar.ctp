<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

    				<button class="btn btn-default" id="generarModulos">
                    <i class="fa fa-plus-circle  "></i> Generar M�dulos</button>
                    
<script type="text/javascript">
	var options = {target:'#content'}; 
 	$('#<?php echo $modelo;?>IndexMasivoForm').ajaxForm(options);
    $('#generarModulos').click(function(event){
        event.preventDefault();
        
    $("#<?php echo $modelo;?>IndexMasivoForm").ajaxSubmit({success:function(data){$('#cuerpoApp').html(data);}});
    });
</script>
