<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

                 <button class="btn btn-default" id="botonBorrarMensajeRecibido">
                    <i class="fa fa-trash  "></i> Borrar Mensaje</button>
                    
<?php echo $this->element("dialog",array(
    'header'  =>ucwords($controlador),
    'body'    =>'�Realmente desea eliminar este Mensaje?',
    'dialogId' => 'registroDialog'
));?>
<script type="text/javascript">
 	$('#botonBorrarMensajeRecibido').click(
	 	function(event){
	        event.preventDefault();
	        $('#registroDialog').modal('toggle');
	    }
    );
    $("#dialogButtonOk").click( 
        function (event) {
            event.preventDefault();
            $('#registroDialog').modal('hide');
            $.post("<?php echo $controlador;?>"+"/delete/",{'id':"<?php echo $this->request->data[$modelo]['id'];?>"});
            $('#cuerpoApp').load("<?php echo $controlador;?>"+'/recibidos');
        }
    );
</script>