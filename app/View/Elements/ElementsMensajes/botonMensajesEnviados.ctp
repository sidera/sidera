<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php //(actualizado en Sidera 1.0.6)  ?>  
<button class="btn btn-default" id="botonMensajesEnviados">
                    <i class="fa fa-upload-alt  "></i> Mensajes Enviados</button>
                    
 
<script type="text/javascript">
 	$("#botonMensajesEnviados").click(
	 	function(event){
	        event.preventDefault();
            event.stopPropagation();
	        $('#cuerpoApp').load("<?php echo $controlador;?>"+'/enviados');
	    }
    );
</script>