<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="btn-mini pull-right" >  
    <?php echo $this->Form->input("registrosPorPagina",array(
        "id"=>"registrosPorPagina",
        "class"=>"chzn-select",
        "label"=>false,
        "default"=>$parametrosModulo['registros_pagina'],
         "options"=>array('10'=>'10','1'=>'1','2'=>'2','10'=>'10','25'=>'25','50'=>'50','75'=>'75','100'=>'100')  
    ));?>
</div>


<script type="text/javascript" id="myScript">
//<![CDATA[
$(document).ready(function() { 
        $(".chzn-select").chosen();
        var options = {target:"#content"};
	$('#registrosPorPagina').change(function(event) {
                 event.stopPropagation();
                 $.post("<?php echo $controlador;?>"+"/registrosPorPagina",{ page: $(this).val()});
                 $('#cuerpoApp').load("<?php echo $controlador;?>"+'/recibidos/');
        });
});
//]]>
</script>