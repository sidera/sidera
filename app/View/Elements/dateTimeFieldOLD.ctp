<div id="<?php echo $campo.ucfirst($type);?>" class="input-prepend <?php echo $accion;?>" title="Doble click para la eliminar">
    <label><?php echo $label;?></label>
    <button type="button" class="btn light-blue inverse date-button add-on datetimepicker-button">
        <i class="icon-time"></i>
    </button>
    <input 	id="<?php echo $campo;?>"
    		type="text" 
    		ondblclick="$(this).val('')" 
                <?php if(!isset($readonly) || ($readonly==true)){
                   echo "readonly='readonly'";
                }?> 
    		class="input-small center datepicker"
           	name="data[<?php echo $modelo?>][<?php echo $campo;?>]"
           <?php if(isset($this->request->data[$modelo][$campo])){
                $format = "d/m/Y"; 
                if($type === "date"  ||$type === "dateFrom" ||$type === "dateTo" ){
                    $format = "d/m/Y"; 
                }else if($type === "dateTime" ){
                    $format = "d/m/Y h:i:s"; 
                }else if($type === "time" ){
                    $format = "h:i:s"; 
                }
           ?>
           value="<?php echo $this->Time->format($format,$this->request->data[$modelo][$campo]);?>"
           <?php } ?>
    />
</div>

<script type="text/javascript">
$(document).ready(function(){
   <?php  if($type == "date"){
               echo "$('#".$campo.ucfirst($type)."').datetimepicker({pickTime: false,format:'dd/MM/yyyy', weekStart:1});\n";
        }else if($type == "dateTime"){
              echo "$('#".$campo.ucfirst($type)."').datetimepicker({format:'dd/MM/yyyy hh:mm:ss', weekStart:1});\n";
        }else if ($type=="time"){
              echo "$('#".$campo.ucfirst($type)."').datetimepicker({pickDate: false,format:'hh:mm:ss'});\n";
        }else if($type == "dateFrom"){
             echo "$('#".$campo.ucfirst($type)."').datetimepicker({pickTime: false,format:'dd/MM/yyyy'})
                 .on('changeDate',function (e) {
                        $('#".$dateTo."DateTo').datetimepicker('setStartDate', new Date(e.date));
                    })
                .on('show',function (e) {
                        if($('#".$dateTo."').val().length>0) 
                            $('#".$campo.ucfirst($type)."').datetimepicker('setEndDate',  $('#".$dateTo."DateTo').data('datetimepicker').getDate());
                    });";
 
        }else if($type == "dateTo"){
              echo "$('#".$campo.ucfirst($type)."').datetimepicker({pickTime: false,format:'dd/MM/yyyy'})
                 .on('changeDate',function (e) {
                        $('#".$dateFrom."DateFrom').datetimepicker('setEndDate', new Date(e.date));
                    })
                .on('show',function (e) {
                    if($('#".$dateFrom."').val().length>0)
                        $('#".$campo.ucfirst($type)."').datetimepicker('setStartDate',  $('#".$dateFrom."DateFrom').data('datetimepicker').getDate());
                    });";
        }
 ?>
    
});    
</script>    