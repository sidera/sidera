<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="modal hide fade" tabindex="-1" role="dialog" id="mainDialog">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="dialogTitle"></h3>
    </div>
    <div class="modal-body">
        <p id='dialogBody'></p>
    </div>
    <div class="modal-footer">
        <button class="btn" id="mainDialogBtnClose">Cerrar</button>
    </div>
</div>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {        
    $("#cuerpoAppDialogBtnClose").bind("click", 
        function (event) {
            $('#dialogTitle').html("");
            $('#dialogBody').html("");
            $('#cuerpoAppDialog').modal('hide');
        }
    );
});
//]]>
</script>
