<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<?php $this->Menu->pintaMenu($usuario,$acl,$recargarMenuDinamico);?>

<script type="text/javascript">
$(document).ready(function() {  
    $('.dropdown-toggle').dropdown();
    
    $('.navbar-toggle').on("click", function(e){
        $( $(this).data("target") ).toggle();
    });
    
    $('.dropdown-submenu a').on("click", function(e){
        $('#menu_parte_superior li.dropdown-submenu ul').css("display",'none');
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });
    
    $("#menu_parte_superior  a").on("click", 
        function (event) {
            event.preventDefault();
            //$("#filtro").val("NO");
            if($(this).attr('href')!='#'){
                $('#menu_parte_superior li').removeClass('open').removeClass('active');
                $(this).parents('ul.nav>li').addClass('active');
                $("#cuerpoApp").load($(this).attr('href'));
            }
        }
    );
    
});
</script>