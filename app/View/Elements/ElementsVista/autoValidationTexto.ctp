$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(/[`~!@#$%^&*()|+\=?�;'\"<>\{\}\[\]\\\/]/);
            return this.optional(element) || !re.test(value);
        },
        "No se admiten caracteres especiales,solo alfanum�ricos"
);
<?php 

foreach($propiedades as $propiedad){
	$nombreCampo = $propiedad['name'];  
	$tipoCampo   = $propiedad['type'];
    //A�ado una funcion para los tipo string para que no puedan introducir caracteres especiales
    //$this->element("ElementsVista".DS."autoValidationTexto",array("novalidar"=>array("nombreCampo","nombreCampo2")));
    //Esta sentencia de arriba es para que no valide los campos que tu le indique y que sean string o text,se hace tanto en el add como en el edit del modelo
    if (($tipoCampo == "string" || $tipoCampo == "text") && ($configModulo[strtolower($accion)]["campos"][$nombreCampo]["mostrar"] == ACTIVO) && (!in_array ($nombreCampo,$novalidar))) {
        //A�ado una funcion para los tipo string para que no puedan introducir caracteres especiales
    ?>
      $("#<?php echo $propiedad['name'];?>").rules("add", { regex: "" });  
        
    <?php }
    
}	
?>
