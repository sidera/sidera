<?php if(!isset($bFilter)){	$bFilter = "true";}?>         
<?php if(count($rows)>0){ ?>

<?php if(count($rows)<=10)$bPaginate="false"; else $bPaginate="true";?>
<div class="listaconsulta">
	<table id="<?php echo $id;?>" class='table table-striped table-hover display'>
		<?php if(isset($caption)){ ?> 
        <caption><?php echo $caption;?> </caption>
		<?php }?> 
		<?php    
			//Cabeceras de la tabla
			$thead = "\t<thead><tr>";
			foreach ($rows[0] as $modeloNombre => $arrayCampos) {
				foreach ($arrayCampos as $campo => $valor) {
					if (!is_array($valor) && $campo != 'id') $thead .= '<th>'. ucwords($campo) . '</th>';
				}
			}
			$thead .= "</tr></thead>\n";
    		echo  $thead;
			// Filas de datos
			$tbody = "\t<tbody class='tbl_container'>";
			foreach ($rows as $row) { //registro con los modelos en cada fila
				$tbody .= "\t<tr>";  
				foreach ($row as $datos) {
					foreach ($datos as $campo => $valor) {
						if (!is_array($valor) && $campo != 'id') $tbody .= '<td>'.  $valor . '</td>';
					}
				}
				$tbody .= "</tr>\n";     
			}
			echo $tbody;
		?>
	</table>
</div>
<script type="text/javascript">
//Usa el plugin datable de Jquery (Incluido con Whitemin)    
$(document).ready(function(){
    var extensions = {
        "sFilter": "custom_filter_class"
    }
    // Used when bJQueryUI is false
    $.extend($.fn.dataTableExt.oStdClasses, extensions);
    // Used when bJQueryUI is true
    $.extend($.fn.dataTableExt.oJUIClasses, extensions);
       
    var oTable<?php echo $id;?> = $('<?php echo "#".$id;?>').dataTable( {
        "bAutoWidth": true,
        "bFilter": <?php echo $bFilter;?>,
        "bDestroy": true,
        "bPaginate": <?php echo $bPaginate;?>,
        "sPaginationType": "full_numbers",
          "sDom": '<"top"flp>rt<"bottom"i><"clear">',
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "�ltimo",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sInfo": "Mostrando de _START_ a _END_   (total _TOTAL_ registros)",
            "sInfoEmpty": "Mostrando 0 registros",   
            "sInfoFiltered": " de un total de _MAX_ ",
            "sEmptyTable": "No existen registros para mostrar.",
            "sSearch": "<i class='fa fa-search'></i> ",
            "sZeroRecords": "No existen registros para mostrar."
        },
        <?php if(isset($order)){?> 
        "aaSorting": [[ <?php echo $order[0];?>, "<?php echo $order[1];?>" ]],
        <?php }?>
    });     
});
</script>
<?php }else echo "<div style='height:400px;width:100%;'> No existen registros para mostrar</div>";?>