<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
$.validator.addMethod("time", function (value, element) {
    return this.optional(element) || /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(value);
}, "Por favor introduzca una hora correcta.");
$("#<?php echo $modelo.$accion;?>Form").validate({
    rules: {
<?php 
foreach($propiedades as $propiedad){
   $nombreCampo = $propiedad['name'];  
   $tipoCampo   = $propiedad['type'];
   
    $comienzoCampo = substr($nombreCampo,0,5);
    $terminacionCampo = substr($nombreCampo,$nombreCampo-3,strlen($nombreCampo));
    if ($comienzoCampo == 'bool_') $tipoCampo="boolean";
    if ($comienzoCampo == 'time_') $tipoCampo="time";

    echo "     \"data[".$modelo."][".$nombreCampo."]\":{\n";
    if (($propiedad['null']=="NO")&&($tipoCampo<>"boolean")){
        echo "          required: true,\n";
    }
    if (($tipoCampo == "string" || $tipoCampo == "text") && (!in_array($nombreCampo, array("dni", "cc_dc")))) {
        if ($tipoCampo == "text") {
            echo "          maxlength: 1000 \n";
        } else {
            if ($propiedad['length'] > 0) {
                echo "          maxlength: " . $propiedad['length'] . "\n";
            }
        }
    }
    
    echo "    },\n";
}
echo "  }\n";
echo "});\n";

foreach($propiedades as $propiedad){
	$nombreCampo = $propiedad['name'];  
	$tipoCampo   = $propiedad['type'];
	$comienzoCampo = substr($nombreCampo,0,5);
	$terminacionCampo = substr($nombreCampo,$nombreCampo-3,strlen($nombreCampo));
    if ($comienzoCampo == 'bool_') $tipoCampo="boolean";
    if (($propiedad['null']=="NO")&&($tipoCampo<>"boolean")){
    	echo "$('#".$nombreCampo."').attr('required', true);";
	}
    
    if ($nombreCampo=='dni'){?>
		$("#dni").rules("add", {
			maxlength: function(element) {
				$("#dni").val($("#dni").val().toUpperCase());
                var value = $("#dni").val();
                if(nie(value)) return 9; else return false;
            },
            messages: {
                maxlength:"DNI incorrecto."
    		}
	});
<?php }elseif ($nombreCampo=='cc_dc'){?>
		$("#cc_dc").rules("add", {
			maxlength: function(element) {
                var numeroBancoSucursal=$("#cc_banco").val()+$("#cc_sucursal").val();
                var numeroCuenta=$("#cc_cuenta").val();
                if(compruebaCuentaBancaria(numeroBancoSucursal,numeroCuenta)==$("#cc_dc").val()) return 2; else return false;
        },
        messages: {
			maxlength:"N�mero de cuenta bancaria incorrecta."
        }});
<?php } ?>
<?php 
        if ($tipoCampo == "float") {
            if ($propiedad['length']) {
                if (isset($propiedad['Length2'])) {
                    $campoFloat = explode(",", $propiedad['length']);
                    $entero = $propiedad['Length2'] - $campoFloat[1];
                    $decimal = ($campoFloat[1] == null) ? 0 : $campoFloat[1];
                    if($entero>23)$entero=12;
                } else {
                    if (isset($propiedad["precision"])) {//Oracle
                        $entero = $propiedad['precision'] - $propiedad['scale'];
                        $decimal = $propiedad['scale'];
                    } else {
                        $campoFloat = explode(",", $propiedad['length']);
                        $entero = $campoFloat[0] - $campoFloat[1];
                        $decimal = ($campoFloat[1] == null) ? 0 : $campoFloat[1];
                    }
                }
                $vMax = str_repeat('9', $entero) . '.' . str_repeat('9', $decimal);
                    echo "
                        $('#" . $nombreCampo . "').autoNumeric('init',{aSep:'.',aDec:',',aSign:'',pSign:'s',vMin:'-" . $vMax . "',vMax:'" . $vMax . "',wEmpty:'empty'});";
            }
            else{
               echo "
                    $('#" . $nombreCampo . "').autoNumeric('init',{aSep:'.',aDec:',',aSign:'',pSign:'s',vMin:'-999999',vMax:'999999',wEmpty:'empty'});"; 
            }
        }elseif( ($tipoCampo=="integer" || $tipoCampo=="biginteger") && $terminacionCampo != "_id" && $nombreCampo != "id" && $nombreCampo != "destino"  && $nombreCampo != "propietario"  && $nombreCampo != "origen"){
                if(isset($propiedad['Length2'])) {
                    if ($propiedad['Length2'] == 10 || $propiedad['Length2'] == 11) {
                        $vMax = 2147483648;
                    } else {
                        $vMax = str_repeat('9', $propiedad['Length2']);
                    }
                } else {
                        if ($propiedad['length'] == 10 || $propiedad['length'] == 11) {
                            $vMax = 2147483648;
                        } else {
                            $vMax = str_repeat('9', $propiedad['length']);
                        }
                }
            echo "
                    if(!$('#".$nombreCampo."').is('select')){
                                    $('#".$nombreCampo."').autoNumeric('init', {aSep: '.', aDec: ',',pSign: 's', mDec: '0',vMin:'-".$vMax."',vMax:'".$vMax."',wEmpty:'empty'});
                    }
            ";
			
	}
}
?>
//control de validacion de listas despleglables con class chzn-select
    $("select[id$='_id']").each(function( index ) {
        var idLista = $(this).attr('id');
        var nameLista = $(this).attr('name');
        $('[name="'+nameLista+'"]').css("position", "absolute").css("z-index","-9999").chosen().show();
        jQuery('[name="'+nameLista+'"]').appendTo('#'+idLista+'_chzn');
});