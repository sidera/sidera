<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div class="input-group">
        <input type="text" class="form-control" id="searchText"<?php if(isset($searchText)) echo "value='".$searchText."' readonly='readonly'";?>></input>
    <?php if(!isset($searchText)){ ?>
        <span type="button" class="input-group-addon fa fa-search btn-default" id="botonSearch" title="Haga clic para buscar. Use ',' para buscar m�s palabras"></span>
        <div class="input-group-btn" id="camposSearch">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa-caret-down fa"></i></button>
    <?php }else{?>
        <span type="button" class="input-group-addon fa fa-remove alert-warning btn-default" id="removeSearch" title="Haga clic para eliminar la b�squeda activa"></span>
        <div class="input-group-btn" id="camposSearch">
            <button type="button" class="btn alert-warning dropdown-toggle" data-toggle="" aria-haspopup="true" aria-expanded="false"> <i class="fa-caret-down fa"></i></button>
    <?php } ?>
                <ul id="listaCamposSearch" class="dropdown-menu dropdown-menu-right">
                    <li class="check">
                        <i class="fa fa-check-square fa-lg"> </i>
                        <label> Ninguno/Todos</label>
                    </li>
                    <li role="separator" class="divider"></li>
                </ul>
        </div>        
</div>                
<script type="text/javascript">
$(document).ready(function() {  
    //Crea la lista de campos
    $('th[data-header]').each(function(index) {
        $( "<li data-header="+$(this).data('header')+" onclick='$(this).children(\"i\").toggleClass(\"fa-square-o\")'><i class='fa fa-lg fa-check-square-o fa-square-o'> </i><label> "+$(this).children('a').text()+"</label></li>" ).appendTo( $( "ul#listaCamposSearch" ) );
    });
    //Checkea los campos por los que se ha buscado, en su defecto marca todos
    <?php 
    if(isset($fieldsForSearch)){
        foreach ($fieldsForSearch as  $fieldForSearch) {
            echo "$(\"#listaCamposSearch li[data-header='".$fieldForSearch."'] i\").removeClass('fa-square-o');";
        }
    }else{
        echo "$(\"#listaCamposSearch li:not(li.check) i\").removeClass('fa-square-o');";
    }    
    ?>
            
    //Obtiene los campos checkeados por el usuario
    jQuery.fn.getFields = function(page){
        var fields  = '';
        $("ul#listaCamposSearch > li.divider ~ li > i:not('.fa-square-o')").each(function(index) {
            field = $(this).parent('').data('header')+',';
            fields += field;
        });
        fields = 'searchFields:' + fields.substring(0,fields.length-1);
        return fields.replace(/\s/g, '');
    } 
    
    //Permite mostrar el menu de campos a buscar sin que se cierre 
    $('#listaCamposSearch').click(function(e){
        e.stopPropagation();
    });
    
    //Marca o desmarca todos los campos
    $("ul#listaCamposSearch > li.check").on("click",function (event) {
        if ($('ul#listaCamposSearch li:not(li.check) i.fa-square-o').length>0){
            $('ul#listaCamposSearch li:not(li.check) i').removeClass( "fa-square-o" );
        }else{
            $('ul#listaCamposSearch li:not(li.check) i').addClass( "fa-square-o" );
        }
    });
    
    $('#searchText').focus();
    
    //Botones de la busqueda REMOVE /SEARCH
    <?php if(isset($searchText)){ ?>
        $("#removeSearch").bind('click',function(event){ $("#cuerpoApp").load('<?php echo $controlador;?>/<?php echo $this->request->params['action'];?>/' + $().getFields());});
    <?php }else{?>
        $("#botonSearch").bind('click',function(event){
            //se envia si hay por lo menos un campo marcado
            if ($('ul#listaCamposSearch li:not(li.check) i:not(.fa-square-o)').length>0) {
                $().sendSearch();
            }
        });
    <?php } ?>
    
    jQuery.fn.sendSearch = function(href){
        if($('#searchText')) $('#searchText').val( $('#searchText').val().trim());
	if(href!=null){
		$("#cuerpoApp").load(href+'/');
	}else{
		$("#cuerpoApp").load('<?=$controlador;?>/<?=$this->request->params['action'];?>/'+$().getFilters() +'/' + $().getFields());
	}
    };
    
    //Controles teclas de prohibidas
    $("#searchText").bind('keypress', function(event) {if(event.which==13)$().sendSearch();});
    $('#searchText').keyup(function() {
        var cadena = $(this).val();
        cadena = cadena.replace(/[`~!@#$%^&*()|+\=?�;'",.<>\{\}\[\]\\\/]/gi,'');
        $(this).val(cadena);
      });
      
     
});
</script>