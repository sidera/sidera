<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

  
<li>
    <a id="delRegistro" data-toggle="modal" data-target="#registroDialog<?=$controlador?>" disabled href="#">Eliminar</a>
</li>

<script type="text/javascript">
if($('#registroDialog<?=$controlador?>').length<1){

    $('<div id="registroDialog<?=$controlador?>" class="modal fade " tabindex="-1" role="dialog" >\
      <div class="modal-dialog" role="document">\
        <div class="modal-content">\
          <div class="modal-header">\
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button>\
            <p class="modal-title"><?=ucfirst($controlador)?></p>\
          </div>\
          <div class="modal-body">\
            <p>�Realmente desea eliminar estos elementos?</p>\
          </div>\
          <div class="modal-footer">\
            <button type="button" class="btn btn-default" data-dismiss="modal" id="dialogButtonCancel">Cancelar</button>\
            <button type="button" class="btn btn-primary" id="dialogButtonOk<?=$controlador?>">Aceptar</button>\
          </div>\
        </div>\
      </div>\
    </div>').appendTo('#cuerpoApp');
                
    $("#delRegistro").click( 
        function (event) {
            $('#registroDialog<?=$controlador?>').on('show.bs.modal', function (event) {keyboard: false});
        }
    );
    $("#dialogButtonOk<?=$controlador?>").on('click', 
        function (event) {
            event.preventDefault();
            $('#registroDialog<?=$controlador?>').modal('hide');
            var items = $('.row-selected').map(function(){ return $(this).attr('itemid'); }).get().join(",");
            $.post("<?=$controlador?>"+"/delete/",{'id':items},function(data){$("#cuerpoApp").html(data);});
        }
    );
}    
    
</script>