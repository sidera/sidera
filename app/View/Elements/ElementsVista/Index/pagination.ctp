<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<?php
if (!isset($modules)) {
	$modulus = 2;
}
if (!isset($model)) {
	$models = ClassRegistry::keys();
	$model = Inflector::camelize(current($models));
}

if($this->params['paging'][$model]['pageCount']>1){
?>
<div class="pagination pull-right"></div>
<script type="text/javascript">
$(document).ready(function() {		
    //Inicialización de la paginación
    $(function() {
        $('.pagination').pagination({
            displayedPages:5,
            itemsOnPage:<?=$this->params['paging'][$model]['limit']?>,
            items:<?=$this->params['paging'][$model]['count']?>,
            currentPage:<?=$this->params['paging'][$model]['page']?>,
            edges:1,
            prevText:'Prev',
            nextText:'Sig',
            cssStyle: 'compact-theme',
            onPageClick:function(pageNumber, event) {
                $("#cuerpoModulo tbody").load('<?php
                    if (RUTA_PROYECTO!="")
                        echo "/".RUTA_PROYECTO."/".$controlador."/index/";
                    else
                        echo "/".$controlador."/index/";

                    ?>'+$().getFilters(pageNumber)+' #cuerpoModulo tbody tr');
            }
        });
    });
});
</script>
<?php }?>