var controlador="<?php echo $controlador;?>"; 
var modelo="<?php echo $modelo;?>";

$('#cuerpoModulo .selectpicker').selectpicker();
///Busqueda y Filtros
////campos a en los que hacer las busquedas

jQuery.fn.getFilters = function(page){
        var limit = "limit:"+ $("#registrosPorPagina option:selected").val();
	var filter  = '';
	if($('#searchText').html() != null){
		if($('#searchText').val() != ''){
			filter = 'searchText:'+escape($('#searchText').val())+'/';
		}
	}
	if('<?php echo $this->Paginator->sortKey();?>' != '') filter += 'sort:<?php echo $this->Paginator->sortKey();?>/direction:<?php echo $this->Paginator->sortDir();?>'+'/' + limit + '/';
	if(page){
		filter += 'page:'+page;
	}else{
		if('<?php echo $this->Paginator->current();?>' != '') filter += 'page:<?php echo $this->Paginator->current();?>';
	}
	return filter;
}

///Ordenar Columnas
$(".orderable").on("click", 
    function (event) {
        event.preventDefault();
        var href = $(this).children("a").attr("href");
        if(href!=null){
            $("#cuerpoApp").load(href+'/');
        }else{
            $("#cuerpoApp").load(href + $().getFilters());
        }
    }
);

///Accion Seleccionar Registro
$("td").on("click", 
    function (event) {
        var id=$(this).parent().attr('itemid');
        $(this).parent().children(':first-child').children().toggleClass( "fa-check-square-o" ).toggleClass( "fa-square-o" );
        $(this).parents('tr').toggleClass( "row-selected" );
        var numFilasSeleccionadas = $('.row-selected').length;
        $().enableEditRemoveButtons(numFilasSeleccionadas);
	}
);
$("th.check").on("click", 
    function (event) {
        if ($('.checkCell.fa-square-o').length>0){
            $('.checkCell').addClass( "fa-check-square-o" ).removeClass( "fa-square-o" );
            $('.checkCell').parents('tr').addClass( "row-selected" );
        }else{
            $('.checkCell').removeClass( "fa-check-square-o" ).addClass( "fa-square-o" );
            $('.checkCell').parents('tr').removeClass( "row-selected" );
        }
        var numFilasSeleccionadas = $('.row-selected').length;
        $().enableEditRemoveButtons(numFilasSeleccionadas);
	}
);

jQuery.fn.enableEditRemoveButtons = function(num){
    switch (num) {
        case (num = 0 ):
            $("#delRegistro").attr("disabled","disabled");
            $("#editRegistro").attr("disabled","disabled");
            break;
        case (num = 1):
            $("#delRegistro").removeAttr("disabled");
            $("#editRegistro").removeAttr("disabled");
            break;
        default:
            $("#delRegistro").removeAttr("disabled");
            $("#editRegistro").removeAttr("disabled");
    }
}
/*$('#barraAccion .navbar-toggle').on("click", function(e){
        $('#barraAccion .in').hide().removeClass('in');
    });*/
    

  
 
///Iconos al Seleccionar Columna
 $( "th a.asc" ).append( ' <i class="fa fa-caret-up"></i>' );
 $( "th a.desc" ).append( ' <i class="fa fa-caret-down"></i>' );
