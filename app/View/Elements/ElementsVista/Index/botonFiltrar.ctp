<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

    <button class="btn btn-small light-blue inverse" id="filtrar" title="Filtrar">
        <i class="fa fa-search  " id="iconoFiltrar"></i> Filtrar</button>
    <button class="btn btn-small orange inverse hide" id="sinFiltro" title="Quitar filtro">&nbsp;<i class="fa fa-remove-sign"></i></button>
<script type="text/javascript">
        if($('#filtro').val()=='SI'){
 	    $('#sinFiltro').removeClass('hide');
 	    $('#filtrar').addClass('orange');
 	}
        if($('#filtro').val()=='NO' || $('#filtro').length==0 ){
            $.get('filtros/filtro/<?php echo $modelo.'/'.$this->request->params['action'];?>',function(data){
                $("#filter").html(data);
            });
        }
        <?php echo $this->element('filter',array('model'=>$modelo));?>
 	$("#sinFiltro").click(function(event){
 	    event.preventDefault();
 	    $("#filtros").html('');
            $("#numeroCondiciones").val("0");
            $("#contenidoCabecera").val("");
            $("#filtro").val("NO");
            $('#cuerpoApp').load($('#controlador').val()+'/<?php echo $this->request->params['action'];?>/do:clearFilter');
 	});
</script>