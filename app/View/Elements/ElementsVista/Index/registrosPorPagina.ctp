<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div class="pull-right"><?php echo $this->params['paging'][$modelo]['count']." registros.";?></div>
<div class="registroPagina  col-md-4 pull-right input-group">
<label class="input-group-addon hidden-xs">Registros por p�gina</label>
    <select  id="registrosPorPagina" name="data[registrosPorPagina]" class="text-center">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="75">75</option>
        <option value="100">100</option>
    </select>
</div>
<script type="text/javascript" id="myScript">
//<![CDATA[
$(document).ready(function() { 
      //  $(".chosen-select").chosen();
        var options = {target:"#content"};
        var limit = 'limit:' + '<?=$registrosPorPagina?>';
        $("#registrosPorPagina option[value=<?=$registrosPorPagina?>]").attr("selected",true);
	$('#registrosPorPagina').change(function(event) {
            event.stopPropagation();
            $('#cuerpoApp').load("<?php echo $controlador;?>"+'/index/limit:' + $(this).val());
                 
        });
});
//]]>
</script>