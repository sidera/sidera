<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

    	<button id="sincronizarModulo" class="btn btn-default btn-admin" title="Haga clic para sincronizar �ste m�dulo">
                <i class=" fa fa-refresh fa-lg "></i>
            </button>

<script type="text/javascript">
$(document).ready(function() {          
    $("#sincronizarModulo").click( 
        function (event) {
            event.preventDefault();
            $.post("permisos/syncResourcesForController/"+"<?php echo ucwords($controlador);?>");
        }
    ); 
   
 });    
 </script>