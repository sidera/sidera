<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	
<?php
    if(!isset($classCampo)){ $classCampo="form-control";}
    if(!isset($name)){ $name="data[".$modelo."][".$id."]";}
    if(!isset($type)){ $type="text";}
    if(!isset($value)){ $value=$this->request->data[$modelo][$id];}
?>
<div class="form-group" title="<?php echo $title;?>">
    <label class="<?php echo $classLabel;?>"><?php echo $label;?></label>
    <input value="<?php echo $value;?>" type="<?php echo $type;?>" name="<?php echo $name;?>" class="<?php echo $classCampo;?>" id="<?php echo $id;?>" placeholder="<?php echo $placeholder;?>">
</div>