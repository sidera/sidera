<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	
<li>
    <a id="guardarAddRegistro<?php echo $modelo;?>" form="<?php echo $modelo;?>AddForm" type="submit" href="#">Guardar</a>
</li>
             
<script type="text/javascript">
    <?php if(!isset($formType)){ ?>
    var options = {target:'#content'}; 
    <?php } ?>
    $('#<?php echo $modelo;?>AddForm').ajaxForm(options);
    $('#guardarAddRegistro<?=$modelo?>').on('click',function(event){  
        event.preventDefault();
        if($("#<?=$modelo?>AddForm")[0].checkValidity()){
            $("#<?php echo $modelo;?>AddForm").ajaxSubmit({success:function(data){
                <?php if(!isset($formType)){ ?>
                $('#cuerpoApp').html(data);
                <?php }else{ ?>
                $('#<?php echo $controlador;?>Dialog').modal('hide');
                 $('#cuerpoApp').dialogCallback<?php echo $modelo?>();
                <?php } ?>
            }});
        }
    });            
</script>
