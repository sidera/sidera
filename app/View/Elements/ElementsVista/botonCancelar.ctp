<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	
<li><a id="cancelarAccion<?php echo $modelo;?>" href="#">Cancelar</a></li>

<script type="text/javascript">
    $("#cancelarAccion<?php echo $modelo;?>").click(
        function(event){
            event.preventDefault();
            <?php 
                if(!isset($formType)){ 
                    if($this->Session->check("page[$modelo]")==TRUE){
                        $pagina="/page:".$this->Session->read("page[$modelo]");
                    }else{
                        $pagina=""; 
                    }
                ?>
                $('#cuerpoApp').load("<?php echo $controlador;?>"+'/index<?php echo $pagina;?>');
            <?php }else{ ?>
                $('#<?php echo $controlador;?>Dialog').modal('hide');
            <?php } ?>
            
        }
    );
</script>