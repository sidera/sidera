<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	
<li>
    <a id="guardarEditRegistro<?=$modelo?>" form="<?=$modelo?>EditForm" type="submit" href="#">Guardar</a>
</li>

<script type="text/javascript">
    var options = {target:'#content'}; 
    $('#<?=$modelo?>EditForm').ajaxForm(options);
    $('#guardarEditRegistro<?=$modelo?>').on('click',function(event){  
        event.preventDefault();
        if($("#<?=$modelo?>EditForm")[0].checkValidity()){
            $("#<?=$modelo?>EditForm").ajaxSubmit({success:function(data){
                <?php if(!isset($formType)){ ?>
                $('#cuerpoApp').html(data);
                <?php }else{ ?>
                $('#<?=$controlador?>Dialog').modal('hide');
                $('#cuerpoApp').dialogCallback<?=$modelo?>();
                <?php } ?>    
            }});
        }

    });
</script>
        