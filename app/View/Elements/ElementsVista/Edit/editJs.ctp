$('form .selectpicker').selectpicker();
$('form .date').datetimepicker({locale: 'es',format:'L'});
$('form .datetime').datetimepicker({locale: 'es',format:'L LT'}); 
$('form .time').datetimepicker({locale: 'es',format:'LT'});
$('input[required],select[required]').parents('div.form-group').addClass('required');
//Muestra mensajes de validacion
$('form input').on( "blur",function() {
    $('span.mensaje-error').remove();
    $('form :invalid').each( function( index, node ) { 
        $("<span class='mensaje-error text-danger'>"+this.validationMessage+"</span>").appendTo($(this).parent());
    });
});
<?php
if (isset($this->request->params['named']['campo_relacionado'])) {
    echo "$('#" . strtolower($this->request->params['named']['campo_relacionado']) . "').parent().detach();";
    echo '$("#' . $modelo . 'EditForm").append("<input id=\'' . strtolower($this->request->params['named']['campo_relacionado']) . '\' name=\'data[' . $modelo . '][' . strtolower($this->request->params['named']['campo_relacionado']) . ']\' type=\'hidden\' value=\'' . $this->request->params['named']['id_relacionado'] . '\'>");';
}
if (isset($formType)) {
    echo '$("#' . $modelo . 'EditForm").append("<input id=\'\' name=\'formType\' type=\'hidden\' value=\'modal\'>");';
}
?>    