<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>


<div id="<?php echo $dialogId;?>" class="modal fade ventana-combo-modal hide <?php if(isset($type)) echo $type;?> modal_top_modificado" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p class="modal-title"><?php echo $header;?></p>
      </div>
      <div class="modal-body" id="dialogBody<?php echo $dialogId;?>">
        <p><?php echo $body;?></p>
      </div>
      <div class="modal-footer">
        <?php if(!isset($cancelButton) || (isset($cancelButton) && $cancelButton!=null)){ ?>
            <button type="button" class="btn btn-default" data-dismiss="modal" id="dialogMessageCancel<?php echo $dialogId;?>"> <?php if(isset($cancelLabel)){ echo $cancelLabel; }else{ echo 'Cancelar';}?></button>
        <?php } ?>
        <?php if(!isset($okButton) || (isset($okButton) && $okButton!=null)){ ?>
            <button type="button" class="btn btn-primary" id="dialogMessageOk<?php echo $dialogId;?>"><?php if(isset($actionLabel)){ echo $actionLabel; }else{ echo 'Aceptar';}?></button>
        <?php } ?>    
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
$(document).ready(function() {  
    
    //Esta funcion se ejecuta cuando se cierra el modal
    $("#<?php echo $dialogId;?>").on('hidden.bs.modal', function (e) {
                        //debugger
                        $("#<?php echo $dialogId;?>").empty();
                });
    
    <?php if(isset($launcherId)){ ?>
            
    $("#<?php echo $launcherId;?>").click(
        function (event) {
            //debugger
            //Este launcher no lo utilizo porque en el java script del boton le digo lo que lanzar cuando se pulsa el boton
            //$('#<?php //echo $dialogId;?>').modal({keyboard: false});
            //$('.main').remove();No se para que sirben esto
        }
    );
    <?php } ?>
    $("#dialogMessageCancel<?php echo $dialogId;?>").live("click", 
        function (event) {
            $('#<?php echo $dialogId;?>').modal('hide');
        }
    );
    <?php if(isset($okButton)){ ?>
    $("#dialogMessageOk<?php echo $dialogId;?>").click(
        function (event) {
            event.preventDefault();
            <?php if(isset($action)): ?>
            $("#main").load('<?php echo $action;?>');
            <?php endif;?>
            <?php if(isset($customAction)): ?>
            $().<?php echo $customAction;?>();
            <?php endif;?>
            $('#<?php echo $dialogId;?>').modal('hide');
        }
    );
    <?php } ?>
    <?php if(isset($url)) { ?>
    $("#dialogBody<?php echo $dialogId;?>").load('<?php echo $url;?>/formType:modal');
    <?php } ?>
    
    jQuery.fn.extend({
        dialogCallback<?php echo $modelo;?>: function() {
            <?php if(isset($callback)) { ?>
            $('#main').<?php echo $callback;?>();
            <?php } ?>
        }
    });
    
     <?php if(isset($urlEvent) && isset($launcherSelector)  && isset($dialogId) ) { ?>
           $("<?php echo $launcherSelector;?>").<?php echo $launcherEvent;?>(
                function (event) {
                    $('#<?php echo $dialogId;?>').modal({keyboard: false});
                    $('.main').remove();
                    $("#dialogBody<?php echo $dialogId;?>").load('<?php echo $urlEvent;?>',{ <?php echo $urlparameters ?>});
                }
            );
    <?php } ?>
    
})

</script>
