<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

     
   <div  id="flashMessages">
            <?php echo $this->Session->flash(); ?>
    </div>
    
<input type='hidden' id='filtrado' value='<?php echo $filtrado;?>'/>
<input type="hidden" id="modelo" value="<?php echo $modelo;?>">
<input type="hidden" id="controlador" value="<?php echo $controlador;?>">
<input type="hidden" id="nombreAcccion" value="<?php echo $this->request->params['action'];?>">
<script type="text/javascript">
    $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#flashMessages').delay(6000).fadeOut(1)
</script>

