<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="modal hide fade" tabindex="-1" role="dialog" id="<?php echo $dialogId;?>">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo $header;?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo $body;?></p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-small  inverse" id="dialogButtonCancel<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?>">Cancelar</button>
        <button class="btn btn-small dark-blue " id="dialogButtonOk<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?>">Aceptar</button>
    </div>
</div>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() {        
    $("<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo "#dialogButtonCancel".$id_subgrid;else echo "#dialogButtonCancel";?>").live("click", 
        function (event) {
            //debugger
            $('#<?php echo $dialogId;?>').modal('hide');
        }
    );
});
//]]>
</script>