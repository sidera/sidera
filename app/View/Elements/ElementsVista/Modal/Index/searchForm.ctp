<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div class="input-append">
	<input type="text" id="searchText"<?php if(isset($searchText)) echo "value='".$searchText."' readonly='readonly'";?>></input>
	<?php if(!isset($searchText)){ ?>
		<button type="button" class="btn  inverse" id="botonSearch" title="Haga clic para buscar. Use ',' para buscar m�s palabras"><i class="icon-search"></i></button>
	<?php }else{?>
		<button type="button" class="btn btn-warning" id="removeSearch" title="Haga clic para eliminar la b�squeda activa">
			<i class="icon-remove"></i>
		</button>
	<?php } ?>
</div>                
<script type="text/javascript">
$(document).ready(function() {      
    $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#searchText').focus();
    <?php if(isset($searchText)){ ?>
    $("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#removeSearch").bind('click',function(event){ $("#main<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo " #".$id_subgrid;?>").load('<?php echo $controlador;?>/<?php echo (isset($id_subgrid) && ($id_subgrid!=""))? $this->request->params['action']."/".$id_relacionado.'/'.$campo_relacionado.'/'.$id_subgrid:$this->request->params['action'];?>');});
    <?php }else{?>
    $("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#botonSearch").bind('click',function(event){$().sendSearch<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?>();});
    //La linea de bajo esta puesta asi para cuando pulses intro en el form de la busqueda no envie el formulario que hay por detras que es la opcion por defecto
    $("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#searchText").bind('keypress', function(event) {if(event.which==13)return false;});
    <?php } ?>
    
    jQuery.fn.sendSearch<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?> = function(href){
        //debugger
	if($('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#searchText')) $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#searchText').val( $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#searchText').val().trim());
	if(href!=null){
		$("#main<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo " #".$id_subgrid;?>").load(href+'/');
	}else{
		$("#main<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo " #".$id_subgrid;?>").load('<?php echo $controlador;?>/<?php echo (isset($id_subgrid) && ($id_subgrid!=""))? $this->request->params['action']."/".$id_relacionado.'/'.$campo_relacionado.'/'.$id_subgrid:$this->request->params['action'];?>/'+$().getFilters<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?>());
	}
    }
    $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#searchText').keyup(function() {
        var cadena = $(this).val();
        cadena = cadena.replace(/[`~!@#$%^&*()|+\=?�;'",.<>\{\}\[\]\\\/]/gi,'');
        $(this).val(cadena);
      });
});
</script>