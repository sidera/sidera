<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <script type="text/javascript">
 	$("<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo "#".$id_subgrid." ";?>tbody.griddata tr").dblclick(
        function (event) {
            event.preventDefault();
            $.ajax({
                url : '<?php echo $controlador?>/cargarFormModal/<?php echo $id_relacionado.'/'.$campo_relacionado.'/'.$id_subgrid.'/'?>'+$(this).attr('itemid'),
                async:false,
                type: "GET",
                cache : false,
                success : function(data) {
                    //debugger
				$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#".$controlador."Div";?>").html(data);
                $("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#".$controlador; ?>Dialog").modal('show');
				//debugger
				$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#".$controlador; ?>Dialog").on('shown.bs.modal', function () {
						$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#".$controlador; ?>Dialog").css({"margin-top": "0px","transition" : "opacity 1s ease-in-out"});
				});
                }
            });
        }
    );
</script>