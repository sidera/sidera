<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <button class="btn btn-small  inverse disabled" id="delRegistro">
    <i class="icon-trash icon-white "></i> Eliminar
</button>                
<?php echo $this->element("ElementsVista".DS."Modal".DS."dialog",array(
    'header'  => ucwords($controlador),
    'body'    =>'�Realmente desea eliminar estos elementos?',
    'dialogId' => (isset($id_subgrid) && ($id_subgrid!=""))? 'registroDialog'.$id_subgrid :'registroDialog'
));?>
<script type="text/javascript">
    $("<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo "#".$id_subgrid." ";?>#delRegistro").click( 
        function (event) {
            //debugger
            event.preventDefault();
            if(!$(this).hasClass('disabled')){
                $('<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo "#registroDialog".$id_subgrid; else echo "#registroDialog" ;?>').modal({keyboard: false});
            }
        }
    );
    $("<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo "#dialogButtonOk".$id_subgrid;else echo "#dialogButtonOk";?>").click( 
        function (event) {
            $('<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo "#registroDialog".$id_subgrid; else echo "#registroDialog" ;?>').modal('hide');
            var items = $('<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo '#'.$id_subgrid.' ';?>.itemSelector').filter(':checked').map(function(){ return $(this).val(); }).get().join(",");
            $.post("<?php echo $controlador;?>"+"/delete/",{'id':items,'formType':'modal'},function(){
                //debugger 
                $('#<?php echo $id_subgrid ?>').load('<?php echo $controlador.'/indexModal/'.$id_relacionado.'/'.$campo_relacionado.'/'.$id_subgrid;?>');  
                });
        }
    );
    $("<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo "#".$id_subgrid." ";?>.griddata td:first-child ").click( 
        function (event) {
            //debugger
            event.stopPropagation();
            if($('<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo '#'.$id_subgrid.' ';?>.itemSelector').filter(':checked').length > 0){
                $("<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo "#".$id_subgrid." ";?>#delRegistro").removeClass("disabled");
            }else{
            	$("<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo "#".$id_subgrid." ";?>#delRegistro").addClass("disabled");
            }
        }
    );
</script>