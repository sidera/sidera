<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="btn-mini pull-right" >  
    <?php echo $this->Form->input((isset($id_subgrid) && ($id_subgrid!=""))? "registrosPorPagina".$id_subgrid :"registrosPorPagina",array(
        "id"=>(isset($id_subgrid) && ($id_subgrid!=""))? "registrosPorPagina".$id_subgrid :"registrosPorPagina",
        "class"=>"chzn-select",
        "label"=>false,
        "default"=>$configModulo["general"]["config"]["registrosPagina"],
         "options"=>array('10'=>'10','1'=>'1','2'=>'2','10'=>'10','25'=>'25','50'=>'50','75'=>'75','100'=>'100')  
    ));?>
</div>


<script type="text/javascript" id="myScript">
//<![CDATA[
$(document).ready(function() { 
        $("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>.chzn-select").chosen();
        var options = {target:"#content"};
	$('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#registrosPorPagina<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?>').change(function(event) {
                 event.stopPropagation();
                 //debugger
                 $.post("<?php echo $controlador;?>"+"/registrosPorPagina",{ page: $(this).val()},function (){$('#main<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo ' #'.$id_subgrid;?>').load("<?php echo $controlador;?>"+'/<?php echo (isset($id_subgrid) && ($id_subgrid!=""))? "indexModal/".$id_relacionado.'/'.$campo_relacionado.'/'.$id_subgrid:"index"?>/');});
                 
        });
});
//]]>
</script>