<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

<script type="text/javascript">
       $("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>.icon-check").bind("click", 
       function (event) {
           event.stopPropagation();
           //debugger
           if ($('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector').is(':checked')){
                       $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector').removeAttr('checked');
               $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector').parent().parent().parent().parent().removeClass('row-selected');
                       $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector').parent().removeClass('checked');
                       if ($('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#delRegistro').length)  $("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#delRegistro").addClass("disabled");
                    if ($('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#editRegistro').length)
                        $("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#editRegistro").addClass("disabled");
                } else {
                    $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector').attr('checked', true);
                    $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector').parent().addClass('checked');
                    $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector').parent().parent().parent().parent().addClass('row-selected');
                    if ($('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector').length)
                        $("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#delRegistro").removeClass("disabled");
                    if ($('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector').length)
                        $("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#editRegistro").addClass("disabled");
                }
            }
    );
</script>