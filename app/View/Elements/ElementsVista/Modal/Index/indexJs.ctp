var controlador="<?php echo $controlador;?>"; 
var modelo="<?php echo $modelo;?>";

jQuery.fn.enableEditRemoveButtons<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?> = function(num){
	switch (num) {
		case (num = 0 ):
			$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#delRegistro").addClass("disabled");
			$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#editRegistro").addClass("disabled");
			break;
		case (num = 1):
			$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#delRegistro").removeClass("disabled");
			$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#editRegistro").removeClass("disabled");
			break;
		default:
			$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#delRegistro").removeClass("disabled");
			$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#editRegistro").addClass("disabled");
	}
}

jQuery.fn.getFilters<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?> = function(page){
	var filter  = '';
	if($('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#searchText').html() != null){
		if($('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#searchText').val() != ''){
			filter = 'searchText:'+escape($('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>#searchText').val())+'/';
		}
	}
	if('<?php echo $this->Paginator->sortKey();?>' != '') filter += 'sort:<?php echo $this->Paginator->sortKey();?>/direction:<?php echo $this->Paginator->sortDir();?>/';
	if(page){
		filter += 'page:'+page;
	}else{
		if('<?php echo $this->Paginator->current();?>' != '') filter += 'page:<?php echo $this->Paginator->current();?>';
	}
	return filter;
}

///Accion Seleccionar Fila
$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>tbody.griddata tr").bind("click", 
    function (event) {
        event.preventDefault();
        var id = $(this).attr('itemid');
        var elemento = $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector[value='+id+']');
        if (elemento.is(':checked')){
			elemento.removeAttr('checked');
            elemento.parent().parent().parent().parent().removeClass('row-selected');
            elemento.parent().removeClass('checked');
        }else{
			elemento.attr('checked',true);
            elemento.parent().parent().parent().parent().addClass('row-selected');
            elemento.parent().addClass('checked');
        }
        var numFilasSeleccionadas = $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector').filter(':checked').size();
        $().enableEditRemoveButtons<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?>(numFilasSeleccionadas);
	}
);

$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>.itemSelector").bind("click", 
    function (event) {
        //event.preventDefault();
        var id=$(this).parents("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>tr.gradeA").attr('itemid');
        var elemento = $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.itemSelector[value='+id+']');
        if (elemento.is(':checked')){
			elemento.parent().parent().parent().parent().addClass('row-selected');
            elemento.parent().addClass('checked');
        }else{
			elemento.parent().parent().parent().parent().removeClass('row-selected');
            elemento.parent().removeClass('checked');
        }
        var numFilasSeleccionadas = $('.itemSelector').filter(':checked').size();
        $().enableEditRemoveButtons<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?>(numFilasSeleccionadas);
	}
);

$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>#<?php echo $modelo;?>GridData [type='checkbox']").uniform();
$("<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo "#" . $id_subgrid . " "; ?>.chzn-select").chosen();
   