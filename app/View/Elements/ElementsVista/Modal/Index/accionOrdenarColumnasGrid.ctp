<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<script type="text/javascript">
 	jQuery.fn.sendSearchColumn<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?> = function(href){
        var filter  = '';
        if(href!=null){
            $("#main<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo " #".$id_subgrid;?>").load(href+'/');
        }else{
            $("#main<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo " #".$id_subgrid;?>").load('<?php echo strtolower($this->name);?>/index/'+$().getFilters<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?>());
        }
    }
 	$("<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo "#".$id_subgrid." ";?>.orderable").click( 
        function (event) {
            event.preventDefault();
            $().sendSearchColumn<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?>($(this).children("a").attr("href"));
        }
    );
</script>