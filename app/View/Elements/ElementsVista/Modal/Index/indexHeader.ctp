<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
$esCampoEsAjeno=$campo."DisplayField";
if (!isset($$esCampoEsAjeno)){
?>
    <th data-header="<?php echo $campo;?>" class=" <?php echo $campo;?> orderable <?php echo $configModulo['index']['campos'][$campo]['orden']?>">
        <i class="dragtable-drag-handle icon-ko"></i><?php echo $this->Paginator->sort($campo,$configModulo['index']['campos'][$campo]["etiqueta"]); ?>
        <?php if($this->Paginator->sortKey() == $campo):?> 
         <span class="DataTables_sort_icon css_right ui-icon 
            ui-icon-carat-1-<?php if($this->Paginator->sortDir()=='asc') echo'n';else echo 's';?>"></span>
        <?php endif;?>
    </th>
<?php 
}else{
   //Permite obtener el Modelo.DisplayField para poder ordenar la columna por el campoAjeno
   $modelosAjenos = $configModulo["relaciones"]["belongsTo"];
   $displayField=$$esCampoEsAjeno;
   foreach ($modelosAjenos as $modelo ){
       if($campo==$modelo['foreignKey']){
           $modeloAjeno[$campo]=$modelo['className'];
       }
   }
?>
        <th data-header="<?php echo $campo;?>" class=" <?php echo $campo;?> orderable <?php echo $configModulo['index']['campos'][$campo]['orden']?>">
            <i class="dragtable-drag-handle icon-ko"></i><?php echo $this->Paginator->sort($modeloAjeno[$campo].".".$displayField,$configModulo['index']['campos'][$campo]["etiqueta"]); ?>
            <?php if($this->Paginator->sortKey() == $modeloAjeno[$campo].".".$displayField):?> 
             <span class="DataTables_sort_icon css_right ui-icon 
                ui-icon-carat-1-<?php if($this->Paginator->sortDir()=='asc') echo'n';else echo 's';?>"></span>
            <?php endif;?>
        </th>               
<?php }?>
