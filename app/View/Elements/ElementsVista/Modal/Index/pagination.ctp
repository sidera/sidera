<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<?php
if (!isset($modules)) {
	$modulus = 2;
}
if (!isset($model)) {
	$models = ClassRegistry::keys();
	$model = Inflector::camelize(current($models));
}

if($this->params['paging'][$model]['pageCount']>1){
?>
<div class="span6">
    <div class="pagination blue-pag light-theme blue-theme pull-right">
<?php
    echo $this->Paginator->numbers(array(
         'first'     => 'inicio',
         'class'     => false,
         'tag'      => 'li',
         'separator' => false
    ));
?>
    </div>
    <div class="pull-right registrosIndex">
    <?php echo $this->params['paging'][$model]['count']." registros.";?>
   </div>    
</div>
<script type="text/javascript">
$(document).ready(function() {		
    //Inicialización de la paginación
    $(function() {
	    $('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>.pagination').pagination({
	        displayedPages:2,
	        itemsOnPage:<?php echo $this->params['paging'][$model]['limit']?>,
	        items:<?php echo $this->params['paging'][$model]['count']?>,
	        currentPage:<?php echo $this->params['paging'][$model]['page']?>,
	        edges:1,
	        prevText:'Prev',
	        nextText:'Sig',
	        cssStyle: 'blue-themeS',
	        onPageClick:function(pageNumber, event) {
	            $("#main<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo " #".$id_subgrid;?>").load('<?php 
                            if (RUTA_PROYECTO!="")
                                echo (isset($id_subgrid) && ($id_subgrid!=""))? "/".RUTA_PROYECTO."/".$controlador."/indexModal/".$id_relacionado.'/'.$campo_relacionado.'/'.$id_subgrid."/":"/".RUTA_PROYECTO."/".$controlador."/index/";
                            else
                                echo (isset($id_subgrid) && ($id_subgrid!=""))? "/".$controlador."/indexModal/".$id_relacionado.'/'.$campo_relacionado.'/'.$id_subgrid."/":"/".$controlador."/index/";;

                            ?>'+$().getFilters<?php if(isset($id_subgrid) && ($id_subgrid!="")) echo $id_subgrid;?>(pageNumber));
	        }
	    });
		$('<?php if (isset($id_subgrid) && ($id_subgrid != "")) echo '#' . $id_subgrid . ' '; ?>div.pagination>a.page-link').each(function(index) {                                     
			$(this).removeAttr("href");
		});
	});
});
</script>
<?php } else {?>
<div class="span4">
<div class="pull-right registrosIndex">
    <?php echo $this->params['paging'][$model]['count']." registros.";?>
</div>    
</div>    
<?php }?>