<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<script type="text/javascript">
<?php foreach ($configModulo["general"]["campos"] as $campo=>$configCampo){?>
	<?php if (!$configModulo["index"]["campos"][$campo]["buscar"]){?> 
		$("#li_<?php echo $campo;?> > div > .buscarIndex").addClass("active");
		$("#li_<?php echo $campo;?> > div > .buscarIndex").attr('title','No se busca en Index');
	<?php }?>
	<?php if (!$configModulo["index"]["campos"][$campo]["filtrar"]){?> 
		$("#li_<?php echo $campo;?> > div > .filtrarIndex").addClass("active");
		$("#li_<?php echo $campo;?> > div > .filtrarIndex").attr('title','No se incluye en el filtro');
	<?php }?>
	<?php if (!$configModulo["index"]["campos"][$campo]["mostrar"]){?> 
		$("#li_<?php echo $campo;?> > div > .mostrarIndex").addClass("active");
		$("#li_<?php echo $campo;?> > div > .mostrarIndex").attr('title','No se muestra en Index');			
	<?php }?> 
	<?php if (!$configModulo["add"]["campos"][$campo]["mostrar"]){?> 
		$("#li_<?php echo $campo;?> > div > .mostrarAdd").addClass("active");
		$("#li_<?php echo $campo;?> > div > .mostrarAdd").attr('title','No se muestra en Add');
	<?php }?>
	<?php if (!$configModulo["edit"]["campos"][$campo]["mostrar"]){?> 
		$("#li_<?php echo $campo;?> > div > .mostrarEdit").addClass("active");
		$("#li_<?php echo $campo;?> > div > .mostrarEdit").addClass("active").attr('title','No se muestra en edit');
	<?php }?>
	<?php if (!$configModulo["edit"]["campos"][$campo]["disabled"]){?> 
		$("#li_<?php echo $campo;?> > div > .disabledEdit ").addClass("active");
		$("#li_<?php echo $campo;?> > div > .disabledEdit ").addClass("active").attr('title','No disabled en edit');
	<?php }?>
	<?php if (!$configModulo["general"]["campos"][$campo]["displayfield"]){?> 
		$("#li_<?php echo $campo;?> > div > .displayfield").addClass("active");
		$("#li_<?php echo $campo;?> > div > .displayfield").attr('title','No es displayField');			
	<?php }?>
	<?php if($configModulo["general"]["columnas"]["columna2"]=="hide"){$configModulo["general"]["columnas"]["columna2"]="";?> 
		jQuery(this).children(":first").val('0');
	<?php }?>
<?php } ?>

	$("button[class^='opcion']").click(function(event){
    	event.stopPropagation();
		id = jQuery(this).parent().parent().attr('id');
		if ($(this).hasClass('active')){
			if ($(this).children().hasClass('fa fa-list')){
	    		$(".opcion.btn-derecha").addClass('active');
	       		$(".opcion.btn-derecha").attr('title','No es displayField');
	       		$(".opcion.btn-derecha").each(function(){$(this).children(":first").val('0');});
	       		jQuery(this).attr('title','Es displayField');
	       		jQuery(this).removeClass('active');
	       		jQuery(this).children(":first").val('1');
	       	}else{	
		       if (!$(this).children().hasClass('fa fa-list')){
			       if ($(this).children().hasClass('fa fa-eye-open')) jQuery(this).attr('title','Se muestra en Index');
			       if ($(this).children().hasClass('fa fa-search')) jQuery(this).attr('title','Se busca en Index');
			       if ($(this).children().hasClass('fa fa-filter')) jQuery(this).attr('title','Se incluye en el filtro');
                   if ($(this).children().hasClass('fa fa-pencil')) jQuery(this).attr('title','Se muestra en Add');
                   if ($(this).children().hasClass('fa fa-edit')) jQuery(this).attr('title','Se muestra en Edit');
                   if ($(this).children().hasClass('fa fa-ban-circle')) jQuery(this).attr('title','Disabled en Edit');
			       jQuery(this).removeClass('active');
			       jQuery(this).children(":first").val('1');
		       }
		    }
	    }else{
            if ($(this).children().hasClass('fa fa-eye-open')) jQuery(this).attr('title','No se muestra en Index');
            if ($(this).children().hasClass('fa fa-search')) jQuery(this).attr('title','No se busca en Index');
            if ($(this).children().hasClass('fa fa-filter')) jQuery(this).attr('title','No se incluye en el filtro');
            if ($(this).children().hasClass('fa fa-pencil')) jQuery(this).attr('title','No se muestra en Add');
            if ($(this).children().hasClass('fa fa-edit')) jQuery(this).attr('title','No se muestra en Edit');
            if ($(this).children().hasClass('fa fa-ban-circle')) jQuery(this).attr('title','No disabled en Edit');
            if (!$(this).children().hasClass('fa fa-list')){ 
                jQuery(this).addClass('active');
                jQuery(this).children(":first").val('0');
            }
	    }
	});
	$("button[class^='boton'], button[class^='accion']").click(function(event){
    	event.preventDefault();
    	id=jQuery(this).parent().parent().attr('id');
		if ($(this).hasClass('active')){
			jQuery(this).children(":first").val('1');
		}else{
		    jQuery(this).children(":first").val('0');
		}
    });
</script>