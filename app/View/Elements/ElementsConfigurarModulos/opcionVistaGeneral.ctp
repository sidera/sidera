<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="tab-pane" id="tab6">
<div class="row-fluid">
<div class="span8">   
   <legend class="label alert" id="labelaplicacion" >Par�metros Generales de las Vistas:</legend>
   <label> Campos :</label>
           <div class="row-fluid">
               <div class="span6 box bordered center listaOrdenableConfig" >
                   <input type="text" class="full-width box-header light-blue btn inverse etiquetaColumnaConfig" placeholder="Vista General" title="Vista General" value="Vista General" readonly="readonly"/>
                   <ul id="generalColumna1" class="generalColumna1">
                       <?php $i=0;foreach ($configModulo['general']['campos'] as $campo=>$configCampo){$i++;?>
                          <li id="li_<?php echo $campo;?>" class=" ui-state-default elementoListaConfig" value="">
                              <div data-toggle="buttons-checkbox" class="btn-group ">
                                   <input type="text" class="campoConfig" value="<?php echo $configModulo['general']['campos'][$campo]['etiqueta'];?>" title="<?php echo $campo;?>" placeholder="<?php echo $campo;?>" name="data[general][campos][<?php echo $campo;?>][etiqueta]">
                                   <button class="opcion btn btn-small light-blue btn-derecha inverse botonConfig displayfield" type="button" title="Es displayfield">
                                        <input type="hidden" value="<?php echo $configModulo[general][campos][$campo][displayfield];?>" name="data[general][campos][<?php echo $campo;?>][displayfield]"/><i class="fa fa-list "></i>
                                    </button>
                              </div>

                          </li>     
                        <?php }?>
                    </ul>
               </div>
               <div class="span6">   
                <?php echo $this->Form->input('modelo',array(
                'before'=>'<label>',
                'after'=>' Establecer �stas etiquetas para todas las vistas del M�dulo</label>',
                'id'=>'etiquetasFijas',
                'name' => 'data[general][etiquetasFijas]',
                'type' => 'checkbox',
                'label'=>false,
                'class'=>'',
                'checked'=>FALSE,
                ));?>
            </div>
           </div>
           </br>
        </div>
        </div>
        <div class="row-fluid">
        <div class="span8"> 
           <legend class="label alert " id="labelaplicacion2" >Gesti�n de campos del M�dulo:</legend>
           <label> A�adir Campos al M�dulo :</label>
                <div class="row-fluid">
                    <div class="span5">
                    <?php echo $this->Form->input("listaNuevosCampos",array(
                                "id"=>"listaNuevosCampos",
                                "name"=>"data[listaNuevosCampos]",
                                "label"=>false,
                                "options"=> $nuevosCampos,
                                "class"=>"input-xlarge chzn-select",
                                "empty"=>"Seleccione un campo...",
                                "placeholder"=>"Seleccione el campo que quiere a�adir al m�dulo"
                    ));?>
                    </div>
                    <div class="span7">
                        <button class="btn btn-small btnNuevosCampos light-blue inverse" title="A�adir Campo" id="addNuevoCampo"><input type="hidden" value="" name="data[addCampo]"/>A�adir Campo</button>                    
                    </div>
                </div>    
            <label> Eliminar Campos del M�dulo :</label>
                <div class="row-fluid">
                    <div class="span5">
                    <?php echo $this->Form->input("listaEliminarCampos",array(
                                "id"=>"listaEliminarCampos",
                                "name"=>"data[listaEliminarCampos]",
                                "label"=>false,
                                "options"=> $camposActuales,
                                "class"=>"input-xlarge chzn-select",
                                "empty"=>"Seleccione un campo...",
                                "placeholder"=>"Seleccione el campo que quiere eliminar del m�dulo"
                    ));?>
                    </div>
                    <div class="span7">
                        <button class="btn btn-small btnNuevosCampos light-blue inverse" title="Eliminar campo s�lo en la aplicaci�n" id="delCampoApp"><input type="hidden" value="" name="data[delCampoApp]"/>Eliminar Campo (Aplicaci�n)</button>                    
                        <button class="btn btn-small btnNuevosCampos light-blue inverse" title="Eliminar campo en la aplicaci�n y en la base de datos" id="delCampoAppBD"><input type="hidden" value="" name="data[delCampoAppBD]"/>Eliminar Campo (Aplicaci�n y B.D.)</button>                    
                    </div>
                </div>
        </div>        
        </div>
</div><!-- tab5 -->
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() { 

    //Llamadas ajax del formulario
    $('#<?php echo $modelo;?>ConfigurarModuloForm').ajaxForm(options);

    $('#addNuevoCampo').click(function(event){
        event.preventDefault();
        $('#accionForm').val('addCampo');
        $("#<?php echo $modelo;?>ConfigurarModuloForm").ajaxSubmit();
        $("#cuerpoApp").load('<?php echo $controlador;?>/index');
    });
    
    $('#delCampoApp').click(function(event){
        event.preventDefault();
        $('#accionForm').val('delCampoApp');
        $("#<?php echo $modelo;?>ConfigurarModuloForm").ajaxSubmit();
        $("#cuerpoApp").load('<?php echo $controlador;?>/index');
    });
    
    $('#delCampoAppBD').click(function(event){
        event.preventDefault();
        $('#accionForm').val('delCampoAppBD');
        $("#<?php echo $modelo;?>ConfigurarModuloForm").ajaxSubmit();
        $("#cuerpoApp").load('<?php echo $controlador;?>/index');
    });

    $('#etiquetasFijas').click(function() {
        if($(this).is(':checked')) {
            $('.campoVista').each(function() {
                $(this).attr( 'readonly','readonly' );
            });

        } else {
           $('.campoVista').each(function() {
                $(this).removeAttr( 'readonly' );
            });
        }
    });
});
//]]>
</script>