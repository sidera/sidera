<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="tab-pane" id="tab4">
    <div class="span8">   
		 <legend class="label alert" id="labelGeneral">Seleccionar tipo de auditor�a</legend>
                
                       <?php echo $this->Form->input('modelo',array(
                        'before'=>'<label>',
                        'after'=>'Auditoria b�sica(Add,Edit,Delete)</label>',
                        'id' => 'opcionAuditoriaBasic',
                        'name' => 'data[general][auditoria][basic]',
                        'type' => 'checkbox',
                        'label'=>false,
                        'class'=>'opcionauditoria',
                        ));?>
                         <?php echo $this->Form->input('modelo',array(
                        'before'=>'<label>',
                        'after'=>'Auditoria de Consultas</label>',
                        'id' => 'opcionAuditoriaView',
                        'name' => 'data[general][auditoria][view]',
                        'type' => 'checkbox',
                        'label'=>false,
                        'class'=>'opcionauditoria',
                        ));?>
                    
                  <legend class="label alert" id="labelGeneral">Hist�rico de datos</legend>
                        <?php echo $this->Form->input('controlador',array(
                        'before'=>'<label>',
                        'after'=>'Activado</label>',
                        'id' => 'opcionHistorico',    
                        'name' => 'data[general][historico]',
                        'type' => 'checkbox',
                        'label'=>false,
                        'class'=>'opcionauditoria',
                        ));?>
                  
				    
    </div>
</div><!-- tab4 -->


<script type="text/javascript">
    $(document).ready(function() {      
          <?php if ($configModulo[general][auditoria][basic]){?> $("#opcionAuditoriaBasic").attr("checked","checked");<?php }?>
          <?php if ($configModulo[general][auditoria][view]){?> $("#opcionAuditoriaView").attr("checked","checked");<?php }?>
          <?php if ($configModulo[general][historico]){?> $("#opcionHistorico").attr("checked","checked");<?php }?>
    });  
</script>