<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="tab-pane" id="tab3">
    <div class="row-fluid">
        <div class="span8">   
          <legend class="label alert" id="labelaplicacion" >Relacionar 2 Combos del M�dulo:</legend>
          <label> Combo Principal :</label>
                    <div class="row-fluid">
                        <div class="span6">
                        <?php echo $this->Form->input("comboPrincipal",array(
                                    "id"=>"comboPrincipal",
                                    "name"=>"data[comboPrincipal]",
                                    "label"=>false,
                                    "options"=> $camposComboActuales,
                                    "class"=>"input-xlarge chzn-select",
                                    "empty"=>"Seleccione el campo...",
                                    "placeholder"=>"Seleccione campo..."
                        ));?>
                        </div>

                    </div>     
          <label> Combo Relacionado :</label>
                    <div class="row-fluid">
                        <div class="span6">
                        <?php echo $this->Form->input("comboRelacionado",array(
                                    "id"=>"comboRelacionado",
                                    "name"=>"data[comboRelacionado]",
                                    "label"=>false,
                                    "options"=> $camposComboActuales,
                                    "class"=>"input-xlarge chzn-select",
                                    "empty"=>"Seleccione el campo...",
                                    "placeholder"=>"Seleccione campo..."
                        ));?>
                        </div>
                    </div> 
           <div class="row-fluid">
                <div class="span9">   
                    <?php echo $this->Form->input('modelo',array(
                    'before'=>'<label>',
                    'after'=>'Relaci�n Avanzada</label>',
                    'id'=>'realacionAvanzada',
                    'name' => 'data[realacionAvanzada]',
                    'type' => 'checkbox',
                    'label'=>false,
                    'class'=>'',
                    'checked'=>FALSE,
                    ));?>
                </div>
                <div id="divRelacionAvanzada" class="span7 chart-container mini-chart hide">
                    <label> Lista de campos (Separados por comas):</label>
                        <input type="text" class="input-large" value="" placeholder="Ej: Campo1,Campo2..." id="listaCampos" name="data[listaCampos]" required/>	
                    <label> Separador de campos:</label>
                        <input type="text" class="input-mini" value="," placeholder="" id="separadorCampos" name="data[separadorCampos]" required/>	    
                </div>    
          </div>          
          <div class="row-fluid">
                <div class="span6">
                    <button class="btn btn-small btnNuevosCampos light-blue inverse disabled" disabled="disabled" title="Relacionar Combos" id="relacionarCombos"><input type="hidden" value="" name="data[addCampo]"/>Relacionar Combos</button>                    
                </div>
           </div>
        </div>
    </div>    
    <div class="row-fluid">
        <div class="span8">
            <legend class="label alert" id="labelaplicacion" >Modificar un Combo para que pueda a�adir Nuevos Elementos Din�micamente:</legend>
          <label> Combo:</label>
                    <div class="row-fluid">
                        <div class="span6">
                        <?php echo $this->Form->input("comboModal",array(
                                    "id"=>"comboModal",
                                    "name"=>"data[comboModal]",
                                    "label"=>false,
                                    "options"=> $camposComboActuales,
                                    "class"=>"input-xlarge chzn-select",
                                    "empty"=>"Seleccione el combo...",
                                    "placeholder"=>"Seleccione el combo..."
                        ));?>
                        </div>

                    </div>  
        </div>
        <div class="row-fluid">
                <div class="span6">
                    <button class="btn btn-small btnNuevosCampos light-blue inverse disabled" disabled="disabled" title="Modificar Combo" id="modificarCombo"><input type="hidden" value="" name="data[modificarCombo]"/>Modificar Combo</button>                    
                </div>
        </div>
    </div>    
</div><!-- tab3 -->
<div class="modal hide fade" tabindex="-1" role="dialog" id="comboDialog">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Atenci�n</h3>
    </div>
    <div class="modal-body">
        <p>El Combo Principal y el Combo Relacionado han de ser distintos!!.</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-small dark-blue " id="comboDialogButtonOk">Aceptar</button>
    </div>
</div>
<div class="modal hide fade" tabindex="-1" role="dialog" id="comboAvanzadoDialog">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Atenci�n</h3>
    </div>
    <div class="modal-body">
        <p>Rellene los campos "Lista de campos" y "Separador de campos".</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-small dark-blue " id="comboAvanzadoDialogButtonOk">Aceptar</button>
    </div>
</div>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() { 
    
    $("#comboDialogButtonOk").live("click", 
        function (event) {
            $('#comboDialog').modal('hide');
        }
    );

    $("#comboAvanzadoDialogButtonOk").live("click", 
        function (event) {
            $('#comboAvanzadoDialog').modal('hide');
        }
    );

    $("#comboPrincipal").change(function(event){
        event.preventDefault();
        if($("#comboPrincipal option:selected").val()!="" && $("#comboRelacionado option:selected").val()!=""){
            $('#relacionarCombos').removeClass('disabled');
            $('#relacionarCombos').removeAttr("disabled");
        }else{
            $('#relacionarCombos').addClass('disabled');
            $('#relacionarCombos').attr("disabled", "disabled");
        }
    });

    $("#comboRelacionado").change(function(event){
         event.preventDefault();
         if($("#comboPrincipal option:selected").val()!="" && $("#comboRelacionado option:selected").val()!=""){
            $('#relacionarCombos').removeClass('disabled');
            $('#relacionarCombos').removeAttr("disabled");
        }else{
            $('#relacionarCombos').addClass('disabled');
            $('#relacionarCombos').attr("disabled", "disabled");
        }
    });

    $('#relacionarCombos').click(function(event){
        event.preventDefault();
        var modelo = "<?php echo $modelo;?>";
        var comboPrincipal = $("#comboPrincipal option:selected").val();
        var comboRelacionado = $("#comboRelacionado option:selected").val();
        var relacionAvanzada = ($("#realacionAvanzada").is(':checked')) ? true : false;
        var listaCampos = $("#listaCampos").val().split(',');
        var separadorCampos = $("#separadorCampos").val().replace(/\s/g,"&nbsp;");

        if(comboPrincipal!=comboRelacionado){
             if($('#realacionAvanzada').is(':checked')) {  
                if((listaCampos != "") && (separadorCampos != "")) {
                    $("#cuerpoApp").load('combos/crearCodigoCombosRelacionados/'+modelo+'/'+comboPrincipal+'/'+comboRelacionado+'/'+relacionAvanzada+'/'+listaCampos+'/'+separadorCampos);
                }else{
                     $('#comboAvanzadoDialog').modal('toggle');
                }
             }else{
                $("#cuerpoApp").load('combos/crearCodigoCombosRelacionados/'+modelo+'/'+comboPrincipal+'/'+comboRelacionado+'/'+relacionAvanzada);
            }
        }else{
             $('#comboDialog').modal('toggle');
        }
    });
    
    $('#realacionAvanzada').click(function() {
        if($(this).is(':checked')) {
            $('#divRelacionAvanzada').removeClass('hide');
        } else {
            $('#divRelacionAvanzada').addClass('hide');
        }
    });
    
    $("#comboModal").change(function(event){
         event.preventDefault();
         if($("#comboModal option:selected").val()!="" && $("#comboModal option:selected").val()!=""){
            $('#modificarCombo').removeClass('disabled');
            $('#modificarCombo').removeAttr("disabled");
        }else{
            $('#modificarCombo').addClass('disabled');
            $('#modificarCombo').attr("disabled", "disabled");
        }
    });
    
    $('#modificarCombo').click(function(event){
        event.preventDefault();
        var modelo = "<?php echo $modelo;?>";
        var comboModal = $("#comboModal option:selected").val();
        $("#cuerpoApp").load('combos/crearCodigoComboModal/'+modelo+'/'+comboModal);
    });
});
//]]>
</script>
