<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="tab-pane active" id="tab1">
    <div class="span8">   
        <legend class="label alert" id="labelaplicacion" >Par�metros Generales del M�dulo:</legend>
            <label> Etiqueta del M�dulo :</label>
                <input type="text" value="<?php echo $configModulo["general"]["config"]["etiquetaModulo"];?>" maxlength="30"  title="<?php echo $modelo;?>" placeholder="<?php echo $modelo;?>" name="data[general][etiquetaModulo]">
            <label> Registros por P�gina del M�dulo :</label>
            	<input type="text" class="input-mini" value="<?php echo $configModulo["general"]["config"]["registrosPagina"];?>" placeholder="N�mero de registros por p�gina..." name="data[general][config][registrosPagina]"/>	
            <label> Botones del M�dulo :</label>
                <div data-toggle="buttons-checkbox" class="btn-group">
                    <button class="boton btn btn-default <?php if($configModulo["general"]["botones"]["btBuscar"]==0) echo "active";?>" title="Bot�n Buscar">
                        <input type="hidden" value="<?php echo $configModulo["general"]["botones"]["btBuscar"];?>" name="data[general][botones][btBuscar]"/>Buscar</button>
                    <button class="boton btn btn-default <?php if($configModulo["general"]["botones"]["btNuevo"]==0) echo "active";?>"  title="Bot�n Nuevo">
                        <input type="hidden" value="<?php echo $configModulo["general"]["botones"]["btNuevo"];?>" name="data[general][botones][btNuevo]"/>Nuevo</button>
                    <button class="boton btn btn-default <?php if($configModulo["general"]["botones"]["btEditar"]==0) echo "active";?>"  title="Bot�n Editar">
                        <input type="hidden" value="<?php echo $configModulo["general"]["botones"]["btEditar"];?>" name="data[general][botones][btEditar]"/>Editar</button>
                    <button class="boton btn btn-default <?php if($configModulo["general"]["botones"]["btEliminar"]==0) echo "active";?>" title="Bot�n Eliminar">
                        <input type="hidden" value="<?php echo $configModulo["general"]["botones"]["btEliminar"];?>" name="data[general][botones][btEliminar]"/>Eliminar</button>
                    <button class="boton btn btn-default <?php if($configModulo["general"]["botones"]["btFiltrar"]==0) echo "active";?>" title="Bot�n Filtrar">
                        <input type="hidden" value="<?php echo $configModulo["general"]["botones"]["btFiltrar"];?>" name="data[general][botones][btFiltrar]"/>Filtrar</button>
                    <button class="boton btn btn-default <?php if($configModulo["general"]["botones"]["btImportar"]==0) echo "active";?>" title="Bot�n Importar">
                        <input type="hidden" value="<?php echo $configModulo["general"]["botones"]["btImportar"];?>" name="data[general][botones][btImportar]"/>Importar</button>
                    <button class="boton btn btn-default <?php if($configModulo["general"]["botones"]["btExportar"]==0) echo "active";?>" title="Bot�n Exportar">
                        <input type="hidden" value="<?php echo $configModulo["general"]["botones"]["btExportar"];?>" name="data[general][botones][btExportar]"/>Exportar</button>
                </div>
            <label> Acciones del M�dulo :</label>
                <div data-toggle="buttons-checkbox" class="btn-group">
                    <button class="accion btn btn-default <?php if($configModulo["general"]["acciones"]["accionEditar"]==0) echo "active";?>" title="Acci�n Editar">
                        <input type="hidden" value="<?php echo $configModulo["general"]["acciones"]["accionEditar"];?>"name="data[general][acciones][accionEditar]"/>Editar</button>
                    <button class="accion btn btn-default <?php if($configModulo["general"]["acciones"]["accionChekearTodos"]==0) echo "active";?>" title="Acci�n Checkear Todos">
                        <input type="hidden" value="<?php echo $configModulo["general"]["acciones"]["accionChekearTodos"];?>" name="data[general][acciones][accionChekearTodos]"/>Checkear Todos</button>
                    <button class="accion btn btn-default <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]==0) echo "active";?>" title="Acci�n AutoValidaci�n">
                        <input type="hidden" value="<?php echo $configModulo["general"]["acciones"]["accionAutovalidacion"];?>" name="data[general][acciones][accionAutovalidacion]"/>AutoValidaci�n</button>
                    <button class="accion btn btn-default <?php if($configModulo["general"]["acciones"]["accionOrdenarCampos"]==0) echo "active";?>" title="Acci�n Ordenar Campos en Formularios">
                        <input type="hidden" value="<?php echo $configModulo["general"]["acciones"]["accionOrdenarCampos"];?>" name="data[general][acciones][accionOrdenarCampos]"/>Ordenar Campos</button>
                </div>
    </div>
</div>