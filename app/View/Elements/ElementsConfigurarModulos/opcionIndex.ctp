<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	
<div class="tab-pane" id="tab5">
	<div class="span8">   
		<legend class="label alert" id="labelaplicacion" >Parámetros de la Vista Index:</legend>
		<label> Campos :</label>
		<div class="row-fluid">
			<div class="span6 box bordered center listaOrdenableConfig" >
				<input type="text" class="full-width box-header light-blue btn inverse etiquetaColumnaConfig" placeholder="Index" title="Index" value="Index" readonly="readonly"/>
				<ul id="indexColumna1" class="indexColumna1">
				<?php $i=0;foreach ($configModulo['general']['campos'] as $campo=>$configCampo){$i++;?>
					<li id="li_<?php echo $campo;?>" class="cursor-move ui-state-default elementoListaConfig orden<?php if($configModulo['index']['campos'][$campo]['orden']!=0)echo $configModulo['index']['campos'][$campo]['orden'];else echo $i+1;?>" value="<?php if($configModulo['index']['campos'][$campo]['orden']!=0)echo $configModulo['index']['campos'][$campo]['orden'];else echo $i+1;?>">
						<i class="fa fa-resize-vertical iconoElementoLista"/> 
						<div data-toggle="buttons-checkbox" class="btn-group ">
							<input type="text" class="campoConfig campoVista" value="<?php echo $configModulo['index']['campos'][$campo]['etiqueta'];?>" title="<?php echo $campo;?>" placeholder="<?php echo $campo;?>" name="data[index][campos][<?php echo $campo;?>][etiqueta]">
							<input type="hidden" value="<?php if($configModulo['index']['campos'][$campo]['orden']!=0)echo $configModulo['index']['campos'][$campo]['orden'];else echo $i+1;?>" name="data[index][campos][<?php echo $campo;?>][orden]">
							<button class="opcion btn btn-default botonConfig mostrarIndex" type="button" title="Se muestra en Index">
								<input type="hidden" value="<?php echo $configModulo['index']['campos'][$campo]['mostrar'];?>" name="data[index][campos][<?php echo $campo;?>][mostrar]"/>
								<i class="fa fa-eye-open "></i>
							</button>
							<button class="opcion btn btn-default botonConfig buscarIndex" type="button" title="Se busca en Index">
								<input type="hidden" value="<?php echo $configModulo['index']['campos'][$campo]['buscar'];?>" name="data[index][campos][<?php echo $campo;?>][buscar]"/>
								<i class="fa fa-search "></i>
							</button>
							<button class="opcion btn btn-default botonConfig filtrarIndex" type="button" title="Se incluye en el filtro">
								<input type="hidden" value="<?php echo $configModulo['index']['campos'][$campo]['filtrar'];?>" name="data[index][campos][<?php echo $campo;?>][filtrar]"/>
								<i class="fa fa-filter "></i>
							</button>
						</div>
					</li>     
				<?php }?>
				</ul>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() { 
//Ordeno los elementos del grid segun su numero de Orden
<?php for($i =1; $i <= sizeof($camposModelo)*3; $i++){ ?>
	$('.orden<?php echo $i;?>').appendTo('#indexColumna1');
<?php }?> 

	//Funcion que reordena los elementos de la columna  pasada por parametro
	function ordenarColumnaIndex(columna){
        var contador=0;
        contador=1;
        $('.'+columna+' li').each( function(e) {
               if(!$(this).attr('id'))return;
               $(this).children().children(':nth-child(2)').val(contador);
               $(this).val(contador);
               contador = contador+2;
        });
    }
    
    //Funcion que controla el comportamiento de los elementos de la Columna1
    $('.indexColumna1').sortable({
        zIndex: 99,
        cursor: 'pointer',
        update: function(event, ui) {
        ordenarColumnaIndex("indexColumna1");
        }
    });
});
</script>