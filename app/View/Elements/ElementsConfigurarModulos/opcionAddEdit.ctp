<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="tab-pane" id="tab2">
    <div class="span8">   
        <legend class="label alert" id="labelaplicacion" >Gesti�n de los Campos del M�dulo:</legend>
        <label> Campos :</label>
                <div class="row-fluid">
                    <div class="span5 box bordered center listaOrdenableConfig" >
                        <input type="text" class="full-width box-header light-blue btn inverse etiquetaColumnaConfig" placeholder="Columna1" title="Columna1" value="<?php echo $configModulo['general']['columnas']['columna1'];?>" name="data[general][columnas][columna1]"/>
                        <ul id="columna1" class="columna1">
                            <li class="placeholder1">Arrastre aqu� los campos que quiera en la Columna1 .</li>
                            <?php $i=0;foreach ($configModulo['general']['campos'] as $campo=>$configCampo){$i++;?>
                               <li id="li_<?php echo $campo;?>" class="cursor-move ui-state-default elementoListaConfig <?php if($configModulo['add']['campos'][$campo]['orden']!=0)echo $configModulo['add']['campos'][$campo]['orden'];else echo $i+1;?>" value="<?php if($configModulo['add']['campos'][$campo]['orden']!=0)echo $configModulo['add']['campos'][$campo]['orden'];else echo $i+1;?>">
                                   <i class="fa fa-resize-vertical iconoElementoLista"/> 
                                   <div data-toggle="buttons-checkbox" class="btn-group ">
                                        <input type="text" class="campoConfig campoVista" value="<?php echo $configModulo['add']['campos'][$campo]['etiqueta'];?>" title="<?php echo $campo;?>" placeholder="<?php echo $campo;?>" name="data[add][campos][<?php echo $campo;?>][etiqueta]">
                                        <input type="hidden" value="<?php if($configModulo['add']['campos'][$campo]['orden']!=0)echo $configModulo['add']['campos'][$campo]['orden'];else echo $i+1;?>" name="data[general][campos][<?php echo $campo;?>][orden]">
                                       <button class="opcion btn btn-default botonConfig mostrarAdd" type="button" title="Se muestra en Add">
                                           <input type="hidden" value="<?php echo $configModulo['add']['campos'][$campo]['mostrar'];?>" name="data[add][campos][<?php echo $campo;?>][mostrar]"/>
                                             <i class="fa fa-pencil "></i>
                                       </button>
                                        <button class="opcion btn btn-default botonConfig mostrarEdit" type="button" title="Se muestra en Edit">
                                           <input type="hidden" value="<?php echo $configModulo['edit']['campos'][$campo]['mostrar'];?>"name="data[edit][campos][<?php echo $campo;?>][mostrar]"/>
                                             <i class="fa fa-edit  "></i>
                                       </button>
                                       <button class="opcion btn btn-default botonConfig disabledEdit" type="button" title="S�lo lectura en Edit">
                                           <input type="hidden" value="<?php echo $configModulo['edit']['campos'][$campo]['disabled'];?>" name="data[edit][campos][<?php echo $campo;?>][disabled]">
                                             <i class=" fa fa-ban-circle  "></i>
                                       </button> 
                                   </div>
                                     <input type="hidden" value="<?php echo $campo;?>" name="data[general][campos][<?php echo $campo;?>][<?php echo $campo;?>]"/>
                               </li>     
                             <?php }?>
                         </ul>
                    </div>
                    <div class="span2">
                         <div><button title="Haz clic mover todos los campos a la columna2" id="addAllColumna2" class="btn btn-small yellow"><i class="fa fa-chevron-right"><i class="fa fa-chevron-right"></i></i></button></div>
                         <div><button title="Haz clic mover todos los campos a la columna1" id="addAllColumna1" class="btn btn-small yellow"><i class="fa fa-chevron-left"><i class="fa fa-chevron-left"></i></i></button></div>
                    </div>
                    <div class="span5 box bordered center listaOrdenableConfig">
                        <input type="text" class="full-width box-header light-blue btn inverse etiquetaColumnaConfig" placeholder="Columna2" title="Columna2" value="<?php if( $configModulo['general']['columnas']['columna2']=="hide"){ $configModulo['general']['columnas']['columna2']="";}echo  $configModulo['general']['columnas']['columna2'];?>" name="data[general][columnas][columna2]"/>
                         <ul id="columna2"class="columna2">
                             <li class="placeholder2">Arrastre aqu� los campos que quiera en la Columna2 .</li>

                         </ul>
                    </div>
                </div>
            
    </div>
</div><!-- tab2 -->
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() { 
   
   //Boton mover todo a columna2
   $('#addAllColumna2').click(function(event){ 
       event.preventDefault();
       $('.columna1 li').each( function(e) {
            if(!$(this).attr('id'))return;
            $(this).appendTo('#columna2');
       });
       $('.placeholder1').removeClass('hide');
       $('.placeholder2').addClass('hide');
       ordenarColumna("columna2");
   });
   
   //Boton mover todo a columna1
   $('#addAllColumna1').click(function(event){ 
       event.preventDefault();
       $('.columna2 li').each( function(e) {
            if(!$(this).attr('id'))return;
            $(this).appendTo('#columna1');
       });
       $('.placeholder1').addClass('hide');
       $('.placeholder2').removeClass('hide');
       ordenarColumna("columna1");
   });
   
   //Ordeno los elementos de las 2 columnas segun su numero de Orden
   <?php 
         $columna="1";
         for($i =1; $i <= sizeof($camposModelo)*2; $i++){
            if (($i%2)!=0) $columna="1";else $columna="2";?>
            $('.<?php echo $i;?>').appendTo('#columna<?php echo $columna;?>');
    <?php }?> 
    
     if($('#columna1 li').size()>1) $('.placeholder1').addClass('hide'); 
     if($('#columna2 li').size()>1) $('.placeholder2').addClass('hide');
 	
    //Funcion que reordena los elementos de la columna  pasada por parametro
    function ordenarColumna(columna){
        var contador=0;
        if (columna=="columna1") contador=1; else contador=2;
        $('.'+columna+' li').each( function(e) {
               if(!$(this).attr('id'))return;
               $(this).children().children(':nth-child(2)').val(contador);
               $(this).val(contador);
               contador=contador+2;
        });
    }
    
    //Funcion que controla el comportamiento de los elementos de la Columna1
    $('.columna1').sortable({
        zIndex: 99,
        cursor: 'pointer',
        update: function(event, ui) {
            ordenarColumna("columna1");
        }
    }).droppable({
        accept: '.columna2 li:not(.placeholder2)',
        tolerance:'touch',
        activeClass: 'highlight',
        drop: function(event, ui) {
            var $el = ui.draggable;
            var $li = $('<li  id="'+$el.attr("id")+'" class="'+$el.attr("class")+'">').html(ui.draggable.html());
            $li.appendTo(this);
            ui.draggable.remove();
            $('.placeholder1').addClass('hide');
            if ($('#columna2 li').size()<3) $('.placeholder2').removeClass('hide'); 
            ordenarColumna("columna1");
        }
    });
    
    //Funcion que controla el comportamiento de los elementos de la Columna2
    $('.columna2').sortable({
        zIndex: 99,
        cursor: 'pointer',
        update: function(event, ui) {
            ordenarColumna("columna2");
        }
    }).droppable({
        accept: '.columna1 li:not(.placeholder1)',
        tolerance:'touch',
        activeClass: 'highlight',
        drop: function(event, ui) {
            var $el = ui.draggable;
            var $li = $('<li  id="'+$el.attr("id")+'" class="'+$el.attr("class")+'">').html(ui.draggable.html());
            $li.appendTo(this);
            ui.draggable.remove();
            $('.placeholder2').addClass('hide');
            if ($('#columna1 li').size()<3) $('.placeholder1').removeClass('hide'); 
            ordenarColumna("columna2");
        }
    });
    
    
    
    
    
});
//]]>
</script>