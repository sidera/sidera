<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div id="cabeceraApp" class="fila row-fluid navbar  navbar-default navbar-fixed-top"> 
    <div id="menuResponsive" class="columna navbar-left navbar-header columna col-md-3">
        <span class="elemento fa-connectdevelop fa fa-2x btn" aria-hidden="true" href="#"></span>
        <span class="tituloApp"> <?php echo Configure::read('App.name');?></span>
        <span id="menuResponsiveCabeceraApp">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menuSuperiorDerecha" aria-expanded="false">
                <span class="fa fa-user fa-lg"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menuCabecera" aria-expanded="false">
                <span class="fa fa-navicon fa-lg"></span>
            </button>
        </span>    
    </div>

    <!--<div id="menuCabecera" class="columna col-md-3 navbar-righ collapse navbar-collapse">-->
        <?php if($this->Session->read('Auth.User.username')!='') echo $this->element("menuSuperior",array("recargarMenuDinamico"=>false)); ?>

    <!--</div>-->
</div> 
</head>
