<div class="row-fluid">   
            <div class="span6">   
                <legend class="label alert" id="labelPrincipal"><?php echo $lista1Titulo; ?></legend>
                <?php 
                if (!isset($listaValues)){
                    echo $this->Form->input($modelo,array(
                        "id"     => $lista,
                        "options"=> $listaOptions,
                        "class"=>"searchable",
                        "label"  => false,
                        "multiple" => "multiple",
                        "name" => $listaName,
                    ));
                }else{
                    echo $this->Form->input($modelo,array(
                        "id"     => $lista,
                        "options"=> $listaOptions,
                        "class"=>"searchable",
                        "label"  => false,
                        "selected" =>$listaValues,
                        "multiple" => "multiple",
                        "name" => $listaName,
                    ));
                } 
                ?>
            </div>
            <?php if($listaAvanzada) { ?>
            <div class="span6">   
                <legend class="label alert " id="labelOrden<?php echo $lista;?>"><?php echo $lista2Titulo;?></legend>
                <label ><b><?php echo $labelDetalles;?></b></label>
                <div class="chart-container-duallistbox list" id="detalles<?php echo $lista;?>"></div>   
                <div id="camposOcultos<?php echo $lista;?>" class="hide"></div>
                <div id="camposOcultosNaN<?php echo $lista;?>" class="hide"></div>
            </div>
            <?php } ?>
    </div>
    
<script type="text/javascript">
$(document).ready(function() {
    var lista="<?php echo $lista;?>";
    var modelo="<?php echo $modelo;?>";
    //Actualiza los campos del div detallesLista con los valores de la bd
    jQuery.fn.actualizarCamposNaN = function(lista){
        //alert('actualizarCamposNaN'+lista);
        $(".campoNaN-"+lista).each(function(index) {
            var elemento = $(this).attr('id');
            //alert($('#campoOcultoNaN_'+lista+'_'+elemento).attr('id'));
            if (($('#'+elemento).hasClass('datepicker')) && ($('#campoOcultoNaN_'+lista+'_'+elemento).val()!='')){
                $formatoFecha=$('#campoOcultoNaN_'+lista+'_'+elemento).val();
                $formatoFecha=$formatoFecha.slice(8,10)+'/'+$formatoFecha.slice(5,7)+'/'+$formatoFecha.slice(0,4);
                $('#'+elemento).attr('value',$formatoFecha);
                $('#'+elemento).text($formatoFecha);
            }else{    
                $('#'+elemento).attr('value',$('#campoOcultoNaN_'+lista+'_'+elemento).val());
                $('#'+elemento).text($('#campoOcultoNaN_'+lista+'_'+elemento).val());
            }
            if(($('#'+elemento).attr('value')==1) && ($('#'+elemento).attr('type')=="checkbox")){
                $('#'+elemento).prop('checked',true);
                $.uniform.update('#'+elemento);
            }    
        });
    };
    
    //Carga los campos de todos los elementos del DualListBox
    <?php 
    if(!empty($listaDatos)) foreach($listaDatos as $key=>$value) {
           $indice=$key+1;    
           if(!empty($value["$modelo"])) foreach ($value["$modelo"] as $nombreCampo => $valorCampo) {
                if(in_array($nombreCampo, $camposOcultosLista)){?>
                    $('#camposOcultos'+lista).append('<input type="hidden" value="<?php echo $valorCampo;?>" id="<?php echo "campoOculto_".$lista."_".$nombreCampo."_".$value["$modelo"]['id'];?>">');
    <?php       }
           }
    }?>
    //Carga los campos de los elementos selecionados en el DualListBox con datos de la Lista $camposOcultosListaNaN
    <?php 
    $arrayDatosNaN=$this->request->data[$modelo];
         if(!empty($arrayDatosNaN)) foreach ($arrayDatosNaN as $keyNaN => $valueNaN) {
            $indiceNaN=$keyNaN+1;
            if(!empty($valueNaN["$modeloNaN"])) foreach ($valueNaN["$modeloNaN"] as $nombreCampoNaN => $valorCampoNaN) {
               if(in_array($nombreCampoNaN, $camposOcultosListaNaN)){
                    //echo 'alert("'.$modelo.'"+"'.$nombreCampoNaN.'");';?>
                    $('#camposOcultosNaN'+lista).append('<input type="hidden" value="<?php echo $valorCampoNaN;?>" id="<?php echo "campoOcultoNaN_".$lista."_".$nombreCampoNaN."_".$valueNaN['id'];?>">');
                    
    <?php       }
           }
    }?>

    //Configura el DualListBox.
    $('#'+lista).bootstrapDualListbox({
        nonselectedlistlabel: "<b><?php echo $lista1Titulo;?></b>",
        selectedlistlabel: "<b><?php echo $lista2Titulo;?></b>",
        bootstrap2compatible:true,
        preserveselectiononmove: 'false',
        infotext:'{0} elementos ',
        infotextempty: 'Sin elementos',
        filtertextclear:'Limpiar',
        filterplaceholder:'Buscar',
        showfilterinputs        : "<?php echo $listaBuscar;?>",
        moveonselect: false
    }); 

    //Actualiza los campos al cambiar los elementos seleccionados en el DualListBox.
    <?php if($listaAvanzada) { ?>$("#"+lista).change(function(){
        var elementosSeleccionados= [];
        var existeElemento = 0;    
        $('div.bootstrap-datetimepicker-widget').remove();
        //a�ade los campos de lo elementos que estan seleccionados en el DualListBox.
        $("#"+lista+" option:selected").each(function() {
            var elemId = $(this).val();
            var elemNombre = $(this).text();
            var elemListaId = lista+$(this).val();
            elementosSeleccionados.push(elemListaId);
            if (!$('#'+elemListaId).length) {
                $('#detalles'+lista).append('<div class="elemento-'+lista+' alert-white" id="'+elemListaId+'">'+<?php echo $divDetallesLista;?>+'</div>');
                if(lista!="Ruta"){
                //Reemplazamos LISTA, NOMBRE, ELEMID pos sus valores correspondientes
                $('#detalles'+lista).html( $('#detalles'+lista).html().replace(/__NOMBRE/g,elemNombre).replace(/__LISTA/g,lista).replace(/__ELEMID/g,elemId) );
                //por cada campo del modelo relacionado mostramos su valor
                <?php foreach ($camposOcultosLista as  $campoOculto) {?>
                    $('#detalles'+lista).html( $('#detalles'+lista).html().replace(/__<?php echo $campoOculto;?>/g,$("#campoOculto_"+lista+"_<?php echo $campoOculto;?>_"+elemId).val()) );
                <?php } ?>
                //por cada campo del modelo NaN reconfiguramos sus atributos id, name y class    
                <?php foreach ($camposOcultosListaNaN as  $campoOcultoNaN) {?>
                   var nombreCampo = '<?php echo $campoOcultoNaN;?>';
                   var idCampo =nombreCampo+'_'+elemId;
                   $('#detalles'+lista).html( $('#detalles'+lista).html().replace(/__<?php echo $campoOcultoNaN;?>/g, idCampo) );
                   $('#'+idCampo).attr('name','data['+lista+']['+elemId+']['+nombreCampo+']').addClass('campoNaN-'+lista);
                <?php } ?>}
                $("#detalles"+lista+" :checkbox" ).uniform();
                $("#detalles"+lista+" .date").datetimepicker({pickTime: false,format:'dd/MM/yyyy', weekStart:1});
            }
        }); 
        
        //elimina los campos de lo elementos que no ya no estan seleccionados en el DualListBox.
        $(".elemento-"+lista).each(function(indice,elemento) {
            var elementoDiv=$(elemento).attr('id');
            existeElemento = jQuery.inArray( elementoDiv, elementosSeleccionados );
            if(existeElemento==-1){
                $('#'+elementoDiv).remove();
                
            }
        });
        
        
        $("#detalles"+lista+" .chzn-select").trigger("liszt:updated");
        $().actualizarCamposNaN(lista);
        if (typeof $().actualizarDetalles == 'function')  $().actualizarDetalles();
    }); 
    <?php } ?>
 
});
    
</script> 
