<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <button class="btn btn-default disabled" id="imprimirInforme">
                    <i class=" fa fa-print "></i> Imprimir</button>
                    

<script type="text/javascript">
	var options = {target:'#content'}; 
 	$('#'+"<?php echo $modelo;?>"+'EditForm').ajaxForm(options);
 	
  
     $('#imprimirInforme').click(function(event){
     var items = $('.itemSelector').filter(':checked').map(function(){ return $(this).val(); }).get();
            
                var cadenaParams=new Array();
                var cadenaValues=new Array();
           //obtenemos todos los valores de los parámetros     
           $('#parametros .param').each(function() {
            cadenaParams.push($(this).attr('id'));
            cadenaValues.push($(this).val().replace(/,/g,'|').replace(/\//g,'*'));
            });     
                
            window.open("<?php echo $controlador;?>"+"/imprimir/"+items+"/"+cadenaParams+"/"+cadenaValues);
           
         });     
    
     $(".griddata td:first-child ").click( 
        function (event) {
            event.stopPropagation();
            if($('.itemSelector').filter(':checked').length > 0){
                $("#imprimirInforme").removeClass("disabled");
            }else{
                $("#imprimirInforme").addClass("disabled");
            }
        }
    );
</script>