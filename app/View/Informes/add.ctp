<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php echo $this->Form->create("$modelo",array("url" => array("controller" => $controlador, "action" => "create")));?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); //Bloque para mensajes con setFlash ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsVista".DS."Add".DS."botonGuardarAdd"); //Elemento con funcionalidad de boton de Guardar Add ?>
        <?php echo $this->element("ElementsVista".DS."botonCancelar"); //Elemento con funcionalidad de boton de Cancelar ?>       
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <i class="fa fa-pencil"></i> Nuevo Registro - <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __($modelo);?>
        </div>
        <div class="row-fluid">
        	<div class="span6">
				<div class="padded">
					<div class="section-title <?php if($configModulo["general"]["columnas"]["columna1"]=="") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna1"]; ?> </div>
					<div class="vpadded input-xlarge " id="columna1">
                        <?php $nombre="inicio";?>
						<?php echo $this->Form->input("nombre",array(
						"before"=>"<label>".$configModulo["add"]["campos"]["nombre"]["etiqueta"]."</label>",
						"id"=>"nombre",
						"label"=>false,
			            "class"=>"input-xlarge",
			            "placeholder"=>"Escriba el nombre ...",
			            "div"=>array("class"=>" ".$configModulo["add"]["campos"]["nombre"]["orden"]."")
			            ));?>
			<?php $nombre="fin";?>	
                         <?php $report="inicio";?>                    
						<?php echo $this->Form->input("report",array(
						"before"=>"<label>".$configModulo["add"]["campos"]["report"]["etiqueta"]."</label>",
						"id"=>"report",
						"label"=>false,
			            "class"=>"input-xlarge",
			            "placeholder"=>"Escriba el report ...",
			            "div"=>array("class"=>" ".$configModulo["add"]["campos"]["report"]["orden"]."")
			            ));?>
			<?php $report="fin";?>	
                        <?php $informestipo_id="inicio";?>                           
						<?php echo $this->Form->input("informestipo_id",array(
						"before"=>"<label>".$configModulo["add"]["campos"]["informestipo_id"]["etiqueta"]."</label>",
						"id"=>"informestipo_id",
						"label"=>false,
			               "class"=>"input-xlarge chzn-select",
			            "placeholder"=>"Escriba el informestipo_id ...",
			            "div"=>array("class"=>" ".$configModulo["add"]["campos"]["informestipo_id"]["orden"].""),
                        "empty" => '(Seleccionar)'
			            ));?>
                        <?php $informestipo_id="fin";?>                    
		                            
			 <?php $grupo_id="inicio";?>  				
						<?php echo $this->Form->input("grupo_id",array(
						"before"=>"<label>".$configModulo["add"]["campos"]["grupo_id"]["etiqueta"]."</label>",
						"id"=>"grupo_id",
						"label"=>false,
			            "class"=>"input-xlarge chzn-select",
			            "placeholder"=>"Escriba el grupo_id ...",
			            "div"=>array("class"=>" ".$configModulo["add"]["campos"]["grupo_id"]["orden"].""),
			            "empty" => '(Seleccionar)'
			            ));?>
			 <?php $grupo_id="fin";?>   
                      <?php $espacioFuturosCampos=true; ?>                                            
           			</div>
				</div>
			</div>
			<div class="span6">
				<div class="padded">
					<div class="section-title <?php if($configModulo["general"]["columnas"]["columna2"]=="hide") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna2"]; ?> </div>
					<div class="vpadded" id="columna2">
					</div>
				</div>
			</div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
    $(".chzn-select").chosen();
	<?php echo $this->element("ElementsVista".DS."javascriptRequerido"); //Elemento con el javascript requerido para esta vista ?>
    <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); //Elemento con funcionalidad de validar formulario ?>
    <?php if($configModulo["general"]["acciones"]["accionOrdenarCampos"]) echo $this->element("ElementsVista".DS."accionOrdenarCamposFormularios"); //Elemento con funcionalidad de ordenar los campos del formulario ?>
    <?php echo $this->element("ElementsVista".DS."Add".DS."accionMostrarEnAdd"); // Elemento con funcionalidad de mostrar (o no) columnas en el formulario Add ?>

}); 
</script>