<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php //(actualizado en Sidera 1.0.6)  ?>  
<div class="row-fluid">    
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); //Bloque para mensajes con setFlash ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php if($configModulo["general"]["botones"]["btNuevo"]) echo $this->element("ElementsVista".DS."Index".DS."botonNuevo"); //Elemento con funcionalidad de boton de Nuevo ?>
        <?php if($configModulo["general"]["botones"]["btFiltrar"]) echo $this->element("ElementsVista".DS."Index".DS."botonFiltrar"); //Elemento con funcionalidad de boton de Filtrar ?>
        <?php if($configModulo["general"]["botones"]["btEliminar"]) echo $this->element("ElementsVista".DS."Index".DS."botonEliminar"); //Elemento con funcionalidad de boton de Eliminar ?>
        <?php if($configModulo["general"]["botones"]["btImportar"]) echo $this->element("ElementsVista".DS."Index".DS."botonImportar"); //Elemento con funcionalidad de boton de Importar ?>
        <?php if($configModulo["general"]["botones"]["btExportar"]) echo $this->element("ElementsVista".DS."Index".DS."botonExportar"); //Elemento con funcionalidad de boton de Exportar ?>
        <?php if($configModulo["general"]["botones"]["btconfigurar"]) echo $this->element("ElementsVista".DS."Index".DS."botonConfigurar"); //Elemento con funcionalidad de boton de Configurar ?>
        <?php if($configModulo["general"]["acciones"]["accionEditar"]) echo $this->element("ElementsVista".DS."Index".DS."accionEditar"); //Elemento con funcionalidad de Editar al hacer click ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."accionOrdenarColumnasGrid"); //Elemento con funcionalidad de Ordenar las Columnas del Grid al hacer click ?>
        <?php if($configModulo["general"]["acciones"]["accionChekearTodos"]) echo $this->element("ElementsVista".DS."Index".DS."accionCheckearTodos"); //Elemento con funcionalidad de Checkear Todos en el grid ?>
        
         <?php echo $this->element("ElementsInformes".DS."botonImprimir"); //Elemento con funcionalidad de Ordenar las Columnas del Grid al hacer click ?>
      </div>
      <div class="btn-group pull-right right span4 input-xxlarge">
        <?php if($configModulo["general"]["botones"]["btBuscar"]) echo $this->element("ElementsVista".DS."Index".DS."searchForm",array("model"=>"$controlador"));?>
      </div>
    </div>
    <hr class="main">
</div>
<?php echo $this->Form->create("$modelo",array('action' => 'imprimir'));?>
<div class="row-fluid"> 
<div class="container-fluid span6">
    <div class="box bordered">
        <div>
            <div class="box-header span6">
                <i class="fa fa-user"></i> <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __("$modelo");?>
            </div>
            <div class="groupPagination">
                <?php echo $this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>$modelo));?>
            </div>    
        </div>
        <div role="grid" class="dataTables_wrapper" id="DataTables_Table_0_wrapper">
            <table class="table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="ui-state-default check" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                            <i class="fa fa-check"></i>
                        </th>
                       	<?php $id="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"id","configModulo"=>$configModulo));?>
                        <?php $nombre="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"nombre","configModulo"=>$configModulo));?>
                        <?php $informestipo_id="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"informestipo_id","configModulo"=>$configModulo));?>
                        <?php $grupo_id="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"grupo_id","configModulo"=>$configModulo));?>
                    </tr>
                </thead>
                <tbody class="griddata">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?php echo $registro[$modelo]["id"];?>" class="gradeA" title="Haga clic para editar el registro"> 
                        <td class="checkCell">
                            <input type="checkbox" value="<?php echo $registro[$modelo]["id"];?>" name="id" class="itemSelector" title="Seleccione los elementos que desee eliminar">
                        </td>
                        <td class="id"><?php echo h($registro[$modelo]["id"]); ?></td>
						<td class="nombre"><?php echo h($registro[$modelo]["nombre"]); ?></td>
						<td class="report hide" ><?php echo h($registro[$modelo]["report"]); ?></td>
						<td class="informestipo_id">
			                	<?php echo $this->Html->link($registro["Informestipo"]["nombre"], array(
			                    "controller" => "informestipos", 
			                    "action" => "view",
			                    $registro["Informestipo"]["id"]),array("class"=>"menu")); ?>
			            	</td>
						<td class="grupo_id">
			                	<?php echo $this->Html->link($registro["Grupo"]["nombre"], 
                                                        array(
                                                            "controller" => "grupos", 
                                                            "action" => "view",
                                                            $registro["Grupo"]["id"]),array("class"=>"menu")); ?>
			            	</td>
                                        
                                        <?php $espacioFuturosCampos=true; ?>
						</tr>
						
						
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- //PARÁMETROS -->
<?php echo $this->Element("ElementsVista".DS."Informes".DS."informesParametros"); //Bloque para configurar los parámetros. ?>

</div>
<?php echo $this->Form->end();?>
<script type="text/javascript" id="myScript">
//<![CDATA[
$(document).ready(function() { 
	$(":checkbox").uniform();
	var controlador="<?php echo $controlador;?>"; 
	var modelo="<?php echo $modelo;?>"; 
	<?php echo $this->element("ElementsVista".DS."Index".DS."accionMostrarEnIndex"); // Elemento con funcionalidad de mostrar (o no) columnas en el grid ?>
    <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); //Elemento con el javascript requerido para esta vista ?>
        
});
//]]>
</script>