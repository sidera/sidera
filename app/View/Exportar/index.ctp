<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<?php echo $this->Form->create("$modelo",array('onsubmit'=>"window.open('','fenster')",'method'=>'post','target'=>'fenster','action'=>'exportarEjecutar'));?>
<div class="row-fluid">
    <hr class="main">
    <div class="container-fluid actionsBar">
        <div class="btn-group">
            <?php echo $this->element("ElementsExportar".DS."botonExportarFichero");?> <!--Elemento con funcionalidad de boton de Guardar Add -->
            <?php echo $this->element("ElementsVista".DS."botonCancelar");?> <!--Elemento con funcionalidad de boton de Cancelar -->
            <?php echo $this->element("ElementsExportar".DS."accionExportar");?> <!--Elemento con funcionalidad de Checkear Todos en el grid-->
      </div>
    </div>
    <hr class="main">    
</div>
<div class="container-fluid">
    <div class="nota padded">
        <div class="nota-title"><b>Exportar M�dulo <?php echo ucfirst($modelo);?></b></div><br>
        <i class=" fa fa-caret-right  "></i><span class="">  Selecciona los campos y el tipo de exportaci�n.</span><br>
    </div>  
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6">
            <div class="box bordered">
                <div role="grid" class="dataTables_wrapper" id="DataTables_Table_0_wrapper">
                    <?php    
                       $nombresModelos=array_keys($modelos);
                       $m=0;    
                       foreach ($modelos as $model): ?>   
                    <table class="table table-striped data-table dataTable " id="DataTables_Table_ <?php echo $m?> ">
	                    <thead>
	                    	<tr role="row">
	                        	<th class="ui-state-default check" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
	                            	<i class="fa fa-check <?php echo $nombresModelos[$m];?>th"></i>
	                        	</th>
	                        	<th><i class="fa fa-download"></i> Datos <?php echo $nombresModelos[$m];?></th>
	                    	</tr>
	                    </thead>
	                    <tbody role="alert" aria-live="polite" aria-relevant="all">
	                        <?php foreach ($model as $keyCampo=>$campo): ?> 
	                        <tr itemid="<?php echo $keyCampo;?>">
	                                <?php $odd=true;?>
                                     <td class='span1'>
                                        <input id="<?php echo $nombresModelos[$m].'-'.$keyCampo;?>" 
                                        class="<?php echo $nombresModelos[$m];?>td"
                                        type="checkbox" 
                                        value="<?php echo $campo['etiqueta'];?>" 
                                        >
                                        
                                    </td>
                                    <td><?php echo h($campo['etiqueta']); ?></td>
	                       </tr>
	                        <?php endforeach; ?>      
	                    </tbody>
					</table>
                     <?php 
                        $m++;
                        endforeach; 
                     ?>
                </div>
            </div>
        </div>
        <div class="span6">
             <div class="box bordered center listaOrdenableExportar" >
				<input type="text" readonly class="full-width box-header light-blue btn inverse etiquetaColumnaExportar" placeholder="A�adir y Ordenar Datos" title="Orden de los campos"/>
				<ul id="ordenCampos" name="ordenCampos" class="ordenCampos"></ul>
            </div>
        </div>
    </div>
</div>
<input name="tipoimpresion" id="tipoimpresion" value="pdf" class="hidden">
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
    $(":checkbox").uniform();    
    var controlador="<?php echo $controlador;?>"; 
    var modelo="<?php echo $modelo;?>";  
               
    //Acciones sobre la lista ordenable
    $('#ordenCampos').sortable({
          axis: 'y'
    }).droppable({
        tolerance:'touch',
        activeClass: 'highlight'
    });
}); 
</script>