<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */


// set document information

//die($html);
ob_end_clean();//Hace que se borre todo el contenido html a mostrar que haya antes de lo que viene abajo
//Esto lo hago para que se asocie bien los conten-type del xls y pdf  y se habra con el excel automaticamente
//los xls y con el adobe acrobat los pdf
// set document information

$textfont = 'freesans';
$this->Pdf->core->SetCreator(PDF_CREATOR);
$this->Pdf->core->SetTitle('Gobierno de Extremadura');
$this->Pdf->core->SetSubject('');
// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING);
$this->Pdf->core->SetHeaderData('', '', 'Gobierno de Extremadura', '');

// set header and footer fonts
$this->Pdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', "12"));
$this->Pdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//set margins
$this->Pdf->core->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$this->Pdf->core->SetHeaderMargin(PDF_MARGIN_HEADER);
$this->Pdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
//set auto page breaks
$this->Pdf->core->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// ---------------------------------------------------------
$this->Pdf->core->AddPage();
$this->Pdf->core->SetTextColor(0, 0, 0);
//$this->Pdf->core->SetFont($textfont,'',8);


$html=iconv('cp1252' ,'UTF-8',$html);

$this->Pdf->core->writeHTML($html, true, false, true, false, '');
$this->Pdf->core->Output($controlador.'.pdf', 'D');

 
