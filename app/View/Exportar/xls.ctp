<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

ob_end_clean();//Hace que se borre todo el contenido html a mostrar que haya antes de lo que viene abajo
//Esto lo hago para que se asocie bien los conten-type del xls y pdf  y se habra con el excel automaticamente
//los xls y con el adobe acrobat los pdf
// set document information


header('Content-type: application/vnd.ms-excel;');
header("Content-Disposition: attachment; filename=$controlador.xls");
header("Pragma: no-cache");


  echo $html;
