<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	
<?php echo $this->Form->create("$modelo");?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); //Bloque para mensajes con setFlash ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsVista".DS."Add".DS."botonGuardarAdd"); //Elemento con funcionalidad de boton de Guardar Add ?>
        <?php echo $this->element("ElementsVista".DS."botonCancelar"); //Elemento con funcionalidad de boton de Cancelar ?>       
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <i class="fa fa-pencil"></i> Nuevo Registro - <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __($modelo);?>
        </div>
        <div class="row-fluid">
        	<div class="span6">
				<div class="padded">
					<div class="section-title <?php if($configModulo["general"]["columnas"]["columna1"]=="") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna1"]; ?> </div>
					<div class="vpadded input-xlarge" id="columna1">
        
                    <?php $id="inicio";?>
							<?php echo $this->Form->input("id",array(
							"before"=>"<label>".$configModulo["add"]["campos"]["id"]["etiqueta"]."</label>",
							"id"=>"id",
							"label"=>false,
							"class"=>"input-xlarge",
							"placeholder"=>"Escriba el id ...",
							"div"=>array("class"=>" ".$configModulo["add"]["campos"]["id"]["orden"]."")
							));?>
					<?php $id="fin";?>
                    <?php $usuario="inicio";?>
							<?php echo $this->Form->input("usuario",array(
							"before"=>"<label>".$configModulo["add"]["campos"]["usuario"]["etiqueta"]."</label>",
							"id"=>"usuario",
							"label"=>false,
							"class"=>"input-xlarge",
							"placeholder"=>"Escriba el usuario ...",
							"div"=>array("class"=>" ".$configModulo["add"]["campos"]["usuario"]["orden"]."")
							));?>
					<?php $usuario="fin";?>
                    <?php $modulo="inicio";?>
							<?php echo $this->Form->input("modulo",array(
							"before"=>"<label>".$configModulo["add"]["campos"]["modulo"]["etiqueta"]."</label>",
							"id"=>"modulo",
							"label"=>false,
							"class"=>"input-xlarge",
							"placeholder"=>"Escriba el modulo ...",
							"div"=>array("class"=>" ".$configModulo["add"]["campos"]["modulo"]["orden"]."")
							));?>
					<?php $modulo="fin";?>
                    
                    <?php $datos_antiguo="inicio";?>
							<?php echo $this->Form->input("datos_antiguo",array(
							"before"=>"<label>".$configModulo["add"]["campos"]["datos_antiguo"]["etiqueta"]."</label>",
							"id"=>"datos_antiguo",
							"label"=>false,
							"class"=>"input-xlarge",
							"placeholder"=>"Escriba el datos_antiguo ...",
							"div"=>array("class"=>" ".$configModulo["add"]["campos"]["datos_antiguo"]["orden"]."")
							));?>
					<?php $datos_antiguo="fin";?>
                    <?php $datos_nuevo="inicio";?>
							<?php echo $this->Form->input("datos_nuevo",array(
							"before"=>"<label>".$configModulo["add"]["campos"]["datos_nuevo"]["etiqueta"]."</label>",
							"id"=>"datos_nuevo",
							"label"=>false,
							"class"=>"input-xlarge",
							"placeholder"=>"Escriba el datos_nuevo ...",
							"div"=>array("class"=>" ".$configModulo["add"]["campos"]["datos_nuevo"]["orden"]."")
							));?>
					<?php $datos_nuevo="fin";?>
                   
                        
                    <?php $accion="inicio";?>
                    <?php echo $this->Form->input("accion",array(
                    "before"=>"<label>".$configModulo["add"]["campos"]["accion"]["etiqueta"]."</label>",
                    "id"=>"accion",
                    "label"=>false,
                    "class"=>"input-xlarge",
                    "placeholder"=>"Escriba el accion ...",
                    "div"=>array("class"=>" ".$configModulo["add"]["campos"]["accion"]["orden"]."")
                    ));?>
                    <?php $accion="fin";?>
            <?php $espacioFuturosCampos=true; ?>
           			</div>
				</div>
			</div>
			<div class="span6">
				<div class="padded">
					<div class="section-title <?php if($configModulo["general"]["columnas"]["columna2"]=="hide") echo "hide" ?>"> <?php echo $configModulo["general"]["columnas"]["columna2"]; ?> </div>
					<div class="vpadded input-xlarge" id="columna2">
					</div>
				</div>
			</div>
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
	<?php echo $this->element("ElementsVista".DS."javascriptRequerido"); //Elemento con el javascript requerido para esta vista ?>
    <?php if($configModulo["general"]["acciones"]["accionAutovalidacion"]) echo $this->element("ElementsVista".DS."autoValidation"); //Elemento con funcionalidad de validar formulario ?>
    <?php if($configModulo["general"]["acciones"]["accionOrdenarCampos"]) echo $this->element("ElementsVista".DS."accionOrdenarCamposFormularios"); //Elemento con funcionalidad de ordenar los campos del formulario ?>
    <?php echo $this->element("ElementsVista".DS."Add".DS."accionMostrarEnAdd"); // Elemento con funcionalidad de mostrar (o no) columnas en el formulario Add ?>
}); 
</script>