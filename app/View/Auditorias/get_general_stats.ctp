<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
 <div class="span6">
    <div class="padded">
        <div class="section-title">Monitor <a class="btn btn-mini light-blue inverse pull-right" href="#" id="whole"><i class="fa fa-refresh"></i></a></div>
      
        <div class="chart-container">
            <div id="placeholder" class="chart-placeholder"></div>
        </div>
        <div class="chart-container" id="mini-chart">
            <div id="overview" class="chart-placeholder"></div>
        </div>
    </div>
</div>
<script type="text/javascript" id="myScript">
$(document).ready(function() {
    var d = [
        <?php 
            foreach($auditorias as $auditoria){
                $dateTime = date_parse($auditoria['Auditoria']['created']);
                echo "[Date.UTC($dateTime[year],".(intval($dateTime['month'])-1).",$dateTime[day],$dateTime[hour],0,0),".$auditoria[0][action]."],";
              }  
        ?>        
    ];

    // first correct the timestamps - they are recorded as the daily
    // midnights in UTC+0100, but Flot always displays dates in UTC
    // so we have to add one hour to hit the midnights in the plot
    for (var i = 0; i < d.length; ++i) {
        d[i][0] += 60 * 60 * 1000;
    }

    // helper for returning the weekends in a period
    function weekendAreas(axes) {
            var markings = [],d = new Date(axes.xaxis.min);
            // go to the first Saturday
            d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
            d.setUTCSeconds(0);
            d.setUTCMinutes(0);
            d.setUTCHours(0);
            var i = d.getTime();
            // when we don't set yaxis, the rectangle automatically
            // extends to infinity upwards and downwards
            do {
                markings.push({ xaxis: { from: i, to: i + 2 * 24 * 60 * 60 * 1000 } });
                i += 7 * 24 * 60 * 60 * 1000;
            } while (i < axes.xaxis.max);
            return markings;
    }

    var options = {
            series: {bars: { show: true,lineWidth: 1},shadowSize: 0},
            xaxis:  {mode: "time",tickLength: 5},
            selection: {mode: "x"},
            grid: {markings: weekendAreas}
    };

    var plot = $.plot("#placeholder", [d], options);

    var overview = $.plot("#overview", [d], {
            series: { bars: { show: true,lineWidth: 1}},
            xaxis: {ticks: [], mode: "time"},
            yaxis: {ticks: [],min: 0,autoscaleMargin: 0.1},
            selection: {mode: "x"}
    });
    // now connect the two
    $("#placeholder").bind("plotselected", function (event, ranges) {
            plot = $.plot("#placeholder", [d], $.extend(true, {}, options, {
                    xaxis: {min: ranges.xaxis.from,max: ranges.xaxis.to}
            }));
            overview.setSelection(ranges, true);
    });

    $("#overview").bind("plotselected", function (event, ranges) {
            plot.setSelection(ranges);
    });

    $("#whole").click(function () {
            $.plot("#placeholder", [d], options);
    });
});

