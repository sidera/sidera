<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); //Bloque para mensajes con setFlash ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="text-info"><h4>Gestor de Contenido     <h4></div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="padded">
        <div id="elfinder" class="elfinder-text"></div>
    </div>
</div>

<script>
$(document).ready(function(){
      var directorio='<?php echo $directorio;?>'; 
      var alias='<?php echo $alias;?>';
      var path='<?php echo $path;?>';
      var url='<?php echo $url;?>';
      var pattern='<?php echo $pattern;?>';
      var read='<?php echo $read;?>';
      var write='<?php echo $write;?>';
      var rm='<?php echo $rm;?>';
      var hidden='<?php echo $hidden;?>';
      var locked='<?php echo $locked;?>';
      var f = $('#elfinder').elfinder({
      lang: 'es',
      url : './app/webroot/lib/elfinder/php/connector.php?directorio='+directorio+'&alias='+alias+'&path='+path+'&url='+url+'&pattern='+pattern+'&read='+read+'&write='+write+'&rm='+rm+'&hidden='+hidden+'&locked='+locked,
      height: 400,
      uiOptions : {
    // toolbar configuration
    toolbar : [
        ['back', 'forward'],
        ['home', 'up'],
        ['mkdir', 'mkfile', 'upload'],
        ['open', 'download','quicklook'],
        ['copy', 'cut', 'paste','rm'],
        ['duplicate', 'rename','search'],
        ['view']
    ]}
    }).elfinder('instance');    
});
</script>
