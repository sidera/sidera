<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        
    <?php
         $this->Html->css('/app/webroot/whitemin/stylesheets/application.css', null, array('inline' => false));     
        $this->Html->script('/app/webroot/whitemin/javascripts/application.js', array('inline' => false));
        // $this->Html->script('jquery-1.10.2.min', array('inline' => false));
        // $this->Html->css( '/app/webroot/lib/bootstrap/css/bootstrap.min.css', null, array('inline' => false));
         
         $this->Html->script('/app/webroot/lib/jquery-steps/jquery.steps.min', array('inline' => false));
         $this->Html->css( '/app/webroot/lib/jquery-steps/css/jquery.steps.css', null, array('inline' => false));
         
         //$this->Html->script('/app/webroot/lib/chosen/chosen.jquery.min.js', array('inline' => false));
         //$this->Html->css('/app/webroot/lib/chosen/chosen.min.css', null, array('inline' => false));       
    
       

        
         $this->Html->meta('icon', $this->Html->url('/app/webroot/img/sidera/sidera_favicon.ico'));
        
        $this->Html->script('jquery.form.js', array('inline' => false));
        $this->Html->script('../lib/jquery-validation-1.10.0/jquery.validate.min.js', array('inline' => false));
        $this->Html->script('../lib/jquery-validation-1.10.0/messages_es.js', array('inline' => false));
        $this->Html->script('jquery.transit.min.js', array('inline' => false));
    ?>    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!-- there's an IE separated stylesheet with the following resets for IE -->
    <!--[if lte IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
          <script src="../javascripts/html5shiv.js" type="text/javascript"></script>
          <script src="../javascripts/excanvas.js" type="text/javascript"></script>
          <script src="../javascripts/ie_fix.js" type="text/javascript"></script>
          <link href="../stylesheets/ie_fix.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body>
    <div class="page-header">
        <h1 class="center ">Instalación de <?php echo APLICACION;?></h1>
    </div>
    <div class="container well">


    <?php echo $this->Form->create("Instalador",array("url" => array("controller" => "Instalador", "action" => "finalizarInstalacion")));?>
       <div id="wizard">
                <h2>INTRODUCCIÓN Y REQUISITOS</h2>
                <section>
                    <p style="text-align: justify;"> Este proyecto consiste  en un <span style="color: #800000;">Sístema de Desarrollo Rápido</span> de aplicaciones web (<span style="color: #800000;"><strong>SIDERA</strong></span>), basado en el framework CakePHP, con potencia para soportar otros gestores de Base de Datos.</p>
                    <p> Entre sus <strong>caracteristicas</strong> se encuentran:</p>
                    
                    <dl>
                        <dt>Independencia del Sistema Opertivo.</dt>
                            <dd>SIDERA  se ejecuta como aplicación web, por lo que no depende del S.O.</dd>
                        <dt>Independencia de los datos.</dt>
                            <dd>SIDERA soporta varios motores de BD; Mysql, Oracle y SqlServer, entre otros.</dd>
                        <dt>Rapidez de Desarrollo.</dt>
                            <dd>SIDERA puede generar módulos automáticamente e incorporarles  herramientas y funcionalidades de uso común en aplicaciones corporativas.</dd>
                    </dl>
                    
                    <p class="strong">Requisitos:</p>
                    <ul>
                      <li>PHP 5.3 o superior.</li>
                      <li>Base de datos usando convenciones de CakePHP.</li>
                      <li>Navegador Web. Se recomienda usar Mozilla Firefox.</li>
                    </ul>

                </section>

                <h2>APLICACIÓN</h2>
                <section>
                   <div class="span6">   
                                    <label> Nombre de la aplicación:</label>
                                            <input class="full-width required" type="text" placeholder="Nombre de la aplicación..." value="<?php echo APLICACION;?>" maxlength="256" name="data[Configuracion][aplicacion]">
                                    <label> Nombre del departamanto:</label>
                                            <input class="full-width required" type="text" placeholder="Organismo..." value="<?php echo ORGANISMO;?>" maxlength="256" name="data[Configuracion][organismo]">
                                    <label> Nombre de la organización:</label>
                                            <input class="full-width required" type="text" placeholder="Consejería..." value="<?php echo CONSEJERIA;?>" maxlength="256" name="data[Configuracion][consejeria]">
                                                 
                     </div>
                </section>
                <h2>BASE DE DATOS</h2>
                <section>
                             <label> Datasource</label>
                            <?php echo $this->Form->input("datasource",array(
                                        "id"=>"datasource",
                                        "value"=> DEFAULT_DATASOURCE,
                                        "name"=>"data[Configuracion][datasource]",
                                        'options' => array('Database/Mysql'=>"Database/Mysql",'Database/Oracle'=>"Database/Oracle"),
                                        "label"=>false,
                                        "class"=>"input-xlarge required"
                            ));?>
                         
                              <?php echo $this->Form->input("host",array(
                                        "before"=>"<label>Host </label>",
                                        "id"=>"host",
                                        "value"=> DEFAULT_HOST,
                                        "name"=>"data[Configuracion][host]",
                                        "label"=>false,
                                        "class"=>"input-xlarge required",
                                        "placeholder"=>"Escriba el host de la BD..."
                                        ));?>
                                        <?php echo $this->Form->input("login",array(
                                        "before"=>"<label>Login </label>",
                                        "id"=>"login",
                                        "value"=> DEFAULT_LOGIN,    
                                        "name"=>"data[Configuracion][login]",    
                                        "label"=>false,
                                        "class"=>"input-xlarge required",
                                        "placeholder"=>"Escriba el login ...",    
                                        ));?>  
                                        <?php echo $this->Form->input("password",array(
                                        "before"=>"<label> Password</label>",
                                        "id"=>"password",
                                        "value"=> DEFAULT_PASSWORD,       
                                        "name"=>"data[Configuracion][password]",      
                                        "label"=>false,
                                        "class"=>"input-xlarge",
                                        "placeholder"=>"Escriba el password ...",    
                                        ));?>  
                                        <?php echo $this->Form->input("database",array(
                                        "before"=>"<label>Database </label>",
                                        "id"=>"database",
                                        "value"=> DEFAULT_DATABASE,       
                                        "name"=>"data[Configuracion][database]",       
                                        "label"=>false,
                                        "class"=>"input-xlarge required",
                                        "placeholder"=>"Escriba el database ...",    
                                        ));?>  
                                       
                </section>
                <h2>SERVIDORES</h2>
                <section>
                   <div class="span6">   
                        <div class="span6">
                            <label> Tipo de protocolo:</label>
                                <?php echo $this->Form->input('protocolo',array(
                                    'name'    => 'data[Configuracion][protocolo]',
                                    'type'    => 'select',
                                    'value'   => PROTOCOLO,
                                    'options' => array('http'=>"http",'https'=>"https"),
                                    'label'   => false,
                                    'class'   => 'input-xlarge required'
                                 ));?>
                        </div>    
                        <div class=" span6 nota badge">
                            <label> "http"  <i class=" fa fa-caret-right  "></i> Localhost y Desarrollo</label>
                            <label> "https" <i class=" fa fa-caret-right  "></i> Preproducción y Producción.</label>
                        </div>
        
                       <div class="span6">   
                        <label> Ruta del Proyecto:</label>
                        <input class="input-xlarge required" type="text" placeholder="Ruta del Proyecto..." value="<?php echo RUTA_PROYECTO;?>" maxlength="256" name="data[Configuracion][ruta_proyecto]">
                       </div>      
                        <div class="span6 nota badge">
                            <label> "miapp"  <i class=" fa fa-caret-right  "></i> Localhost </label>
                            <label> "00/miapp" <i class=" fa fa-caret-right  "></i> Desarrollo, Preproducción y Producción.</label>
                        </div>

                    </div>
                </section>

                <h2>FINALIZANDO</h2>
                <section>
                    <p class="lead">Compruebe que los parámetros de instalación son correctos antes de finalizar la instalación.</p>
                    <p class="lead">Será reedirigido a la página de autentificación del proyecto.</p>
                    
                    <dl class="dl-horizontal">
                        <dt>Usuario</dt>
                            <dd>admin</dd>
                        <dt>Password</dt>
                            <dd>admin</dd>
                       
                    </dl>
                </section>
            </div>
   </div>
</body>
<?php echo $this->Form->end();?>    
 <script type="text/javascript" id="myScript">
//<![CDATA[
$(document).ready(function() { 
  
    
    
  $("#wizard").steps({
                        headerTag: "h2",
                        bodyTag: "section",
                        transitionEffect: "slideLeft",
                        stepsOrientation: "vertical",
                        
                        onFinishing: function (event, currentIndex) {
                            $("#InstaladorInstalarForm").submit();
                        }, 
                        onStepChanging: function (event, currentIndex, newIndex) {
                            if (currentIndex > newIndex){return true;}
                            return $("#InstaladorInstalarForm").valid();
                        },         
                        
                        labels: {
                            finish: "Finalizar",
                            next: "Siguiente",
                            previous: "Anterior",
                        }
                    });
                    
 
});
//]]>
</script>   
</html>