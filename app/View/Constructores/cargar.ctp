<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php 
if (sizeof($relacionesPerteneceA)>0){
	$num = 0;
	foreach($relacionesPerteneceA as $p) {
		$num++;
                echo '<div class="form-group">
                        <input type="hidden" name="data[Constructor][relaciones][pertenecea][check]['.$num.']" value="1" onclick="$(this).val()==\'0\' ? $(this).val(\'1\') : $(this).val(\'0\')" />
                        <i class=" fa fa-lg fa-check-square-o  col-md-1" onclick="$(this).toggleClass(\'fa-square-o\').siblings().trigger(\'click\');"></i>
                        <label class="label-right">(Un/Una) '.ucwords($p['principal']).' <b>pertenece a</b> (un/una) '.ucwords($p['modelo']).'</label>
                    </div>';
                echo '<input type="hidden" value="'.$p["modelo"].'" id="pertneceA'.$p["modelo"].'" name="data[Constructor][relaciones][pertenecea]['.$num.']">';
	}
	echo '<input type="hidden" value="'.$num.'" name="data[Constructor][relaciones][pertenecea][numRelaciones]">';
}

if (sizeof($relacionesTieneMuchos)>0){
	$num2 = 0;
	foreach($relacionesTieneMuchos as $t){
		$num2++;
               echo '<div class="form-group">
                        <input type="hidden" name="data[Constructor][relaciones][tienemuchos][check]['.$num2.']" value="1" onclick="$(this).val()==\'0\' ? $(this).val(\'1\') : $(this).val(\'0\')" />
                        <i class="fa fa-lg fa-check-square-o  col-md-1" onclick="$(this).toggleClass(\'fa-square-o\').siblings().trigger(\'click\');"></i>
                        <label class="label-right">(Un/Una) '.ucwords($t['principal']).' <b>tiene (muchos/muchas)</b> '.ucwords(Inflector::pluralize($t['modelo'])).'</label>
                    </div>';
		echo '<input type="hidden" value="'.$t["modelo"].'" id="tienMuchos'.$t["modelo"].'" name="data[Constructor][relaciones][tienemuchos]['.$num2.']">';
	}
	echo '<input type="hidden" value="'.$num2.'" name="data[Constructor][relaciones][tienemuchos][numRelaciones]">';
}

if (($num == 0) && ($num2 == 0)) echo '<div class="form-group"><i class="fa fa-angle-double-right"> </i> No se han encontrado relaciones para este modelo.</div>';
echo "////"; //Esta linea sirve para dividir posteriormente el data de javascrip en 2 parametros
if (sizeof($propiedadesDeTabla)>0){
	foreach($propiedadesDeTabla as $pt) {
		echo '<option  value="'.$pt["name"].'">'.$pt["name"].'</option>';
	}
}
?>