<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php echo $this->Form->create("$modelo",array("url" => array("controller" => $controlador, "action" => "addMasivo")));?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); //Bloque para mensajes con setFlash ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
         <?php echo $this->element("ElementsConstructor".DS."Masivo".DS."botonGenerar"); //Elemento con funcionalidad de boton de Nuevo ?>
          <?php echo $this->element("ElementsVista".DS."botonCancelar"); //Elemento con funcionalidad de boton de Cancelar ?>       
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <i class="fa fa-magic"></i> Constructor Masivo
        </div>
        
       <div class="row-fluid">    
           
           
       <div class="span8 padded">
                <select  multiple="multiple" id="nuevos" name="nuevos[]" class="searchable">
                    <?php 
                                       
                    foreach ($tablasDisponibles as $tabla) {
                        echo '<option value="'.$tabla.'">'.$tabla.'</option>'; 
                    }
                    ?>
                </select>
       </div>     
        
        </div> 
        <div class="row-fluid">   
      
         
           
        
<div id="accordion" class="span8 padded">
<h3>Estructura del m�dulo a generar</h3>
 <div>  
                <div data-toggle="buttons-checkbox" class="btn-group span4 center">
                    <label>Generar Modelos</label>  
                    <button class="boton btn btn-default " title="Generar modelos">
                        <input type="hidden" value="1" name="data[opciones][modelo]"/>Activado</button>
                 </div> 
                 <div data-toggle="buttons-checkbox" class="btn-group span4 center">     
                     <label>Generar controladores</label>   
                    <button class="boton btn btn-default "  title="Genreal controladores">
                        <input type="hidden" value="1" name="data[opciones][controlador]"/>Activado</button>
                  </div> 
                <div data-toggle="buttons-checkbox" class="btn-group span4 center">   
                    <label>Generar vistas</label>   
                    <button class="boton btn btn-default " id="btvistas"title="Generar vistas">
                        <input type="hidden" value="1" name="data[opciones][vista]"/>Activado</button>
                </div>
 </div>              
<h3>Opciones de Men�</h3>
 <div>  
                <div data-toggle="buttons-checkbox" class="btn-group span6 center">
                     <label>Se desea que el constructor le asigne un men�</label>  
                    <button class="boton btn btn-default" id="btmenus" title="Generar modelos">
                       <input type="hidden" value="1" name="data[opciones][menu]" id="menus"/>Activado</button>
                </div>    
             <div id="divMenuPadre span6 center">
                 <label>Men� padre (contenedor) para los m�dulos</label>  
                    <input id="menuPadre" title="Men� padre para los m�dulos"  class="input-xlarge" type="text" placeholder="No rellenar si no desea un men� contenedor" maxlength="20" name="data[opciones][menuPadre]" value="Gesti�n">
                </div>
       </div>        

</div>
 </div> 
        
        
        
      
    </div>
</div>
<script type="text/javascript" id="myScript">
     <?php $this->Html->css('../lib/bootstrap-duallistbox/bootstrap-duallistbox.css', null, array('inline' => false));
  $this->Html->script('../lib/bootstrap-duallistbox/jquery.bootstrap-duallistbox.js', array('inline' => false));
  ?>
//<![CDATA[
$(document).ready(function() { 
    
     $( "#accordion" ).accordion();
     
    $('#nuevos').bootstrapDualListbox({
    nonselectedlistlabel: '<b>Tablas disponibles (Sin YML)</b>',
    selectedlistlabel: '<b>M�dulos que se van a construir</b>',
    bootstrap2compatible:true,
    preserveselectiononmove: 'moved',
    infotext:'{0} elementos ',
    infotextempty: 'Sin elementos',
    filtertextclear:'Limpiar',
    filterplaceholder:'Buscar',
    moveonselect: true
    
    
});
   $("button[class^='boton'], button[class^='accion']").click(function(event){
    	event.preventDefault();
		if ($(this).hasClass('active')){
                    jQuery(this).children(":first").val('1');
                    $(this).text('Activado');
		}else{
		    jQuery(this).children(":first").val('0');
                     $(this).text('Desactivado');
		}
    }); 
    
    $("#btvistas").click(function(event){
        if ($(this).hasClass('active')){
		    $("#menus").val('1');
                    $("#btmenus").removeClass('active');
                    $("#btmenus").text('Activado');
                    
		}else{
                     $("#menus").val('0');
                     $("#btmenus").addClass('active');
                     $("#btmenus").text('Desactivado');
                }
       }); 
       
      $("#ConstructorAddForm").ajaxSubmit({async:false});
});
//]]>
</script>
