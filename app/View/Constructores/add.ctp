<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a id="addRegistro" title="Haga clic para crear el m�dulo" href="#">Crear M�dulo</a></li>
            <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
        </ul>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6">
            <i id="iconoModulo" class="fa fa-magic fa-lg"> </i> 
            <span id="etiquetaModulo"> Nuevo M�dulo</span> 
        </div><!--columnaCabecera-->
    </div><!--cabeceraModulo--> 
    
    <div id="cuerpoModulo" class="container-fluid">
        <form action="/sidera/constructores/create/" id="ConstructorAddForm" method="post" accept-charset="iso-8859-1">
            <div id="fila1" class="row-fluid">
                <div id="columna1" class="col-md-4 padded">
                    
                    <div id="listaModulosDiv" class="form-group" title="Seleccione la tabla que servir� de base para construir el m�dulo">
                        <label>1.- Tabla del Modelo</label>
                        <select  id="listaModulos" name="data[modelo]" class="chosen-select">
                            <option value=''>Seleccione una tabla...</option>
                        <?php foreach ($tablasDisponibles as $clave => $valor) {
                            echo "<option value='".$clave."'>".$valor."</option>";
                        }?>
                        </select>
                    </div><!--listaModulosDiv-->
                    
                    <div id="campoEtiquetaDiv" class="form-group" title="Opcional. Escriba un nombre para este m�dulo">
                        <label>2.-  Etiqueta del M�dulo</label>
                        <input id="campoEtiqueta"   class="form-control " type="text" placeholder="Etiqueta del m�dulo..." maxlength="20" name="data[Constructor][etiqueta]">
                    </div><!--campoEtiquetaDiv-->
                    
                    <div id="listaCamposDiv" class="form-group" title="Opcional. Seleccione el campo que se mostrar� en los combos de otros m�dulos">
                        <label>3.-  Campo displayField</label>
                        <select class="chosen-select" id="listaCampos"  name="data[Constructor][general][displayfield]" value="id">
                        </select>
                    </div><!--listaCamposDiv-->
                    
                    <div id="relacionesDiv" class="form-group" title="Revise y marque las relacciones que tiene el m�dulo">
                        <label>4.-   Relaciones del Modelo</label>
                        <div class="well well-constructor" id="relaciones"></div>
                    </div><!--relacionesDiv-->
                    
                    <div id="modalDiv" class="form-group" title="Marque esta opci�n si este m�dulo puede aparecer como m�dulo auxiliar en otro">
                        <label>5.-  Gestionado en otro M�dulo (Modal)</label>
                        <input type="hidden" name="data[Constructor][general][crearIndexModal]" value="0" onclick="$(this).val()=='0' ? $(this).val('1') : $(this).val('0')" />
                        <i class="form-control fa fa-lg fa-check-square-o fa-square-o" onclick="$(this).toggleClass('fa-square-o').siblings().trigger('click');"></i>
                    </div><!--modalDiv-->
                    
                </div><!--columna1-->
            </div><!--fila1-->  
        </form>
    </div><!--cuerpoModulo-->
    
</div><!--modulo-->   
<script type="text/javascript">
$(document).ready(function() {
    $.post("configuraciones/borrarCache");
    $(".chosen-select").chosen();
    var options = {target:'#content'}; 
    $('#ConstructorAddFormAddForm').ajaxForm(options);
    $('#listaModulos').change(function(event){
        event.preventDefault();
       if(($("#listaModulos option:selected").val()!="")){
            var tabla = $("#listaModulos option:selected").val();
                $.post("<?php echo $controlador;?>"+"/cargar/",{'tabla':tabla},
                    function(data){
                        var parametrosData = data.split('////');
                        $("#relaciones").html(parametrosData[0]);
                        $("#listaCampos").html(parametrosData[1]);
                        $('#listaCampos option[value="id"]').attr("selected",true);
                        $("#listaCampos").trigger("chosen:updated");
                         $('#addRegistro').removeClass("disabled");
                });
            }else{
                $('#addRegistro').addClass("disabled");
                $('#listaCampos').empty();
                $("#listaCampos").trigger("chosen:updated");
                $("#relaciones").html("");
            }
    });
    
    $('#addRegistro').click(function(event){
        event.preventDefault();
        if (!$("#addRegistro").hasClass("disabled")){
	        $("#ConstructorAddForm").ajaxSubmit({async:false});
	        $('#cuerpoApp').load('constructores/index');
	}
    });
}); 
</script>
