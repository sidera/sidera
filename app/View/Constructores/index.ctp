<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."Index".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a onclick='$("#cuerpoApp").load("constructores/add")' href="#" >Nuevo</a></li>
            <?=$this->element("ElementsVista".DS."Index".DS."botonEliminar")?>
        </ul>
    </div>  
    <div id="columnaBusquedaBarraAccion" class="pull-right col-md-3 col-xs-8 ">
       <?=$this->element("ElementsVista".DS."Index".DS."searchForm",array("model"=>"constructores"))?></li>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>
<div id="modulo" class="container-fluid form">
    
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6">
            <i id="iconoModulo" class=" fa fa-magic fa-lg"> </i> 
            <span id="etiquetaModulo"> Constructor</span> 
        </div><!--columnaCabecera-->
        <div id="columnaPaginacion" class="col-md-6 pull-right">
            <?=$this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>"Constructor"))?>
        </div><!--columnaPaginacion-->
    </div><!--cabeceraModulo--> 
 
    <div id="cuerpoModulo" class="fila row-fluid">  
        <div  class="columna dataTables_wrapper" role="grid" id="DataTables_Table_0_wrapper">
            <table class="elemento table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="check"><i class="fa fa-check-square fa-lg"></i></th>
                        <th data-header="modelo" class="orderable"><?=$this->Paginator->sort("modelo","Modelo")?></th>
                        <!--nuevosCamposCabecera-->
                	</tr>
                </thead>
                <tbody class="griddata" id="PruebaGridData">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?=$registro?>" class="gradeA" title="Haga doble clic para editar el registro"> 
                        <td><i value="<?=$registro?>" class="checkCell fa fa-lg fa-square-o"></i></td>
                        <td class="id"><?=$registro?></td>
                        <!--nuevosCamposCuerpo-->
                     </tr>
                     <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div id="pieModulo" class="row-fluid">
        <div id="columnaPie" class="col-md-8 ">
        </div><!--columnaPie-->
        
        <div id="columnaPaginacionPie" class="col-md-4 pull-right">
            <?//=$this->element("ElementsVista".DS."Index".DS."registrosPorPagina")?>
        </div><!--columnaPaginacionPie-->     
    </div><!--pieModulo--> 
    
</div><!--modulo--> 
<div id="menuOculto" class=""><?php echo $this->element("menuSuperior",array("recargarMenuDinamico"=>true));?></div>
<script type="text/javascript" id="myScript">
$(document).ready(function() {
    $("#menu_parte_superior").html('');
    $("#menu_parte_superior").append($("#menuOculto"));
    <?=$this->element("ElementsVista".DS."Index".DS."indexJs")?>
});
</script>