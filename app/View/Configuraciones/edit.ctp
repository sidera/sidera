<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>
<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <?php echo $this->element("ElementsConfigurarApp".DS."botonGuardarEditConfiguracion");?>
            <li><a onclick='location.reload();' href="#">Cancelar</a></li>
        </ul>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6">
            <i id="iconoModulo" class="fa fa-edit fa-lg"> </i> 
            <span id="etiquetaModulo"> Editar Configuración: (Sidera <?=VERSION?>)</span> 
        </div><!--columnaCabecera-->
    </div><!--cabeceraModulo--> 
    
    <div id="cuerpoModulo" class="container-fluid">
        <form action="/sidera/configuraciones/guardarConfig" id="ConfiguracionEditForm" method="post" accept-charset="iso-8859-1">
            <div id="fila1" class="row-fluid">
                <div id="panel">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#opcionParametrosApp" data-toggle="tab">Aplicación</a></li>
                        <li><a href="#opcionGeneral" data-toggle="tab">General</a></li>
                        <li><a href="#opcionJasperserver" data-toggle="tab">Jasper</a></li>
                        <li><a href="#opcionCodificacion" data-toggle="tab">Codificación</a></li>
                        <li><a href="#opcionEmail" data-toggle="tab">Correo</a></li>
                        <li><a href="#opcionLibrerias" data-toggle="tab">Librerías</a></li>
                    </ul>
                    <div class="tab-content alto-fijo">
                        <?php echo $this->element("ElementsConfigurarApp".DS."opcionParametrosApp"); ?>
                        <?php echo $this->element("ElementsConfigurarApp".DS."opcionGeneral"); ?>
                        <?php echo $this->element("ElementsConfigurarApp".DS."opcionJasperserver"); ?>
                        <?php echo $this->element("ElementsConfigurarApp".DS."opcionCodificacion"); ?>
                        <?php echo $this->element("ElementsConfigurarApp".DS."opcionEmail"); ?>
                        <?php echo $this->element("ElementsConfigurarApp".DS."opcionLibrerias"); ?>    
                    </div>
                </div>
            </div><!--fila1-->  
        </form>
    </div><!--cuerpoModulo-->
    
    <div id="pieModulo" class="row-fluid hide">
        <div id="columnaPie" class="col-md-12 ">
        </div><!--columnaPie-->
    </div><!--pieModulo-->  
</div><!--modulo-->
<script type="text/javascript">
$(document).ready(function() {
    <?php echo $this->element("ElementsVista".DS."Edit".DS."editJs"); ?>
       
    //<!--espacioFuturosControles-->
}); 
</script>