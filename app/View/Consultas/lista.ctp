<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

     <?php 
        $numpaginas= sizeof($Botones[nav_numeros]); 
        if ($numpaginas!=0) echo"
       
    <div class='container-fluid actionsBar btn-group'>          
        <button class='btn btn-default' id='inicio' >
        <i class='fa fa-fast-backward  '></i>
        </button>                         
        <button class='btn btn-default' id='anterior' >
        <i class='fa fa-backward  '></i></button>                         
        <button class='btn btn-default' id='siguiente' >
        <i class='fa fa-forward  '></i></button>                         
        <button class='btn btn-default' id='final' >
        <i class='fa fa-fast-forward  '></i></button>                                                 
    </div>"?>
<?php if ($numpaginas!=0) echo "Pagina actual: " . $paginaactual . " de " . sizeof($Botones[nav_numeros])  ?>       
<?php echo $txtrespuesta;?>     
<script>
var p=<?php echo $paginaactual;?>; 
var ultimapagina=<?php echo sizeof($Botones[nav_numeros]);?>;
$("#inicio").click(function(event){
                    event.preventDefault();  
                    p=1;
                    irpagina(p);
}); 	

$("#anterior").click(function(event){ 
             event.preventDefault();             
            if (p>1){  
              p=p-1;   
              irpagina(p);
            } 

}); 	
$("#siguiente").click(function(event){ 
              event.preventDefault();
             if (p<ultimapagina){
              p=p+1;   
              irpagina(p);              
              }     

}); 	
$("#final").click(function(event){ 
              event.preventDefault();
              p=ultimapagina;
              irpagina(p);                            
              
}); 
function irpagina(){
           $.ajax({			
	      type: "POST",
	      url: '<?php echo $this->Html->url(array(
	   		                             'controller'=>"consultas",
			                             'action'=>'lista'));?>/' + $("#txt_consulta").val() + '/' + $("#listaConexiones").val()+ '/' + p,
	      success: function(msg){
                                         $("#divresultado").empty().html(msg);
			            } 
	   });			

}



</script>


    
