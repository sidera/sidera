<?php

App::uses('AppHelper', 'View/Helper');

class SideraHelper extends AppHelper {
    
    var $helpers  = array('Html','javascript','Session');
    var $columns = array(array(),array());
    
    public function is_odd($value){
        if(is_numeric($value) && !is_null($value)){
            if(is_integer($value)){
                if($value%2==0){
                    return true;
                }
            }
        }
        return false;
    }
    
    /*
     * Las columnas se dibujan en pares (0/1) (2/3) (4,5) 
     */
        public function organizeFieldsInColumns($order, $content,$columna=0){
        if($this->is_odd($order)){
            $this->columns[$columna+1][$order] = $content;
        }else{
            $this->columns[$columna][$order] = $content;
        }
    }
    
    public function organizeSectionsInColumns($order, $content, $section, $title=null){
        if($this->is_odd($order)){
            if($title != null) $this->columns[1]['section'][$section]['title'] = $title;
            $this->columns[1]['section'][$section][$order]  = $content;
         }else{
            if($title != null) $this->columns[0]['section'][$section]['title'] = $title;
            $this->columns[0]['section'][$section][$order]  = $content;
         }
    }
    
    public function drawColumn($column){
        if(isset($this->columns[$column]['section'])){
    	    foreach($this->columns[$column]['section'] as $s=>$section){
			foreach($section as $ind => $sectionElement){
				if(is_numeric($ind)){
					$this->columns[$column][$ind]=$this->columns[$column]['section'][$s];    
					break;
				}
			}
            }                    
            unset($this->columns[$column]['section']);     
        }
        ksort($this->columns[$column]);
        
        echo '<div class="row-fluid">';
        if(isset($this->columns[$column]['title'])) echo '<div class="section-title"> '.$this->columns[$column]['title'].'</div>';
        foreach ($this->columns[$column] as $key => $value) {
            if(is_numeric($key)){
                echo $value;
            }

        }
        echo "</div>";   
	}  
}