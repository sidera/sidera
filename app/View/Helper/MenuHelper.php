<?php
 /*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt
    
 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppHelper', 'View/Helper');

class MenuHelper extends AppHelper {
    
    var $helpers = array('Html','javascript','Session');
    
    private $menu;
    private $usuario;
    private $acl;
    
    public function pintaMenu($usuario, $acl, $recargarMenuDinamico = false){
        $this->usuario = $usuario;
        $this->acl     = $acl;
        
        $this->pintaMenuSuperior();
        if(!$recargarMenuDinamico) $this->pintaMenuEstaticoDerecha();
    }
    
    private function pintaMenuSuperior() {
        $menus = $this->getMenus();
        $menuCanvas = "";
        foreach ($menus as $menu) {
            $menuCanvas .= $this->dibujaMenu($menu);
        }
        $menuCanvas = $this->Html->tag('ul', $menuCanvas, array('class' => 'nav navbar-nav'));
        $menuCanvas = $this->Html->tag('span', $menuCanvas, array('id' => 'menu_parte_superior'));
        $menuCanvas = $this->Html->tag('div', $menuCanvas, array(
            'id' => 'menuCabecera',
            'class'=>"columna col-md-3 collapse navbar-collapse"));
        echo $menuCanvas;
    }

    private function getMenus(){
        App::import('Model', 'Menu');
        $this->menu  = new Menu();
        $menus = $this->menu->find('all',array(
            'conditions' => array(
                'Menu.menu_id' => 0),
            'order'      => 'Menu.orden'));
        return $menus;
    }
    
    private function dibujaMenu($menu) {
        $menuCanvas = "";
        if ($this->estaMenuActivo($menu) && $this->tieneSolicitantePermisoEnMenu($menu)) {
            $menuCanvas .= $this->creaMenu($menu);
            if($this->menuTieneHijos($menu)){
                $hijos = $this->obtenerHijos($menu);
                $menuCanvasHijo = '';
                foreach ($hijos as $menuHijo) {
                    $menuCanvasHijo .= $this->dibujaMenu($menuHijo);
                }
                $menuCanvas .= $this->Html->tag('ul',$menuCanvasHijo , array('class' => 'dropdown-menu'));
            }
            if(!$this->menuEsPrincipal($menu) && $this->menuTieneHijos($menu)){ 
                $menuCanvas = $this->Html->tag('li',$menuCanvas , array('class' => 'dropdown-submenu'));
            }else{
                $menuCanvas = $this->Html->tag('li',$menuCanvas , array('class' => 'dropdown','id'=>'menu_sup'.$menu['Menu']['id']));
            }
        }
        return $menuCanvas;
    }
    
    private function estaMenuActivo($menu) {
        if ( $menu['Menu']['estado'] == "Activo" ) return true;
        return false;
    }
    
    private function tieneSolicitantePermisoEnMenu($menu) {
       if( $this->tieneGrupoPermisoEnMenu($menu) || $this->tieneUsuarioPermisoEnMenu($menu) ) return true;
       return false;
    }
    
    private function tieneGrupoPermisoEnMenu($menu) {
        if($this->checkMenuAcoExists($menu)){
            if($this->acl->check( 
                    array('model' => 'Grupo', 'foreign_key' => $this->usuario['grupo_id']),
                    array('model' => 'Menu', 'foreign_key' => $menu['Menu']['id']))){
                return true;
            }
        }
        return false;
    }
    
    private function tieneUsuarioPermisoEnMenu($menu) {
        if($this->checkMenuAcoExists($menu)){
            if($this->acl->check( 
                    array('model' => 'Usuario', 'foreign_key' => $this->usuario['id']), 
                    array('model' => 'Menu', 'foreign_key' => $menu['Menu']['id']))){
                return true;
            }
        }
        return false;
    }
    
    private function checkMenuAcoExists($menu){
        try{
            $menuNode = $this->acl->Aco->node(array('model' => 'Menu','foreign_key' => $menu['Menu']['id']));
            if (isset($menuNode)) {
                return true;
            }
            return false;
        }catch(CakeException $e){
            return false;
        }
    }
    
    private function creaMenu($menu){
        $menuCanvas = "";
        if ( $this->menuEsPrincipal($menu) ) {
            if($this->esMenuSistema($menu)) {
                $menuCanvas = $this->pintarMenuSistema();
            } else {
                $menuCanvas = $this->pintarMenuPrincipal($menu);
            }
        }else{
            $menuCanvas = $this->pintarSubmenu($menu);
        }
        return $menuCanvas;
    }
    
    private function menuEsPrincipal($menu){
        if($menu['Menu']['menu_id']==0) return true;
        return false;
    }
    
    private function esMenuSistema($menu){
        if($menu['Menu']['id']==1) return true;
        return false;
    }
    
    private function pintarMenuSistema(){
        $icono = $this->Html->tag('i', '', array('class' => 'fa fa-cogs fa-lg '));
        $menuCanvas = $this->Html->tag('a',$icono, array(
                    'class' => 'dropdown-toggle ', 
                    'data-toggle' => 'dropdown',
                    'href'=>'#'));
        return $menuCanvas;
    }
    
    private function pintarMenuPrincipal($menu){
        $icon = $this->Html->tag('i', '', array( 'class' => ''));
        $href = '#';
        if($this->menuTieneControlador($menu)){
            $href = './'.$menu['Menu']['controlador'].'/'.$menu['Menu']['accion'];
        }
        $iconoDespliegue = "";
        if( $this->menuTieneHijos($menu)){
            $iconoDespliegue = $this->Html->tag('b','',array('class'=>'caret'));
        }
        $menuCanvas = $this->Html->tag('a',$icon.$menu['Menu']['titulo'].$iconoDespliegue, array(
            'href' => $href,
            'data-toggle' => 'dropdown'));
        return $menuCanvas;
    }
    
    private function menuTieneControlador($menu) {
        if ($menu['Menu']['controlador']) return true;
        return false;
    }
    
    private function pintarSubmenu($menu){
        if($this->menuTieneControlador($menu)){
            if(!$this->menuTieneIcono($menu)){
                $menu['Menu']['icono'] = "fa fa-chevron-right";
            }
            $icon = $this->Html->tag('i', '', array('class' => ''.$menu['Menu']['icono'].' '));
            $href = '#';
            $href = './'.$menu['Menu']['controlador'].'/'.$menu['Menu']['accion'];
        }else{
            if(!$this->menuTieneIcono($menu)){
                $menu['Menu']['icono'] = "fa fa-reorder";
            } 
            $icon = $this->Html->tag('i', '', array('class' => ''.$menu['Menu']['icono'].' '));
            $href = '#';
            $caret = $this->Html->tag('span', '', array('class' => 'fa fa-caret-right'));
        }
        
        $menuCanvas = $this->Html->tag('a',$icon." ".$menu['Menu']['titulo']." ".$caret, array(
            'href' => $href));
        return $menuCanvas;
    }
    
    private function menuTieneIcono($menu){
        if($menu['Menu']['icono']) return true;
        return false;
    }
    
    private function menuTieneHijos($padre) {
        $hijos = $this->menu->find('count',array('conditions' => array('Menu.menu_id' => $padre['Menu']['id'])));
        if ($hijos >= 1) return true;
        return false;
    }

    private function obtenerHijos($padre) {
        $hijos = $this->menu->find('all',array(
            'conditions' => array(
                'Menu.menu_id' => $padre['Menu']['id']),
            'order'      => 'Menu.orden'));
        return $hijos;
    }

    private function pintaMenuEstaticoDerecha(){
        $menuCanvas  = $this->pintaMenuUsuario();
        $menuCanvas .= $this->pintaMenuAyuda();
        if($this->acl->check(
        	array('model' => 'Grupo', 'foreign_key' => $this->usuario['grupo_id']),
        	'Elfinderfiles/vista') == 1){
        	$menuCanvas .= $this->pintaMenuContenido();
        }
        
        $menuCanvas = $this->Html->tag('ul',$menuCanvas,array('id'=>'menuSuperiorDerechaL0','class'=>'dropdown nav navbar-nav'));
        $menuCanvas = $this->Html->tag('span', $menuCanvas, array('class' => ''));
        $menuCanvas = $this->Html->tag('div', $menuCanvas, array(
            'id' => 'menuSuperiorDerecha',
            'class'=>"columna col-md-3 navbar-right collapse navbar-collapse"));
        echo $menuCanvas;
    }
    
    private function pintaMenuUsuario(){
        $icono         = $this->Html->tag('i', '', array('class' => 'fa fa-user fa-lg', 'title'=>'Usuario'));
        $menuItem      = $this->Html->tag('a',$icono, array( 'href'=>'#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'));
        
        $icono         = $this->Html->tag('i', '', array('class' => 'fa fa-user', 'title'=>'Usuario'));
        $userName      = $this->Html->tag('a',$icono." ".$this->Session->read('Auth.User.nombre'), array( 
			'href'        => './usuarios/pass/'.$this->Session->read('Auth.User.id'),
			'class'       => 'menu-header',
        	'title'       => "Haga clic para cambiar sus datos", 
            'data-toggle' => 'dropdown'));
        $userNameItem  = $this->Html->tag('li',$userName);
        $cleanCacheItem = $cleanLogsItem = "";
        if($this->isUserOnAdminGroup()){
            $icono          = $this->Html->tag('i', '', array('class' => 'fa fa-trash'));
            $cleanCache     = $this->Html->tag('a',$icono." Limpiar Cach�", array('href'=>'configuraciones/borrarCache','class' => 'noResponseAction'));
            $cleanCacheItem = $this->Html->tag('li',$cleanCache);

            $icono          = $this->Html->tag('i', '', array('class' => 'fa fa-trash'));
            $cleanLogs      = $this->Html->tag('a',$icono." Limpiar Logs", array('href'=>'logmessages/borrarLogs','class' => 'noResponseAction'));
            $cleanLogsItem  = $this->Html->tag('li',$cleanLogs);
            
            $icono          = $this->Html->tag('i', '', array('class' => 'fa fa-warning-sign'));
            $showLogs       = $this->Html->tag('a',$icono." Mostrar Logs", array('href'=>'logmessages/getErrorLogMessages','target'=>'_blank'));
            $showLogsItem   = $this->Html->tag('li',$showLogs);
        }
        
        $divider       = $this->Html->tag('li', '', array('class' => 'divider'));
        $icono         = $this->Html->tag('i', '', array('class' => 'fa fa-off'));
        $exitLink      = $this->Html->tag('a',$icono." Salir", array('href'=>'usuarios/logout','class' => ''));
        $exitLinkItem  = $this->Html->tag('li',$exitLink);
        
        $menuList      = $this->Html->tag('ul',$userNameItem.$divider.$cleanCacheItem.$cleanLogsItem.$showLogsItem.$divider.$exitLinkItem,array('class'=>'dropdown-menu'));
        
        $menuCanvas    = $this->Html->tag('li',$menuItem.$menuList,array('class'=>'dropdown pull-right'));
        return $menuCanvas;
    }
    
    private function pintaMenuAyuda(){
        $icono         = $this->Html->tag('i', '', array('class' => 'fa fa-question fa-lg', 'title'=>'Ayuda'));
        $menuItem      = $this->Html->tag('a',$icono, array( 'href'=>'#', 'class' => 'dropdown-toggle ', 'data-toggle' => 'dropdown'));
        
         $icono       = $this->Html->tag('i', '', array('class' => 'fa fa-book'));
         if($this->isUserOnAdminGroup()){
              $manual       = $this->Html->link('Manuales','http://sidera2.wordpress.com',array('target'=>'_blank'));
         }else{
              $manual       = $this->Html->link('Manuales','http://sidera2.wordpress.com/2014/12/02/manual-de-sidera-entorno/',array('target'=>'_blank'));
         }
        $manualItem  = $this->Html->tag('li',$manual);
        
        $menuList    = $this->Html->tag('ul',$manualItem,array('class'=>'dropdown-menu'));
        
        $menuCanvas  = $this->Html->tag('li',$menuItem.$menuList,array('class'=>'dropdown pull-right'));
        return $menuCanvas;
    }
    

    private function pintaMenuContenido(){
        $icono         = $this->Html->tag('i', '', array('class' => 'fa fa-folder-open  fa-lg', 'title'=>'Contenido'));
        $menuItem      = $this->Html->tag('a',$icono, array( 
                            'class' => ' menu-header contenido', 
                            'href'  => './elfinderfiles/vista'));
        
        $menuCanvas    = $this->Html->tag('li',$menuItem,array('id'=>'elementoContenido','class'=>'dropdown pull-right'));
        return $menuCanvas;
    }
    
       
    private function isUserOnAdminGroup(){
        if($this->usuario['grupo_id'] == 1) return true;
    }
    
    private function isUserOnCoordinatorGroup(){
        if($this->usuario['grupo_id'] == 3) return true;

    }
}
