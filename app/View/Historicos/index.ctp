<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); //Bloque para mensajes con setFlash ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php if($configModulo["general"]["botones"]["btNuevo"]) echo $this->element("ElementsVista".DS."Index".DS."botonNuevo"); //Elemento con funcionalidad de boton de Nuevo ?>
        <?php if($configModulo["general"]["botones"]["btEditar"]) echo $this->element("ElementsVista".DS."Index".DS."botonEditar"); //Elemento con funcionalidad de boton de Consultar ?>
        <?php if($configModulo["general"]["botones"]["btEliminar"]) echo $this->element("ElementsVista".DS."Index".DS."botonEliminar"); //Elemento con funcionalidad de boton de Eliminar ?>
        <?php if($configModulo["general"]["botones"]["btFiltrar"]) echo $this->element("ElementsVista".DS."Index".DS."botonFiltrar"); //Elemento con funcionalidad de boton de Filtrar ?>
        <?php if($configModulo["general"]["botones"]["btImportar"]) echo $this->element("ElementsVista".DS."Index".DS."botonImportar"); //Elemento con funcionalidad de boton de Importar ?>
        <?php if($configModulo["general"]["botones"]["btExportar"]) echo $this->element("ElementsVista".DS."Index".DS."botonExportar"); //Elemento con funcionalidad de boton de Exportar ?>
        <?php if($configModulo["general"]["botones"]["btconfigurar"]) echo $this->element("ElementsVista".DS."Index".DS."botonConfigurar"); //Elemento con funcionalidad de boton de Configurar ?>
        <?php if($configModulo["general"]["acciones"]["accionEditar"]) echo $this->element("ElementsVista".DS."Index".DS."accionEditar"); //Elemento con funcionalidad de Editar al hacer click ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."accionOrdenarColumnasGrid"); //Elemento con funcionalidad de Ordenar las Columnas del Grid al hacer click ?>
        <?php if($configModulo["general"]["acciones"]["accionChekearTodos"]) echo $this->element("ElementsVista".DS."Index".DS."accionCheckearTodos"); //Elemento con funcionalidad de Checkear Todos en el grid ?>
      </div>
      <div class="btn-group pull-right right span4 input-xxlarge">
        <?php if($configModulo["general"]["botones"]["btBuscar"]) echo $this->element("ElementsVista".DS."Index".DS."searchForm",array("model"=>"$controlador"));?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered">
        <div>
            <div class="box-header span4">
                <i class="fa fa-user"></i> <?php if($configModulo["general"]["config"]["etiquetaModulo"]) echo __($configModulo["general"]["config"]["etiquetaModulo"]); else echo __("$modelo");?>
            </div>
            <div class="groupPagination span6 pull-right">
                <?php echo $this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>$modelo));?>
                <?php echo $this->element("ElementsVista".DS."Index".DS."registrosPorPagina");?>
            </div>
        </div>
        <div role="grid" class="dataTables_wrapper" id="DataTables_Table_0_wrapper">
            <table class="table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="ui-state-default check" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                            <i class="fa fa-check"></i>
                        </th>
                       	<?php $id="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"id","$configModulo"=>$configModulo));?><?php $id="fin";?>
						<?php $usuario="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"usuario","$configModulo"=>$configModulo));?><?php $usuario="fin";?>
						<?php $modulo="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"modulo","$configModulo"=>$configModulo));?><?php $modulo="fin";?>
						<?php $accion="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"accion","$configModulo"=>$configModulo));?><?php $accion="fin";?>
						<?php $datos_antiguo="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"datos_antiguo","$configModulo"=>$configModulo));?><?php $datos_antiguo="fin";?>
						<?php $created="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"created","$configModulo"=>$configModulo));?><?php $created="fin";?>
					    <?php $espacioFuturosCampos=true; ?>
               </tr>
                </thead>
                <tbody class="griddata">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?php echo $registro[$modelo]["id"];?>" class="gradeA" title="Haga clic para editar el registro"> 
                        <td class="checkCell">
                            <input type="checkbox" value="<?php echo $registro[$modelo]["id"];?>" class="itemSelector" title="Seleccione los elementos que desee eliminar">
                        </td>
                        <td class="id"><?php echo h($registro[$modelo]["id"]); ?></td>
						<td class="usuario"><?php echo h($registro[$modelo]["usuario"]); ?></td>
						<td class="modulo"><?php echo h($registro[$modelo]["modulo"]); ?></td>
						<td class="accion"><?php echo h($registro[$modelo]["accion"]); ?></td>
						<td class="datos_antiguo"title="<?php echo h($registro[$modelo]["datos_antiguo"]);?>"><?php echo h(String::truncate($registro[$modelo]["datos_antiguo"]),200); ?></td>
						<td class="created"><?php echo $this->Time->format('d/m/Y h:m:s',$registro[$modelo]["created"]); ?></td>
						
                        <?php $espacioFuturosCampos=true; ?>
                        </tr>
                     <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript" id="myScript">
//<![CDATA[
$(document).ready(function() { 
    $(":checkbox").uniform();    
    <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); //Elemento con el javascript requerido para esta vista ?>
    <?php echo $this->element("ElementsVista".DS."Index".DS."accionMostrarEnIndex"); // Elemento con funcionalidad de mostrar (o no) columnas en el grid ?>
});
//]]>
</script>