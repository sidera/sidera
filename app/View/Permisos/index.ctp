<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
        <?php echo $this->element("ElementsVista".DS."Index".DS."botonNuevo"); ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."botonEliminar"); ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."accionEditar"); ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."accionOrdenarColumnasGrid");  ?>
        <?php echo $this->element("ElementsVista".DS."Index".DS."accionCheckearTodos"); ?>
        <button class="btn btn-default" id="checkRecursos" data-loading-text="Comprobando...>
            <i class="fa fa-refresh  "></i> Comprobar Recursos 
        </button>
        <button class="btn btn-default" id="syncRecursos">
            <i class="fa fa-refresh  "></i> Sinc. Recursos
            <img src="img/sidera/loading.gif" style="display:none;width:12px;" id="syncLoading">
        </button>
      </div>
      <div class="btn-group pull-right right span4 input-xxlarge">
        <?php echo $this->element("searchForm",array("model"=>"$controlador"));?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered span8">
        <div>
            <div class="box-header span4">
                <i class="fa fa-user"></i> <?php if($parametrosModulo[etiqueta]) echo __("$parametrosModulo[etiqueta]"); else echo __("$modelo");?>
            </div>
             <div class="groupPagination span6 pull-right">
                <?php echo $this->element("pagination",array("model"=>$modelo));?>
                <?php echo $this->element("ElementsVista".DS."registrosPorPagina");?>
           </div>
        </div>
        <div role="grid" class="dataTables_wrapper" id="DataTables_Table_0_wrapper">
            <table class="table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="ui-state-default check">
                            <i class="fa fa-check"></i>
                        </th>
                        <th class="ui-state-default nombre orderable">
                            <?php echo $this->Paginator->sort("Solicitante"); ?>
                             <?php if($this->Paginator->sortKey() == 'Solicitante'):?> 
                             <span class="DataTables_sort_icon css_right ui-icon 
                                ui-fa fa-carat-1-<?php if($this->Paginator->sortDir()=='asc') echo'n';else echo 's';?>"></span>
                             <?php endif;?>
                        </th>
                    </tr>
                </thead>
                <tbody class="griddata">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?php echo $registro["Permitido"]["id"];?>" class="gradeA">
                        <td><input type="checkbox" value="<?php echo $registro["Permitido"]["id"];?>" class="itemSelector" title="Seleccione los elementos que desee eliminar"></td>
                        <td><?php echo $registro["Permitido"]["model"].'/'.$solicitantes[$registro["Permitido"]["model"]][$registro["Permitido"]["foreign_key"]]; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div style="position:relative;" class="span1">
    <div style="position:relative;" class="box bordered span7">
        <div class="padded">
            <div class="section-title">Resultados de la Sincronización</div>
            <div class="chart-container logErrorMessages" id="logSync"></div>
        </div>
    </div>
</div>
<script type="text/javascript" id="myScript">
$(document).ready(function() { 
    var controlador="<?php echo $controlador;?>"; 
    var modelo="<?php echo $modelo;?>";
    $(":checkbox").uniform();
    <?php echo $this->element("ElementsVista".DS."javascriptRequerido"); ?>
    
    function check(){
        $.get("permisos/checktosync",function(data){
            var obj = jQuery.parseJSON(data);
            $("#logSync").html('');
            if(obj.sync=='TRUE'){
                var message='';
                $.each(obj.message,function(key,value){message += value;});
                $("#logSync").html(message);
            }
            $('#checkRecursos').button('reset');
        });
    }
    $('#checkRecursos').click(function(event){
        event.preventDefault();
        $(this).button('loading');
        check();
    });
    $('#syncRecursos').click(function(event){
        event.preventDefault();
        $("#logSync").html('');
        $.get("permisos/sync",function(data){
            var obj = jQuery.parseJSON(data);
            if(obj.sync=='TRUE'){
                var message='';
                $.each(obj.message,function(key,value){message += value;});
                $("#logSync").html(message);
            }
            $('#checkRecursos').button('reset');
        });
    });
    $(".griddata td:first-child ").bind("click", 
        function (event) {
            event.stopPropagation();
            if($('.itemSelector').filter(':checked').length > 0){
                $("#delMenu").removeClass("disabled");
            }else{
                $("#delMenu").addClass("disabled");
            }
            //colorea las filas checkeadas    
            if ($(this).children().children().children().is(':checked')){
                $(this).parent().addClass('row-selected');
            }else{
                $(this).parent().removeClass('row-selected');
            }
        }
    );
});
</script>
