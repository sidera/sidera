<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <?php echo $this->Form->create("$modelo");?>
<div class="row-fluid">
    <?php echo $this->Element("ElementsVista".DS."flashMessages"); ?>
    <hr class="main">
    <div class="container-fluid actionsBar">
      <div class="btn-group">
       <button class="btn btn-default" id="permisoAddRegistro">
            <i class="fa fa-plus-circle  "></i> Guardar</button>
        <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
      </div>
    </div>
    <hr class="main">
</div>
<div class="container-fluid">
    <div class="box bordered form">
        <div class="box-header">
           <i class="fa fa-lock"></i>Nuevo Permiso
        </div>
        <div class="row-fluid">
            <div class="row-fluid padded">
                <label for="aro_id">Solicitante</label>
                <select class="input-xlarge chzn-select" id="aro_id" name="data[Permiso][aro_id]">
                <?php
                    foreach($permitidos as $permitido){
                        if( $solicitantes[$permitido['Permitido']['model']][$permitido['Permitido']['foreign_key']] != null){
                            echo "<option value='".$permitido['Permitido']['id']."'>";
                            echo $permitido['Permitido']['model']."/".$solicitantes[$permitido['Permitido']['model']][$permitido['Permitido']['foreign_key']];
                            echo "</option>";
                        }
                    }
                ?>
                </select>
            </div>
            <div class="row-fluid">
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active"><a href="#acciones">Acciones</a></li>
                    <li><a href="#menu">Men�s</a></li>
                </ul>
                <div class="tab-content padded">
                    <div class="tab-pane active" id="acciones">
                        <div class="controls controls-row">
                            <div class="span3">
                                <label>Recursos</label>
                                <?php echo $this->Form->select("modulos",$modulos,array(
                                    "multiple"    => true,
                                    "class"       => "input-xlarge longList",
                                    "name"        => "",
                                    "hiddenField" => false));?>
                            </div>
                            <div class="span1 permisosButtons">
                                <div><button title="Haz clic para a�adir permisos" id="addPermiso" class="btn btn-small yellow"><i class="fa fa-chevron-right"></i></button></div>
                                <div><button title="Haz clic para eliminar permisos" id="removePermiso" class="btn btn-small yellow"><i class="fa fa-chevron-left"></i></button></div>
                            </div>
                            <div class="span3">
                                <label for="PermisoModulos">Permitidos</label>
                                <input type="hidden" id="PermisoModulos_" value="" name="data[Permiso][aco_id]">
                                <select id="permisosList" class="input-xlarge longList" multiple="multiple" name="data[Permiso][aco_id][]">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="menu">
                        <div class="controls controls-row">
                            <div class="span3">
                                <label for="MenuModulos">Recurso</label>
                                <?php 
                                    echo $this->Form->select("menus",$menus,array(
                                        "label"    => "Menus",
                                        "type"     => "select",
                                        "multiple" => true,
                                        "class"    => "input-xlarge longList",
                                        "name"     => "",
                                        "hiddenField"=>false,
                                        "id"       => "MenuModulos", 
                                        "div"      => array("class"=>"span3")
                                     ));?>
                            </div>
                            <div class="span1 permisosButtons">
                                <div><button title="Haz clic para a�adir menus" id="addMenu" class="btn btn-small yellow"><i class="fa fa-chevron-right"></i></button></div>
                                <div><button title="Haz clic para eliminar menus" id="removeMenu" class="btn btn-small yellow"><i class="fa fa-chevron-left"></i></button></div>
                            </div>
                            <div class="span3">
                                <label for="MenuList">Visibles</label>
                                <select id="menusList" class="input-xlarge longList" multiple="multiple" name="data[Permiso][aco_id][]">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
             </div>   
        </div>
     </div>
</div>
<?php echo $this->Form->end();?>
<script type="text/javascript">
$(document).ready(function() {
    $(".chzn-select").chosen();
    var controlador="<?php echo $controlador;?>"; 
    var modelo="<?php echo $modelo;?>";  
    var options = {target:"#cuerpoApp"}; 
    $('#myTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
    $('#'+"<?php echo $modelo;?>"+'AddForm').ajaxForm(options);
    $('#permisoAddRegistro').click(function(event){
        event.preventDefault();
        $("#permisosList > optgroup > option").each(function(){ $(this).attr("selected",true); });
        $("#menusList > optgroup > option").each(function(){ $(this).attr("selected",true); });
        $("#<?php echo $modelo;?>AddForm").ajaxSubmit({success:function(data){$('#cuerpoApp').load('permisos/index');}});
    });
    
    $("#PermisoModulos").removeAttr('name');
    $("#MenuModulos").removeAttr('name');
    $('#PermisoModulos > optgroup').click(function () {
        $(this).children().attr('selected', true);
    });
    $('#PermisoModulos > optgroup > option').click(function (e) {
        if (!e) var e = window.event;
        e.cancelBubble = true;
        e.stopPropagation();
    });
    $("#permisosList > optgroup").on('click',function () {
        $(this).children().attr('selected', true);
    });
    $('#permisosList > optgroup > option').on('click',function (e) {
        if (!e) var e = window.event;
        e.cancelBubble = true;
        e.stopPropagation();
    });
    function addItem(source,target,optgroup){    
        if(optgroup==true){
            var existGroup=0;
            var group=$(source+' option:selected').parent("optgroup").attr('label');
            var parent = $(source+' option:selected').closest('optgroup');
            $(target + ' optgroup').each(function(key,value){
                if(value.label==group){
                    $(source+' option:selected').remove().appendTo(this);
                    existGroup=1;
                }
            });
            if(existGroup==0) {
                var optgroup = $('<optgroup/>');
                optgroup.attr('label', $(source+' option:selected').parent("optgroup").attr('label'));
                $(source+' option:selected').remove().appendTo(target).wrapAll(optgroup);
            }
            $(target+' optgroup').each(function(key,value){
                $(this).children().attr('selected', false);
            });
            if(parent.children().length==0) parent.remove();
        }else{
            $(source+' option:selected').remove().appendTo(target);
        }
    }
    function removeItem(source,target,optgroup){
        if(optgroup==true){
            var existGroup=0;
            var parent = $(source+' option:selected').closest('optgroup');
            var group=$(source+' option:selected').parent("optgroup").attr('label');
            $(target+' optgroup').each(function(key,value){
                if(value.label==group){
                    $(source+' option:selected').remove().appendTo(this);
                }
            });
            if(existGroup==0) {
                var optgroup = $('<optgroup/>');
                optgroup.attr('label', $(source+' option:selected').parent("optgroup").attr('label'));
                $(source+' option:selected').remove().appendTo(target).wrapAll(optgroup);
            }
            $(target+' optgroup').each(function(key,value){
                $(this).children().attr('selected', false);
            });
            if(parent.children().length==0) parent.remove();
        }else{
            $(source+' option:selected').remove().appendTo(target);
        }
    }
    
    $('#addPermiso').click(function(e) {  
        e.preventDefault();
        addItem('#PermisoModulos','#permisosList',true);
    });
    
    $('#removePermiso').click(function(e) {  
        e.preventDefault();
        removeItem('#permisosList','#PermisoModulos',true);
    });
    
    $('#addMenu').click(function(e) {  
        e.preventDefault();
        addItem('#MenuModulos','#menusList',false);
    });
    
    $('#removeMenu').click(function(e) {  
        e.preventDefault();
        removeItem('#menusList','#MenuModulos',false);
    });
    
}); 
</script>