<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

App::uses('AppController', 'Controller');

class MenusController extends AppController {

    public  $components      = array('Configurar');
    public  $layout          = "ajax";
    private $tablasPrivadasSistema = TABLAS_SISTEMA;

    public function beforeFilter(){
       parent::beforeFilter();
        $this->modelo      = "Menu";
        $this->controlador = "menus";
        $this->modelosAjenos = array();
        $this->modelosAjenosRelacionados = array();
   }
   
    public function index() {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        
        $this->comprobarPaginacion();
        $this->Esquema->comprobarSessionModeloYML($modelo);
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $camposModelo = $configModulo[general][campos];
       // $campos_visibles_en_index   = $this->ComponenteControlador->obtenerCamposAMostrarEnIndex($configModulo,$camposModelo,$modelo);
        
        $fieldsForSearch            = $this->Esquema->obtenerCamposABuscarEnIndex($this,$configModulo,$camposModelo,$modelo);
        $this->$modelo->recursive = 0;
        $this->comprobarFiltroActivo();
        $this->comprobarBusquedaActiva($fieldsForSearch);
        
        //$obj->paginate['fields']    = $campos_visibles_en_index;
        $this->paginate['limit']     = $configModulo['general']['config']['registrosPagina'];
        $this->paginate['order']     = array("$modelo.id" => 'DESC');
        
        if (!MOSTRAR_MENUSSISTEMA){
            $this->paginate['conditions']['AND'][0]['NOT']['Menu.ruta LIKE']="%>Sistema%";
            $this->paginate['conditions']['AND'][1]['NOT']['Menu.ruta LIKE']="%>Ayuda%";
            $this->paginate['conditions']['AND'][2]['NOT']['Menu.ruta LIKE']="%>Mensajes%";
        }
        
        $registros=$this->paginate();
        
        $menuses = $this->Menu->find('all',array('order'=>'Menu.menu_id ASC,Menu.orden ASC'));
        
        $this->set('menuses',$menuses);
	$this->set('menusPadre',$this->Menu->MenuParent->find('list',array('fields'=>array('MenuParent.id','MenuParent.ruta'))));
        $this->set(compact('registros','modelo','controlador','configModulo','menuses','menusPadre'));
   
    }

    public function add() {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        $mostrarMenusSistema = $this->mostrarMenusSistema;
        
        $this->set('modelo', $modelo);
        $this->set('controlador', $controlador);
        $tablasSistema=unserialize (TABLAS_SISTEMA);
        $this->set('tablasSistema', $tablasSistema);
        $this->checkModal();
        //para que muestre los menus propios de Sidera quitar de la condicion --> 'MenuParent.ruta NOT LIKE' => '%>Sistema%'
        if ($mostrarMenusSistema){
            $this->set('menusPadre', array(0=>'') + $this->Menu->MenuParent->find('list',array(
                'conditions NOT' => array( 
                    'AND' => array(
                        'MenuParent.ruta  LIKE' => '>%>%>%')),
                'order' => array(
                    'MenuParent.ruta'),
                'fields' => array(
                    'MenuParent.id',
                    'MenuParent.ruta'))));
        }else{
            $this->set('menusPadre', array( 0 => '' ) + $this->Menu->MenuParent->find('list',array(
                'conditions' => array(
                    'AND NOT' => array(
                        //Mantener los espacios para no machacar las claves del registro
                        'MenuParent.ruta  LIKE' => '>%>%>%',
                        'MenuParent.ruta   LIKE' =>'%>Sistema%',
                        'MenuParent.ruta    LIKE' => '%>Ayuda%',
                        'MenuParent.ruta     LIKE' => '%>Mensajes%')),
                'order' => array( 
                        'MenuParent.ruta' ),
                'fields' => array(
                        'MenuParent.id',
                    'MenuParent.ruta'))));
        } 
    }
    
    public function create(){
        $modelo     =$this->modelo;
        $controlador=$this->controlador;
        
         if ((substr_count($this->request->data[Menu][ruta],">")<3)||(!$this->request->data[Menu][controlador]=="")){
                if (!$this->request->data['Menu']['menu_id']){
                            $this->request->data['Menu']['ruta']=">".$this->request->data[Menu][titulo];
                            $this->request->data['Menu']['menu_id']=0;
                }
                $this->Menu->create();
                if ($this->Menu->save($this->request->data)) {
                    $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
                } else {
                    $this->Session->setFlash(MSG_SAVEDERROR, ERROR);
                }
                  $this->redirectSidera($controlador,"index");   
            }
             
      
    }     

    public function edit($id = null) {
            $modelo       = $this->modelo;
            $controlador  = $this->controlador;
            $mostrarMenusSistema = $this->mostrarMenusSistema;
            
            $this->set('modelo', $modelo);
            $this->set('controlador', $controlador);
            $tablasSistema=unserialize (TABLAS_SISTEMA);
            $this->set('tablasSistema', $tablasSistema);
            //para que  muestre los menus propios de Sidera quitar de la condicion --> 'MenuParent.ruta NOT LIKE' => '%>Sistema%'
             if ($mostrarMenusSistema){
                $this->set('menusPadre',array(0=>'') + $this->Menu->MenuParent->find('list',array(
                    'conditions' => array( 
                            'AND NOT' => array( 
                                'MenuParent.ruta LIKE' => '>%>%>%')),
                    'order'=>array(
                            'MenuParent.ruta'),
                    'fields'=>array(
                            'MenuParent.id','MenuParent.ruta'))));
            }else{
                
                $this->set('menusPadre',array(0=>'') + $this->Menu->MenuParent->find('list',array(
                    'conditions' => array( 
                            'AND NOT' => array( 
                                //Mantener los espacios para no machacar las claves del registro
                                'MenuParent.ruta LIKE' => '>%>%>%',
                                'MenuParent.ruta  LIKE' => '%>Sistema%',
                                'MenuParent.ruta   LIKE' => '%>Ayuda%',
                                'MenuParent.ruta    LIKE' => '%>Mensajes%')),
                    'order' => array(
                             'MenuParent.ruta'),
                    'fields' => array(
                             'MenuParent.id','MenuParent.ruta'))));
            }
            $this->set(compact('menusPadre'));
            $this->Menu->id = $id;
            if ((substr_count($this->request->data[Menu][ruta],">")<3)||(!$this->request->data[Menu][controlador]=="")){
                    if (!$this->Menu->exists()) {
                        throw new NotFoundException(__('Men� no v�lido'));
                    }
                    
                    $registro = $this->Menu->read(null, $id);
                    
            }
            
            $this->set(compact('menusPadre','registro'));
    }
    
     public function update($id = null){
         $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        $this->$modelo->id = $id;
        
         if (!$this->request->data['Menu']['menu_id']){
            $this->request->data['Menu']['ruta']=">".$this->request->data[Menu][titulo];
            $this->request->data['Menu']['menu_id']=0;
        }
            if ($this->Menu->save($this->request->data)) {
                    $this->Session->setFlash(MSG_EDITSUCCESS, SUCCESS);
            } else {
                    $this->Session->setFlash(MSG_EDITERROR, ERROR);
            }	
           //Comprobar si viene de una pagina
            if($this->Session->check("page[$this->modelo]")==TRUE){    
              $this->redirectSidera($controlador,"index/page:".$this->Session->read("page[$this->modelo]"));   
            }else{
              $this->redirectSidera($controlador,"index");        
            }  
    }

    public function delete($id = null) {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->autoRender=false;
        $exploded_id = explode(',', $_POST['id']);
        for($i=0;$i<count($exploded_id);$i++){ 
            $this->Menu->id = $exploded_id[$i];
            if (!$this->Menu->exists()) {
                throw new NotFoundException(__('Men� no v�lido'));
            }
            $this->Menu->delete();
        }
        $this->Session->setFlash(MSG_DELETESUCCESS, SUCCESS);
       $this->redirectSidera($controlador,"index");       
    }

    public function draw(){}
    
}
