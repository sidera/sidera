<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014
 *
 * Organization:
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('PermisosController', 'Controller');
App::uses('Model', 'Model');
App::uses('AppModel', 'Model');
App::uses('ConnectionManager', 'Model');

class ConstructoresController extends AppController {

    public  $components = array('Files','Creador');
    public  $layout     = "ajax";
    var $uses           = false;
    protected $conexion   = "default";
    
    public function beforeFilter(){
       parent::beforeFilter();
        $this->modelo      = "Constructor";
        $this->controlador = "constructores";
        $this->modelosAjenos = array();
        $this->modelosAjenosRelacionados = array();
   }
   
    public function index() {
        $modelo          = $this->modelo;
        $controlador     = $this->controlador;
        $this->Esquema->cargarSesionesYML();
        $rutaProyecto    = $this->Files->configModulosProyecto;
        $modulosProyecto = $this->Files->archivosSinExt($rutaProyecto);
        $registros       = $modulosProyecto;
	$this->set(compact('registros','modelo','controlador'));
    }

    public function add() {
        $modelo          = $this->modelo;
        $controlador     = $this->controlador;
        //Para comparar con los nombres de modulo.
        $tablasSistema   = unserialize(TABLAS_SISTEMA);
        $tablasDisponibles = $this->tablasDisponibles();
        
        $this->set(compact('modelo','controlador','tablasSistema','tablasDisponibles'));
    }
    
    public function create() {
        $this->autoRender=false;
         $creacionModulo=$this->Creador->construirModulo($this);
            $controladorACrear = $creacionModulo[1]; 
            switch ($creacionModulo[0]) {
                 case "Creado":
                     $this->Esquema->cargarSessionYML(ucwords(Inflector::singularize($controladorACrear)));
                     $this->requestAction("permisos/syncResourcesForController/".ucwords($controladorACrear));
                     $this->Session->setFlash("El m�dulo ".ucwords($controladorACrear)." ha sido creado correctamente!!.", SUCCESS);
                     break;
                 case "No Creado":
                     $this->Session->setFlash("El men� de ".ucwords($controladorACrear)." no ha sido creado.NO CREADO", ERROR);
                     break;
                 case "No Existe":
                     $this->Session->setFlash("Error existe alg�n archivo o directorio de este m�dulo.NO EXISTE", ERROR);
                     break;
             }
    }

    public function cargar($tabla = null) {
	$tabla                 = $_POST['tabla'];
        $conexion              = $_POST['conexion'];
        $modeloDeTabla         = ucwords(Inflector::singularize($tabla));
        $this->loadModel($modeloDeTabla);
        $this->$modeloDeTabla->useDbConfig = $this->conexion;
        $schema                = $this->$modeloDeTabla->schema(true);
        $propiedadesDeTabla    = $this->Esquema->propiedadesModelo($schema);	
        $clavesAjenas          = $this->Esquema->buscarClavesAjenas($this,$modeloDeTabla);
        $relacionesPerteneceA  = $clavesAjenas;
        $relacionesTieneMuchos = $this->Esquema->buscarRelacionesTieneMuchos($this,$modeloDeTabla,$this->conexion);
        $this->set(compact('relacionesPerteneceA','relacionesTieneMuchos','propiedadesDeTabla','menusPadre'));
    }

    public function tablasDisponibles(){
        $db                    = ConnectionManager::getDataSource($this->conexion);
        $tablasConexion        = $db->listSources();        
        $tablasPrivadasSistema = unserialize(TABLAS_SISTEMA);
        $modulosConstruidos    = $this->Files->archivosSinExt($this->Files->configModulosProyecto);
        $tablasConstruidas     = array();
        foreach ($modulosConstruidos as $key => $modulo) {
            $tablasConstruidas[$key] = strtolower(Inflector::pluralize($modulo));
        } 
        $tablasTotales     = array_merge($tablasPrivadasSistema,$tablasConstruidas);
        $tablasDisponibles = array_diff($tablasConexion,$tablasTotales);
        
        $tablasDisponibles=  array_combine(array_values($tablasDisponibles), array_values($tablasDisponibles));
        return $tablasDisponibles;
    }

    public function delete($id = null) {	
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->autoRender = false;
        $exploded_id = explode(',', $_POST['id']);
        for( $i = 0; $i < count($exploded_id); $i++){
            $modeloABorrar = $exploded_id[$i];
            $controladorABorrar = strtolower(Inflector::pluralize($modeloABorrar)); 
            $this->Creador->borrarModulo($modeloABorrar);
            $this->loadModel("Menu");
            $borrarMenu     = $this->Menu->find('all',array('conditions' => array("Menu.controlador" => $controladorABorrar),'fields'=>array("Menu.id")));
            $this->Menu->id = $borrarMenu[0]['Menu']['id'];
            $this->Menu->delete();
            $this->requestAction("permisos/cleanAcosFromConstructor/".$controladorABorrar);
            $this->Session->setFlash("Modulo ".$modeloABorrar." borrado correctamente.", SUCCESS);
        }
        $this->Esquema->borrarSessionYML($modeloABorrar);
        $this->redirectSidera($this->controlador,"index");
    }
    
    public function indexMasivo() {
        $modelo            = $this->modelo;
        $controlador       = $this->controlador;
        $tablasDisponibles = $this->tablasDisponibles("default");
        $this->set(compact('tablasDisponibles','controlador','modelo'));
    }
    
    public  function addMasivo() {
        $controlador    = $this->controlador;
        $this->conexion = "default";
        $menuPadre=null;
        
          $this->tablas     = $this->request->data["nuevos"];
          $this->elementos  = $this->request->data["opciones"];
          $this->propiedadesTablas = $this->Esquema->cargarTodosLosModelos($this,$this->conexion);
          
          if(sizeof($this->tablas)>0){
               $menuContenedor=$this->comprobarMenuPadre();
          }
                
          foreach ($this->tablas as $tabla) {
              $this->generarModulo($tabla);
              $this->comprobarMenuModulo($menuContenedor);
              if ( $this->elementos["controlador"]==ACTIVO){
                  $this->requestAction("permisos/syncResourcesForController/".ucwords($tabla));         
              }
          }
          
          $this->redirectSidera($controlador,"index");   
      }
      
    private function comprobarMenuPadre() {
        $menuPadre = "0";
        if(!empty($this->elementos["menuPadre"]) && $this->elementos["menu"]==ACTIVO){
            $menu["titulo"]  = $this->elementos["menuPadre"];
            $menu["orden"]   = "1";
            $menu["menu_id"] = "0";
            $menu["ruta"]    = ">".$menu["titulo"];
            $menu["estado"]  = "Activo";
            $this->loadModel("Menu");
            $this->Menu->schemaName = null;
            $this->Menu->create();
            $this->Menu->save($menu);
            $menuPadre=$this->Menu->getInsertID();  
        }
        $menu["menuPadre"]= $menuPadre; 
        return $menu;
    }
     
    private function generarModulo($tabla) {
        $modeloDeTabla           = ucwords(Inflector::singularize($tabla)); 
        $this->modeloACrear      = $modeloDeTabla;
        $this->controladorACrear = ucwords($tabla);

        $this->loadModel($modeloDeTabla);
        $this->$modeloDeTabla->useDbConfig = $this->conexion;  
        $schema                  = $this->$modeloDeTabla->schema(true);
        $clavesAjenas            = $this->Esquema->buscarClavesAjenas($this,$modeloDeTabla);
        $this->relacionesPerteneceA  = $clavesAjenas;
        $this->relacionesTieneMuchos = $this->Esquema->buscarRelacionesTieneMuchos($this,$modeloDeTabla,$this->conexion);     
        $this->Creador->construirMasivo($this);
    }

    private function comprobarMenuModulo($contenedor) {
        if($this->elementos["menu"]==ACTIVO){
            $menu["titulo"]  = $this->controladorACrear;
            $menu["accion"]  = "index";
            $menu["orden"]   = "1";
            $menu["controlador"] = $this->controladorACrear;
            $menu["menu_id"] = $contenedor["menuPadre"];
            $menu["estado"] = "Activo";
            if($contenedor["menuPadre"]!="0"){
                $menu["ruta"] = ">".$contenedor["titulo"].">".$this->controladorACrear;
            }else{
                $menu["ruta"] = ">".$this->controladorACrear;
            }

            $this->loadModel("Menu");
            $this->Menu->schemaName = null;
            $this->Menu->create();
            $this->Menu->save($menu);
        }
    }
}
