<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt   

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */


class CombosController extends AppController {

    public $components = array("Files");
    var $layout = "ajax";
    public   $modelo      = "Combo";
    public   $controlador = "combos";
    var $uses = false;
 	
    public function crearCodigoCombosRelacionados($modelo = null, $comboPrincipal = null, $comboRelacionado = null, $avanzado = false,$listaCampos = null, $separador = null) {
        //die(print_r($comboRelacionado));
        $this->autoRender = false;
        $this->Configurar->addNuevaRelacionCombos($this);
        $controlador = Inflector::pluralize($modelo);
        $this->redirectSidera($controlador,"index"); 
	}
    
    public function crearCodigoComboModal($modelo = null, $comboModal = null) {
        //die(print_r($comboModal));
        $this->autoRender = false;
        $this->Configurar->addNuevoComboModal($this);
        $controlador = Inflector::pluralize($modelo);
        $this->redirectSidera($controlador,"index"); 
	}

	public function cargarComboRelacionado() {
        $comboPrincipal = $this->request->data['comboPrincipal'];
        $modeloRelacionado = ucfirst($this->request->data['modeloRelacionado']);
        $comboPrincipalId = $this->request->data['comboPrincipalId'];
        $this->loadModel($modeloRelacionado);
        $datosComboRelacionado = $this->$modeloRelacionado->find('list', array(
            'conditions' => array("$modeloRelacionado.$comboPrincipal" => $comboPrincipalId
                )));
        $this->set(compact('datosComboRelacionado'));
    }
    
    public function cargarComboRelacionadoAvanzado() {
        $comboPrincipal = $this->request->data['comboPrincipal'];
        $modeloRelacionado = ucfirst($this->request->data['modeloRelacionado']);
        $comboPrincipalId = $this->request->data['comboPrincipalId'];
        $listaCampos = $this->request->data['listaCampos'];
        array_unshift($listaCampos, 'id'); 
        $separador = $this->request->data['separador'];
        $this->loadModel($modeloRelacionado);
        $datosComboRelacionado=$this->$modeloRelacionado->find('all',array(
            'conditions' => array("$modeloRelacionado.$comboPrincipal" => $comboPrincipalId),
            'fields' => $listaCampos
            ));
        $this->set(compact('modeloRelacionado','datosComboRelacionado','separador'));
    }
   
}
