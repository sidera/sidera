<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

App::uses("AppController", "Controller");

class MensajesController extends AppController {

    public $components  = array();
    public $helpers     = array("Time");
    var $layout         = "ajax";

    public function beforeFilter(){
       parent::beforeFilter();
        $this->modelo      = "Mensaje";
        $this->controlador = "mensajes";
        $this->modelosAjenos = array("Origen","Destino","Propietario");
        $this->modelosAjenosRelacionados = array();
   }
   
    public function comprobarMensajesNuevos($obj){
        $user_id=$obj->Auth->user('id');
        $numMensajesSinLeer=$obj->Mensaje->find('count',array(
            'conditions' => array('Mensaje.propietario' => $user_id,'Mensaje.destino' => $user_id,'Mensaje.estado' => '1'), 
            'fields' => array('Mensaje.id')
        ));
        return $numMensajesSinLeer;
    }

    public function enviados() {
        $this->cargarDatosVistas();   
		$user_id=$this->Auth->user('id');
        
        $this->paginate['conditions']['AND']["Mensaje.origen ="] = $user_id;    
        $this->paginate['conditions']['AND']["Mensaje.propietario ="] = $user_id; 

        $registros = $this->paginate();
        
        $this->set(compact('registros'));
    }

    public function recibidos() {
        $this->cargarDatosVistas();   
		$user_id=$this->Auth->user('id');
      
        $this->paginate['conditions']['AND']["Mensaje.destino ="] = $user_id;    
        $this->paginate['conditions']['AND']["Mensaje.propietario ="] = $user_id; 
      
        $registros=$this->paginate();
        $this->set(compact('registros'));
    }
        
    private function cargarDatosVistas() {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;

        $this->comprobarPaginacion($this);
        $this->Esquema->comprobarSessionModeloYML($modelo);
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $camposModelo = $configModulo[general][campos];
        
         $fieldsForSearch            = $this->Esquema->obtenerCamposABuscarEnIndex($this,$configModulo,$camposModelo,$modelo);
        $this->$modelo->recursive = 0;
        $this->comprobarFiltroActivo($this);
        $this->comprobarBusquedaActiva($this,$fieldsForSearch);
        
        $mensajesNuevos  = $this->comprobarMensajesNuevos($this);
        $this->set('mensajesNuevos', $mensajesNuevos);
      
        $this->paginate['limit']     = $configModulo['general']['config']['registrosPagina'];
        $this->paginate['order']     = array("$modelo.id" => 'DESC');
        
        
		$user_id=$this->Auth->user('id');
        $this->set('user_id',$user_id);
        
        $this->loadModel('Usuario');
        $this->set('usuarios',$this->Usuario->find('list'));
             
        $this->set(compact('modelo','controlador','configModulo','usuarios','user_id'));

    }
        
    public function add() {
		$modelo      = $this->modelo;
        $controlador = $this->controlador;
        $user_id     = $this->Auth->user('id');
        $this->set('user_id',$user_id);
        $this->loadModel('Usuario');
        $usuariosData = $this->Usuario->find('all',array(
            'recursive' => 1,
            'fields'    => array( 
                'Usuario.id',
                'Usuario.nombre',
                'Usuario.apellido1',
                'Usuario.apellido2',
                'Grupo.nombre'),
            'conditions' => array(
                'Grupo.id !=' => '1',
                'Usuario.id !=' => $user_id),
            'order' => array(
                'Grupo.nombre ASC',
                'Usuario.apellido1 ASC',
                'Usuario.apellido2 ASC',
                'Usuario.nombre ASC')));
        
        foreach($usuariosData as $usuarioData){
            $usuarios[$usuarioData['Grupo']['nombre']][$usuarioData['Usuario']['id']] = 
                    $usuarioData['Usuario']['apellido1'].' '.$usuarioData['Usuario']['apellido2'].', '.$usuarioData['Usuario']['nombre'];
        }
        
        $schema       = $this->$modelo->schema();
        $propiedades  = $this->Esquema->propiedadesModelo($schema); 
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $configModulo = $this->Esquema->identificarCamposObligatorios($configModulo,$propiedades,"add");
        $this->Esquema->obtenerDatosModelosAjenos($this);
        $accion       = "Add";
        $this->set(compact("modelo","propiedades","controlador","accion","configModulo","usuarios","user_id"));
    }
    
    
     public function enviar(){
        $modelo      = $this->modelo;
        $controlador = $this->controlador;
        
        $this->request->data["Mensaje"]["created"]=date('d/m/Y h:i:s', time());
            $destinos=$this->request->data["Mensaje"]['destino'];
            $crear_origen=true;
            foreach ( $destinos as $destino){
                if($destino==$this->request->data["Mensaje"]['origen']) $crear_origen=false;
            }
            if ($crear_origen){
                $this->request->data["Mensaje"]['estado']="0";
                $this->request->data["Mensaje"]['propietario']=$this->request->data["Mensaje"]['origen'];
                $this->request->data["Mensaje"]['destino']=$destinos[0];
                $this->$modelo->create();
                if ($this->$modelo->save($this->request->data)) {
                    $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
                } else {
                    $this->Session->setFlash(MSG_SAVEDERROR, ERROR);
                }
            }

            foreach ( $destinos as $destino){
                $this->request->data["Mensaje"]['estado']="1";
                $this->request->data["Mensaje"]['destino']=$destino;
                $this->request->data["Mensaje"]['propietario']=$destino;
                $this->$modelo->create();
                if ($this->$modelo->save($this->request->data)) {
                    $this->Session->setFlash("El Mensaje se ha enviado correctamente", SUCCESS);
                } else {
                    $this->Session->setFlash("El Mensaje no se ha podido enviar", ERROR);
                }
            }
            $this->redirectSidera($controlador,"recibidos");
    }

    public function edit_enviados($id = null) {
	$modelo=$this->modelo;
        $controlador=$this->controlador;
        
        $this->$modelo->id = $id;
        $this->$modelo->saveField('estado', '0');
        $mensajesNuevos  = $this->comprobarMensajesNuevos($this);
        $this->set('mensajesNuevos', $mensajesNuevos);
        
        $user_id=$this->Auth->user('id');
        $this->set('user_id',$user_id);
        $schema       = $this->$modelo->schema(true);
        
        $this->Esquema->comprobarSessionModeloYML($modelo);
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        
        $origen = $this->Mensaje->find('all',array('fields' => array('Origen.nombre','Origen.apellido1','Origen.apellido2'),'conditions' => array('Mensaje.id =' => $id)));
        $origen=$origen[0]['Origen']['nombre'].' '.$origen[0]['Origen']['apellido1'].' '.$origen[0]['Origen']['apellido2'];
        $this->set('origen',$origen);
        $destino = $this->Mensaje->find('all',array('fields' => array('Destino.nombre','Destino.apellido1','Destino.apellido2'),'conditions' => array('Mensaje.id =' => $id)));
        $destino=$destino[0]['Destino']['nombre'].' '.$destino[0]['Destino']['apellido1'].' '.$destino[0]['Destino']['apellido2'];
      
        $this->set('accion',"Edit");
        $this->set(compact('modelo','controlador','accion','configModulo','origen','destino','mensajesNuevos'));
        
        $this->request->data = $this->$modelo->read(null, $id);
        
	}
        
        public function responderEnviados($id = null) {
            $modelo=$this->modelo;
            $controlador=$this->controlador;
            
            $this->$modelo->id = $id;
        
            $this->request->data['Mensaje']['id'] = '';
            $this->request->data["Mensaje"]["created"]=getdate();
            $this->request->data['Mensaje']['estado']=0;
            $this->request->data['Mensaje']['propietario']=$this->request->data['Mensaje']['origen'];
            $this->$modelo->create();
            if ($this->$modelo->save($this->request->data)) {
                $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
            } else {
                $this->Session->setFlash(MSG_SAVEDERROR, ERROR);
            }
            $this->request->data['Mensaje']['estado']=1;
            $this->request->data['Mensaje']['propietario']=$this->request->data['Mensaje']['destino'];
            $this->$modelo->create();
            if ($this->$modelo->save($this->request->data)) {
                $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
            } else {
                $this->Session->setFlash(MSG_SAVEDERROR, ERROR);
            }
            
            $this->redirectSidera($controlador,"recibidos");
        }
    
    public function edit_recibidos($id = null) {
        $modelo=$this->modelo;
        $controlador=$this->controlador;
        
        $this->$modelo->id = $id;
        $this->$modelo->saveField('estado', '0');
        $mensajesNuevos  = $this->comprobarMensajesNuevos($this);
        $this->set('mensajesNuevos', $mensajesNuevos);
        
        $user_id=$this->Auth->user('id');
        $this->set('user_id',$user_id);
       
         $this->Esquema->comprobarSessionModeloYML($modelo);
         $configModulo = $this->Esquema->leerConfigSession($modelo);
         
        $origen = $this->Mensaje->find('all',array('fields' => array('Origen.nombre','Origen.apellido1','Origen.apellido2'),'conditions' => array('Mensaje.id =' => $id)));
        $origen=$origen[0]['Origen']['nombre'].' '.$origen[0]['Origen']['apellido1'].' '.$origen[0]['Origen']['apellido2'];
      
        $this->set('accion',"Edit");
        $this->set(compact('modelo','controlador','accion','configModulo','origen','mensajesNuevos'));
       
         $this->request->data = $this->$modelo->read(null, $id);
        		
    }
    
    public function responderRecibidos($id = null) {
            $modelo=$this->modelo;
            $controlador=$this->controlador;
            
            $this->$modelo->id = $id;
            //mensaje para el origen
            $this->request->data['Mensaje']['id'] = '';
            $this->request->data["Mensaje"]["created"]=getdate();
            $origen_old=$this->request->data['Mensaje']['origen'];
            $destino_old=$this->request->data['Mensaje']['destino'];
            $this->request->data['Mensaje']['origen']=$destino_old;
            $this->request->data['Mensaje']['destino']=$origen_old;
            $this->request->data['Mensaje']['estado']=1;
            $this->request->data['Mensaje']['propietario']=$origen_old;
            $this->$modelo->create();
                if ($this->$modelo->save($this->request->data)) {
                    $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
                } else {
                    $this->Session->setFlash(MSG_SAVEDERROR, ERROR);
                }
            //mensaje para el destino
            $this->request->data['Mensaje']['estado']=0;
            $this->request->data['Mensaje']['propietario']=$destino_old;
             $this->$modelo->create();
                if ($this->$modelo->save($this->request->data)) {
                    $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
                } else {
                    $this->Session->setFlash(MSG_SAVEDERROR, ERROR);
                }
            
            $this->redirectSidera($controlador,"recibidos");
        }

    
    public function delete($id = null) {
        $this->autoRender = false;
        $modelo=$this->modelo;
        $controlador=$this->controlador;
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $datos=$this->request->data['id'];
        $exploded_id = explode(',', $datos);
        for($i=0;$i<count($exploded_id);$i++){
            $this->$modelo->id = $exploded_id[$i];
            if (!$this->$modelo->exists()) {
                $this->Session->setFlash(MSG_DELETENOTICE, NOTICE);
            }
            $this->$modelo->delete(); 
        }
        $this->Session->setFlash(MSG_DELETESUCCESS, SUCCESS);  
          $this->redirectSidera($controlador,"recibidos");
    }

    public function getLastMessages(){
        $numLastMessages = 5;
        $lastMessages = $this->Mensaje->find('all',array(
            'conditions' => array(
                'Mensaje.propietario' => $this->usuario['id'],
                'Mensaje.destino'     => $this->usuario['id']),
            'order'      => 'Mensaje.created DESC',
            'limit'      => $numLastMessages,
        ));
        $this->set('lastMessages',$lastMessages);
    }
    
    public function getNotReadMessagesCounter(){
        $notReadMessagesCounter = $this->Mensaje->find('count',array(
            'conditions' => array(
                'Mensaje.propietario' => $this->usuario['id'],
                'Mensaje.destino'     => $this->usuario['id'],
                'Mensaje.estado'      => 1)
        ));
        $this->set('notReadMessagesCounter',$notReadMessagesCounter);
    }

    public  function registrosPorPagina(){
         parent::registrosPorPagina($this); 
    }

    public function filtro() {
        parent::filtro($this);
    }
    
     public function exportarMostrar() {
    	parent::exportarMostrar($this);
    }   

      public function exportarEjecutar() {
    	parent::exportarEjecutar($this);
    }
    
    public function configurarModulo(){
        parent::configurarModulo($this);
    }
    
    public function configurarModuloGuardar(){
        parent::configurarModuloGuardar($this);
    }
}