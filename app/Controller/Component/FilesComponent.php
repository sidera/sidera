<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014
 *
 * Organization:
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Component', 'Controller');
App::uses('Folder','Utility');
App::uses('File','Utility');
App::import('Vendor', 'Spreadsheet_Excel_Reader');

class FilesComponent extends Component {
	public $components                = array();
	public $viewPath                  = null;
	public $modelPath                 = null;
	public $controllerPath            = null;
	private $tmpPath                  = null;
	private $sideraconstantsPath      = null;
	private $applicationConstantsPath = null;
	private $databasePath             = null;
	private $backupsPath              = null;
	private $templatesPath            = null;
	public $errorPath                 = null;
	public $tablasSistema             = null;
	public $configModulosProyecto     = null;
	public $configModulosSistema      = null;
     
    function __construct(ComponentCollection $collection, $settings = array()) {
        parent::__construct($collection,$settings);
        $this->viewPath                 =   APP.'AppModulos'.DS.'View'.DS;
        $this->modelPath                =   APP.'AppModulos'.DS.'Model'.DS;
        $this->controllerPath           =   APP.'AppModulos'.DS.'Controller'.DS;
        $this->sideraconstantsPath      =   APP.'Config'.DS.'sideraConstants.php';
        $this->applicationConstantsPath =   APP.'AppModulos'.DS.'Config'.DS.'applicationConstants.php';
        $this->databasePath             =   APP.'Config'.DS.'database.php';
        $this->backupsPath              =   APP.'tmp'.DS.'backups'.DS;
        $this->errorPath                =   APP.'tmp'.DS.'logs'.DS;
        $this->cachePath                =   APP.'tmp'.DS.'cache'.DS;
        $this->templatesPath            =   APP.'View'.DS.'Templates'.DS;
        $this->configModulosProyecto    =   APP.'AppModulos'.   DS. 'Config'.DS.'configModulos'.DS;
        $this->configModulosSistema     =   APP.'Config'.       DS.'configModulos'.DS;
        $this->tablasSistema            = unserialize (TABLAS_SISTEMA);       
	}
   
	function crearArchivo($directorio,$plantilla,$nombreArchivo,$modelo,$controlador,$propiedadesModelo,$opciones){
            $file = fopen($directorio.$nombreArchivo, "w");
            if($opciones["Constructor"]["etiqueta"]==""){ 
                $opciones["Constructor"]["etiqueta"] = ucwords($controlador);
            }    
            require($this->templatesPath.'Constructor'.DS.$plantilla.'.php');
            fwrite($file, $archivo); 
            fclose($file);
            return true;
	}
    
	function crearDirectorio($ruta,$directorio){
		if(!file_exists($ruta.$directorio)){
			mkdir($ruta.$directorio, 0777,true);
		}
        return true;
    }
	
	function borrarArchivo($directorio,$archivo){
		chown($directorio.DS.$archivo,666);
		unlink($directorio.DS.$archivo);
        return true;
    }
	
    function reescribirApplicationConstants($opciones){
        $this->crearArchivoDeSeguridad('AppModulos'.DS.'Config','applicationConstants.php');
        $fichero        = $this->applicationConstantsPath;
        $databaseConfig = fopen( $fichero,'r+');
        $contenido      = fread($databaseConfig,filesize($fichero));
        fclose($databaseConfig);
        $contenido      = explode('//(cofiguracion dinamica)', $contenido);
        $file           = fopen($this->applicationConstantsPath, "w");
        require($this->templatesPath.'Config'.DS.'applicationConstants.php');
        fwrite($file, $contenido[0].$archivo); 
        fclose($file);
        return true;
    }
	
    function reescribirDatabase($bdConexiones){
        $this->crearArchivoDeSeguridad('Config','database.php');
        $file = fopen($this->databasePath, "w");
        require($this->templatesPath.'Config'.DS.'database.php');
        fwrite($file, $archivo); 
        fclose($file);
        return true;
    }

    public function unlinkRecursive($dir, $deleteRootToo = null){
		if(!$dh = @opendir($dir)){return;}
			while (false !== ($obj = readdir($dh))){
                if($obj == '.' || $obj == '..'){continue;}
                @chown($dir.DS.$obj,666);
                if (!@unlink($dir .DS. $obj)){
                    $this->unlinkRecursive($dir.DS.$obj, true);
                }
            }
        closedir($dh);
        return;
    }    

    public function borrarTemporales($obj) {
        $this->unlinkRecursive($this->cachePath);
        $this->unlinkRecursive($this->errorPath);
        return true;
    }
    
    function crearArchivoDeSeguridad($directorio,$nombreArchivo){
        $dirTmpBackups=$this->backupsPath;
        $file = APP.$directorio.DS.$nombreArchivo;
        if(file_exists($file)){
			if(!file_exists($dirTmpBackups.$directorio)){
				mkdir($dirTmpBackups.$directorio,0777,true);
            }
            $fileBak = $dirTmpBackups.$directorio.DS.$nombreArchivo;
            copy($file, $fileBak);
        }
      	return true;
    }
    
    public function archivosSinExt ($dir,$ext=".yml") {
       $archivos = scandir($dir);
       foreach ($archivos as $key => $archivo) {
           if($archivo != "." && $archivo != ".." && $archivo!= "empty"){
               $archivos[$key]=basename($archivo, $ext);              
           }else{
               unset($archivos[$key]);
           }
       }
       return $archivos;
    }
    
    public function leerArchivoLogs(){
        $url       = $this->errorPath . 'error.log'; 
        $contenido = "";
        if ($d = @fopen($url,"r")){
            while($aux = fgets($d,1024)){
            	$aux = str_replace("Warning:", '<b class="nota-alert-logs alert yellow pull-left"><i class="fa fa-warning-sign "></i>Warning:</b><hr class="nota-hr-logs">', $aux) ; 
            	$aux = str_replace("Error:", '<b class="nota-alert-logs alert red pull-left"><i class="fa fa-warning-sign "></i>Error:</b><hr class="nota-hr-logs">', $aux) ; 
            	$contenido .= $aux;
            }
            @fclose($d);    
        }
        return $contenido;
    }

	public function comprobarDirectoriostemporales() {
		if(!file_exists($this->backupsPath)){
			mkdir($this->backupsPath,0777,true);
		}
		if(!file_exists( $this->errorPath)){
			mkdir($this->errorPath,0777,true);
		}
		if(!file_exists( $this->cachePath)){
			mkdir($this->cachePath,0777,true);
			mkdir($this->cachePath.'models',0777,true);
			mkdir($this->cachePath.'persistent',0777,true);
		}else{
			if(!file_exists( $this->cachePath.'models')){
				mkdir($this->cachePath.'models',0777,true);
			}
			if(!file_exists( $this->cachePath.'persistent')){
				mkdir($this->cachePath.'persistent',0777,true);
			}
		}
		return true;
    }

	function crearArchivoMasivo($directorio,$plantilla,$obj){
		$propiedadesModelo = $obj->propiedadesTablas[$obj->modeloACrear];
		$modelo = $obj->modeloACrear;
		try {
			require($this->templatesPath.'Constructor'.DS.'Masivo'.DS.$plantilla.'.php');
			file_put_contents($directorio.$obj->nombreFile, $archivo);
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
		}
    }
}
