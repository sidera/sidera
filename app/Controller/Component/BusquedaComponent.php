<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Component', 'Controller');

class BusquedaComponent extends Component {
	public  $components            = array('Esquema');
	/* Funcion IsDate:   comprueba si el valor es una fecha.
    * Parametros:       $date : String a comprobar.
    * return:           true si es una fecha.
    */
    function IsDate($date){
	    list($dd,$mm,$yy)=split('[/.-]',$date);
	    if ($dd!="" && $mm!="" && $yy!="")    {
	       if((checkdate($mm,$dd,$yy))||(checkdate($mm,$yy,$dd))) 
                       return true;
               else return false;          
	    }
	    return false;
    }
   /* Funcion busca:   prepara la condicion para el "FIND" que realizara la busqueda
    * Parametros:       $parametros: modelo que queremos crear
    * 			        $modelo: modelo que queremos crear
    * return:           $qwery  si no existe ningun archivo relacionado con el modelo a crear
    * (actualizado en Sidera 2.0.0) 
    */
	function busca($parametros,$modelo,$obj){
           $qwery = array();
           //['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc']
           $comparadores = array(
                         'eq'=>" = ", //equal
                         'ne'=>" <> ", //not equal
                         'lt'=>" < ", //less than
                         'le'=>" <= ",//less than or equal
                         'gt'=>" > ",//greater than
                         'ge'=>" >= ",//greater than or equal
                         'bw'=>" LIKE ", //begins with
                         'bn'=>" NOT LIKE ", //doesn't begin with
                         'in'=>" IN ", //is in
                         'ni'=>" NOT IN ", //is not in
                         'ew'=>" LIKE ", //ends with
                         'en'=>" NOT LIKE ", //doesn't end with
                         'cn'=>" LIKE " , // contains
                         'nc'=>" NOT LIKE " );  //doesn't contain
                         
		 	$tipoCondicion=$parametros['tipoCondicion'];//OR - AND
			$numCondiciones=$parametros['numeroCondiciones'];
			$condiciontotal="";
            
			foreach (range(1,$numCondiciones) as $number) {
				if($parametros["campo$number"]){
					$nuevoCampo=$modelo.'.'.$parametros["campo$number"];
					$nuevoComparador=$parametros["comparador$number"];
					$nuevoContenido=$parametros["contenido$number"];
					if ($this->IsDate($nuevoContenido)){
						list( $day, $month, $year ) = split( '[/.-]', $nuevoContenido );
		    			$nuevoContenido=$year."-".$month."-".$day;
						//$nuevoContenido=date('Y-d-m', strtotime($nuevoContenido));
					}
					$campo=$parametros["campo$number"];
					if (substr($campo,-3)=="_id"){
						$controladorAjeno=strtolower(Inflector::pluralize(substr($campo,0,-3)));
						$modeloAjeno=ucwords(substr($campo,0,-3));
                        $configModulo=$this->Esquema->leerConfigSession($modeloAjeno);
                        $displayField=$configModulo[general][displayfield][campo1];
						if (sizeof($displayField)>0){
							$nuevoCampo=$modeloAjeno.'.'.$displayField;
						}
					}
                    
                    //Localiza los virtualFields y los sustituye por su primer campo individual
                    $coincidenciasCampo = preg_split("/[.]+/",$nuevoCampo);
                    $modeloCampo=$coincidenciasCampo[0];
                    $nombreCampo=$coincidenciasCampo[1];
                    $esvirtual = false;
                    if($modeloCampo!=$modelo){
                        if ($obj->$modelo->$modeloCampo->isVirtualField($nombreCampo)) {
                            $campoVirtual=$obj->$modelo->$modeloCampo->getVirtualField($nombreCampo);
                            $coincidencias = preg_split("/[\s\(\),]+/",$campoVirtual);
                            $camposDelVirtualField = preg_grep("/\./", $coincidencias);
                            sort($camposDelVirtualField);
                            $nuevoCampo=$camposDelVirtualField[0];
                             //Pongo esta variable para indicar a la hora de hacer la condicion ,de que tiene que a�adir todos los campos que forman parte del virtual field no solo el primero como estaba hasta ahora
                            $esvirtual = true;
                        }
                    }
                    //lo que esta comentado es lo antiguo
					/*switch($nuevoComparador){
					   case 'bw':      $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "$nuevoContenido%";
                       		break;
                       case 'bn':      $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "$nuevoContenido%";
                       		break;
                       case 'ew':      $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "%$nuevoContenido";
                           	break;
                       case 'en':      $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "%$nuevoContenido";
                       		break;
                       case 'cn':      $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "%$nuevoContenido%";
                           break;
                       case 'nc':      $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "%$nuevoContenido%";
                           break;
                       default:        $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "$nuevoContenido";
				   		   break;
                    }*/
                    switch ($nuevoComparador) {
                    case 'bw':
                        if ($esvirtual && count($camposDelVirtualField)>1 ) {
                            $ii=0;
                            foreach ($camposDelVirtualField as $campoDelVirtualField2) {
                                $ii++;
                                $qwery[$tipoCondicion]["$number"]['OR']["$number$ii"]["$campoDelVirtualField2$comparadores[$nuevoComparador]"] = "$nuevoContenido%";
                            }
                        } else {
                            $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "$nuevoContenido%";
                        }
                        break;
                    case 'bn':
                        if ($esvirtual && count($camposDelVirtualField)>1) {
                            $ii=0;
                            foreach ($camposDelVirtualField as $campoDelVirtualField2) {
                                $ii++;
                                $qwery[$tipoCondicion]["$number"]['OR']["$number$ii"]["$campoDelVirtualField2$comparadores[$nuevoComparador]"] = "$nuevoContenido%";
                            }
                        } else {
                            $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "$nuevoContenido%";
                        }
                        break;
                    case 'ew':
                        if ($esvirtual && count($camposDelVirtualField)>1) {
                            $ii=0;
                            foreach ($camposDelVirtualField as $campoDelVirtualField2) {
                                $ii++;
                                $qwery[$tipoCondicion]["$number"]['OR']["$number$ii"]["$campoDelVirtualField2$comparadores[$nuevoComparador]"] = "%$nuevoContenido";
                            }
                        } else {
                            $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "%$nuevoContenido";
                        }
                        break;
                    case 'en':
                        if ($esvirtual && count($camposDelVirtualField)>1) {
                            $ii=0;
                            foreach ($camposDelVirtualField as $campoDelVirtualField2) {
                                $ii++;
                                $qwery[$tipoCondicion]["$number"]['OR']["$number$ii"]["$campoDelVirtualField2$comparadores[$nuevoComparador]"] = "%$nuevoContenido";
                            }
                        } else {
                            $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "%$nuevoContenido";
                        }
                        break;
                    case 'cn':
                        if ($esvirtual && count($camposDelVirtualField)>1) {
                            $ii=0;
                            foreach ($camposDelVirtualField as $campoDelVirtualField2) {
                                $ii++;
                                $qwery[$tipoCondicion]["$number"]['OR']["$number$ii"]["$campoDelVirtualField2$comparadores[$nuevoComparador]"] = "%$nuevoContenido%";
                            }
                        } else {
                            $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "%$nuevoContenido%";
                        }
                        break;
                    case 'nc':
                        if ($esvirtual && count($camposDelVirtualField)>1) {
                            $ii=0;
                            foreach ($camposDelVirtualField as $campoDelVirtualField2) {
                                $ii++;
                                $qwery[$tipoCondicion]["$number"]['OR']["$number$ii"]["$campoDelVirtualField2$comparadores[$nuevoComparador]"] = "%$nuevoContenido%";
                            }
                        } else {
                            $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "%$nuevoContenido%";
                        }
                        break;
                    default:
                        if ($esvirtual && count($camposDelVirtualField)>1) {
                            $ii=0;
                            foreach ($camposDelVirtualField as $campoDelVirtualField2) {
                                $ii++;
                                $qwery[$tipoCondicion]["$number"]['OR']["$number$ii"]["$campoDelVirtualField2$comparadores[$nuevoComparador]"] = "$nuevoContenido";
                            }
                        } else {
                            $qwery[$tipoCondicion]["$number"]["$nuevoCampo$comparadores[$nuevoComparador]"] = "$nuevoContenido";
                        }
                        break;
                }
				}
			}
		  return $qwery;
       }
	   
}
