<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Component', 'Controller');
App::uses('Folder','Utility');
App::uses('File','Utility');
App::import('vendor', 'Spyc');

/* Class EsquemaComponent
 * Descripción: Reune los métodos relacionados con BD .
 * (actualizado en Sidera 2.0.0)
 */
class EsquemaComponent extends Component {
    	
	public  $components = array('Esquema','Busqueda','Importacion','Files','Session');
	private $configPath = null;
    
	function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection,$settings);
		//Ruta para los archivos que no son de SIDERA, desde aquí modificamos para todo el constructor.        
        $this->configPath = APP.'AppModulos'.DS.'Config'.DS;
	}
	
    /* Funcion camposModelo:   Obtiene los campos y las propiedades del fichero YML del modelo correspondiente.
    * Parametros:               $modelo: modelo que buscamos.
    * return:           lista de campos del fichero YML
    *(actualizado en Sidera 2.0.0) 
    */	
	public function camposModeloYML($modelo) {
		$configModulo=  $this->Session->read("config[$modelo]");
		return  $configModulo['general']['campos'];
	}
      
    /* Funcion cargarConfig:   Obtiene todo la configuración de un módulo
    * Parametros:               $modelo: modelo que buscamos.
    * return:           array con configuración
    *(actualizado en Sidera 2.0.0) 
    */	
	public function cargarConfigYML($modelo) {
		$ruta = null;
		$modulosSistema = $this->Files->archivosSinExt($this->Files->configModulosSistema);
		if(in_array($modelo,$modulosSistema)){
			$ruta = $this->Files->configModulosSistema;
		}else{
			$ruta=$this->Files->configModulosProyecto;
		}
        $configModulo = Spyc::YAMLLoad($ruta.$modelo.'.yml');
        return  $configModulo;
      }
      
   /* Funcion cargarSesionesYML:   Carga en  Session todas las configuraciónes de los modelos de la aplicacion 
    * return:                   TRUE
    *(actualizado en Sidera 2.0.0) 
    */	
	public function cargarSesionesYML() {
		$ruta              = null;
		$modulosSistema    = $this->Files->archivosSinExt($this->Files->configModulosSistema);
        $modelosAplicacion = $this->Files->archivosSinExt($this->Files->configModulosProyecto);
        $ymls              = array_merge($modulosSistema, $modelosAplicacion);
        foreach ($ymls as $modelo) {
            $this->borrarSessionYML($modelo);
            $this->cargarSessionYML($modelo);
        }
        return  TRUE;
      } 

	/* Funcion borrarSessionesYML:   Borra de Session todas las configuraciónes de modelos que haya
    * Parametros:               
    * return:                   TRUE
    *(actualizado en Sidera 2.0.0) 
    */	
	public function borrarSessionesYML() {
		foreach ($_SESSION as $keySession => $valorSession) {
			$variablesSistemaEnSession= array('Config','Auth');
			if (!in_array($keySession ,$variablesSistemaEnSession)){
				unset($_SESSION[$keySession]);
			}
		}
	}
      
   /* Funcion borrarSessionYML:   Borra de Session la configuración del modelo indicado 
    * Parametros:               $modelo: modelo que cargaremos.
    * return:                   array con configuración
    *(actualizado en Sidera 2.0.0) 
    */	
      public function borrarSessionYML($modelo) {
        unset($_SESSION["config[".$modelo."]"]);
        return  TRUE;
      } 
      
    /* Funcion cargarSessionYML:   Carga en Session la configuración del modelo indicado 
    * Parametros:               $modelo: modelo que cargaremos.
    * return:                   array con configuración
    *(actualizado en Sidera 2.0.0) 
    */	
	public function cargarSessionYML($modelo) {
		$configModulo = null;
        $configModulo = $this->cargarConfigYML($modelo);   
		$this->Session->write("config[$modelo]",$configModulo);            
		return  $configModulo;
	}
      
    /* Funcion obtenerConfigSession:   Obtiene  la configuración del modelo indicado
    * Parametros:               $modelo: modelo que deseamos.
    * return:                   array con configuración
    *(actualizado en Sidera 2.0.0) 
    */	
	public function leerConfigSession($modelo) {
		$configModulo = null;
		$configModulo = $this->Session->read("config[$modelo]");
		return $configModulo;
	}
   
   /* Funcion comprobarSessionModeloYML:   obtiene solo los campos que se van a mostrar en el index
    * Parametros:        $modelo: modelo a obtener su configuracion.
    * return:          TRUE
    * (actualizado en Sidera 2.0.0)
    */
	public function comprobarSessionModeloYML($modelo){
		if(!$this->leerConfigSession($modelo)){
			$this->cargarSessionYML($modelo);
		}
		return TRUE;
    }   
      
   /* Funcion propiedadesModelo:   Pasado el esquema de un modelo devuelve todas las propiedades de su campos.
    * Parametros:       $schema : esquema de un modelo.
    * return:           $propiedades contiene (nombre,tipo,longitud,nulo).
    */
	function propiedadesModelo($schema){
		$campos = array_keys($schema);
		for($i=0;$i<sizeof($campos);$i++){ 
			$propiedades[$i]['name'] = $campos[$i];
			$propiedades[$i]['type'] = $schema[$campos[$i]]['type'];
			if ($schema[$campos[$i]]['null']=='Y' || $schema[$campos[$i]]['null']==1){
				$propiedades[$i]['null']="SI";
			}else{
				$comienzoCampo=substr($propiedades[$i]['name'],0,5);
				if(($propiedades[$i]['type']=='boolean')||($comienzoCampo=="bool_"))
					$propiedades[$i]['null']="SI";
				else
					$propiedades[$i]['null']="NO";
			}
			 $propiedades[$i]['length'] = $schema[$campos[$i]]['length'];
            //Esto es para el SQLserver para saber el lengh real de los numeros
            if (isset($schema[$campos[$i]]['Length2'])) {
                $propiedades[$i]['Length2'] = $schema[$campos[$i]]['Length2'];
            }
            if (isset($schema[$campos[$i]]['precision'])) {
                $propiedades[$i]['precision']   = $schema[$campos[$i]]['precision'];
                $propiedades[$i]['scale']       = $schema[$campos[$i]]['scale'];
            }
        }
        return $propiedades;
     }
	
	/* Funcion cargarTodosLosModelos:   carga todos los modelos de una conexion.
    * Parametros:       $obj: objeto $this de metodo padre.
	* 					$conex : conexion de db.
    * return:           $propiedadesTablas contiene todas las propiedades de todas las tablas de la conexion.
    */
	public function cargarTodosLosModelos($obj,$conex) {
		$tablas    = $obj->Esquema->tablasSistema($obj,$conex);
        $numTablas = array_keys($tablas,end($tablas));
        $numTablas = $numTablas[0];
		for($i = 0 ; $i <= $numTablas ; $i ++) {
			if (!key_exists($i, $tablas)) continue;
			$tabla = ucwords(Inflector::singularize($tablas[$i]));
			$obj->$tabla->useDbConfig = $conex;
			//Compruebo si el nombre de la tabla esta bien formado
			if($tablas[$i]<>$obj->$tabla->useTable) continue;
			$db = ConnectionManager::getDataSource($conex);
            $database = $db->config['database'];
            //sqlserver  requiere quel schema sea el correcto
            if($db->config['datasource']=="Database/Sqlserver" || $db->config['datasource']=="Database/Sybase"){
                $obj->$tabla->schemaName = $db->config['schema'];
            }
            else{
                $obj->$tabla->schemaName = $database;
            }
			$schema = $obj->$tabla->schema(true);
            $propiedadesTablas[$tabla] = $obj->Esquema->propiedadesModelo($schema);
		}
		return $propiedadesTablas;
	}
	
	/* Funcion tablasSistema: Devuelve todas las tablas de una conexion.
    * Parametros:       $obj: objeto $this de metodo padre.
	* 					$conex: conexion de db.
    * return:           $tablas contiene los nombres de todas las tablas.
    */
	public function tablasSistema($obj,$conex) {
		if(empty($conex)) $conex = 'default';
		$db = ConnectionManager::getDataSource($conex);
		$tablas = $db->listSources();
		for ( $i = 0 ; $i < count($tablas) ; $i ++) {
			$tabla=ucwords(Inflector::singularize($tablas[$i]));
			$obj->loadModel($tabla);
			$obj->$tabla->useDbConfig = $conex;
			//Compruebo si el nombre de la tabla esta bien formado
			if($tablas[$i]<>$obj->$tabla->useTable) continue;
			else $tablas1[$i]=$tablas[$i];
		}
		$tablas=$tablas1;
		return $tablas;
	}
	
	/* Funcion buscarClavesAjenas:   busca todos los campos acabados en "_id" 
	* 								 correspondientes a claves ajenas, esto 
	* 								 tambien indica las relaciones "pertenece a"
    * Parametros:       $obj: objeto $this de metodo padre.
	* 					$modeloDeTabla : nombre de la tabla en cuestion
    * return:           $clavesAjenas contiene cada uno de los campos "..._id"
    */
	public function buscarClavesAjenas($obj,$modeloDeTabla) {
		$clavesAjenas = array();
		$schema       = $obj->$modeloDeTabla->schema(true);
		$propiedadesTabla[$modeloDeTabla] = $obj->Esquema->propiedadesModelo($schema);
		$numCampos    = array_keys($schema);
		for($i=0;$i<sizeof($numCampos);$i++){
			$campo      = $propiedadesTabla[$modeloDeTabla][$i]["name"];
			$teminacion = substr($campo,$campo-3,strlen($campo));
			if($teminacion == "_id"){
				$modeloRelacion = substr($campo,0,-3);
				$clavesAjenas[$i]['principal'] = $modeloDeTabla;
				$clavesAjenas[$i]['modelo']    = $modeloRelacion;
				$clavesAjenas[$i]['campo']     = $campo;
			}
		}
		return $clavesAjenas;
	}
	
	/* Funcion buscarRelacionesTieneMuchos:   identifica todos los modelos que contienen
	* 										  algun campo que se llamen "miModelo_id", esto
	* 										  permite identificar la relaciones "tiene muchos"
    * Parametros:       $obj: objeto $this de metodo padre.
	* 					$modeloTablaPrincipal : nombre de mi modelo
	* 					$conex : conexion de db.
    * return:           $relacionesTieneMuchos contiene los nombres de lo modelos que contienen algun campo "miModelo_id".
    */
	public function buscarRelacionesTieneMuchos($obj,$modeloTablaPrincipal,$conex) {
        $modeloTablaPrincipal  = strtolower($modeloTablaPrincipal);
        $claveAjenaTablaModelo = $modeloTablaPrincipal."_id";
        $tablas = $obj->Esquema->tablasSistema($obj,$conex);
        $propiedadesTablas = $obj->Esquema->cargarTodosLosModelos($obj,$conex);
        $relacionesTieneMuchos = array();
        for($i=0;$i<sizeof($tablas);$i++){
			$modeloTabla = ucwords(Inflector::singularize($tablas[$i]));
			//comento lo de abajo porque no realiza bien los singulares.Ya que solo quitaba la s
			//y no funcionanba con los que termnaban en consonante
			//$modeloTabla=ucwords(substr($tablas[$i],0,-1));
			//Tambien he cambiado en el for de abajo el sizeof de tablas po el sizeof de $propiedadesTablas[$modeloTabla]
			for($j=0;$j<sizeof($propiedadesTablas[$modeloTabla]);$j++){
				$campo = $propiedadesTablas[$modeloTabla][$j]["name"];
				if($campo == $claveAjenaTablaModelo){
					$relacionesTieneMuchos[$i]['principal'] = $modeloTablaPrincipal;
					$relacionesTieneMuchos[$i]['modelo']    = $modeloTabla;
				}
			}
        }
        return $relacionesTieneMuchos;
    }
	
	/* Funcion cargarDatosMenu:   carga los datos del nuevo modelo 
	* 							  necesarios para crear un menu nuevo.
    * Parametros:       $modeloACargar: nombre del nuevo modelo
    * return:           $datosMenu contiene esos datos.
    */
	public function cargarDatosMenu($modeloACargar,$etiquetaModulo){
		if($etiquetaModulo){
			$datosMenu["titulo"] = $etiquetaModulo;
		}else{
			$datosMenu["titulo"] = ucwords(Inflector::pluralize($modeloACargar));
		}
		$datosMenu["controlador"] = strtolower(Inflector::pluralize($modeloACargar));
		$datosMenu["accion"]      = "index";
		$datosMenu["orden"]       = "1";
		$datosMenu["menu_id"]     = "0";
		$datosMenu["ruta"]        = ">".ucwords(Inflector::pluralize($modeloACargar));
		$datosMenu["estado"]      = "Activo";
		return $datosMenu;
	}
	
    /* Funcion cofigurarAplicacion:   regenera el archivo sideraConstants.php con los nuevos parametros.
    * Parametros:       $obj: objeto $this de metodo padre.
    * 					$modelo : modelo en uso
    * return:           true si es correcto
    */
	public function configurarAplicacion($obj) {
		$opciones = $obj->request->data;
		$this->Files->reescribirApplicationConstants($opciones);
		$obj->Session->setFlash(MSG_CONFIGSUCCESS, SUCCESS);
		return true;
	}
	
    /* Funcion obtenerConfiguracionModulo:   obtiene la configuracion de los campos del modulo dado
    * Parametros:        $obj: objeto $this de metodo padre.
    * 					 $modelo: modelo a obtener su configuracion.
    * return:            $parametrosModulo contiene los parametros de configuracion
    */
    public function obtenerConfiguracionModulo($obj,$modelo){
        $obj->loadModel('Constructor');
        $obj->$tabla->useDbConfig = "default";
        $parametrosModulo = $obj->Constructor->query("SELECT * FROM constructores Constructores WHERE Constructores.modelo='".strtolower(Inflector::pluralize($modelo))."'");
        $parametrosModulo = $parametrosModulo[0]["Constructores"];
        if ($parametrosModulo['registros_pagina']==0) $parametrosModulo['registros_pagina']= REGISTROS_PAGINA;
        return $parametrosModulo;
    }
    
    /* Funcion obtenerConfiguracionCampos:   obtiene la configuracion de los campos del modulo dado
    * Parametros:        $obj: objeto $this de metodo padre.
    * 					 $modelo: modelo a obtener su configuracion.
    * return:            $configCampos contiene los parametros de configuracion
    */
    public function obtenerConfiguracionCampos($obj, $modelo, $propiedades){
        $obj->loadModel("Constructor");
        $obj->$tabla->useDbConfig = "default";
        $modeloCampos   = strtolower(Inflector::pluralize($modelo));
        $campos         = $obj->Constructor->find('all',array('conditions' => array('Constructor.modelo' => $modeloCampos)));
        $campos         = $campos[0]['Campo'];
        for($i=0;$i<sizeof($campos);$i++){
			$configCampos[$campos[$i]['campo']]=array(
				'modelo'   => $modelo,
				'campo'    => $campos[$i]['campo'],
				'etiqueta' => $campos[$i]['etiqueta'],
				'orden'    => $campos[$i]['orden'],
				'mostrar_en_index'  => $campos[$i]['mostrar_en_index'],
				'buscar_en_index'   => $campos[$i]['buscar_en_index'],
				'mostrar_en_add'    => $campos[$i]['mostrar_en_add'],
				'mostrar_en_edit'   => $campos[$i]['mostrar_en_edit'],
				'displayfield'      => $campos[$i]['displayfield']
			);
                
			for($j =0; $j < sizeof($propiedades); $j++){
				if (($propiedades[$j]['null']=="NO")&&(($propiedades[$j]['name']==$campos[$i]['campo']))){
					$configCampos[$campos[$i]['campo']]['etiqueta']='<span class="color-red">*</span> '.$campos[$i]['etiqueta'];
				}
            }
        }
        return $configCampos;
    }
    
     /* Funcion obtenerCamposABuscarEnIndex:   obtiene los campos por los cuales se puede buscar en el index
    *                                         y ademas, obtiene los DISPLAYFIELDS para la vista.
    * Parametros:        $obj: objeto $this de metodo padre.
    *                    $configModulo: contiene la configuracion del modelo.
    *                    $camposModelo: contiene los campos del modelo.
    * 					 $modelo: modelo a obtener su configuracion.
    * return:           $fieldsForSearch, contiene los campos por los que se va a buscar
    */
    public function obtenerCamposABuscarEnIndex($obj,$configModulo,$camposModelo,$modelo){
      
        $schema       = $obj->$modelo->schema();
        $fieldsForSearch = array();
        $modelosAjenos   = $configModulo['relaciones']['belongsTo'];
        $camposAjenos    = $this->obtenerDisplayfields($obj,$modelosAjenos);
        foreach ($camposModelo as $campo => $etiqueta){
           if ($configModulo['index']['campos'][$campo]['buscar']){
               if (in_array($campo, $camposAjenos)){
                    $modeloAjeno        = array_search($campo, $camposAjenos);
                    $campoAjeno         = $campo;
                    $configModuloAjeno  = $this->leerConfigSession($modeloAjeno);
                    $displayField       = $configModuloAjeno['general']['displayfield']['campo1'];
                    //el campo id es obligatorio que este en $fieldsForSearch
                    if(isset($displayField)) $fieldsForSearch[] = $modeloAjeno.".".$displayField;
					else $fieldsForSearch[] = $modeloAjeno.".id";
                }else{
                    $comienzoCampo2="";
                    $comienzoCampo2 = substr($campo,0,5);
                    if($comienzoCampo2 == 'bool_') $schema[$campo]['type']=="boolean";
                    if($schema[$campo]['type']!="boolean")
					$fieldsForSearch[] = $modelo.".".$campo;
                }
            }
         }
        
        //Localiza los virtualFields y los sustituye por sus campos individuales
        foreach ($fieldsForSearch as $field) {
            $coincidenciasCampo = preg_split("/[.]+/",$field);
            $modeloCampo        = $coincidenciasCampo[0];
            $nombreCampo        = $coincidenciasCampo[1];
            if($modeloCampo != $modelo){
                if ($obj->$modelo->$modeloCampo->isVirtualField($nombreCampo)) {
                    $campoVirtual  = $obj->$modelo->$modeloCampo->getVirtualField($nombreCampo);
                    $coincidencias = preg_split("/[\s\(\),]+/",$campoVirtual);
                    $camposDelVirtualField = preg_grep("/\./", $coincidencias);
                    $keyField = array_search($field, $fieldsForSearch);
                    unset($fieldsForSearch[$keyField]);
                    foreach ($camposDelVirtualField as $campoDelVirtualField) {
                        $fieldsForSearch[]=$campoDelVirtualField;
                    }
                }
            }
        }
        return $fieldsForSearch;
    }
    
    /* Funcion obtenerDisplayfields:   obtiene los Displayfields 
    *  Parametros:        $obj: objeto $this de metodo padre.
    *                    $modelosAjenos: contiene un array con los modelos ajenos.
    *  return:           $camposAjenos, devuelve los campos ajenos del modelo
    */
    public function obtenerDisplayfields($obj,$modelosAjenos){
        //Forma un array con todas las claves ajenas del modelo
        foreach ($modelosAjenos as $modeloAjeno => $datosModeloAjeno ){
            $configModuloAjeno = null;
            $this->comprobarSessionModeloYML($modeloAjeno);
            $configModuloAjeno = $this->leerConfigSession($modeloAjeno);
            $displayField      = $configModuloAjeno["general"]["displayfield"]["campo1"];
            $camposAjenos[$datosModeloAjeno['className']] = $datosModeloAjeno['foreignKey'];
            $obj->set($datosModeloAjeno['foreignKey']."DisplayField", $displayField);
        }
        return $camposAjenos;
    }
    
    /* Funcion identificarCamposObligatorios:   identifica los campos obligatorios y les antepone un * en Add y Edit
    * Parametros:        $configModulo: contiene la configuracion de los campos del modelo.
    * 					 $propiedades: propiedades de los campos del modelo
    * return:           $configModulo, devuelve la configuracion del modulo con lo campos obligatorios actualizados
    */
    public function identificarCamposObligatorios($configModulo,$propiedades,$vista){
        for($i=0;$i<sizeof($propiedades);$i++){
            $propiedadCampo = $propiedades[$i];
			if ((($propiedadCampo['null']=="NO")) && (isset($configModulo[$vista]["campos"][$propiedadCampo['name']]))){
				$configModulo[$vista]["campos"][$propiedadCampo['name']]["etiqueta"]='<span class="color-red">*</span> '.$configModulo[$vista]["campos"][$propiedadCampo['name']]["etiqueta"];
			}
		}
        return $configModulo;
    }
    
    /* Funcion obtenerDatosModelosAjenos:   Por cada clave ajena (ej. grupo_id) envia una variable 
    *                                       con los datos relacionados de ese modelo (ej. Grupo)
    * Parametros:        $obj: objeto $this de metodo padre.
    * 					 $modelo: modelo a obtener su configuracion.
    *                    $modelosAjenos: array con todos lo modelos ajenos de este modulo
    */
    public function obtenerDatosModelosAjenos($obj){
        $modelo        = $obj->modelo;
        $modelosAjenos = $obj->modelosAjenos;
        foreach ($modelosAjenos as $modeloAjeno){
            $configModuloAjeno = null;
            $this->comprobarSessionModeloYML($modeloAjeno);
            $configModuloAjeno = $this->leerConfigSession($modeloAjeno);
            $displayField      = $configModuloAjeno["general"]["displayfield"]["campo1"];
            $datosModeloAjeno  = null;
            if (sizeof($displayField)>0){
				$datosModeloAjeno  = $obj->$modelo->$modeloAjeno->find('list',array('fields' => array($displayField),'order'=>$displayField. ' ASC'));
            }else{
				$datosModeloAjeno = $obj->$modelo->$modeloAjeno->find('list');
            }
            $obj->set(strtolower(Inflector::pluralize($modeloAjeno)),$datosModeloAjeno);
        }
    }
    
    /* Funcion obtenerDatosModelosAjenosRelacionados:   Calculas los registros del modelo relacionado segun el valor
    *                                                   selecionado del modelo Principal del que depende
    * Parametros:        $registroId: registro selecionado en el modelo principal del que depende.
    * 					 $modelo: modelo a obtener su configuracion.
    *                    $modelosAjenosRelacionados: array con todos lo modelos ajenos relacionados de este modulo
    * return:           true si es correcto
    *(actualizado en Sidera 2.0.0) 
    */
    public function obtenerDatosModelosAjenosRelacionados( $registroId = null , $obj = null ){
        $modelo=$obj->modelo;
        $modelosAjenosRelacionados=$obj->modelosAjenosRelacionados;
        if(!$obj->data){
            $registros=$obj->$modelo->findById($registroId);
        }else{
            $registros=$obj->data;
        }
     
        foreach ($modelosAjenosRelacionados as $modeloAjenoRelacionado => $datos) {
            $datosModeloAjeno = null;
            $datosNuevos=null;
            
            list( $nombreCampoAjeno, $separador, $listaCampos ) =$datos;
            $valorCampoAjeno=$registros[$modelo][$nombreCampoAjeno];
        
            if(isset($listaCampos)){  //Si el modelo ajeno relacionado es avanzado (compuesto de varios campos)
                $listaCampos = explode(',', $listaCampos);
                if(!in_array ("id",$listaCampos))array_unshift($listaCampos, 'id');   
                $datosModeloAjeno = $obj->$modelo->$modeloAjenoRelacionado->find('all',array(
					'fields' => $listaCampos,
					'conditions' => array("$modeloAjenoRelacionado.$nombreCampoAjeno =" => $valorCampoAjeno),
				));
                //Formo el array del combo con los campos separados por el separador
                foreach ($datosModeloAjeno as $camposElementoLista) {
                    $valorElementoLista="";
                    foreach ($camposElementoLista[$modeloAjenoRelacionado] as $campo => $valorCampo) {
                        if ($campo != 'id') $valorElementoLista.=$valorCampo.$separador;
                    }
                    $valorElementoLista=substr($valorElementoLista,0,-strlen($separador));
                    $datosNuevos[$camposElementoLista[$modeloAjenoRelacionado]['id']]=$valorElementoLista;
                }
                $obj->set(strtolower(Inflector::pluralize($modeloAjenoRelacionado)),$datosNuevos);
            }else{ //Si el modelo ajeno relacionado es simple (tipo lista (id,displayField) )
                    $this->Esquema->comprobarSessionModeloYML($modeloAjenoRelacionado);
                    $configModuloAjeno=$this->Esquema->leerConfigSession($modeloAjenoRelacionado);
                    $displayField= $configModuloAjeno["general"]["displayfield"]["campo1"];
            
                if (sizeof($displayField)>0){
                    $datosModeloAjeno = $obj->$modelo->$modeloAjenoRelacionado->find('list',array(
                        'fields' => array($displayField),
                        'conditions' => array("$modeloAjenoRelacionado.$nombreCampoAjeno =" => $valorCampoAjeno),
                        'order' => array($displayField)
                        ));
                }else{
                    $datosModeloAjeno = $obj->$modelo->$modeloAjenoRelacionado->find('list',array(
                        'conditions' => array("$modeloAjenoRelacionado.$nombreCampoAjeno =" => $valorCampoAjeno),
                        'order' => array($displayField)
                        ));
                }
                $obj->set(strtolower(Inflector::pluralize($modeloAjenoRelacionado)),$datosModeloAjeno);
            }
        }         
    }

    public function organizaArrayDuallistbox($obj,$modeloLista,$claveLista){
        $array=array_unique($obj->request->data[$modeloLista][$claveLista]);
            unset($obj->request->data[$modeloLista][$claveLista]);
            $obj->request->data[$modeloLista][0]="";
            foreach($array as $k=>$valor){
              $obj->request->data[$modeloLista][$valor][$claveLista]=$valor;
            }
    }

	public function cargarConfigJasperserversYML() {
		$servidores=Spyc::YAMLLoad($this->configPath.'jasperreporServers.yml');
		return  $servidores["servidores"];
	} 
}