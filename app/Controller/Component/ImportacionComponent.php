<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Component', 'Controller');
App::uses('Folder','Utility');
App::uses('File','Utility');
App::import('Vendor', 'Spreadsheet_Excel_Reader');

class ImportacionComponent extends Component {
		
	var $components = array('Session');
	var $ext_permitidas = array('csv');
    var $tipos_permitidos = array('text/comma-separated-values', 'text/csv', 'application/csv',
					'application/excel', 'application/vnd.msexcel', 'text/anytext',
					'application/vnd.ms-excel [official]', 'application/msexcel', 'application/x-msexcel', 'application/x-ms-excel', 
					'application/vnd.ms-excel', 'application/x-excel', 'application/x-dos_ms_excel', 'application/xls', 'application/x-xls', 
					'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
					'application/vnd.oasis.opendocument.spreadsheet', 'application/x-vnd.oasis.opendocument.spreadsheet');
	
	
	private function recodeToSaveData(&$datos)
    {
	    foreach( $datos as $key => $value){
	      if (!isset($datos[$key]) || $datos[$key]=="")continue;        
	      if (is_numeric($value) && is_float($value)) number_format($value, 2, ",", "");
	      else {
	            if ($this->IsDate($value)){$datos[$key] = $this->converToSaveDate($value);}
	            elseif(is_array($value))  {$datos[$key] = $this->recodeToSaveData($value);}              
	            else {$datos[$key]=  iconv('UTF-8', 'ISO-8859-1', $datos[$key]);}                            
	      }                 
	    }
       return; 
    }
   
   
	/* Funcion converToSaveDate:   Convierte una fecha al formato de la BD Oracle.
	* Parametros:                  $date : Fecha con formato dia mes anio.
	* return:                      Fecha en Formato Oracle
	*/
   function converToSaveDate($date){
    list( $day, $month, $year ) = split( '[/.-]', $date );
    return $month."/".$day."/".$year;
   //return str_replace("/","-",$date);
   }

	/* Funcion checkDateTime:   comprueba si el valor es una fecha y hora.
    * Parametros:       $date : String a comprobar.
    * return:           true si es una fecha.
    */
	function checkDateTime($date) {
	    if ((date('Y-m-d H:i:s', strtotime($date)) == $date) ||(date('d-m-Y H:i:s', strtotime($date)) == $date)) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
	/* Funcion IsTime:   comprueba si el valor es una hora.
    * Parametros:       $date : String a comprobar.
    * return:           true si es una hora.
    */
	function IsTime($date) {
	    if (date('H:i:s', strtotime($date)) == $date) {
	        return true;
	    } else {
	        return false;
	    }
	}

   /* Funcion IsDate:   comprueba si el valor es una fecha.
    * Parametros:       $date : String a comprobar.
    * return:           true si es una fecha.
    */
   function IsDate($date){
	    list($dd,$mm,$yy)=split('[/.-]',$date);
	    if ($dd!="" && $mm!="" && $yy!="")    {
	       if((checkdate($mm,$dd,$yy))||(checkdate($mm,$yy,$dd))) return true;else return false;          
	    }
	    return false;
   }
   
   
   	/* Funcion notificar:   genera los mensajes de error
	* Parametros:                  $mensajeN : tipo de mensaje (Tipo , Longitud)
	* 								$parametroN: tipo de dato
	* 								$campoN: nombre del campo
	* 								$numFilaN: numero de fila donde esta el error
	* 								$numCampoN: nuemro de campo donde esta el error
	*/
   function notificar($mensajeN,$parametroN,$campoN,$numFilaN,$numCampoN){
   		switch ($mensajeN) {
   			case 'tipo':
				$this->Session->setFlash('Tipo de campo incorrecto en linea '.$numFilaN.'. Se espera un tipo de dato '.$parametroN.' en el campo '.$numCampoN.'. Dato recibido :"'.$campoN.'"', ERROR);
				break;
			case 'longitud':
   				$this->Session->setFlash('Longitud de campo incorrecta en linea '.$numFilaN.'. Se espera un n�mero m�ximo de '.$parametroN.' caracteres  en el campo '.$numCampoN, ERROR);
				break;
			default:
				break;
		}
   }
   
   /* Funcion checkearTipo:   comprueba si el campo recibido tiene el mismo tipo del 
    * 						  campo que se espera
	* Parametros:                  $tipo : tipo de dato
	* 							   $campo: nombre del campo
	* 							   $numFila: numero de fila
	* 							   $numCampo: numero de campo en la fila
    *  return:           true si esta todo ok.
	*/
   function checkearTipo($tipo,$campo,$numFila,$numCampo){
   		switch ($tipo) {
		    case ($tipo=='integer' || $tipo=='number'):
		    	if (!is_numeric($campo)) {
		    		$this->notificar("tipo",$tipo,$campo,$numFila,$numCampo);
					return false;
				}
		        break;
			case 'float':
				if (is_float($campo)) {
					$this->notificar("tipo",$tipo,$campo,$numFila,$numCampo);
					return false;
				}
		        break;
		    case 'datetime':
				if (!$this->checkDateTime($campo)) {
					$this->notificar("tipo",$tipo,$campo,$numFila,$numCampo);
					return false;
				}
		        break;
			case 'date':
				if (!$this->IsDate($campo)) {
					$this->notificar("tipo",$tipo,$campo,$numFila,$numCampo);
					return false;
				}
		        break;
			case 'time':
				if (!$this->IsTime($campo)) {
					$this->notificar("tipo",$tipo,$campo,$numFila,$numCampo);
					return false;
				}
		        break;
			case 'boolean':
				if (!is_bool($campo)) {
					$this->notificar("tipo",$tipo,$campo,$numFila,$numCampo);
					return false;
				}
		        break;
			case 'string':
				if (!is_string($campo)) {
					$this->notificar("tipo",$tipo,$campo,$numFila,$numCampo);
					return false;
				}
		        break;
			default:
				break;
		}
	 	return true;          
    }
   /* Funcion checkearLongitud:   comprueba si el campo recibido tiene la misma longitud  del 
    * 						  campo que se espera
	* Parametros:                  $longitud : longitud del dato
	* 							   $campo: nombre del campo
	* 							   $numFila: numero de fila
	* 							   $numCampo: numero de campo en la fila
    *  return:           true si esta todo ok.
	*/
   	function checkearLongitud($longitud,$campo,$numFila,$numCampo){
   		if(($longitud!=0) && (strlen($campo)!=0)){
   			if (strlen($campo)>$longitud) {
   				$this->notificar("longitud",$longitud,$campo,$numFila,$numCampo);
				return false;
			}
		}
		return true;
	}
	
    /* Funcion checkearCampo:   comprueba si el campo es nulo, tipo de datos correcto y longitud correcta.
    * Parametros:       $nulo : atributo Nulo del campo a comprobar.
	* 					$tipo : atributo Tipo del campo a comprobar.		
	* 					$longitud : atributo Longitud del campo a comprobar.
	* 					$campo : campo a comprobar.
	* 					$numFila : Numero de fila en la que se encuentra el campo a comprobar.
	* 					$numCampo : posicion en la fila del campo a comprobar.
    * return:           true si es correcto.
    */
    function checkearCampo($nulo,$tipo,$longitud,$campo,$numFila,$numCampo){
    	if (($nulo=="NO")){
			if(strlen($campo)!=0){
				// Si el campo es NOT NULL y tiene contenido
				if (!$this->checkearTipo($tipo,$campo,$numFila,$numCampo)) return false;
			}else{
				// Si el campo es es NOT NULL y NO tiene contenido y Si no es el campo ID
				if($numCampo!=0) {
					$this->Session->setFlash('Valor de campo incorrecto en linea '.$numFila.'.Se espera alg�n valor para el campo '.$numCampo, ERROR);
					return false;
				}
			}
		}else{
			if ((($nulo)&&((strlen($campo)!=0)&&($campo!="")&&(strtolower($campo))!="null"))){
				// Si el campo es NULL y tiene contenido
				if (!$this->checkearTipo($tipo,$campo,$numFila,$numCampo)) return false;
			}else{
				// Si el campo es NULL y NO tiene contenido
				$campos[$numCampo]=null;
			}
		}
		if (!$this->checkearLongitud($longitud,$campo,$numFila,$numCampo)) return false;
		return true;
    }
	
	/* Funcion crearArchivoServidor:   Crea y Analiza el fichero a importar,
    * Parametros:       $archivo : Nombre del archivo a importar.
	 * 					$modelo : Nombre del modelo donde se va a importar el archivo.		
	 * 					$propiedadesCamposModelo : atributos de los campos del modelo.
	 * 					$delimitadorCampo: por defecto ','.
	 *  				$delimitadorTexto: por defecto '"'.
	 *  				$delimitadorFila: por defecto '\r\n'.
    * return:           $data contiene un array con los registros a importar en la tabla.
    */
	function crearArchivoServidor($archivo,$modelo,$propiedadesCamposModelo,$delimitadorCampo,$delimitadorTexto,$delimitadorFila){
		//$this->Session->setFlash('N�mero de campos incorrecto en linea ' . $i, ERROR);
		$destino = new Folder('..'.DS.'webroot'.DS.'files');
		ini_set('date.timezone','Europe/Madrid');
        $ahora = date('Ymd_His');
        $nombreArchivo = $modelo.'_'.$ahora.'_'.$archivo['name'];
		$numCamposModelo = sizeof($propiedadesCamposModelo);
		//Llevamos el fichero a la carpeta Webroot/files
		move_uploaded_file($archivo['tmp_name'],$destino->path.$nombreArchivo);
		$file= new File($destino->path.$nombreArchivo);
        //Comprobamos el n�mero de campos
		if (!is_readable ($file->path)) {
			$this->Session->setFlash('No se puede leer ' . $file, ERROR);
			unlink($file->path);
			return "error";
		}
        $handle = fopen($file->path, "r");
		$contenido = fread($handle, filesize($file->path));
		$contenido=str_replace( $delimitadorTexto, '', $contenido ); 
		$filas=array();
        $filas = explode($delimitadorFila, $contenido);
		$data = array();
		//Por cada fila del fichero...
		for($i=0;$i<sizeof($filas);$i++) {
			$campos = explode($delimitadorCampo, $filas[$i]);
			if($campos[0]==$propiedadesCamposModelo[0][name]) continue;
			if (count($campos)!=$numCamposModelo) {
				$this->Session->setFlash('N�mero de campos incorrecto en linea ' . $i, ERROR);
				unlink($file->path);
				return "error";
			}
			//Por cada campo de la fila...
			for($j = 0; $j < $numCamposModelo; $j++){
					$tipoCM=$propiedadesCamposModelo[$j]['type'];
					$nuloCM=$propiedadesCamposModelo[$j]['null'];
					$longitudCM=$propiedadesCamposModelo[$j]['length'];
					if(strlen($propiedadesCamposModelo[$j]['length'])==0) $longitudCM=0;
					if (!$this->checkearCampo($nuloCM,$tipoCM,$longitudCM,$campos[$j],$i,$j)){
						unlink($file->path);
						return "error";
					}
					$data[$i][$modelo][$propiedadesCamposModelo[$j][name]]=$campos[$j];
			}
		}
		fclose($handle);
		unlink($file->path);
		return $data;
	}
	
	/* Funcion compruebaExtension:   comprueba que la extension del archivo sea correcta
    * Parametros:       $archivo : Nombre del archivo a importar.
	* 					
    * return:           true si es correcto.
	*/
	function compruebaExtension($archivo){
		$extension = $this->getFileExtension($archivo['name']);
		foreach($this->ext_permitidas as $ext) {
			if($ext == $extension) return true;
		}
		return false;
	}
	
	/* Funcion getFileExtension:   obtiene la extension del archivo
    * Parametros:       $file : Nombre del archivo a importar.
	* 					
    * return:            $fileArray extension del archivo
	*/
	function getFileExtension($file){
		$fileArray = explode('.',$file);
		return $fileArray[count($fileArray)-1];
	}
	
	/* Funcion compruebaTipo:   comprueba que el tipo esta permitido
    * Parametros:       $archivo : Nombre del archivo a importar.
	* 					
    * return:           true si es corercto.
	*/
	function compruebaTipo($archivo){
		foreach($this->tipos_permitidos as $tipo) {
			if($tipo == $archivo['type']) return true;
		}
		return false;
	}
	
	/* Funcion importar:   Importa el fichero selecionado
    * Parametros:       $obj: objeto $this de metodo padre.
	* 				   $modelo : nombre del modelo en el que importar el archivo
    * return:          true si esta todo correcto.
    */
	function importar($obj,$modelo){
		$archivo = $obj->params->form['file'];
		$delimitadorCampo=$obj->data['delimitadorCampo'];
		$delimitadorTexto=$obj->data['delimitadorTexto'];
		$delimitadorFila=$obj->data['delimitadorFila'];
		//die(print_r($delimitadorCampo));
		if ($delimitadorFila=="auto") $delimitadorFila = "\r\n";
			//$nombreModelo = $this->modelClass;
			$errorFile=false;
			if(!$obj->$modelo->isUploadedFile($archivo)) {
				//Comprobamos si la extenson del fichero es .csv
				if(!$obj->Importacion->compruebaExtension($archivo)){
					 $obj->Session->setFlash(MSG_IMPORTFILEERROR, ERROR);
					 $errorFile=true;
				}
				//Comprobamos si el tipo del fichero es valido
				if(!$obj->Importacion->compruebaTipo($archivo)){
					$obj->Session->setFlash(MSG_IMPORTFILEERROR, ERROR);
					$errorFile=true;
				}
				//Comprobamos el tama�o
				if($archivo['size']>15360){
					$obj->Session->setFlash(MSG_IMPORTSIZEERROR, ERROR);
					$errorFile=true;
				}
				//Si el fichero es correcto
				if (!$errorFile){
					$schema=$obj->$modelo->schema(true);
					$propiedades=$obj->Esquema->propiedadesModelo($schema);
				    $registros=$obj->Importacion->crearArchivoServidor($archivo,$modelo,$propiedades,$delimitadorCampo,$delimitadorTexto,$delimitadorFila);
					//si esta bien formado
					if ($registros!="error"){
						if($obj->$modelo->saveAll($registros, array('validate'=>false))) {
							$obj->Session->setFlash(MSG_IMPORTSUCCESS.'('.count($registros).' registros importados)', SUCCESS);
						} else {
						   	$obj->Session->setFlash(MSG_IMPORTERROR, ERROR);
						}
					}
				}
			}
		return true;
	}
	
}