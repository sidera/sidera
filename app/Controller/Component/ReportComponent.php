<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Component', 'Controller');

class ReportComponent extends Component {
	
	var $jasperWS;
	/*var $jasperUser="jasperadmin";
	var $jasperPass="jasper";*/
        var $jasperUser=DEFAULT_LOGIN_JASPER;
        var $jasperPass=DEFAULT_PASSWORD_JASPER;
	var $reportUnit;
	var $reportFormat;
	var $reportParams;
	
/*	//called before Controller::beforeFilter()
	function initialize(&$controller, $settings = array()) {
		// saving the controller reference for later use
		$this->controller =& $controller;
	}*/
	/*//called after Controller::beforeFilter()
	function startup(&$controller) {
	}
	//called after Controller::beforeRender()
	function beforeRender(&$controller) {
	}
	//called after Controller::render()
	function shutdown(&$controller) {
	}
	//called before Controller::redirect()
	function beforeRedirect(&$controller, $url, $status=null, $exit=true) {
	}*/
	function redirectSomewhere($value) {
		// utilizing a controller method
		$this->controller->redirect($value);
	}
	  
    /**
     * Establece $reportUnit.
     * @param string $reportUnit
     * @see CORE_Informe::$reportUnit
     */
    public function setReportUnit($reportUnit)
    {
        $this->reportUnit = $reportUnit;
    }
	
    /**
     * Establece $reportFormat.
     * @param string $reportFormat
     * @see CORE_Informe::$reportFormat
     */
    public function setReportFormat($reportFormat)
    {
        $this->reportFormat = $reportFormat;
    }
	
	/**
    * Establece $reportParams.
    * @param array $reportParams
    * @see CORE_Informe::$reportParams
    */
    public function setReportParams($reportParams)
    {
        $this->reportParams = $reportParams;
    }
	
	private function ws_obtenerInforme()
	{
		$this->jasperWS= "http://".REPORT_SERVER."/jasperserver/services/repository?wsdl";
        
		$cliente = new SoapClient(null, array(
	        'location'     => $this->jasperWS,
	        'uri'          => 'urn:',
	        'login'        => $this->jasperUser,
	        'password'     => $this->jasperPass,
	        'trace'    	   => 1,
	        'exception'	   => 1,
	        'soap_version' => SOAP_1_1,
	        'style'        => SOAP_RPC,
	        'use'          => SOAP_LITERAL
	        
		
		));
		
		// Creamos la información para comunicarnos con el WS del JasperServer
		$op_xml = "<request operationName=\"runReport\">"
				 ."<argument name=\"RUN_OUTPUT_FORMAT\">$this->reportFormat</argument>\n"
				 ."<resourceDescriptor name=\"\" wsType=\"reportUnit\" uriString=\"$this->reportUnit\" isNew=\"false\">"
				 ."<label></label>";


      
		// Añadimos los parámetros del informe
		if (is_array ($this->reportParams))
		{
			$keys = array_keys($this->reportParams);
			foreach ($keys AS $key) $op_xml .="<parameter name=\"$key\"><![CDATA[".$this->reportParams[$key]."]]></parameter>"; 	
		}
		$op_xml .="</resourceDescriptor></request>";
	
		try
		{
			$informe = $cliente->__soapCall("runReport",array(new SoapParam($op_xml,"requestXmlString")));
		
		}
		catch(SoapFault $e)
		{
			
			$informe = $cliente->__getLastResponse();
			
		}		
	
		return $informe;
		
	}
	/* Funci�n:obtenerInforme
     * Descripci�n:
     */
	function obtenerInforme($reportUnit, $reportFormat, $reportParams, $reportName){
	  	$this->setReportUnit($reportUnit);
		$this->setReportFormat($reportFormat);
        if($reportParams!=NULL){
                $this->setReportParams($reportParams);
        }
               
		$informe = $this->ws_obtenerInforme();
		$informeAux = explode("<report>",$informe);
		$informeFinal =ltrim($informeAux[1]);
		
		switch($this->reportFormat){
			case 'PDF': 
						$cab_tipo = "Content-type: application/pdf";
						$cab_mime = 'Content-Disposition: attachment; filename="'.$reportName.'.pdf"';
						break;
			case 'HTML':
						$cab_tipo = "Content-type: text/html";
						$cab_mime = "";
						break;
			case 'XLS': 
						$cab_tipo = 'Content-type: application/xls';
						$cab_mime = 'Content-Disposition: attachment; filename="'.$reportName.'.xls"';
						break;
			case 'RTF':
						$cab_tipo = 'Content-type: document/rtf';
						$cab_mime = 'Content-Disposition: attachment; filename="'.$reportName.'.rtf"';
						break;
			case 'XML': 
						$cab_tipo = 'Content-type: text/xml';
						$cab_mime = 'Content-Disposition: attachment; filename="'.$reportName.'.xml"';
						break;
		}
        
       
		header($cab_tipo);
		header($cab_mime);
		echo $informeFinal;
	}
}