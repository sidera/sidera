<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Component', 'Controller');
App::uses('Folder','Utility');
App::uses('File','Utility');
App::import('vendor', 'Spyc');

/* 
 * Class ConfigurarComponent
 * Descripci�n: Reune los m�todos usados para configurar un m�dulo.
 * (actualizado en Sidera 2.0.0)
 */

class ConfigurarComponent extends Component {
		
	public  $components     = array('Files','Esquema');
	private $viewPath       = null;
	private $modelPath      = null;
	private $controllerPath = null;
     
	private $archivoAdd     = null;
	private $archivoIndex   = null;
	private $archivoView    = null;	
	private $archivoEdit    = null;
	private $configPath     = null;
        
    function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection,$settings);
		
		//Ruta para los archivos que no son de SIDERA, desde aqu� modificamos para todo el constructor.
		$this->viewPath       = APP.'AppModulos'.DS.'View'.DS;
		$this->modelPath      = APP.'AppModulos'.DS.'Model'.DS;
		$this->controllerPath = APP.'AppModulos'.DS.'Controller'.DS;
		$this->configPath     = APP.'AppModulos'.DS.'Config'.DS;
        
        $this->archivoAdd     = 'add.ctp';
		$this->archivoIndex   = 'index.ctp';
		$this->archivoView    = 'view.ctp';
		$this->archivoEdit    = 'edit.ctp';
	}

	public function addNuevaRelacionCombos($obj) {
		$modelo = $obj->request->params['pass'][0];
        
		$this->Files->crearArchivoDeSeguridad("AppModulos/Controller".DS.ucwords(Inflector::pluralize($modelo))."Controller",$this->controllerPath);
		$registros = $this->reescribirModelosAjenosControlador($obj);

		$this->Files->crearArchivoDeSeguridad("AppModulos/View".DS.ucwords(Inflector::pluralize($modelo)),$this->archivoAdd);
		$registros = $this->reescribirAddEditRelacionarCombos($obj,'add');

		$this->Files->crearArchivoDeSeguridad("AppModulos/View".DS.ucwords(Inflector::pluralize($modelo)),$this->archivoEdit);
		$registros = $this->reescribirAddEditRelacionarCombos($obj,'edit');

		return true;
	}
     
	public function addNuevoComboModal($obj) {
		$modelo = $obj->request->params['pass'][0];
        
		$this->Files->crearArchivoDeSeguridad("AppModulos/View".DS.ucwords(Inflector::pluralize($modelo)),$this->archivoAdd);
		$registros = $this->reescribirAddEditComboModal($obj,'add');

		$this->Files->crearArchivoDeSeguridad("AppModulos/View".DS.ucwords(Inflector::pluralize($modelo)),$this->archivoEdit);
		$registros = $this->reescribirAddEditComboModal($obj,'edit');

		return true;
	}

	public function reescribirModelosAjenosControlador($obj) {
		$modelo            = $obj->request->params['pass'][0];
		$comboPrincipal    = $obj->request->params['pass'][1];
		$comboRelacionado  = $obj->request->params['pass'][2];
		$avanzado          = $obj->request->params['pass'][3];
		$listaCampos       = $obj->request->params['pass'][4];
        $separador         = str_replace("&nbsp;"," ",$obj->request->params['pass'][5]);
        $archivo           = $this->controllerPath.ucwords(Inflector::pluralize($modelo))."Controller.php";
        $contenido         = file_get_contents($archivo);
        $modeloRelacionado = ucwords(substr($comboRelacionado,0,$nuevoCampo-3));
        if ($avanzado=='true') {
            $modeloRelacionadoClave = '"'.$modeloRelacionado.'"=>array("'.$comboPrincipal.'","'.$separador.'","'.$listaCampos.'")';
        }else{
            $modeloRelacionadoClave = '"'.$modeloRelacionado.'"=>array("'.$comboPrincipal.'")';
        }
        
        //Sustitucion de "public $modelosAjenos"
        $patronModelosAjenos = "/.*this->modelosAjenos[^Relacionados].*/";
        preg_match($patronModelosAjenos,$contenido,$coindicencias);
        preg_match($patronModelosAjenosRelacionados,$sustitucionAjenos,$coindicenciasRelacionados);
        $nuevaCadenaModelosAjenos = preg_replace("/,\"".$modeloRelacionado."\"/", "", $coindicencias[0]);
        $nuevaCadenaModelosAjenos = preg_replace("/\"".$modeloRelacionado."\",/", "", $nuevaCadenaModelosAjenos);
        $nuevaCadenaModelosAjenos = preg_replace("/\"".$modeloRelacionado."\"/", "", $nuevaCadenaModelosAjenos);
        $sustitucionAjenos        = preg_replace($patronModelosAjenos, $nuevaCadenaModelosAjenos, $contenido);
        
        //Sustitucion de "public $modelosAjenosRelacionados"
        $patronModelosAjenosRelacionados = "/.*this->modelosAjenosRelacionados.*/";
        preg_match($patronModelosAjenosRelacionados,$sustitucionAjenos,$coindicenciasRelacionados);
        preg_match("/\(\)/",$coindicenciasRelacionados[0],$arrayVacio);
        if (isset($arrayVacio[0])) {
            $nuevaCadenaModelosAjenosRelacionados = preg_replace("/\(\)/","(".$modeloRelacionadoClave.")", $coindicenciasRelacionados[0]);
            $sustitucionAjenosYAjenosRelacionados = preg_replace($patronModelosAjenosRelacionados, $nuevaCadenaModelosAjenosRelacionados, $sustitucionAjenos);
        }else{
            $nuevaCadenaModelosAjenosRelacionados = preg_replace("/\"\)\)/","\"),".$modeloRelacionadoClave.")", $coindicenciasRelacionados[0]);
            $sustitucionAjenosYAjenosRelacionados = preg_replace($patronModelosAjenosRelacionados, $nuevaCadenaModelosAjenosRelacionados, $sustitucionAjenos);
        }
        $contenido = $sustitucionAjenosYAjenosRelacionados;
        file_put_contents($archivo, $contenido);
        return true;
	}

	function reescribirAddEditRelacionarCombos($obj,$vista){
        $modelo            = $obj->request->params['pass'][0];
        $comboPrincipal    = $obj->request->params['pass'][1];
        $comboRelacionado  = $obj->request->params['pass'][2];
        $avanzado          = $obj->request->params['pass'][3];
        $listaCampos       = $obj->request->params['pass'][4];
        $listaCampos       = explode(',', $listaCampos);
        $listaCampos       = implode('","', $listaCampos);
        $separador         = str_replace("&nbsp;"," ",$obj->request->params['pass'][5]);
        $modeloRelacionado = ucwords(substr($comboRelacionado,0,$nuevoCampo-3));
        $archivo           = null;
        $abrir             = null;
        $contenido         = null;
        $archivo           = $this->viewPath.ucwords(Inflector::pluralize($modelo)).DS.$vista.'.ctp';
        $abrir             = fopen( $archivo,'r+');
        $contenido         = fread($abrir,filesize($archivo));
        fclose($abrir);
        // Separar contenido por tramos
        $contenido  = explode('<?php $espacioFuturosControles=true; ?>', $contenido); // _SLN = "\r\n"
        $formulario = null;
        if ($avanzado=='true'){
        $formulario = '
    $("#'.$comboPrincipal.'").change(function(event){
        event.preventDefault();
        var modeloRelacionado="'.$modeloRelacionado.'";
        var comboPrincipal="'.$comboPrincipal.'";
        var comboPrincipalId=$("#'.$comboPrincipal.' option:selected").val();
        var listaCampos = new Array("'.$listaCampos.'");   
        var separador = "'.$separador.'";
        $.post("combos/cargarComboRelacionadoAvanzado/",{"comboPrincipal":comboPrincipal,"modeloRelacionado":modeloRelacionado,"comboPrincipalId":comboPrincipalId,"listaCampos":listaCampos,"separador":separador},
            function(data){
                $("#'.$comboRelacionado.'").html(data);
                $("#'.$comboRelacionado.'").trigger("liszt:updated");
        });
     });

     ';
        }else{
        $formulario = '
    $("#'.$comboPrincipal.'").change(function(event){
        event.preventDefault();
        var modeloRelacionado="'.$modeloRelacionado.'";
        var comboPrincipal="'.$comboPrincipal.'";
        var comboPrincipalId=$("#'.$comboPrincipal.' option:selected").val();
        $.post("combos/cargarComboRelacionado/",{"comboPrincipal":comboPrincipal,"modeloRelacionado":modeloRelacionado,"comboPrincipalId":comboPrincipalId},
            function(data){
                $("#'.$comboRelacionado.'").html(data);
                $("#'.$comboRelacionado.'").trigger("liszt:updated");
        });
     });

     ';
		} 
		// Unir archivo
		$contenido[0] = $contenido[0].$formulario;
		$contenido    = implode('<?php $espacioFuturosControles=true; ?>',$contenido);
        
		// Guardar Archivo
		$abrir = fopen($archivo,'w');
		fwrite($abrir,$contenido);
        fclose($abrir);
        return true;
    }

	function reescribirAddEditComboModal($obj,$vista){
		$modelo            = $obj->request->params['pass'][0];
        $controlador       = strtolower(Inflector::pluralize($modelo));
        $comboModal        = $obj->request->params['pass'][1];
        $modeloRelacionado = ucwords(substr($comboModal,0,$nuevoCampo-3));
        $archivo           = null;
        $abrir 			   = null;
        $contenido         = null;
        $archivo           = $this->viewPath.ucwords(Inflector::pluralize($modelo)).DS.$vista.'.ctp';
        $abrir             = fopen( $archivo,'r+');
        $contenido         = fread($abrir,filesize($archivo));
        fclose($abrir);
        // Separar contenido por tramos
        // Primera Parte
        $isTagged  = (preg_match('/<\?#(.*?)\?>(.*?)<\?##(.*?)\?>/si', $contenido)==1);
        if($isTagged){
        	$contenido             = explode('<?#'.$comboModal.'?>', $contenido);
        	$contenido2            = explode('<?##'.$comboModal.'?>', $contenido[1]);
        }else{
        	$contenido             = explode('<?php $'.$comboModal.'="inicio";?>', $contenido);
        	$contenido2            = explode('<?php $'.$comboModal.'="fin";?>', $contenido[1]);
        }
        $campoComboModal       = substr(ucfirst($comboModal),0,-3);
        $controladorComboModal = Inflector::pluralize(substr($comboModal,0,-3));
        
        if($isTagged){
        	$contenido[1]          = '
				<?#'.$comboModal.'?>
					<?php 
					if($configModulo["'.$vista.'"]["campos"]["'.$comboModal.'"]["mostrar"] == ACTIVO){
						$after = (!isset($formType))? "<button type=\'button\' class=\'btn btn-small inverse boton-combo-modal light-blue\' id=\''.$vista.$campoComboModal.'\'><i class=\'fa fa-plus-circle\'> </i></button>":"";
						$contenido = $this->Form->input("'.$comboModal.'",array(
							"after" => $after,
							"id"    => "'.$comboModal.'",
							"empty" => "Seleccione una opci�n...",
							"label" => $configModulo["'.$vista.'"]["campos"]["'.$comboModal.'"]["etiqueta"],
							"class" => "input-xlarge chzn-select",
							"div"   => array("class"=>"input-append combo-modal")
						));
						$this->Sidera->organizeFieldsInColumns($configModulo["'.$vista.'"]["campos"]["'.$comboModal.'"]["orden"],$contenido);
	                }
	            	?>
				<?##'.$comboModal.'?>';
        }else{
        	$contenido[1]          = '
				<?php $'.$comboModal.' = "inicio";?>
					<?php 
						$after = (!isset($formType))? "<button type=\'button\' class=\'btn btn-small inverse boton-combo-modal light-blue\' id=\''.$vista.$campoComboModal.'\'><i class=\'fa fa-plus-circle\'> </i></button>":"";
						echo $this->Form->input("'.$comboModal.'",array(
							"after" => $after,
							"id"    => "'.$comboModal.'",
							"label" => $configModulo["'.$vista.'"]["campos"]["'.$comboModal.'"]["etiqueta"],
							"class" => "input-xlarge chzn-select",
							"empty" => "Seleccione una opci�n...",
							"div"   => array("class"=>"input-append combo-modal")
					));?>
				<?php $'.$comboModal.'="fin";?>
                ';
        }
        $contenido[2] = $contenido2[1];
        $contenido    = implode('',$contenido);
        
        //Segunda Parte
        $contenido    = explode('<?php echo $this->Form->end();?>', $contenido);
        $contenido[2] = $contenido[1];
        $contenido[1] = '
<?php echo $this->Form->end();?>
<?php if(!isset($formType)){ ?>        		
<?php echo $this->element("dialogMessage",array(
	"dialogId" => "'.$controladorComboModal.'Dialog",
	"launcherId" => "'.$vista.$campoComboModal.'",
	"header"   => "'.$controladorComboModal.'",
	"url"      => "'.$controladorComboModal.'/add",
	"cancelButton" => false, 
	"oklButton"    => false,
	"type"     => "formDialog",
	"callback" => "updateFromDialog'.$campoComboModal.'",
	"modelo"   => "'.$campoComboModal.'" 
));?>
<?php } ?>
    ';
        $contenido    = implode('',$contenido);
        //Tercera Parte
        $contenido    = explode('<?php $espacioFuturosControles=true; ?>', $contenido);
        $contenido[2] = $contenido[1];
        $contenido[1] = '
    <?php if(!isset($formType)){ ?>
    jQuery.fn.extend({
        updateFromDialog'.$campoComboModal.': function() {
            $("#'.$comboModal.'").load("'.$controlador.'/'.$vista.' #'.$comboModal.' option",function(response, status, xhr){
                $("#'.$comboModal.'").trigger("liszt:updated");
            });
        }
    });
    <?php } ?>

    <?php $espacioFuturosControles=true; ?>        
    ';
        $contenido = implode('',$contenido);
        // Guardar archivo
        $abrir = fopen($archivo,'w');
        fwrite($abrir,$contenido);
        fclose($abrir);
        return true;
    }

    public function addNuevoCampo($obj,$modelo,$nuevoCampo) {
		$opciones  = $obj->request->data;
                
		$schema    = $obj->$modelo->schema();
		$propiedadesCampos  = $obj->Esquema->propiedadesModelo($schema);

		$this->Files->crearArchivoDeSeguridad("AppModulos/View".DS.ucwords(Inflector::pluralize($modelo)),$this->archivoIndex);
		$this->Files->crearArchivoDeSeguridad("AppModulos/View".DS.ucwords(Inflector::pluralize($modelo)),$this->archivoAdd);
		$this->Files->crearArchivoDeSeguridad("AppModulos/View".DS.ucwords(Inflector::pluralize($modelo)),$this->archivoEdit);
                 
		//Comprobamos si es version nueva o anterior. 
		$archivo   = $this->viewPath.ucwords(Inflector::pluralize($modelo)).DS.'index.ctp';
		$contenido = file_get_contents($archivo);

		if($this->isTagged($contenido)){
			$this->aniadirCampoIndex($obj,$modelo,$nuevoCampo,$propiedadesCampos); 
			$this->aniadirCampoAddEdit($obj,$modelo,$nuevoCampo,$propiedadesCampos,"add"); 
			$this->aniadirCampoAddEdit($obj,$modelo,$nuevoCampo,$propiedadesCampos,"edit");
		}else{
			$this->reescribirIndexAniadir($obj,$modelo,$propiedadesCampos,$nuevoCampo);
			$this->reescribirAddEditAniadir($obj,$modelo,$nuevoCampo,$propiedadesCampos,'add');
			$this->reescribirAddEditAniadir($obj,$modelo,$nuevoCampo,$propiedadesCampos,'edit');
		}
		return true;
	}


	public function delCampoApp($obj,$modelo,$campoAEliminar) {
		$opciones = $obj->request->data;

		$this->Files->crearArchivoDeSeguridad("AppModulos/View".DS.ucwords(Inflector::pluralize($modelo)),$this->archivoIndex);
		$registros=$this->reescribirIndexEliminar($obj,$modelo,$campoAEliminar);

		$this->Files->crearArchivoDeSeguridad("AppModulos/View".DS.ucwords(Inflector::pluralize($modelo)),$this->archivoAdd);
		$registros=$this->reescribirAddEditViewEliminar($obj,$modelo,$campoAEliminar,'add');

		$this->Files->crearArchivoDeSeguridad("AppModulos/View".DS.ucwords(Inflector::pluralize($modelo)),$this->archivoEdit);
		$registros=$this->reescribirAddEditViewEliminar($obj,$modelo,$campoAEliminar,'edit');
		return true;
	}
        
	function reescribirIndexAniadir($obj,$modelo,$propiedadesCampos,$nombreCampo){
		$archivo   = $this->viewPath.ucwords(Inflector::pluralize($modelo)).DS.'index.ctp';
		$contenido = file_get_contents($archivo);
                                
        // Separar contenido por tramos
        $contenido = explode('<?php $espacioFuturosCampos=true; ?>', $contenido); // _SLN = "\r\n"
        
        //Establecer cabecera 
        $cabecera  = '
       					<?php $'.$nombreCampo.'="inicio";?><?php echo $this->element("ElementsVista".DS."Index".DS."indexHeader",array("campo"=>"'.$nombreCampo.'","$configModulo"=>$configModulo));?><?php $'.$nombreCampo.'="fin";?>
						';
        // Unir archivo
        $contenido[0] = $contenido[0].$cabecera;
        
        //tipo de campo
        foreach ($propiedadesCampos as $propiedad){
			if ($propiedad['name'] == $nombreCampo) $tipoCampo = $propiedad['type'];
		}
                
        $comienzoCampo = substr($nombreCampo,0,5);
        if($comienzoCampo=='bool_') $tipoCampo = "boolean";
        $terminacionCampo = substr($nombreCampo,$nombreCampo-3,strlen($nombreCampo));
		$modeloAjeno = ucwords(substr($nombreCampo,0,-3));

        switch ($tipoCampo) {
			case "boolean":
					$cuerpo.='<td class="'.$nombreCampo.'"><?php echo $this->Form->input("",array("type" => "checkbox","disabled"=>"true",checked =>$registro[$modelo]["'.$nombreCampo.'"])); ?></td>';
                                    break; 
			case "float":
				if($isTagged){
					$cuerpo.='
						<?php 
							if($configModulo["index"]["campos"]["'.$nombreCampo.'"]["mostrar"] == ACTIVO){ 
								$cols[$configModulo["index"]["campos"]["'.$nombreCampo.'"]["orden"]] = \'<td class="'.$nombreCampo.'">\'.$this->Number->format($registro[$modelo]["'.$nombreCampo.'"], array(
									"places"    => 2,
									"before"    => false,
                                    "decimals"  => ",",
                                    "thousands" => "."
                                    )).\'</td>\';
							}
						?>';
				}else{
					$cuerpo.='<td class="'.$nombreCampo.'"><?php if(!is_null($registro[$modelo]["'.$nombreCampo.'"])) echo $this->Number->format($registro[$modelo]["'.$nombreCampo.'"], array(
								"places" => 2,
		                        "before" => false,
		                        "decimals" => ",",
		                        "thousands" => "."
		                        )); ?></td>';
				}
				break;
			case "date":
					$cuerpo.='<td class="'.$nombreCampo.'"><?php if(!is_null($registro[$modelo]["'.$nombreCampo.'"])) echo $this->Time->format("d/m/Y",$registro[$modelo]["'.$nombreCampo.'"]); ?></td>';
			case (($tipoCampo=="datetime")||($tipoCampo=="timestamp")):
					$cuerpo.='<td class="'.$nombreCampo.'"><?php if(!is_null($registro[$modelo]["'.$nombreCampo.'"])) echo $this->Time->format("d/m/Y h:i:s",$registro[$modelo]["'.$nombreCampo.'"]); ?></td>';
				break;
			case ($tipoCampo=="time"):
					$cuerpo.='<td class="'.$nombreCampo.'"><?php if(!is_null($registro[$modelo]["'.$nombreCampo.'"])) echo $this->Time->format("H:i:s",$registro[$modelo]["'.$nombreCampo.'"]); ?></td>';
				break;
			default:
				if($terminacionCampo != "_id"){
						$cuerpo.='<td class="'.$nombreCampo.'"><?php if(!is_null($registro[$modelo]["'.$nombreCampo.'"])) echo h($registro[$modelo]["'.$nombreCampo.'"]); ?></td>';
				}else{
						$cuerpo.='<td class="'.$nombreCampo.'">
                                                            <?php echo $registro["'.$modeloAjeno.'"][$'.$nombreCampo.'DisplayField]; ?></td>';
				}
                        }
        $contenido[1] = $contenido[1].$cuerpo;
        $contenido    = implode('
        				<?php $espacioFuturosCampos=true; ?>',$contenido);

        file_put_contents($archivo, $contenido);
		return true;
	}

	function isTagged($contenido) {
            return (preg_match('/<\?#(.*?)\?>(.*?)<\?##(.*?)\?>/si', $contenido)==1);
        }

	function aniadirCampoIndex($obj,$modelo,$nombreCampo,$propiedadesCampos){
		$archivo   = $this->viewPath.ucwords(Inflector::pluralize($modelo)).DS.'index.ctp';
		$contenido = file_get_contents($archivo);

        include APP.'View'.DS.'Templates'.DS.'Constructor'.DS.'Elementos'.DS.'elementosTiposIndex.php';

        $cabecera = '
        		<?#'.$nombreCampo.'?>'.
       					$headerIndex.'
        		<?##'.$nombreCampo.'?>
                            <?#nuevosCamposCabecera##?>';

        $nuevoContenido = preg_replace('/<\?#nuevosCamposCabecera##\?>/',$cabecera, $contenido);

        foreach ($propiedadesCampos as $propiedad){
			if ($propiedad['name'] == $nombreCampo) $tipoCampo = $propiedad['type'];
		}

        $comienzoCampo = substr($nombreCampo,0,5);
        if($comienzoCampo=='bool_') $tipoCampo = "boolean";
        $terminacionCampo = substr($nombreCampo,$nombreCampo-3,strlen($nombreCampo));
	$modeloAjeno = ucwords(substr($nombreCampo,0,-3));
                
         if ($propiedad["type"] == "float") {
                                $decimales = 0;
                                if (isset($propiedad["length"])) {
                                    $decimales = explode(',', $propiedad["length"]);
                                    $decimales = $decimales['1'];
                                    if($decimales=="" || $decimales==null)
                                        $decimales = 0;
                                }
            include APP.'View'.DS.'Templates'.DS.'Constructor'.DS.'Elementos'.DS.'elementosTiposIndex.php';
        }         

        switch ($tipoCampo) {
			case "boolean":
					$formulario.=$tipoBoolean;
				break;
			case "float":
					$formulario.=$tipoFloat;
				break;   
			case "date":
					$formulario.=$tipoDate;
				break;    
			case (($tipoCampo=="datetime")||($tipoCampo=="timestamp")):   
					$formulario.=$tipoDateTime;
				break;
			case ($tipoCampo=="time"):
					$formulario.=$tipoTime;
				break;
                          case ($tipoCampo=="integer" || $tipoCampo=="biginteger"):
                                        $formulario.=$tipoInteger;
                                break;        
			default:
				if($terminacionCampo != "_id"){
					$formulario.=$tipoSimple;
				}else{
					$formulario.=$tipoAjeno;
				}
			}
          $nuevoCampo =       '<?#'.$nombreCampo.'?>'.
                                    $formulario.
                                '<?##'.$nombreCampo.'?>
                                    
                                <?#nuevosCampos##?>';

        $contenidoFinal = preg_replace('/<\?#nuevosCampos##\?>/',$nuevoCampo, $nuevoContenido);
        
        file_put_contents($archivo, $contenidoFinal);
		return true;
	}
	
    function reescribirAddEditAniadir($obj,$modelo,$nombreCampo,$propiedadesCampos,$formulario){
		$vista     = $formulario;
        $archivo   = $this->viewPath.ucwords(Inflector::pluralize($modelo)).DS.$formulario.'.ctp';
        $contenido = file_get_contents($archivo);
        
        // Separar contenido por tramos
        $contenido = explode('<?php $espacioFuturosCampos=true; ?>', $contenido); 
        
        // Establecer tipo de campo en el formulario
        foreach ($propiedadesCampos as $propiedad){
			if ($propiedad['name'] == $nombreCampo) $tipoCampo = $propiedad['type'];
		}
        $formulario  = '';
       
        	$formulario .= '
		<?php $'.$nombreCampo.'="inicio";?>';
                
        $comienzoCampo = substr($nombreCampo,0,5);
        if ($comienzoCampo=='bool_') $tipoCampo = "boolean";
        if ($comienzoCampo=='time_') $tipoCampo = "time";
        $terminacionCampo = substr($nombreCampo,strlen($nombreCampo)-3,strlen($nombreCampo));

        
		if ($vista=="edit") $valueEdit='value="<?php echo $this->request->data['.$modelo.']['.$nombreCampo.'];?>"';
		else $valueEdit='';
              
		switch ($tipoCampo) {
			case "date":
					$formulario.='
				<div id="'.$nombreCampo.'Datepicker" class="input-prepend date <?php echo $configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["orden"];?>" title="Doble click para la eliminar" >
                	<label><?php echo $configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["etiqueta"];?></label>
					<button class="btn light-blue inverse date-button add-on datetimepicker-button">
						<i class="fa fa-calendar"></i>
					</button>
					<input type="text" ondblclick="$(this).val(\'\')" readonly="readonly"  class="input-small center datepicker" '.$valueEdit.' name="data['.$modelo.']['.$nombreCampo.']" >
				</div>';
				break;
			case (($tipoCampo=="datetime")||($tipoCampo=="timestamp")):
					$formulario.='
				<div id="'.$nombreCampo.'DateTimepicker" class="input-prepend <?php echo $configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["orden"];?>" title="Doble click para la eliminar">
					<label><?php echo $configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["etiqueta"];?></label>
					<button type="button"  class="btn light-blue inverse date-button add-on datetimepicker-button">
						<i class="fa fa-calendar"></i>
					</button>
					<input type="text"  ondblclick="$(this).val(\'\')" readonly="readonly" class="input-small center datepicker"  '.$valueEdit.' name="data['.$modelo.']['.$nombreCampo.']"/>
				</div>';
				break;
			case "time":
    				$formulario.='
				<div  id="'.$nombreCampo.'Timepicker" class="input-prepend <?php echo $configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["orden"];?>" title="Doble click para la eliminar">
					<label><?php echo $configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["etiqueta"];?></label>
					<button type="button"  class="btn light-blue inverse date-button add-on datetimepicker-button">
						<i class="fa fa-time"></i>
					</button>
					<input type="text" ondblclick="$(this).val(\'\')" readonly="readonly" class="input-small center datepicker"  '.$valueEdit.' name="data['.$modelo.']['.$nombreCampo.']"/>
				</div>';
				break;
			case "boolean":
				$formulario.='
				<?php echo $this->Form->input("'.$nombreCampo.'",array(
					"type"   => "checkbox",
					"id"     => "'.$nombreCampo.'",
					"label"  => $configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["etiqueta"],
					"class"  => "input-xlarge",
					"placeholder" => "Escriba el '.$nombreCampo.' ...",
					"div"    => array("class"=>" ".$configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["orden"])
				));?>';
				break;    
			default:
				if($terminacionCampo == "_id"){
					
						$formulario.='
				<?php echo $this->Form->input("'.$nombreCampo.'",array(
					"id"     => "'.$nombreCampo.'",
					"label"  => $configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["etiqueta"],
					"class"  => "input-xlarge chzn-select",
					"empty"  => "Seleccione una opci�n...",
					"div"    => array("class"=>" ".$configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["orden"]."")
				));?>';
				}else{
					$alinea = "";
					if($tipoCampo == "float" || $tipoCampo == "integer") $alinea = "right";

						$formulario.='
				<?php echo $this->Form->input("'.$nombreCampo.'",array(
					"id"     => "'.$nombreCampo.'",
					"label"  => $configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["etiqueta"],
					"class"  => "input-xlarge",
					"placeholder" => "Escriba el '.$nombreCampo.' ...",
					"div"    => array("class"=>" ".$configModulo["'.$vista.'"]["campos"]["'.$nombreCampo.'"]["orden"]."")
				));?>';

				}
				break;
		}
		$formulario .= '
			<?php $'.$nombreCampo.'="fin";?>';

        // Unir archivo
        $contenido[0] =  $contenido[0].$formulario;
        $contenido    = implode('
        	<?php $espacioFuturosCampos=true; ?>',$contenido);

         $nuevoContenido = preg_replace('<?php $espacioFuturosCampos=true; ?>',$nuevoCampo, $contenido);

        file_put_contents($archivo, $nuevoContenido);

        return true;
	}

function aniadirCampoAddEdit($obj,$modelo,$nombreCampo,$propiedadesCampos,$vista){

        $archivo   = $this->viewPath.ucwords(Inflector::pluralize($modelo)).DS.$vista.'.ctp';
        $contenido = file_get_contents($archivo);

        foreach ($propiedadesCampos as $propiedad){
			if ($propiedad['name'] == $nombreCampo) $tipoCampo = $propiedad['type'];
		}

        $comienzoCampo = substr($nombreCampo,0,5);
        if ($comienzoCampo=='bool_') $tipoCampo = "boolean";
        if ($comienzoCampo=='time_') $tipoCampo = "time";
        $terminacionCampo = substr($nombreCampo,$nombreCampo-3,strlen($nombreCampo));

        include APP.'View'.DS.'Templates'.DS.'Constructor'.DS.'Elementos'.DS.'elementosTiposAddEdit.php';

		if ($vista=="edit") $valueEdit='value="<?php echo $this->request->data['.$modelo.']['.$nombreCampo.'];?>"';
		else $valueEdit='';

		switch ($tipoCampo) {
			case "date":
                                    $formulario.=$tipoDate;
				break;
			case (($tipoCampo=="datetime")||($tipoCampo=="timestamp")):
                                    $formulario.=$tipoDateTime;
				break;
			case "time":
                                    $formulario.=$tipoTime;
				break;
			case "boolean":
                                    $formulario.=$tipoBoolean;

				break;
			default:
				if($terminacionCampo == "_id"){
                                    $formulario.=$tipoCombo;

				}else{
					$alinea = "";
					if($tipoCampo == "float" || $tipoCampo == "integer") $alinea = "right";
						$formulario.=$tipoSimple;
                                    }
				break;
		}
                
            $nuevoCampo =       '<?#'.$nombreCampo.'?>'.
                                    $formulario.
                                '<?##'.$nombreCampo.'?>
                                    
                                <?#nuevosCampos##?>';

        $nuevoContenido = preg_replace('/<\?#nuevosCampos##\?>/',$nuevoCampo, $contenido);
        file_put_contents($archivo, $nuevoContenido);
	
        return true;
	}

    /* Funcion reescribirIndexEliminar: reescribe el archivo index.ctp */
	function reescribirIndexEliminar($obj,$modelo,$campoAEliminar){
        $archivo   = $this->viewPath.ucwords(Inflector::pluralize($modelo)).DS.'index.ctp';
        $contenido = file_get_contents($archivo);
        
        if( $this->isFieldTagged($campoAEliminar,$contenido) ){
			$contenido = preg_replace('/<\?#'.$campoAEliminar.'\?>(.*?)<\?##'.$campoAEliminar.'\?>/si','', $contenido);
		}else{
			// Separar contenido por tramos. Se mantiene por compatibilidad
	        $contenido    = explode('<?php $'.$campoAEliminar.'="inicio";?>', $contenido);
	        $contenido2   = explode('<?php $'.$campoAEliminar.'="fin";?>', $contenido[1]);
	        $contenido3   = explode('<td class="'.$campoAEliminar.'">', $contenido2[1]);
	        $contenido4   = explode('</td>', $contenido3[1]);
	        unset($contenido4[0]);
	        $contenido4   = implode('</td>',$contenido4);
	        $contenido[1] = $contenido3[0];
	        $contenido[2] = $contenido4;
	        // Unir archivo sin el tramo del campo
	        $contenido    = implode('',$contenido);
		}
        // Guardar Archivo
        file_put_contents($archivo, $contenido);
		return true;
	}
    
    /* Funcion reescribirAddEditViewEliminar: reescribe los archivos add.ctp,edit.ctp,view.ctp */
	function reescribirAddEditViewEliminar($obj,$modelo,$campoAEliminar,$vista){
		// Abrir el archivo
		$archivo   = $this->viewPath.ucwords(Inflector::pluralize($modelo)).DS.$vista.'.ctp';
		$contenido = file_get_contents($archivo);
        
		if( $this->isFieldTagged($campoAEliminar,$contenido) ){
			$contenido = preg_replace('/<\?#'.$campoAEliminar.'\?>(.*?)<\?##'.$campoAEliminar.'\?>/si','', $contenido);
		}else{
	        // Separar contenido por tramos. Se mantiene por compatibilidad
	        $contenido    = explode('<?php $'.$campoAEliminar.'="inicio";?>', $contenido);
	        $contenido2   = explode('<?php $'.$campoAEliminar.'="fin";?>', $contenido[1]);
	        $contenido[1] = $contenido2[1];
	
	        // Unir archivo sin el tramo del campo
	        $contenido = implode('',$contenido);
		}
        // Guardar Archivo
        file_put_contents($archivo, $contenido);
		return true;
	}

    /* Funcion reescribirYMLModulo:  Reescribe el YML del M�dulo correspondiente cuando �ste se configura
    * Parametros:       $datosFormulario: Datos procedentes del configurar del m�dulo.
    *                   $campos: Campos del M�dulo.
    *                   $modelo: Modelo del M�dulo. 
    * return:           true si todo esta ok
    */
    public function reescribirYMLModulo($datosFormulario,$campos,$obj){
       $modelo = $obj->modelo;
       $this->Esquema->comprobarSessionModeloYML($modelo);
       $configModuloAntiguo = $this->Esquema->leerConfigSession($modelo);
       if ($datosFormulario['general']['columnas']['columna2']=='') $datosFormulario['general']['columnas']['columna2'] = 'hide'; 
        //GENERAL
        $configModulo['general']['config']['etiquetaModulo']         = iconv($this->inCharset, $this->outCharset,$datosFormulario['general']['etiquetaModulo']);
        $configModulo['general']['auditoria']['basic']               = $datosFormulario['general']['auditoria']['basic'];
        $configModulo['general']['auditoria']['view']                = $datosFormulario['general']['auditoria']['view'];
        $configModulo['general']['historico']                        = $datosFormulario['general']['historico'];
        $configModulo['general']['displayfield']['campo1']           = $configModuloAntiguo['general']['displayfield']['campo1'];
        $configModulo['general']['displayfield']['campo2']           = $configModuloAntiguo['general']['displayfield']['campo2'];
        $configModulo['general']['displayfield']['campo3']           = $configModuloAntiguo['general']['displayfield']['campo3'];
        $configModulo['general']['botones']['btconfigurar']          = $configModuloAntiguo['general']['botones']['btconfigurar'];
        $configModulo['general']['botones']['btBuscar']              = $datosFormulario['general']['botones']['btBuscar'];
        $configModulo['general']['botones']['btNuevo']               = $datosFormulario['general']['botones']['btNuevo'];
        $configModulo['general']['botones']['btEditar']              = $datosFormulario['general']['botones']['btEditar'];
        $configModulo['general']['botones']['btEliminar']            = $datosFormulario['general']['botones']['btEliminar'];
        $configModulo['general']['botones']['btFiltrar']             = $datosFormulario['general']['botones']['btFiltrar'];
        $configModulo['general']['botones']['btImportar']            = $datosFormulario['general']['botones']['btImportar'];
        $configModulo['general']['botones']['btExportar']            = $datosFormulario['general']['botones']['btExportar'];
        $configModulo['general']['acciones']['accionEditar']         = $datosFormulario['general']['acciones']['accionEditar'];
        $configModulo['general']['acciones']['accionChekearTodos']   = $datosFormulario['general']['acciones']['accionChekearTodos'];
        $configModulo['general']['acciones']['accionAutovalidacion'] = $datosFormulario['general']['acciones']['accionAutovalidacion'];
        $configModulo['general']['acciones']['accionOrdenarCampos']  = $datosFormulario['general']['acciones']['accionOrdenarCampos'];
        $configModulo['general']['config']['registrosPagina']        = $datosFormulario['general']['config']['registrosPagina'];
        $configModulo['general']['columnas']['columna1']             = iconv($this->inCharset, $this->outCharset, $datosFormulario['general']['columnas']['columna1']);
        $configModulo['general']['columnas']['columna2']             = iconv($this->inCharset, $this->outCharset, $datosFormulario['general']['columnas']['columna2']);
        
        foreach ($campos as $campo => $valor) {
			$configModulo['general']['campos'][$campo]['etiqueta']     = iconv($this->inCharset, $this->outCharset, $datosFormulario['general']['campos'][$campo]['etiqueta']);
            $configModulo['general']['campos'][$campo]['displayfield'] = $datosFormulario['general']['campos'][$campo]['displayfield'];
            
            if ($datosFormulario['general']['campos'][$campo]['displayfield']){
				$configModulo['general']['displayfield']['campo1'] = $campo;
            }
        }

        //VISTA INDEX
        foreach ($campos as $campo => $valor) {
			$configModulo['index']['campos'][$campo]['buscar']  = $datosFormulario['index']['campos'][$campo]['buscar'];
			$configModulo['index']['campos'][$campo]['filtrar'] = $datosFormulario['index']['campos'][$campo]['filtrar'];
			$configModulo['index']['campos'][$campo]['mostrar'] = $datosFormulario['index']['campos'][$campo]['mostrar'];
			$configModulo['index']['campos'][$campo]['orden']   = $datosFormulario['index']['campos'][$campo]['orden'];
			if ($datosFormulario['general']['etiquetasFijas']){
				$configModulo['index']['campos'][$campo]['etiqueta'] = iconv($this->inCharset, $this->outCharset, $datosFormulario['general']['campos'][$campo]['etiqueta']);
			}else{
				$configModulo['index']['campos'][$campo]['etiqueta'] = iconv($this->inCharset, $this->outCharset, $datosFormulario['index']['campos'][$campo]['etiqueta']);
			}
        }
        
        $vistas = array("add","edit");
        foreach ($vistas as $vista ) {
			$configModulo[$vista]['columnas']['columna1'] = iconv($this->inCharset, $this->outCharset, $datosFormulario['general']['columnas']['columna1']);   
			$configModulo[$vista]['columnas']['columna2'] = iconv($this->inCharset, $this->outCharset, $datosFormulario['general']['columnas']['columna2']);      
			foreach ($campos as $campo => $valor) {
				$configModulo[$vista]['campos'][$campo]['mostrar']  = $datosFormulario[$vista]['campos'][$campo]['mostrar'];
				$configModulo[$vista]['campos'][$campo]['orden']    = $datosFormulario['general']['campos'][$campo]['orden'];
				$configModulo['edit']['campos'][$campo]['disabled'] = $datosFormulario['edit']['campos'][$campo]['disabled'];
				if ($datosFormulario['general']['etiquetasFijas']){
					$configModulo[$vista]['campos'][$campo]['etiqueta'] = iconv($this->inCharset, $this->outCharset, $datosFormulario['general']['campos'][$campo]['etiqueta']);
				}else{
					$configModulo[$vista]['campos'][$campo]['etiqueta'] = iconv($this->inCharset, $this->outCharset, $datosFormulario['add']['campos'][$campo]['etiqueta']);
				}
			}
        }
                             
        $configModulo['relaciones']['belongsTo'] = $obj->$modelo->belongsTo; 
        $this->guardarConfigYML($modelo,$configModulo);
       	return true;
	}

    /* Funcion aniadirCampoYML:   A�ade un nuevo  campo al fichero de configuraci�n YML de un m�dulo.*/
	public function aniadirCampoYML($modelo,$campo) {
		$configModulo = $this->Esquema->cargarConfigYML($modelo);
		
		foreach($configModulo['index']['campos'] as $campoData) $indexOrden[] = $campoData['orden'];
		$indexOrden = max($indexOrden)+1;
		$configModulo['general']['campos'][$campo]['etiqueta'] = iconv($this->inCharset, $this->outCharset,ucwords($campo));
		$configModulo['index']['campos'][$campo]['buscar']     = ACTIVO;
		$configModulo['index']['campos'][$campo]['filtrar']    = ACTIVO;
		$configModulo['index']['campos'][$campo]['mostrar']    = ACTIVO;
		$configModulo['index']['campos'][$campo]['orden']      = $indexOrden;
		$configModulo['index']['campos'][$campo]['etiqueta']   = iconv($this->inCharset, $this->outCharset,ucwords($campo));
             
		$vistas = array("add","edit");
		foreach ($vistas as $vista ) {
			$addEditOrden = array();
			foreach($configModulo[$vista]['campos'] as $campoData) $addEditOrden[] = $campoData['orden'];
			$addEditOrden = max($addEditOrden)+1;
			$configModulo[$vista]['campos'][$campo]['mostrar']  = ACTIVO;
			$configModulo[$vista]['campos'][$campo]['orden']    = $addEditOrden;
			$configModulo['edit']['campos'][$campo]['disabled'] = 0;
			$configModulo[$vista]['campos'][$campo]['etiqueta'] = iconv($this->inCharset, $this->outCharset,ucwords($campo));
        }
        
        // Codificacion correcta de caracteres
        $configModulo['general']['config']['etiquetaModulo'] = utf8_encode($configModulo['general']['config']['etiquetaModulo']);
        $configModulo['general']['columnas']['columna1']     = utf8_encode($configModulo['general']['columnas']['columna1']);
        $configModulo['general']['columnas']['columna2']     = utf8_encode($configModulo['general']['columnas']['columna2']);
        $configModulo['edit']['columnas']['columna1']        = utf8_encode($configModulo['edit']['columnas']['columna1']);   
        $configModulo['edit']['columnas']['columna2']        = utf8_encode($configModulo['edit']['columnas']['columna2']); 
        $configModulo['add']['columnas']['columna1']         = utf8_encode($configModulo['add']['columnas']['columna1']);   
        $configModulo['add']['columnas']['columna2']         = utf8_encode($configModulo['add']['columnas']['columna2']); 
        foreach ($configModulo['general']['campos'] as $campo => $valor) {
			$configModulo['general']['campos'][$campo]['etiqueta'] = utf8_encode($configModulo['general']['campos'][$campo]['etiqueta']);
			$configModulo['index']['campos'][$campo]['etiqueta']   = utf8_encode($configModulo['index']['campos'][$campo]['etiqueta']);
			$configModulo['edit']['campos'][$campo]['etiqueta']    = utf8_encode($configModulo['edit']['campos'][$campo]['etiqueta']);
			$configModulo['add']['campos'][$campo]['etiqueta']     = utf8_encode($configModulo['add']['campos'][$campo]['etiqueta']);
        }
        $this->guardarConfigYML($modelo,$configModulo);
        return true;
	}
    
    /* Funcion eliminarCampoYML:   Elimina un campo del fichero de configuraci�n YML de un m�dulo.*/
	public function eliminarCampoYML($modelo,$campo) {            
		$configModuloAntiguo = $this->Esquema->cargarConfigYML($modelo);
		$vistas = array("general","index","add","edit");

		foreach ($vistas as $vista ) {
			if(array_key_exists($campo,$configModuloAntiguo[$vista]['campos'])){
				unset($configModuloAntiguo[$vista]['campos'][$campo]);
			}
		}
             
		//codificacion correcta de carateres
		$configModuloAntiguo['general']['config']['etiquetaModulo'] = utf8_encode($configModuloAntiguo['general']['config']['etiquetaModulo']);
		$configModuloAntiguo['general']['columnas']['columna1']     = utf8_encode($configModuloAntiguo['general']['columnas']['columna1']);
		$configModuloAntiguo['general']['columnas']['columna2']     = utf8_encode($configModuloAntiguo['general']['columnas']['columna2']);
		$configModuloAntiguo['edit']['columnas']['columna1']        = utf8_encode($configModuloAntiguo['edit']['columnas']['columna1']);   
		$configModuloAntiguo['edit']['columnas']['columna2']        = utf8_encode($configModuloAntiguo['edit']['columnas']['columna2']); 
		$configModuloAntiguo['add']['columnas']['columna1']         = utf8_encode($configModuloAntiguo['add']['columnas']['columna1']);   
		$configModuloAntiguo['add']['columnas']['columna2']         = utf8_encode($configModuloAntiguo['add']['columnas']['columna2']); 
		foreach ($configModuloAntiguo['general']['campos'] as $campo => $valor) {
			$configModuloAntiguo['general']['campos'][$campo]['etiqueta'] = utf8_encode($configModuloAntiguo['general']['campos'][$campo]['etiqueta']);
			$configModuloAntiguo['index']['campos'][$campo]['etiqueta']   = utf8_encode($configModuloAntiguo['index']['campos'][$campo]['etiqueta']);
			$configModuloAntiguo['edit']['campos'][$campo]['etiqueta']    = utf8_encode($configModuloAntiguo['edit']['campos'][$campo]['etiqueta']);
			$configModuloAntiguo['add']['campos'][$campo]['etiqueta']     = utf8_encode($configModuloAntiguo['add']['campos'][$campo]['etiqueta']);
		}
		$this->guardarConfigYML($modelo,$configModuloAntiguo);
		return true;
	}

	/* Funcion guardarConfigYML:   Crea o sobreescribe el archivo de configurai�n YML de un m�dulo */
	public function guardarConfigYML($modelo,$configModulo) {
		$ruta = null;
		$modulosSistema = $this->Files->archivosSinExt($this->Files->configModulosSistema);
		if(in_array($modelo,$modulosSistema)){
			$ruta = $this->Files->configModulosSistema;
		}else{
			$ruta = $this->Files->configModulosProyecto;
		}
		$this->Files->crearArchivoDeSeguridad("AppModulos".DS."Config".DS."configModulos",$modelo.'.yml');
		$textoplano = Spyc::YAMLDump($configModulo);  
		$textoplano = iconv($this->inCharset, $this->outCharset, $textoplano);

		file_put_contents($ruta.$modelo.'.yml', utf8_decode($textoplano));

		$this->Esquema->borrarSessionYML($modelo);
		$this->Esquema->cargarSessionYML($modelo);
		return true;
	}
        
	private function isFieldTagged($campo,$contenido){
		if(preg_match('/<\?#'.$campo.'\?>(.*?)<\?##'.$campo.'\?>/si', $contenido)==1) return true;
		return false;
	}
 }
