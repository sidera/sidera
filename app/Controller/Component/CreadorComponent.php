<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Component', 'Controller');
App::uses('Folder','Utility');
App::uses('File','Utility');

define (ACTIVO,    "1"); 
define (INACTIVO,  "0"); 

class CreadorComponent extends Component {

    public  $components     = array( 'Esquema', 'Busqueda', 'Importacion', 'Files', 'Session', 'Configurar');
    private $viewPath       = null;
    private $modelPath      = null;
    private $controllerPath = null;
    private $configPath     = null;
    private $archivoAdd     = null;
    private $archivoIndex   = null;
    private $archivoView    = null;
    private $archivoEdit    = null;
    private $archivoImport  = null;

    function __construct(ComponentCollection $collection, $settings = array()) {
        parent::__construct($collection,$settings);
        //Ruta para los archivos que no son de SIDERA, desde aqu� modificamos para todo el constructor.
        $this->viewPath       = APP.'AppModulos'.DS.'View'.DS;
        $this->modelPath      = APP.'AppModulos'.DS.'Model'.DS;
        $this->controllerPath = APP.'AppModulos'.DS.'Controller'.DS;
        $this->configPath     = APP.'AppModulos'.DS.'Config'.DS;
        $this->archivoAdd     = 'add.ctp';
        $this->archivoIndex   = 'index.ctp';
        $this->archivoView    = 'view.ctp';
        $this->archivoEdit    = 'edit.ctp';
        $this->archivoCargarFormModal       = 'cargar_form_modal.ctp';
        $this->archivoIndexModal            = 'index_modal.ctp';
        $this->archivoImport  = 'import_file.ctp';   
    }

    public function construirModulo( $obj ) {
        $controladorACrear = $obj->request->data['modelo'];
        $etiquetaModulo    = $obj->request->data['Constructor']['etiqueta'];
       
        $obj->request->data['Constructor']['general']['crearModelo']      = ACTIVO;
        $obj->request->data['Constructor']['general']['crearControlador'] = ACTIVO;
        $obj->request->data['Constructor']['general']['crearIndex']       = ACTIVO;
        $obj->request->data['Constructor']['general']['crearAdd']         = ACTIVO;
        $obj->request->data['Constructor']['general']['crearEdit']        = ACTIVO;
        $obj->request->data['Constructor']['general']['crearImport']      = ACTIVO;
        
        $obj->request->data['Constructor']['etiqueta'] = iconv('UTF-8', 'ISO-8859-1', $obj->request->data['Constructor']['etiqueta']);
        $opciones          = $obj->request->data;
        $opciones['Constructor']['conexion']=$obj->conexion;
        $propiedadesTablas = $obj->Esquema->cargarTodosLosModelos($obj,$opciones['Constructor']['conexion']);
        $modeloACrear      = ucwords(Inflector::singularize($controladorACrear));
        for( $i = 0; $i < sizeof($propiedadesTablas[$modeloACrear]); $i++){
            $obj->request->data['Campo'][$i]["campo"] = $propiedadesTablas[$modeloACrear][$i]['name'];
            $obj->request->data['Campo'][$i]["orden"] = $i + 1;
            if ($obj->request->data['Campo'][$i]["campo"] == $obj->request->data['Constructor']['general']['displayfield']){
                $obj->request->data['Campo'][$i]["displayfield"] = 1;
            }else{
                $obj->request->data['Campo'][$i]["displayfield"] = 0;
            }
            $obj->request->data['Campo'][$i]["etiqueta"] = $propiedadesTablas[$modeloACrear][$i]['name'];
        }
        if ($this->sePuedeCrearModulo($modeloACrear) == "OK"){
            $menu = $obj->Esquema->cargarDatosMenu($modeloACrear,$etiquetaModulo);
            $obj->loadModel("Menu");
            $obj->Menu->useDbConfig = "default";
            
            $obj->Menu->create();
            if ($obj->Menu->save($menu)) {
                $newElement=array("modelo","controlador","index","add","edit");
                if($obj->request->data['Constructor']['general']["crearIndexModal"]==ACTIVO){
                   array_push ($newElement,"cargar_form_modal","index_modal");
                }
                foreach ($newElement as $element) {
                    $this->crearModulo( $modeloACrear, $controladorACrear, $propiedadesTablas[$modeloACrear], $element, $opciones);
                }
                $this->crearYMLModulo( $obj, $modeloACrear);
                
                $this->requestAction("permisos/syncResourcesForController/".ucwords($controladorACrear));
                $obj->Session->setFlash("Modulo ".$modeloACrear." creado", SUCCESS);
            } else {
                return array("No Creado", $controladorACrear);
            }
        }else{
            return array("No Existe", $controladorACrear);
        }
    }

	function crearModulo($modelo,$controlador,$propiedadesModelo,$fichero,$opciones){          
            
		if(($fichero!="modelo") && ($fichero!="controlador")){
                    if (!file_exists ($this->viewPath.''.ucwords($controlador) )) $destinoVista = new Folder($this->viewPath.ucwords($controlador).DS,true,0755);
                        else $destinoVista = new Folder($this->viewPath.''.ucwords($controlador).DS);
                }
                
		switch ($fichero) {
			case "modelo":
				$nombreFile = $modelo.'.php';
				$this->Files->crearArchivo($this->modelPath,$fichero,$nombreFile,$modelo,$controlador,$propiedadesModelo,$opciones);
				break;
			case "controlador":
				$nombreFile = ucwords($controlador).'Controller.php';
				$this->Files->crearArchivo($this->controllerPath,$fichero,$nombreFile,$modelo,$controlador,$propiedadesModelo,$opciones);
				break;
			case "index":
				$this->Files->crearArchivo($this->viewPath.ucwords($controlador).DS,$fichero,$this->archivoIndex,$modelo,$controlador,$propiedadesModelo,$opciones);
				break;
			case "add":
				$this->Files->crearArchivo($this->viewPath.ucwords($controlador).DS,$fichero,$this->archivoAdd,$modelo,$controlador,$propiedadesModelo,$opciones);
				break;
			case "edit":
				$this->Files->crearArchivo($this->viewPath.ucwords($controlador).DS,$fichero,$this->archivoEdit,$modelo,$controlador,$propiedadesModelo,$opciones);
				break;
                        case "index_modal":
				$this->Files->crearArchivo($this->viewPath.ucwords($controlador).DS,$fichero,$this->archivoIndexModal,$modelo,$controlador,$propiedadesModelo,$opciones);
				break;
                        case "cargar_form_modal":
				$this->Files->crearArchivo($this->viewPath.ucwords($controlador).DS,$fichero,$this->archivoCargarFormModal,$modelo,$controlador,$propiedadesModelo,$opciones);
				break;        
        }
        return true;
    }

    function borrarModulo($modelo){
        $directorioModelo         = $this->modelPath;
        $directorioControlador    = $this->controllerPath;
        $directorioVista          = $this->viewPath.ucwords(Inflector::pluralize($modelo)).DS;
        $nombreArchivoModelo      = $modelo.'.php';
        $nombreArchivoControlador = ucwords(Inflector::pluralize($modelo)).'Controller.php';

        if (file_exists ( $directorioModelo.$nombreArchivoModelo )) unlink($directorioModelo.$nombreArchivoModelo);
        if (file_exists ( $directorioControlador.$nombreArchivoControlador )) unlink($directorioControlador.$nombreArchivoControlador);
        if (is_dir ( $directorioVista )){
            $this->Files->unlinkRecursive($directorioVista);
            rmdir($directorioVista);
        }
        
        $ruta = null;
        $modulosSistema=$this->Files->archivosSinExt($this->Files->configModulosSistema);
        if(in_array($modelo,$modulosSistema)){
            $ruta=$this->Files->configModulosSistema;
        }else{
            $ruta=$this->Files->configModulosProyecto;
        }
        if (file_exists ( $ruta.$modelo.'.yml' )) unlink($ruta.$modelo.'.yml');
       
        return true;
    }

    function sePuedeCrearModulo($modelo){
        if (file_exists ( $this->modelPath.$modelo.'.php' )) return $this->modelPath.$modelo.'.php';
        if (file_exists ( $this->controllerPath.$modelo.'sController.php' )) return $this->controllerPath.''.$modelo.'sController.php';
        if (file_exists ( $this->viewPath.$modelo.'s' )) return $this->viewPath.$modelo.'s';
        return "OK";
    }
	
    function crearYMLModulo($obj,$modelo) {     
        $configModulo = null;
        $campos=$obj->request->data['Campo'];

        //GENERAL
        $configModulo['general']['config']['etiquetaModulo']         = iconv(CODIFICACION_BD, CODIFICACION_FILES, $obj->request->data['Constructor']['etiqueta']);
        $configModulo['general']['auditoria']['insertar']            = INACTIVO;
        $configModulo['general']['auditoria']                        = INACTIVO;
        $configModulo['general']['auditoria']['view']                = INACTIVO;
        $configModulo['general']['historico']                        = INACTIVO;
        $configModulo['general']['displayfield']['campo1']           = "id";
        $configModulo['general']['displayfield']['campo2']           = null;
        $configModulo['general']['displayfield']['campo3']           = null;
        $configModulo['general']['botones']['btconfigurar']          = ACTIVO;
        $configModulo['general']['botones']['btBuscar']              = ACTIVO;
        $configModulo['general']['botones']['btNuevo']               = ACTIVO;
        $configModulo['general']['botones']['btFiltrar']             = ACTIVO;
        $configModulo['general']['botones']['btEliminar']            = ACTIVO;
        $configModulo['general']['botones']['btConsultar']           = ACTIVO;
        $configModulo['general']['botones']['btImportar']            = INACTIVO;
        $configModulo['general']['botones']['btExportar']            = ACTIVO;
        $configModulo['general']['acciones']['accionEditar']         = ACTIVO;
        $configModulo['general']['acciones']['accionChekearTodos']   = ACTIVO;
        $configModulo['general']['acciones']['accionAutovalidacion'] = ACTIVO;
        $configModulo['general']['acciones']['accionOrdenarCampos']  = ACTIVO;
        $configModulo['general']['config']['registrosPagina']        = REGISTROS_PAGINA;
        $configModulo['general']['columnas']['columna1']             = "";
        $configModulo['general']['columnas']['columna2']             = "hide";   
        
        foreach ($campos as $campo) {
            $configModulo['general']['campos'][$campo['campo']]['etiqueta'] = ucwords($campo['etiqueta']);
            if ($campo['displayfield']){
                $configModulo['general']['displayfield']['campo1'] = $campo['etiqueta'];
            }
        }
        
         //VISTA INDEX
        foreach ($campos as $campo) {
            $configModulo['index']['campos'][$campo['campo']]['buscar']   = ACTIVO;
            $configModulo['index']['campos'][$campo['campo']]['filtrar']  = ACTIVO;
            $configModulo['index']['campos'][$campo['campo']]['mostrar']  = ACTIVO;
            $configModulo['index']['campos'][$campo['campo']]['orden']    = $campo['orden'];
            $configModulo['index']['campos'][$campo['campo']]['etiqueta'] = ucwords($campo['etiqueta']);
        }
        
        if(isset( $configModulo['index']['campos']['id'])){
              $configModulo['index']['campos']['id']['mostrar']  =   INACTIVO;
        }
        
        $vistas = array("add","edit");
		foreach ($vistas as $vista ) {
			$configModulo[$vista]['columnas']['columna1'] = $obj->request->data["Constructor"]["etiqueta_columna1"];     
			$configModulo[$vista]['columnas']['columna2'] = $obj->request->data["Constructor"]["etiqueta_columna2"];         
			foreach ($campos as $campo) {
				$configModulo[$vista]['campos'][$campo['campo']]['mostrar']  = ACTIVO;
				$configModulo[$vista]['campos'][$campo['campo']]['orden']    = $campo['orden'];
				$configModulo['edit']['campos'][$campo['campo']]['disabled'] = INACTIVO;
				$configModulo[$vista]['campos'][$campo['campo']]['etiqueta'] = ucwords($campo['etiqueta']);
			}
        }
        
        if(isset( $configModulo['edit']['campos']['id'])){
              $configModulo['edit']['campos']['id']['disabled'] =   ACTIVO;
              $configModulo['edit']['campos']['id']['mostrar']  =   INACTIVO;
        }
        
        //Carga las relaciones belongsTo
        App::import($modelo, 'Model');
        $mimodelo = new $modelo();
        $configModulo['relaciones']['belongsTo'] = $mimodelo->belongsTo;
        $this->Configurar->guardarConfigYML($modelo,$configModulo);
     
        return true;
    }	

	public function construirMasivo( $obj ) {
		$newElement=array("modelo","controlador","vista");
		foreach ($newElement as $element) {
			if($obj->elementos[$element]==ACTIVO){
				if($element=="vista") $this->dataObj ($obj); 
				$this->crearModuloMasivo($obj, $element);
			}
		}
	}
       
	function crearModuloMasivo($obj,$elemento){          
		switch ($elemento) {
			case "modelo":
				$obj->nombreFile = $obj->modeloACrear.'.php';
				$this->Files->crearArchivoMasivo($this->modelPath,$elemento,$obj);
				break;
			case "controlador":
				$obj->nombreFile = $obj->controladorACrear.'Controller.php';
				$this->Files->crearArchivoMasivo($this->controllerPath,$elemento,$obj);
				break;
			case "vista":
				$ruta=$this->viewPath.$obj->controladorACrear.DS;
				if (!is_dir ($ruta)){
					mkdir($ruta);
				}
				$obj->nombreFile = "index.ctp";
				$this->Files->crearArchivoMasivo($ruta,"index",$obj);
                        
				$obj->nombreFile = "add.ctp";
				$this->Files->crearArchivoMasivo($ruta,"add",$obj);

				$obj->nombreFile = "edit.ctp";
				$this->Files->crearArchivoMasivo($ruta,"edit",$obj);
                 
				$this->crearYMLModulo( $obj, $obj->modeloACrear);
				break;
		}
    }
    
    private function dataObj(&$obj){
        for( $i = 0; $i < sizeof($obj->propiedadesTablas[$obj->modeloACrear]); $i++){
            $obj->request->data['Campo'][$i]["campo"] = $obj->propiedadesTablas[$obj->modeloACrear][$i]['name'];
            $obj->request->data['Campo'][$i]["orden"] = $i + 1;
            if ($obj->request->data['Campo'][$i]["campo"] == $obj->request->data['Constructor']['general'][displayfield]){
                $obj->request->data['Campo'][$i]["displayfield"] = 1;
            }else{
                $obj->request->data['Campo'][$i]["displayfield"] = 0;
            }
            $obj->request->data['Campo'][$i]["etiqueta"] = $obj->propiedadesTablas[$obj->modeloACrear][$i]['name'];
        }
    }
}