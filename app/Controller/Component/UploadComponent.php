<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Component', 'Controller');
App::uses('Folder','Utility');
App::uses('File','Utility');
App::import('Vendor', 'Spreadsheet_Excel_Reader');

class UploadComponent extends Component {
		
	var $components = array('Session');
	var $ext_permitidas = array('csv');
        var $destino;
        var $tipos_permitidos = array('text/comma-separated-values', 'text/csv', 'application/csv',
					'application/excel', 'application/vnd.msexcel', 'text/anytext',
					'application/vnd.ms-excel [official]', 'application/msexcel', 'application/x-msexcel', 'application/x-ms-excel', 
					'application/vnd.ms-excel', 'application/x-excel', 'application/x-dos_ms_excel', 'application/xls', 'application/x-xls', 
					'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
					'application/vnd.oasis.opendocument.spreadsheet', 'application/x-vnd.oasis.opendocument.spreadsheet');
	
	
	function crearArchivoServidor($archivo){
		//$this->Session->setFlash('N�mero de campos incorrecto en linea ' . $i, ERROR);
		$destino = WWW_ROOT.DS.'applicationFiles'.DS.'Documentacion'.DS.'Videos'.DS;
                $nombreArchivo = $archivo['name'];
		//Llevamos el fichero a la carpeta Webroot/files
		move_uploaded_file($archivo['tmp_name'],$destino->path.$nombreArchivo);
		$file= new File($destino->path.$nombreArchivo);
       		
		fclose($handle);
		unlink($file->path);
		return $data;
	}
	
   /* Funcion compruebaExtension:   comprueba que la extension del archivo sea correcta
    * Parametros:       $archivo : Nombre del archivo a importar.
    * 					
    * return:           true si es correcto.
    */
	function compruebaExtension($archivo){
		$extension = $this->getFileExtension($archivo['name']);
		foreach($this->ext_permitidas as $ext) {
			if($ext == $extension) return true;
		}
		return false;
	}
	
   /* Funcion getFileExtension:   obtiene la extension del archivo
    * Parametros:       $file : Nombre del archivo a importar.
	* 					
    * return:            $fileArray extension del archivo
	*/
	function getFileExtension($file){
		$fileArray = explode('.',$file);
		return $fileArray[count($fileArray)-1];
	}
	
   /* Funcion compruebaTipo:   comprueba que el tipo esta permitido
    * Parametros:       $archivo : Nombre del archivo a importar.
	* 					
    * return:           true si es corercto.
	*/
	function compruebaTipo($archivo){
		foreach($this->tipos_permitidos as $tipo) {
			if($tipo == $archivo['type']) return true;
		}
		return false;
	}
	
   
    /* Funcion compruebaTipo:   comprueba que el tipo esta permitido
    * Parametros:       $obj: objeto $this de metodo padre.
	* 				   $modelo : nombre del modelo en el que importar el archivo
    * return:          true si esta todo correcto.
    */
	function importar($obj,$modelo){
		$archivo = $obj->params->form['file'];
		//die(print_r($delimitadorCampo));
		$errorFile=false;
			if(!$obj->$modelo->isUploadedFile($archivo)) {
				//Comprobamos si la extenson del fichero es .csv
				if(!$obj->Importacion->compruebaExtension($archivo)){
					 $obj->Session->setFlash(MSG_IMPORTFILEERROR, ERROR);
					 $errorFile=true;
				}
				//Comprobamos si el tipo del fichero es valido
				if(!$obj->Importacion->compruebaTipo($archivo)){
					$obj->Session->setFlash(MSG_IMPORTFILEERROR, ERROR);
					$errorFile=true;
				}
				//Comprobamos el tama�o
				if($archivo['size']>15360){
					$obj->Session->setFlash(MSG_IMPORTSIZEERROR, ERROR);
					$errorFile=true;
				}
				//Si el fichero es correcto
				if (!$errorFile){					
				    $registros=$obj->Importacion->crearArchivoServidor($archivo);
					
				}
			}
		return true;
	}
	
}