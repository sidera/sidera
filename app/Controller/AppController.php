<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Component', 'Controller');
App::uses('Controller', 'Controller');
App::uses("AuditoriasController", "Controller");

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    var $inCharset      = 'UTF-8';
    var $outCharset     = 'ISO-8859-1';

    public $components = array( 'Acl', 'Auth', 'Session',"Esquema","Busqueda","Configurar"); 
    public $helpers    = array( 'Html','Form', 'Session', 'Js','Time','Number' => array('Jquery'),'Menu',"Sidera");
    public $paginate   = array( 'limit' => REGISTROS_PAGINA);
    
    protected $usuario;
    protected $auditoria = null;
    protected $configModulo=null;
    
    protected $modelo=null;
    protected $controlador=null;
    
    protected $modelosAjenos = null;
    protected $modelosAjenosRelacionados = null;

    public function beforeFilter() {
       $this->checkUserAuthorizations(); 
       if( $this->isAppEnModoMantenimiento()){
           $this->showAppEnModoMantenimiento();
       }
       $this->auditoria=new AuditoriasController();
    }
 
    private function checkUserAuthorizations(){
        $this->usuario              = $this->Auth->user();
        $this->Auth->authenticate   = array('Form' => array('userModel' => 'Usuario'));
        $this->Auth->authorize      = array('Actions' => array('actionPath' => 'controllers'),'all'=>array('userModel' => 'Usuario'));
        $this->Auth->autoRedirect   = false;
        
       if(!file_exists(APP.'AppModulos'.DS.'Config'.DS.'instalacion')){
             $this->Auth->loginAction    = array('controller' => 'usuarios', 'action' => 'login');
        }else{
             $this->Auth->loginAction    = array('controller' => 'instalador', 'action' => 'instalar');
        }
       
        $this->Auth->loginRedirect  = array('controller' => 'pages','action' => 'display');
        $this->Auth->logoutRedirect = array('controller' => 'usuarios', 'action' => 'login');
        $this->Auth->loginError     = "Datos de acceso incorrectos, int�ntelo de nuevo.";
        $this->Auth->authError      = "Acceso restringido.";
        $this->set('usuario', $this->usuario);
        $this->set('acl', $this->Acl);

        $this->Auth->allow('js','resetSync','syncResourcesForController','finalizarInstalacion','getErrorLogMessages');
    }
    
    private function isAppEnModoMantenimiento(){
        if( (MODO_MANTENIMIENTO==ACTIVO && $this->usuario==null && $this->params->params['named']['u'] != "admin" ) || 
            (MODO_MANTENIMIENTO==ACTIVO && $this->usuario!=null && $this->usuario['username']!='admin')){
            return true;
        }
        return false;
    }
    
    private function showAppEnModoMantenimiento(){
        $this->Auth->logout();
        $this->layout = "maintenance";
        $this->Auth->loginAction    = array('controller' => 'pages', 'action' => 'maintenance');
        return $this->render('/Pages/maintenance');
    }
    
    public function js($view){
        $viewArray = explode('.',$view);
        $this->render('js/index');
    }   
    
    
    protected function redirectSidera($controlador=null, $accion=null, $tipo="header"){
         if($tipo=="header"){
            $proyecto=RUTA_PROYECTO;
            $protocolo=PROTOCOLO;
            if($controlador!=null){
              header('Location: '.$protocolo.'://'.$_SERVER['HTTP_HOST'].'/'.$proyecto.'/'.$controlador.'/'.$accion);   
            }else{
              header('Location: '.$protocolo.'://'.$_SERVER['HTTP_HOST'].'/'.$proyecto.'/');   
            }
           
            exit();  
          }else{
            $this->redirect($accion);  }       
    }
    
    
    public function obtenerDisplayfields(){
        $modelo = $this->modelo;
        foreach ($this->modelosAjenos as $key=>$modeloAjeno) {
            $displayField = $this->$modelo->$modeloAjeno->displayField;
            $this->set($modeloAjeno."DisplayField", $displayField);
        }
        return TRUE;
    }
    
    public function obtenerDatosModelosAjenos(){
        $modelo = $this->modelo;
        foreach ($this->modelosAjenos as $key=>$modeloAjeno) {
            $displayField = $this->$modelo->$modeloAjeno->displayField;
            $datosModeloAjeno  = null;
            if (sizeof($displayField)>0){
                $datosModeloAjeno  = $this->$modelo->$modeloAjeno->find('list',array('fields' => array($displayField),'order'=>$displayField. ' ASC'));
            }else{
                $datosModeloAjeno = $this->$modelo->$modeloAjeno->find('list');
            }
            $this->set(strtolower(Inflector::pluralize($modeloAjeno)),$datosModeloAjeno);
        }
        return TRUE;
    }
    
     /* Funcion comprobarPaginacion:   Checkea en session la paginacion*/
    protected function comprobarPaginacion(){
        $registrosPorPagina = $this->params->named['limit'];
        if(!isset($registrosPorPagina)) $registrosPorPagina='10';
        $this->set('registrosPorPagina', $registrosPorPagina);
        return $registrosPorPagina;
    }
    
     /* Funcion comprobarFiltroActivo:   Comprueba si hay filtro activo*/
    protected function comprobarFiltroActivo(){
        $modelo=$this->modelo;
        $cond = array();
        if (isset($this->params->named["do"]) && ($this->params->named["do"] == 'clearFilter')){
            $this->Session->write("filtro[$modelo]",'');
        }
        //Se acaba de ejecutar el filtro
        if(isset($this->request->data['filtro']) ){
            $this->Session->write("filtro[$modelo]",'');
            $parametrosFiltro = $this->request->data['filtros'];
            
            foreach($parametrosFiltro as $k=>$parametroFiltro){
                $parametrosFiltro[$k] = utf8_decode($parametroFiltro);
            }
            
            $cond = $this->Busqueda->busca($parametrosFiltro,$modelo,$this);
            $this->Session->write("filtro[$modelo]",$parametrosFiltro);
        }else{
             //Comprobamos si ya hab�a un filtro activo
             if($this->Session->check("filtro[$modelo]")==TRUE){
                    $cond=  $this->Busqueda->busca($this->Session->read("filtro[$modelo]"),$modelo,$this);
             }            
        }
        return $cond;
    }
    
    /* Funcion comprobarBusquedaActiva: Comprueba si hay una busqueda activa */
    protected function comprobarBusquedaActiva() {
        $modelo = $this->modelo;
        $cond = array();
        if(isset($this->params->named['searchFields'])){
            $fieldsForSearch = explode(",", $this->params->named['searchFields']);
            $this->set("fieldsForSearch",$fieldsForSearch);
        }    
        if ($this->params->named['searchText'] || $this->data['searchText']) {
            $searchText = ($this->data['searchText']) ? $this->data['searchText'] : $this->params->named['searchText'];
            if ($this->Busqueda->IsDate($searchText)) {
                list( $day, $month, $year ) = split('[/.-]', $searchText);
                $searchText = $year . "-" . $month . "-" . $day;
            }
            $searchText = html_entity_decode($searchText);
            $coincidencias = null;
            $aguja = array("[", "]", "`", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "|", "+", "\\", "=", "?", "�", ";", ":", "'", ".", "<", ">", "{", "}", "[", ")");
            $coincidencias[] = str_replace($aguja, "", $searchText);
            $i = 0;
            foreach ($fieldsForSearch as $fieldForSearch) {

                foreach ($coincidencias as $cadena) {
                    $cadena = trim($cadena);
                    if ($this->Busqueda->IsDate($cadena)) {
                        if ($this->Session->check("filtro[$modelo]")) {
                            $cond["AND"]["OR"][$i]["$fieldForSearch ="] = "$cadena";
                        } else {
                            $cond["OR"][$i]["$fieldForSearch ="] = "$cadena";
                        }
                    } else {
                        if (!$this->Session->check("filtro[$modelo]")) {
                            $cond['OR'][$i]["$fieldForSearch LIKE"] = "%$cadena%";
                        }
                        else{
                            $cond['AND']['OR'][$i]["$fieldForSearch LIKE"] = "%$cadena%";
                        }
                    }
                    $i++;
                }
            }
            $this->Session->write("busqueda[$modelo]", $this->paginate["conditions"]);
            if ($this->Busqueda->IsDate($searchText))
                $searchText = $day . "/" . $month . "/" . $year;
            $this->set("searchText", $searchText);
            $this->Session->write("busqueda[$modelo][searchText]", $searchText);
        }elseif ((isset($this->request->params["named"]["page"]) && $this->Session->check("busqueda[$modelo]") == TRUE)) {
            $cond = $this->Session->read("busqueda[$modelo]");
            $this->paginate["page"] = $this->request->params["named"]["page"];
            $searchText = $this->Session->read("busqueda[$modelo][searchText]");
            $this->set("searchText", $searchText);
        } else {
            $this->Session->delete("busqueda[$modelo]");
            $this->Session->delete("busqueda[$modelo][searchText]");
        }
        return $cond;
    }
 
  /* Funcion exportarControladorAVista:   prepara la informacion del exportar que se enviara al PDF/XLS*/
   public function exportarEjecutar(){
        $controlador=$this->controlador;
        $modelo=$this->modelo;
        $type=$this->data["tipoimpresion"];
        $seleccionados = array_keys($this->data);
        unset($seleccionados[sizeof($seleccionados)-1]); //Borramos el tipoimpresion
        
        foreach ($seleccionados as $key => $campo) {
            $seleccionados[$key]=str_replace('-', '.', $campo);
            $ordenados[$key]= $seleccionados[$key]. " " . $this->data[$campo];
        }
       
        $arrayFields = $seleccionados;
        $sort_range= $ordenados;
        $limit=NULL;
        $arrayConditions=null;
        
        if($this->Session->check("busqueda[$modelo]")==TRUE){
           $arrayConditions= $this->Session->read("busqueda[$modelo]");
        }elseif($this->Session->check("filtro[$modelo]")==TRUE){
           $arrayConditions=  $this->Busqueda->busca($this->Session->read("filtro[$modelo]"),$modelo,$this);
        }
       
        $datos = $this->$modelo->find('all',array('fields'=>$arrayFields,'limit'=>$limit,'order'=>$sort_range,'conditions'=> $arrayConditions));
        //Modificamos el 1� registro con las etiquetas correspondientes
        $registro=$datos[0];
      
        foreach ($registro as $modeloNombre => $arrayCampos) {
            $configModulo=$this->Esquema->leerConfigSession($modeloNombre);
            foreach ($arrayCampos as $campo => $valor) {
              $registro0[$modeloNombre][$configModulo["general"]["campos"][$campo]["etiqueta"]]=$valor;              
            }
         }
        $datos[0]=$registro0;

        $html= $this->requestAction( array('controller' => 'pdfs', 'action' => 'index'),array('data'=>$datos));

        $this->set(compact('html','controlador'));
        if($type=='pdf')  $this->render("/Exportar/pdf");
        else  $this->render("/Exportar/xls");
        return TRUE;
    }
    
  /* Funcion exportControladorAVista:   
   * prepara la informacion del exportar del M�dulo actual  que se enviara a la vista 
   */
    public function exportarMostrar(){
        $modelo = $this->modelo;
      	$controlador = $this->controlador;
      	$bd = $this->Esquema->buscarClavesAjenas($this,$modelo);
        if(sizeof($bd)>0){
            foreach ($bd as $relaciones => $relacion) {
                foreach( $relacion as $clave => $valor) {
                    if($clave == 'modelo'){
                        $modeloR = ucwords($valor);
                        $this->loadModel($modeloR);
                         $datosmodelo= $this->Esquema->camposModeloYML($modeloR);
                         if($datosmodelo!=null){$modelos[$modeloR]=$datosmodelo;}
                    }else if($clave == 'principal'){
                        $modelos[$modelo]=$this->Esquema->camposModeloYML($modelo);
                    }
                }
            }
      	}else{
            $modelos[$modelo]=$this->Esquema->camposModeloYML($modelo);
      	}
        //Eliminamos las claves ajenas para la selecci�n de campos.
        foreach ($modelos as $keyModel=>$model) {
            foreach ($model as $keyCampo=>$campo) {
                if(substr($keyCampo,-3) == "_id" ||(isset($campo["custom"])) ){
                    unset($modelos[$keyModel][$keyCampo]);
                }           
            }      
        }
        $this->set(compact('modelo','modelos','controlador'));
        $this->render("/Exportar/index");
    }

    public function configurarModulo(){
            $modelo=$this->modelo;
            $controlador=$this->controlador;            
            $this->Esquema->comprobarSessionModeloYML($modelo);
            $configModulo = $this->Esquema->leerConfigSession($modelo);
            $camposModelo = $configModulo["general"]["campos"];
            $schema       = $this->$modelo->schema(true);
            $propiedades  = $this->Esquema->propiedadesModelo($schema);
            //Obtenemos Campos Totales y Campos Actuales
            $camposAplicacion=array_keys($configModulo[general][campos]);
            $camposTotales = $propiedades;
            
            $nuevosCampos=array();
            $camposComboActuales=array();
            foreach ($camposTotales as $campo) {
                $camposActuales[$campo['name']]=$campo['name'];
                if (substr($campo['name'],-3)=="_id"){
                       $camposComboActuales[$campo['name']]=$campo['name'];
                }
                if(!in_array($campo['name'],$camposAplicacion)){
                   $nuevosCampos[$campo['name']]=$campo['name'];
                }
            }
            //quito el campo id de camposActuales para que no pueda ser eliminado
            unset($camposActuales["id"]);
            $this->set(compact('modelo','controlador','configModulo','propiedades','nuevosCampos','camposActuales','camposModelo','camposComboActuales'));
            $this->render("/Config/config");
    }
    
    public function configurarModuloGuardar(){
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        if ($this->request->is('post') || $this->request->is('put')) {
            $datos = $this->request->data;
            switch ($datos['accionForm']) {
                case "saveForm":
                    $campos=$this->Esquema->camposModeloYML($modelo);
                    $this->Configurar->reescribirYMLModulo($datos,$campos,$this);
                    break;
                case "addCampo":
                    $nuevoCampo=$datos['listaNuevosCampos'];
                    $this->Configurar->aniadirCampoYML($modelo,$nuevoCampo);
                    $this->Configurar->addNuevoCampo($this,$modelo,$nuevoCampo);
                    break;
               case ($datos['accionForm']=='delCampoApp' || $datos['accionForm']=='delCampoAppBD'): 
                    //Elimino de la tabla Campos el registro de este campo
                    $campoAEliminar=$datos['listaEliminarCampos'];
                    $this->Configurar->eliminarCampoYML($modelo,$campoAEliminar);
                    $this->Configurar->delCampoApp($this,$modelo,$campoAEliminar);
                    if( $datos['accionForm'] == 'delCampoAppBD' ){
                        $this->$modelo->query('ALTER TABLE '.$controlador.' DROP '.$campoAEliminar.'');
                    }
                    break;
            }
            $this->redirectSidera($controlador,"index");  
        }
    }
    
    protected function checkModal(){
        if(isset($this->request->params['named']['formType'])){
            $this->set("formType",$this->request->params['named']['formType']);
        }
    }
}
