<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt
    
 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

class LogmessagesController extends AppController {

    public $name = 'Logmessages';
    public $components  = array("Files");
    public $uses = array();
    
    public function getErrorLogMessages(){
        $this->layout = 'ajax';
        $errorFileLogPath    = '..'.DS.'tmp'. DS.'logs'. DS . 'error.log'; 
        $errorFileLogContent = "";
        if(file_exists($errorFileLogPath)){
            $errorFileLogHandle  = fopen($errorFileLogPath,"r");
            while( $errorFileLogLine = fgets( $errorFileLogHandle, 1024 ) ){
                $errorFileLogContent .= $this->drawErrorLogLine($errorFileLogLine);
            }
            fclose($errorFileLogHandle);
        }
        $this->set('logErrorMessages',$errorFileLogContent);
    }
    
    private function drawErrorLogLine($errorFileLogLine){
        $errorFileLogLine   = str_replace("Warning:", '<b class="nota-alert-logs alert yellow pull-left"><i class="icon-warning-sign "></i>Warning:</b><hr class="nota-hr-logs">', $errorFileLogLine) ; 
        $warningFileLogLine = str_replace("Error:", '<b class="nota-alert-logs alert red pull-left"><i class="icon-warning-sign "></i>Error:</b><hr class="nota-hr-logs">', $errorFileLogLine) ; 
        $fileLogLine        = $errorFileLogLine.$warningFileLogLine;
        return $fileLogLine;
    }
    
       public function mostrarLogs() {
        $this->autoRender=false;
        $contenido=$this->Files->leerArchivoLogs();
        print($contenido);
        return true;
    }
    
    public function borrarLogs() {
        $this->autoRender=false;
        $this->Files->unlinkRecursive($this->Files->errorPath);
    }	
}
