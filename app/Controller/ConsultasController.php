<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses("AppController", "Controller");

/**
 * grupos Controller
 *
 * @property Grupo $Grupo
 */
class ConsultasController extends AppController {
    public   $components  = array("Paginate");
    public   $layout      = "ajax";
  
    public function beforeFilter(){
       parent::beforeFilter();
        $this->modelo      = "Consulta";
        $this->controlador = "consultas";
        $this->modelosAjenos = array();
        $this->modelosAjenosRelacionados = array();
   }
   
    public function index() {                       
        $conexiones= array_keys(ConnectionManager::enumConnectionObjects());            
	$this->set('conexiones',$conexiones);         
    }
    
    
    public function lista($consulta = null,$conexion = NULL, $pagina = null) {        
      $this->layout= 'ajax';   
     
      //$this->autoRender=false;
      
      $propiedadesTablas = $this->Esquema->cargarTodosLosModelos($this,$conexion);
      
      $nombretablas[]=array_keys($propiedadesTablas);        
      $tablareferencia=$nombretablas[0][0];
      
      $consulta=str_replace(",",", ",$consulta); 
      $tipoconsulta =  strtolower(substr(ltrim($consulta),0,6));
        
      try {
         $db = ConnectionManager::getDataSource($conexion);
         $datasource=$db->config['datasource'];
         
         if($this->comprobarTipoConexion($datasource, "Oracle")){ //Si es una conexi�n Oracle y la consulta tiene *

           //pasamos a may�sculas todo excepto lo que est� entre comillas                        
           $consulta_array = explode("'", $consulta);
           foreach ($consulta_array as $key => $value){
              $resto=$key%2;
              if ($resto==0){
                  $consulta_array[$key]=strtoupper($value);
              } 
           }
           $consulta = implode("'", $consulta_array);                 
             
           $pos = strstr($consulta, '*');    
           if ($pos != ""){  //si la consulta contiene un * cogemos todos los campos de todas la tablas  
                $tablasConsulta=$this->obtenerTablasConsulta($conexion,$consulta);
                
                foreach ($tablasConsulta as $key => $value) {
                    
                          $modeloDeTabla=ucwords(Inflector::singularize($value));
                          $schema=$this->$modeloDeTabla->schema(true);   
                          $c = array_keys($schema);
                          
                          if(sizeof($tablasConsulta)>1){//si la consulta tiene mas de una tabla ponemos los nombres delante de los
                                           //campos para evitar el error "columna definida de forma ambigua"
                              foreach ($c as $key1 => $value1) {
                                  $c1[]=$value.".".$value1;
                              }
                              $campos = array_merge((array)$campos, (array)$c1);                            
                          }else{
                              $campos = array_merge((array)$campos, (array)$c);                            
                          } 
                }
                
                $selectlist = implode(", ", $campos);
                $consulta = str_replace("*", $selectlist, $consulta);
           }
         };
         $resultado=$this->$tablareferencia->query($consulta);  
         if($tipoconsulta=="select"){ 
            $resultadoSinPaginar=$this->$tablareferencia->query($consulta);  
         }
         $numeroFilas=count($resultado);
           

                     
                     
          //die("numero de filas tiene--->" . $numeroFilas); 
          //**********************PAGINACION**************************** 
          if($numeroFilas!=0){          
             $oPagina = new PaginateComponent($resultado, $pagina, $sUrl, REGISTROS_PAGINA);           
             $resultado = $oPagina->get_items_to_show();
             $sBotones = $oPagina->get_botones_to_show();          
          }         
          //**********************FIN PAGINACION****************************                         
          $error=0;
      } catch (exception $ex) {            
         $txtrespuesta="<div class='alert input orange  fade in' id='alertaWarning'>
		<span class='input-warning' data-original-title=''>			  	   
		   <i class='fa fa-warning-sign'></i>
		</span>".$ex.
	   "</div>";                       
           $error=1;
      }
         
      if($error==0){        
         if($tipoconsulta=="select"){                 
                if($numeroFilas != 0){                       
                      $txtrespuesta = $txtrespuesta . $this->montarTablaResultado($resultado); 
                      $txtrespuestaSinPaginar = $txtrespuestaSinPaginar . $this->montarTablaResultado($resultadoSinPaginar);                        
                      $txtrespuesta = $txtrespuesta . "\n" . "Total filas encontradas: " .  $numeroFilas ;
                }else{                      
                      $txtrespuesta = "<div class='alert input orange  fade in' id='alertaWarning'>
		           <span class='input-warning' data-original-title=''>			  	   
		           <i class='fa fa-warning-sign'></i>
		           </span> <span id='textoAlertaSuccess'>0 filas devueltas</span>
	                   </div>";   
                 }               
         }else{  //No es una select         
              $txtrespuesta = "<div class='alert input green fade in' id='alertaSuccess'>
           		         <span class='input-success' data-original-title=''>
           			 Correcto
           			 <i class='fa fa-ok-sign'></i>
           		        </span>
           		        <span id='textoAlertaSuccess'>Sentencia ejecutada correctamente</span>
           	                </div>";
         }
      }//error==0      
     
      $this->set('txtrespuesta',$txtrespuesta); 
      $this->set('Botones',$sBotones); 
      $this->set('paginaactual',$pagina);   
      return ($txtrespuestaSinPaginar);
   }   

    public function exportar(){
        $consulta=$this->data['Consulta']['txt_consulta'];
        $conexion=$this->data['listaConexiones'];             
        $html=$this->lista($consulta,$conexion,1);
        $this->set('html', "$html");
        return TRUE;
    }
   

    //Funciona solo en consultas Mysql
    function nombreTablas($ar) {  
        foreach($ar as $k => $v) {            
          $keys[] = $k;
        }        
        $keys1[]=array_keys($ar[0]);
        return $keys1;        
     }
    
     
    /*Devuelve:
                $ncol[tabla1][col1]
     *                       [col2]
     *                      ...    
                $ncol[tabla2][col1]
     *                       [col2]
     *                      ...         
     */
    function nombreColumnas($ar) { 
        //En $ar tenemos resultado[0]

        foreach($ar as $k => $v) {
          $keys[] = $k;             // en keys tenemos keys[0]->tabla1 keys[1]->tabla2 keys[2]->tabla3
        } 
        $tamKeys=sizeof($keys);        
                
        for($i=0;$i<sizeof($keys);$i++){
           foreach($ar[$keys[$i]] as $k1 => $v1) {               
               $ncol[$keys[$i]][]=$k1;               
           }
        }
        return $ncol;        
     }
 
     //Devuelve true si la conexion es del tipo inidicado (Oracle, Mysql...)
     function comprobarTipoConexion($datasource,$conexion) {  
           $resultado = strpos($datasource, $conexion);
           if($resultado !== FALSE){
                       return(true);
           }           
           return(false);
     }
     
     //Devuelve las tablas que intervienen en una consulta (funciona tambien con Oracle)
     function obtenerTablasConsulta($conexion,$consulta) { 
          $consultamin=strtolower($consulta);
          $tablasConexion = $this->Esquema->tablasSistema($this,$conexion);                                     
          foreach($tablasConexion as $k => $v) { 
               $buscar = "from " . $v; 
               $esta = strpos($consultamin, $buscar);
               if($esta !== FALSE){
                  $tablasconsulta[]=$v;     
               }    
               $buscar = "join " . $v; 
               $esta = strpos($consultamin, $buscar);
               if($esta !== FALSE){
                  $tablasconsulta[]=$v;     
               }    
          }          
          return $tablasconsulta;
     } 
     
     /**
      * cargarconexion method
      *
      * @throws MethodNotAllowedException
      * @throws NotFoundException
      * @param string $id
      * @return void
      */
      public function cargarConexion($conexion = null) {
  	  $conexion = $_POST['conexion'];
  	  $tablas   = $this->Esquema->tablasSistema($this,$conexion);          
  	  $this->set('tablasConexion',$tablas);
  	  $this->set(compact('tablasConexion'));
      }
      
      public function montarTablaResultado($resultado = null) {
              //$numeroFilas=count($resultado);
            
           $nombreT=$this->nombreTablas($resultado);     
           
           //die(print_r($resultado)); 
           $numeroTablas = count($nombreT[0]);        
           $nombreC=$this->nombreColumnas($resultado[0]);           
           $cab="";
           $txtrespuesta=$txtrespuesta."<table class='table table-striped dataTable pagination'>";
                    
            //Mostrar cabecera       
            $txtrespuesta=$txtrespuesta."<thead><tr>";
            foreach($nombreC as $k => $v) {
                $cad_columna = $k;
                for($i=0;$i<sizeof($nombreC[$k]);$i++){ 
                   $txtrespuesta=$txtrespuesta."<th>". $nombreC[$k][$i] . "</th>";                    
                }
            }
            $txtrespuesta=$txtrespuesta."</tr></thead>";
            
            //Mostrar resultado   
            $txtrespuesta=$txtrespuesta."<tbody>";           
            for($r=0;$r<sizeof($resultado);$r++){
                $txtrespuesta=$txtrespuesta."<tr>";           
                $linea="";
                for($t=0;$t<sizeof($nombreT[0]);$t++){                            
                    for($c=0;$c<sizeof($nombreC[$nombreT[0][$t]]);$c++){ 
                       $contenidocolumna=$resultado[$r][$nombreT[0][$t]][$nombreC[$nombreT[0][$t]][$c]];                      
                       $txtrespuesta=$txtrespuesta."<td>". $contenidocolumna ."</td>";
                    }                
                }            
            }      
            /*$txtrespuesta=$txtrespuesta."</tbody></table></div>";*/
            $txtrespuesta=$txtrespuesta."</tbody></table>"; 
            return $txtrespuesta;
      }    
}