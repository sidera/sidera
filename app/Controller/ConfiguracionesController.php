<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses("AppController", "Controller");

class ConfiguracionesController extends AppController {

    public $components  = array("Files");
    var $layout         = "ajax";
    var $uses = false;
    
    public function beforeFilter(){
       parent::beforeFilter();
        $this->modelo      = "Configuracion";
        $this->controlador = "configuraciones";
        $this->modelosAjenos = array();
        $this->modelosAjenosRelacionados = array();
   }

    public function edit() {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        
        $this->request->data[$modelo]['mostrar_menus_sistema'] = MOSTRAR_MENUSSISTEMA;
        $this->request->data[$modelo]['modo_mantenimiento']    = MODO_MANTENIMIENTO;
        $this->request->data[$modelo]['auditoria_acceso']      = AUDITORIA_ACCESO;
        $this->request->data[$modelo]['subnavbar']             = SUBNAVBAR;
            
        $servidores= $this->Esquema->cargarConfigJasperserversYML();
        $this->set(compact('modelo','controlador','servidores'));
    }
    
    public function guardarConfig() {
        $this->autoRender = false;
        $modelo           = $this->modelo;       
        $this->Esquema->configurarAplicacion($this);
        session_destroy();
    }
	
    public function borrarCache() {
        $this->autoRender = false;
        $this->Files->borrarTemporales($this);
    }
    
	public function cargarConfigYML($modelo) {
		$ruta = null;
		$modulosSistema=$this->Files->archivosSinExt($this->Files->configModulosSistema);
        if(in_array($modelo,$modulosSistema)){
			$ruta = $this->Files->configModulosSistema;
		}else{
			$ruta = $this->Files->configModulosProyecto;
		}
        $configModulo = Spyc::YAMLLoad($ruta.$modelo.'.yml');
        return  $configModulo;
	}
}