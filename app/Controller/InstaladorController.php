<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses("AppController", "Controller");
class InstaladorController extends AppController {

    public  $layout     = "ajax";
    var     $uses       = false;

   public function beforeFilter(){
       parent::beforeFilter();
   }
   
    public function instalar() {}

    public function finalizarInstalacion(){
        $this->autoRender = false;
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        
        $this->applicationConstantsPath =   APP.'AppModulos'.DS.'Config'.DS.'applicationConstants.php';
        $this->templatesPath            =   APP.'View'.DS.'Templates'.DS;
        
        $opciones=$this->request->data;
        $contenido= file_get_contents($this->applicationConstantsPath);
        require($this->templatesPath.'Instalador'.DS.'applicationConstants.php');       
        
        file_put_contents($this->applicationConstantsPath, $archivo);
        chown(APP.'AppModulos'.DS.'Config'.DS.'instalacion',666);
        unlink(APP.'AppModulos'.DS.'Config'.DS.'instalacion');      
        
        header('Location: '.$opciones["Configuracion"]["protocolo"].'://'.$_SERVER['HTTP_HOST'].'/'.$opciones["Configuracion"]["ruta_proyecto"].'/');   
         exit();  
    }  
  
  
}