<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

App::uses('AppController', 'Controller');

class UsuariosController extends AppController {

    public   $components      = array('Files');
    public   $layout          = "ajax";
    private  $fieldsForSearch = array("Usuario.username","Usuario.nombre","Usuario.apellido1","Usuario.apellido2");
   
    public function beforeFilter() {
        parent::beforeFilter();
        $this->modelo      = "Usuario";
        $this->controlador = "usuarios";
        $this->modelosAjenos = array("Grupo");
        $this->modelosAjenosRelacionados = array();
        $this->Auth->allow(array('login','logout'));
    }

    public function index() {
        $modelo       =$this->modelo;
        $controlador  = $this->controlador;
        
        $this->comprobarPaginacion($this);
        
        $this->Esquema->comprobarSessionModeloYML($modelo);
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $camposModelo = $configModulo[general][campos];
     
        $fieldsForSearch            = $this->Esquema->obtenerCamposABuscarEnIndex($this,$configModulo,$camposModelo,$modelo);
        $this->comprobarFiltroActivo($this);
        $this->comprobarBusquedaActiva($this,$fieldsForSearch);
        
        $this->paginate["limit"]     = $configModulo["general"]["config"]["registrosPagina"];
        $this->paginate["order"]     = array("$modelo.id" => "DESC");
        
        $configModulo["general"]["botones"]["btconfigurar"]=$this->Session->read("CofigurarModulos");
        
        $registros=$this->paginate();
        $this->set(compact("registros","modelo","controlador","configModulo"));
    }

    public function add() {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        
        $schema       = $this->$modelo->schema();
        $propiedades  = $this->Esquema->propiedadesModelo($schema); 
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $configModulo = $this->Esquema->identificarCamposObligatorios($configModulo,$propiedades,"add");
        $this->Esquema->obtenerDatosModelosAjenos($this);
        $accion="Add";$this->checkModal();
        
        $this->set(compact("modelo","propiedades","controlador","accion","configModulo"));
    
    }
    
     public function create(){
        $modelo     =$this->modelo;
        $controlador=$this->controlador;
        
        $this->$modelo->create();
        //Esto lo hago porque necesito esta comprobacion para cuando trabajamos con SQLSERVER
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        if($dataSource->config['datasource']=="Database/Sqlserver" || $dataSource->config['datasource']=="Database/Sybase"){
            $this->request->data['Usuario']['created']=  date("d-m-Y H:i:s"); 
            $this->request->data['Usuario']['modified']=  date("d-m-Y H:i:s");
        }    
        if ($this->$modelo->save($this->request->data)) {
                $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
                $configModulo = $this->Esquema->leerConfigSession($modelo);
            } else {
                $this->Session->setFlash(MSG_SAVEDERROR, ERROR);
        }
             
        $this->redirectSidera($controlador,"index");   
    }

    public function edit($id = null) {
        if($id == 1) return; // No se puede editar el usuario Admin
        $accion = "Edit";  
        $this->modelo->id = $id;
       
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        
        $schema       = $this->$modelo->schema(true);
        $propiedades  = $this->Esquema->propiedadesModelo($schema);  
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $configModulo = $this->Esquema->identificarCamposObligatorios($configModulo,$propiedades,"edit");
        $this->Esquema->obtenerDatosModelosAjenos($this);
        
        $this->request->data = $this->$modelo->read(null, $id);
        $this->set(compact("modelo","propiedades","controlador","accion","configModulo"));
        
    }
    
    public function pass() {
        $accion = "pass"; 
        $id = $this->Auth->user('id');
        $this->modelo->id = $id;
       
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        
        $schema       = $this->$modelo->schema(true);
        $propiedades  = $this->Esquema->propiedadesModelo($schema);  
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $configModulo = $this->Esquema->identificarCamposObligatorios($configModulo,$propiedades,"edit");
        $this->Esquema->obtenerDatosModelosAjenos($this);
        
        $this->request->data = $this->$modelo->read(null, $id);
        $this->set(compact("modelo","propiedades","controlador","accion","configModulo"));
        
    }
    public function updatePass() {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        $id = $this->Auth->user('id');
        $this->Usuario->id   = $id;
        $password = $this->Usuario->field('password');
        
        if($password == sha1($this->request->data['Usuario']['passActual'])) {
            $this->request->data['Usuario']['password'] = $this->request->data['Usuario']['confirmarNuevaPass'];
            unset($this->request->data['Usuario']['passActual']);
            unset($this->request->data['Usuario']['nuevaPass']);
            unset($this->request->data['Usuario']['confirmarNuevaPass']);
            
            if ($this->$modelo->save($this->request->data)) {
                $this->Session->setFlash("La contrase�a ha sido cambiada correctamente.", SUCCESS);
                $this->redirectSidera("usuarios","logout");
            } else {
                $this->Session->setFlash(MSG_EDITERROR, ERROR);
                $this->redirectSidera($controlador,"pass");
            }
        }else{
            $this->Session->setFlash("La contrase�a que has ingresado no concuerda con la actual.", ERROR);
            $this->redirectSidera($controlador,"pass");
        }
    }
    
     public function update($id = null){
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;

        if(empty($this->request->data['Usuario']['password'])) {
            unset($this->request->data['Usuario']['password']);
        }
        $this->$modelo->id   = $id;
        $configModulo        = $this->Esquema->leerConfigSession($modelo);
        
        if ($this->$modelo->save($this->request->data)) {
            $this->Session->setFlash(MSG_EDITSUCCESS, SUCCESS);
        } else {
            $this->Session->setFlash(MSG_EDITERROR, ERROR);
        }
        
        if($this->Session->check("page[$this->modelo]")==TRUE){    
            $this->redirectSidera($controlador,"index/page:".$this->Session->read("page[$this->modelo]"));   
        }else{
            $this->redirectSidera($controlador,"index");        
        }  
    }

    public function delete() {
        if($this->request['data']['id']==1) return; // No se puede eliminar el usuario Admin
        $this->autoRender = false;
        $modelo=$this->modelo;
        $controlador=$this->controlador;
        
        $datos=$this->request->data["id"];
        $exploded_id = explode(",", $datos);
        
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        
        foreach ($exploded_id as $id) {
            $this->$modelo->id=$id;
             if (!$this->$modelo->exists()) {
                $this->Session->setFlash(MSG_DELETENOTICE, NOTICE);
            }
            if($this->isAuditoriaActiva($configModulo)) $this->auditoria->auditar($this);
            if($this->isHistoricoActivo($configModulo)) $this->auditoria->historico($this);
                
            $this->$modelo->delete(); 
        }
        $this->Session->setFlash(MSG_DELETESUCCESS, SUCCESS);  
        $this->redirectSidera($controlador,"index");        
    }

    public function login() {
        $this->layout = 'ajax';
        
       // die(print_r($this->data));
        if ($this->request->is('post')) {
            if(AUDITORIA_ACCESO) $this->registraAccesoUsuario();
            if ($this->Auth->login()) {
                $this->Files->comprobarDirectoriostemporales('');    
                $this->Files->borrarTemporales('');  
                $this->Esquema->borrarSessionesYML();
                $this->Esquema->cargarSesionesYML();
                if ($this->Auth->user('id')=='1') $this->Session->write("CofigurarModulos","1");
                else $this->Session->write("CofigurarModulos","0");
                $this->redirect(array('controller'=>'/','action'=>''));
            } else {
                $this->Session->setFlash(__('Usuario no v�lido, int�ntelo de nuevo'), ERROR);
            }
        }
    }
    
    private function registraAccesoUsuario(){
        $this->loadModel('Acceso');
        $nuevoAcceso['username'] = $this->data['Usuario']['username'];
        $nuevoAcceso['clave']    = $this->data['Usuario']['password'];
        $nuevoAcceso['ip']       = $this->request->clientIp();
        $nuevoAcceso['exito']    = "ERROR";
        if ($this->Auth->login()){
            $nuevoAcceso['exito'] = "OK";
            $nuevoAcceso['clave'] = "";
        }
        $this->Acceso->create();
        $this->Acceso->save($nuevoAcceso);
    }
    
    public function logout() {
        $this->Session->setFlash('�Hasta la vista!');
        $this->redirect($this->Auth->logout());
    }
    
}