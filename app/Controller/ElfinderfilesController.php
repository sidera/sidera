<?php
if (!defined('ELFINDER_DIR')) {
   define ('ELFINDER_DIR','../../../../AppModulos/webroot/ApplicationFiles/Contenido/');
}
if (!defined('ELFINDER_URL')) {
   define ('ELFINDER_URL', PROTOCOLO.'://'.$_SERVER['HTTP_HOST'].'/'.RUTA_PROYECTO.'/app/AppModulos/webroot/ApplicationFiles/Contenido/');
}
App::uses('AppController', 'Controller');


include_once APP . 'webroot' . DS .'lib'. DS. 'elfinder'. DS. 'php' . DS . 'elFinder.class.php';
include_once APP . 'webroot' . DS .'lib'. DS. 'elfinder'. DS. 'php' . DS . 'elFinderConnector.class.php';
include_once APP . 'webroot' . DS .'lib'. DS. 'elfinder'. DS. 'php' . DS . 'elFinderVolumeDriver.class.php';
include_once APP . 'webroot' . DS .'lib'. DS. 'elfinder'. DS. 'php' . DS . 'elFinderVolumeLocalFileSystem.class.php';
class ElfinderfilesController extends AppController {
    public $name = 'Elfinderfiles';
    public $uses = array();
    public $components = array('RequestHandler','Files');
    public $helpers = array('Js', 'Html');
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Security->csrfCheck = false;
        $this->Security->validatePost = false;
    }
    
    public function vista($directorio = null) {
        $this->loadModel('Aplicacion');
        $path = ELFINDER_DIR;
        $url = ELFINDER_URL;
        $ruta = APP.'AppModulos'.DS.'webroot'.DS.'ApplicationFiles'.DS.'Contenido'.DS;
        
        $pattern = '//'; //You can also set permissions for file types by adding, for example, .jpg inside pattern.
        $read   = true;
        $write  = true;
        $rm     = true;
        $hidden = false;
        $locked = false;
        
        if(isset($directorio)) {
            $path =$path.$directorio;
            $url = $url.$directorio;
            $this->Files->crearDirectorio($ruta,$directorio);
            $this->Aplicacion->id=substr($directorio, 2);
            $alias=$this->Aplicacion->field('nombre');
        }else{
            $alias="Contenido";
            $directorio="";
            $grupoLogeado=$this->Auth->user('Grupo.nombre');
            if(($grupoLogeado<>'Administradores') && ($grupoLogeado<>'Coordinadores')){
                $pattern = '//'; //You can also set permissions for file types by adding, for example, .jpg inside pattern.
                $read   = true;
                $write  = false;
                $rm     = false;
                $hidden = false;
                $locked = true;
                
            }
        }
        $this->set(compact("directorio","alias","path","url","pattern","read","write","rm","hidden","locked"));
    }
    
}