<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */

App::uses('AppController', 'Controller');

class PdfsController extends AppController {
     public $components = array('Esquema');
     
     var  $uses = null;
     var  $helpers = array('Pdf');
     
     
     // $propiedades = $this->Esquema->propiedadesModelo($schema);
      
     function index() {
       $this->layout='pdf';
       $datos=$this->request['data'];
       return $this->array2table($datos);
     }
     
        
    /*
     *Funci�n array2table
     * Descripci�n: Transforma un array con formato consulta find de cakephp en una tabla HTML
     */
    function array2table($array){
         
      $table = '<table>';
      $modelos=$array[0]; //Retorna los modelos del nivel 0
    
      //Cabeceras de la tabla
      $table .= "\t<tr>";
      foreach ($modelos as $modeloNombre => $arrayCampos) {
                
           foreach ($arrayCampos as $campo => $valor) {
                //  if (!is_array($valor))$table .= '<th>'.$modeloNombre .'.'. $campo . '</th>';  //Para que salgan con modelo            
                  if (!is_array($valor))$table .= '<th>'. $campo . '</th>';
           }
      }
       // Filas de datos
      $table .= "</tr>\n";
      
        foreach ($array as $registros) { //registro con los modelos en cada fila
            $table .= "\t<tr>";  
                 foreach ($registros as $datos) {
                     foreach ($datos as $campo => $valor) {
                         //introducimos este caracter &#8203; en los xls para qeu no transforme los numero grandes ni las fechas
                         if (!is_array($valor)){
                             if($_REQUEST['tipoimpresion']=="xls"){
                                $table .= '<td >&#8203;'.  $valor . '</td>';
                             }
                             else{
                                $table .= '<td >'.  $valor . '</td>'; 
                             }
                         }
                    }
               }
            $table .= "</tr>\n";     
        }
        // Fin table
        $table .= '</table>';
        $html=$this->styles().$table;
      return   $html;
      
    
    }

    /*
     * funci�n que configura los estilos para la tabla HTML formada
     */
    function styles(){
        return
        
'<style>
    h1 {
        color: navy;
        font-family: times;
        font-size: 24pt;
        text-decoration: underline;
    }
   
    table {
        color: #000000;
        font-family: helvetica;
        font-size: 8pt;
        border-left:  2px solid #000000;
        border-right: 2px solid #000000;
        border-top:  2px solid #000000;
        border-bottom:  2px solid #000000;
 
    }
    
    th{
        border-left:  1px solid #000000;
        border-right: 1px solid #000000;
        border-top:  1px solid #000000;
        border-bottom:  1px solid #000000;
        background-color: #d3d3d3;
        font-weight:bold;
        text-align: center;
    }
    
    td{
        border-left:  1px solid #000000;
        border-right: 1px solid #000000;
        border-top:  1px solid #000000;
        border-bottom:  1px solid #000000;
    }
    
   
   
</style>';
    }


}
