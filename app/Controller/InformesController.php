<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses("AppController", "Controller");
/**
 * informes Controller
 *
 * @property Informe $Informe
 */
class InformesController extends AppController {

    public $components = array("Report");
    var $layout = "ajax";
    

    public function beforeFilter(){
       parent::beforeFilter();
        $this->modelo      = "Informe";
        $this->controlador = "informes";
        $this->modelosAjenos = array("Informestipo","Grupo");
        $this->modelosAjenosRelacionados = array();
   }
   
    public function index($page=null) {
        $modelo       =$this->modelo;
        $controlador  = $this->controlador;
        
        $this->datosParametros($this);
        
        $this->comprobarPaginacion();
        
        $this->Esquema->comprobarSessionModeloYML($modelo);
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $camposModelo = $configModulo["general"]["campos"];
     
        $fieldsForSearch            = $this->Esquema->obtenerCamposABuscarEnIndex($this,$configModulo,$camposModelo,$modelo);
        $this->comprobarFiltroActivo();
        $this->comprobarBusquedaActiva($fieldsForSearch);
        
        $this->paginate["limit"]     = $configModulo["general"]["config"]["registrosPagina"];
        $this->paginate["order"]     = array("$modelo.id" => "DESC");
        
        $configModulo["general"]["botones"]["btconfigurar"]=$this->Session->read("CofigurarModulos");
        
        $registros=$this->paginate();
        $this->set(compact("registros","modelo","controlador","configModulo"));
	}
        
        /* funci�n para pasar los datos necesarios que necesiten los par�metros en caso de necesitarlos (men�s desplegables)
         * @param Class $object, se usa como $this, ya que ha sido pasado como par�metro
         */
        private function datosParametros($object) {
            /*  Ejemplo de uso para cargar un combo en la vista para un par�metro
             * 
              	$object->loadModel('Modelo');
                $datos = $object->Modelo->find('list', 
                                              array('order' => array('Modelo.curso_id' => 'desc')));
	   	$object->set('variable', $datos);
             
             */
        }

      
	/**
	* add method
	*
	* @return void
	*/
	public function add() {
		// $modelosAjenos: detecta los modelo relacionados con el actual y
		// pasa a la vista una variable por cada campo del tipo"_id" 
		// dicha variable es una lista del tipo (id,displayField)
		// Ejemplo: campo->grupo_id // variable->$grupos
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;

        $schema       = $this->$modelo->schema();
        $propiedades  = $this->Esquema->propiedadesModelo($schema); 
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $configModulo = $this->Esquema->identificarCamposObligatorios($configModulo,$propiedades,"add");
        $this->Esquema->obtenerDatosModelosAjenos($this);
        $accion="Add";
        $this->set(compact("modelo","propiedades","controlador","accion","configModulo"));
	}
        
        public function create(){
            $modelo     =$this->modelo;
            $controlador=$this->controlador;
            
            $this->$modelo->create();
            
            if ($this->$modelo->save($this->request->data)) {
                $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
                $configModulo = $this->Esquema->leerConfigSession($modelo);
            } else {
                $this->Session->setFlash(MSG_SAVEDERROR, ERROR);
            }
             
            $this->redirectSidera($controlador,"index");   
         }
	
	public function edit($id = null) {
            // $modelosAjenos: detecta los modelo relacionados con el actual y
            // pasa a la vista una variable por cada campo del tipo"_id" 
            // dicha variable es una lista del tipo (id,displayField)
            // Ejemplo: campo->grupo_id // variable->$grupos
            $modelo       = $this->modelo;
            $controlador  = $this->controlador;

            $accion="Edit";  
            $schema       = $this->$modelo->schema(true);
            $propiedades  = $this->Esquema->propiedadesModelo($schema);  
            $configModulo = $this->Esquema->leerConfigSession($modelo);
            $configModulo = $this->Esquema->identificarCamposObligatorios($configModulo,$propiedades,"edit");
            $this->Esquema->obtenerDatosModelosAjenos($this);

            $this->request->data = $this->$modelo->read(null, $id);
            $this->set(compact("modelo","propiedades","controlador","accion","configModulo"));
	}
		
	 public function update($id = null){
         $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        $this->$modelo->id = $id;
        
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        
            if ($this->$modelo->save($this->request->data)) {
                $this->Session->setFlash(MSG_EDITSUCCESS, SUCCESS);
            } else {
                $this->Session->setFlash(MSG_EDITERROR, ERROR);
            }
            
           //Comprobar si viene de una pagina
            if($this->Session->check("page[$this->modelo]")==TRUE){    
              $this->redirectSidera($controlador,"index/page:".$this->Session->read("page[$this->modelo]"));   
            }else{
              $this->redirectSidera($controlador,"index");        
            }  
    }


    public function delete($id = null) {
        $this->autoRender = false;
        $modelo=$this->modelo;
        $controlador=$this->controlador;
        
        $datos=$this->request->data["id"];
        $exploded_id = explode(",", $datos);
        
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        
        foreach ($exploded_id as $id) {
            $this->$modelo->id=$id;
             if (!$this->$modelo->exists()) {
                $this->Session->setFlash(MSG_DELETENOTICE, NOTICE);
            }
            $this->$modelo->delete(); 
        }
        $this->Session->setFlash(MSG_DELETESUCCESS, SUCCESS);  
        $this->redirectSidera($controlador,"index");        
    }

    /*
    * Funcion imprimir
    * Descripcion: Accion que imprime cualquier informe con par�metros
    * Parametros :  Enviados por desde el m�dulo informes
    * Observaciones: 
    *           - Para pasar un par�metro desde el formulario para que al informe le llegue %, poner la opci�n como 'all' y 
    *                convertir a % aqu�.
    *           - El name del elemento en HTML debe corresponderse con el nombre del par�metro en el informe
    *           - Si usamos la funci�n desde el m�dulo de informe, llegar� el id en base de dato del informe. 
     *          Si lo usamos desde desde otro m�dulo, podemos usar el id o el nombre del report en el servidor.
    */
    function imprimir($id=null, $params=null, $paramsValues=null) {
        $this->autoRender = false;
        
        if(is_numeric($id)){
            $datosInforme   = $this->Informe->findById($id);
            $informe        = $datosInforme[$this->modelo]['report'];
        }else{
            $informe = $id;
        }   
            $reportUnit     = '/'.REPORT_UNIT.'/' . $informe;
            $reportFormat   = 'PDF';
            $reportParams   = NULL;
            
         if($params){
             
           $params      = split(",",$params );           
           $paramsValues= split(",", $paramsValues);
           $param       = 0;
           
           //Se a�aden los par�metros que se van a pasar al Informe
            foreach ($params as $parametro) {
                    if($paramsValues[$param] == 'all' || $paramsValues[$param] == null){
                         $valor = '%';
                     }else{
                         $valor = $paramsValues[$param];
                         $valor = str_replace("|",",",str_replace("*","/",$paramsValues[$param]));
                     }
                     $reportParams[$parametro] = $valor;
              $param++; 
            }
           }
           
           $nombreInforme= APLICACION . " - " . $informe;
         
          $this->Report->obtenerInforme($reportUnit, $reportFormat, $reportParams, $nombreInforme);  
        }


}