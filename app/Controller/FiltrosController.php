<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt   

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
/**
 * Filtros Controller
 *
 * @property Filtro $Filtro
 */
class FiltrosController extends AppController {
	
	public  $components = array('Configurar');
	var $layout         = "ajax";
    var $operadores     = array(
		'string' => array(
			'eq' => '=',
			'ne' => '<>',
			'bw' => 'empieza por',
			'bn' => 'no empieza por',
			'ew' => 'termina por',
			'en' => 'no termina por',
			'cn' => 'contiene',
			'nc' => 'no contiene'),
		'integer'=>array(
			'eq' => '=',
			'ne' => '<>',
			'lt' => '<',
			'le' => '<=',
			'gt' => '>',
			'ge' => '>=',
			'bw' => 'empieza por',
			'bn' => 'no empieza por',
			'ew' => 'termina por',
			'en' => 'no termina por',
			'cn' => 'contiene',
			'nc' => 'no contiene'
		),
        'association'=>array(
            'eq' => 'es',
            'ne' => 'no es'
        )
	);
    
	/**
	 * index method
	 *
	 * @return void
	*/
	public function index() {
		$this->Filtro->recursive = 0;
        if ($this->request->is('post')) {
            $searchText = $this->request->data['searchText'];
            $fieldsForSearch = array('nombre','descripcion');
            foreach ($fieldsForSearch as $fieldForSearch) {
                $this->paginate['conditions']['OR']["$fieldForSearch LIKE"] = "%$searchText%"; 
            }
            $this->set('searchText',$searchText);
        }
        $this->set('filtros', $this->paginate());
	}

	/**
	 * view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	*/
	public function view($id = null) {
		$this->Filtro->id = $id;
		if (!$this->Filtro->exists()) {
			throw new NotFoundException(__('Invalid filtro'));
		}
		$this->set('filtro', $this->Filtro->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	*/
	public function add() {
		if ($this->request->is('post')) {
			$this->autoRender=false;
			$this->Filtro->create();
			$usuario = $this->Session->read('Auth.User.id');
			$this->request->data['Filtro']['usuario_id'] = $usuario;
			if ($this->Filtro->saveAll($this->request->data)) {
				$this->Session->setFlash('Filtro guardado');
			} else {
				$this->Session->setFlash('El filtro no se ha podido guardar');
			}
		}
	}

	/**
	 * edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	*/
	public function edit($id = null) {
		$this->Filtro->id = $id;
		if (!$this->Filtro->exists()) {
			throw new NotFoundException('Filtro no valido');
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Filtro->save($this->request->data)) {
				$this->Session->setFlash('Filtro guardado');
			} else {
				$this->Session->setFlash('No se ha guardado el filtro');
			}
		} else {
			$this->request->data = $this->Filtro->read(null, $id);
		}
	}

	/**
	 * delete method
	 *
	 * @throws MethodNotAllowedException
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	*/
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
        $this->autoRender=false;
        $exploded_id = explode(',', $_POST['id']);
        
        for($i=0;$i<count($exploded_id);$i++){ 
    		$this->Filtro->id = $exploded_id[$i];
    		if (!$this->Filtro->exists()) {
    			throw new NotFoundException('Filtro no valido');
    		}
			$this->Filtro->delete(null,true);
        }
	}
    
public function filtro($nombreModelo,$nombre_accion) {
        $this->loadModel($nombreModelo);
        $this->loadModel('Condicion');
        $this->$nombreModelo->recursive = 0;
        
        $schema       = $this->$nombreModelo->schema(true);
        $tipos        = array();
        $configCampos = array();
        $relaciones   = array();
        $configModulo = $this->Esquema->leerConfigSession($nombreModelo);
        $camposModelo = $configModulo["general"]["campos"];
        
        $i=0;
        
        foreach ($camposModelo as $campo => $etiqueta){
        	if(isset($configModulo["index"]["campos"][$campo]["filtrar"]) && $configModulo["index"]["campos"][$campo]["filtrar"]==1){
	        	$configCampos[$campo] = $etiqueta['etiqueta'];
	            $tipos[$campo] = $schema[$campo]['type'];
	            $i++;
        	}
        }
        $campos            = $configCampos;
        $operadores        = $this->operadores;
        $nombreControlador = Inflector::pluralize($nombreModelo);
        $filtrosGuardados  = $this->Filtro->find('all',
          array(
              'conditions' => array(
              	'Filtro.modelo =' => $nombreModelo,
              	'Filtro.usuario_id' => $this->Session->read('Auth.User.id')
              ),
              'order'      => array('Filtro.titulo'),
              'fields'     => array('Filtro.id','Filtro.titulo')
        ));
        $condicionesFiltrosGuardados = $this->Filtro->find('all',
          array(
              'conditions' => array('Filtro.modelo =' => $nombreModelo),
              'order'      => array('Filtro.titulo'),
              'fields'     => array('Filtro.id','Filtro.titulo')
        ));
        $this->checkModal();
        $parametros = null;
        $parametros = $this->Session->read("filtro[$nombreModelo]");
        $this->set("filtro","NO");
        if($parametros != null) $this->set('filtro',"SI");
        $this->set(compact('filtrosGuardados','condicionesFiltrosGuardados','nombreModelo','campos','tipos','parametros',
                'operadores','nombreControlador','relaciones','nombre_accion'));
        
    }


    public function leerCondicionesFiltro($id) {
        $this->loadModel('Condicion');
        $this->Condicion->recursive = 1;
        $condiciones = $this->Condicion->find('all',
          array(
              'conditions' => array('Filtro.id' => $id),
              'fields'     => array('Filtro.tipo_condicion','Condicion.campo','Condicion.comparador','Condicion.contenido')
        ));
        $this->set('condiciones',json_encode($condiciones));
        $this->render('condicionesFiltro');
    }
    
     /* Funcion limpiarTodosFiltroActivo:   Limpia todos los filtros al al cambiar de menu*/
    public function limpiarTodosFiltroActivo($controlador_menu){
            $this->autoRender = false;
            $modelo_filtro=ucwords(Inflector::singularize($controlador_menu));
            $this->Session->write("filtro[$modelo_filtro]",'');
    }
}
