<?php
/*
* SIDERA : Sistema de Desarrollo Rapido.
* Copyright (C) 2014 
* 
* Organization: 
*           Junta de Extremadura
* Autors:
*		Francisco Gonzalez Lozano
*		Jesus Arance Calvo
*		Javier Mateos Caballero				
*
* This file is part of SIDERA, licensed under The MIT License
* For full copyright and license information, please see the app/lib/LICENSE.txt    

* @since         SIDERA 2.1
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/

App::uses("AppController", "Controller");

class PermisosController extends AppController {

    public  $components      = array('Acl');
    public  $methods         = array();
    public  $layout          = "ajax";
    private $fieldsForSearch = array("Permiso.id");
    private $controllers     = array();
    private $log             = array();
    private $mainNodeController;
    private $aco;
    
    public function beforeFilter(){
        parent::beforeFilter();
        $this->modelo      = "Permiso";
        $this->controlador = "permisos";
        $this->modelosAjenos = array();
        $this->modelosAjenosRelacionados = array();
        $this->aco = $this->Acl->Aco;
    }
   
    public function index() {
        $modelo          = $this->modelo;
        $controlador     = $this->controlador;
        $fieldsForSearch = $this->fieldsForSearch;
        $this->$modelo->recursive = 2;
        $this->set("filtrado","NO");
        $solicitantes    = array();
        $list            = array();
        $this->loadModel('Usuario');
        $this->loadModel('Grupo');
        if ($this->params->named["searchText"] || $this->data["searchText"]) {
            $searchText = ($this->data["searchText"])?$this->data["searchText"]:$this->params->named["searchText"];
            $this->set("searchText",$searchText);
            $solicitantes['Usuario'] = $this->Usuario->find('list',array('fields'=>'id,username','conditions'=>array('Usuario.username LIKE'=>"%$searchText%")));
            $listU=array();
            if(count($solicitantes['Usuario'])>0){
                foreach($solicitantes['Usuario'] as $k=>$s) $listU[]=$k; 
                $this->paginate['conditions']['OR'][]=array('AND'=>array('Permitido.model'=>'Usuario'));
                $this->paginate['conditions']['OR'][]=array('AND'=>array('Permitido.foreign_key'=>$listU));
                
            }
            $solicitantes['Grupo'] = $this->Grupo->find('list',array('fields'=>'id,nombre','conditions'=>array('Grupo.nombre LIKE'=>"%$searchText%")));
            $listG=array();
            if(count($solicitantes['Grupo'])>0){
                foreach($solicitantes['Grupo'] as $k=>$s) $listG[]=$k;
                $this->paginate['conditions']['OR'][]=array('AND'=>array('Permitido.model'=>'Grupo'));
                $this->paginate['conditions']['OR'][]=array('AND'=>array('Permitido.foreign_key'=>$listG));
            }
           
        }else{
            $solicitantes['Usuario'] = $this->Usuario->find('list',array('fields'=>'id,username'));
            $solicitantes['Grupo'] = $this->Grupo->find('list',array('fields'=>'id,nombre'));
        }
        
        $this->set("solicitantes",$solicitantes);
        
        $this->paginate['conditions']['AND']['Permiso.id !=']=1;
        $this->paginate['fields'] = array('Permiso.aro_id');
        $this->paginate['group']  = array('Permiso.aro_id');
        $registros = $this->paginate();
        $this->set("registros", $registros);
        $this->set("modelo", $modelo);
        $this->set("controlador", $controlador);
        $this->set(compact("registros","modelo","controlador"));
    }

    public function add() {
        $modelo      = $this->modelo;
        $controlador = $this->controlador;
        $schema      = $this->$modelo->schema(true);
        $propiedades = $this->Esquema->propiedadesModelo($schema);

        if ($this->request->is("post")) {
            foreach($this->request->data['Permiso']['aco_id'] as $acoid){
                $newData = array('Permiso' => array(
                    'aro_id'    => $this->request->data['Permiso']['aro_id'],
                    'aco_id'    => $acoid,
                    'permitido' => 1));
                $this->$modelo->create();
                $this->$modelo->save($newData);
            }
            $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
        }
        $permitidos = $this->$modelo->Permitido->find("all");

        $this->loadModel('Usuario');
        $solicitantes['Usuario'] = $this->Usuario->find('list',array('fields'=>'id,username'));

        $this->loadModel('Grupo');
        $solicitantes['Grupo'] = $this->Grupo->find('list',array('fields'=>'id,nombre'));

		$modulosControlador = $this->$modelo->Modulo->find("all",array(
            'fields'     => array(
                'id',
                'alias'),
            'conditions' => array(
                'Modulo.parent_id' => 1,
                "Modulo.model"     => NULL,
                'Modulo.alias !='  => 'permisosmenus'),
            'order'      => 'alias'));
        $modulos = array();

        foreach($modulosControlador as $moduloControlador){
            $modulos[$moduloControlador['Modulo']['alias']] = $this->$modelo->Modulo->find("list", array(
                'conditions' => array(
                    'Modulo.parent_id' => $moduloControlador['Modulo']['id']),
                'order'=>'alias'));
		}

        $this->loadModel('Menu');
        $menusAll = $this->Menu->find('all');
        $menus    = array();
        foreach($menusAll as $k=>$menu){
            $menuModulo = $this->$modelo->Modulo->find('all',array(
                'conditions' => array(
                    'Modulo.model'       => 'Menu',
                    'Modulo.foreign_key' => $menu['Menu']['id'])));
            if(count($menuModulo)>0){
                $menus[$menuModulo[0]['Modulo']['id']]=$menu['Menu']['ruta'];
            }
        }
        
        $this->set("modelo",$modelo);
        $this->set("propiedades",$propiedades);
        $this->set("controlador",$controlador);
        $this->set("accion","Add");
        $this->set(compact("modelo","propiedades","controlador","accion"));
        $this->set(compact("permitidos"));
        $this->set("solicitantes",$solicitantes);
        $this->set(compact("modulos"));
        $this->set(compact("menus"));
    }

    public function edit($id = null) {
        $modelo      = $this->modelo;
        $controlador = $this->controlador;
        $this->$modelo->recursive = 0;
        $schema      = $this->$modelo->schema(true);
        $propiedades = $this->Esquema->propiedadesModelo($schema);
        
        $this->set("modelo",$modelo);
        $this->set("propiedades",$propiedades);
        $this->set("controlador",$controlador);
        $this->set("accion","Edit");
        $this->set(compact("modelo","propiedades","controlador","accion"));
        
        $acosList = array();
        $acos     = "";
		if ($this->request->is("post") || $this->request->is("put")) {
	    	$this->Permiso->deleteAll(array('aro_id'=>$this->request->data['Permiso']['aro_id']), false);
	    	foreach($this->request->data['Permiso']['aco_id'] as $acoid){
                $newData = array('Permiso'=>array('aro_id'=>$this->request->data['Permiso']['aro_id'],'aco_id'=>$acoid,'permitido'=>1));
                $this->$modelo->create();
                $this->$modelo->save($newData);
            }
            $this->Session->setFlash(MSG_EDITSUCCESS, SUCCESS);
        }
		$acosIni = $this->Permiso->find('all',array('fields'=>array('aro_id','aco_id'),'conditions'=>array('Permiso.aro_id'=>$id,'Modulo.model'=>NULL)));
        foreach($acosIni as $aco) $acosList[]=$aco['Permiso']['aco_id'];
        $acos = implode(',',$acosList);
        
        $menusIni = $this->Permiso->find('all',array('fields'=>array('aro_id','aco_id'),'conditions'=>array('Permiso.aro_id'=>$id,'Modulo.model'=>'Menu')));
        foreach($menusIni as $menu) $menusList[]=$menu['Permiso']['aco_id'];
        $menusVisibles = implode(',',$menusList);
		
		$permitidos = $this->$modelo->Permitido->find("all",array('conditions'=>array('Permitido.id'=>$id)));
        $this->set(compact("permitidos"));
        
        $this->loadModel('Usuario');
        $solicitantes['Usuario'] = $this->Usuario->find('list',array('fields'=>'id,username'));
        
        $this->loadModel('Grupo');
        $solicitantes['Grupo'] = $this->Grupo->find('list',array('fields'=>'id,nombre'));
        $this->set("solicitantes",$solicitantes);
        
        $modulosControlador = $this->$modelo->Modulo->find("all",array(
            'fields' => array('id','alias'),
            'conditions' => array(
                'Modulo.parent_id' => 1,
                'Modulo.model'     => NULL,
                'Modulo.alias !='  => 'permisosmenus'
                ),
            'order' => 'alias'));
        $modulos = array();
        foreach($modulosControlador as $moduloControlador){
            $modulosHijos = null;
            $modulosHijos = $this->$modelo->Modulo->find("list", array(
                'conditions' => array(
                    'Modulo.parent_id' => $moduloControlador['Modulo']['id']),
                    'order'            => 'Modulo.alias'));
            $modulos[$moduloControlador['Modulo']['alias']] = $modulosHijos;
        }
        $this->loadModel('Menu');
        $menusAll = $this->Menu->find('all');
        $menus = array();
        
        foreach($menusAll as $k=>$menu){
            $menuModulo = $this->$modelo->Modulo->find('all',array(
                'conditions' => array(
                    'Modulo.model' => 'Menu',
                    'Modulo.foreign_key' => $menu['Menu']['id'])));
            if(count($menuModulo)>0){
                $menus[$menuModulo[0]['Modulo']['id']] = $menu['Menu']['ruta'];
            }
        }
        
        $this->set(compact("menus"));
        $this->set(compact("modulos"));
        $this->set(compact("acos"));
        $this->set(compact("menusVisibles"));
    }

    public function delete($id = null) {
        if (!$this->request->is("post")) {
            throw new MethodNotAllowedException();
        }
        $this->autoRender = false;
        $exploded_id = explode(",", $_POST["id"]);
        for($i=0;$i<count($exploded_id);$i++){
            $this->Permiso->deleteAll(array('aro_id'=>$exploded_id[$i]), false);
        }
    }

    public function resetSync() {
        $this->autoRender  = false;
        $this->removeAllAcos();
        $this->createAcoMainNode();
        $this->createControllersNodes();
        $this->createAcosForMenus();
        $this->cleanArosAcos();
        $this->createArosAcosForAdmin();
    }
    
    private function removeAllAcos(){ 
        $this->loadModel('Modulo');
        $this->Modulo->recursive = -1;
        $this->Modulo->deleteAll(array('1=1'), false);
    }
    
    private function createAcoMainNode(){
        $this->aco->create(array('id'=>1,'parent_id' => null, 'model' => null,'alias'=>'controllers' ));
        $this->aco->save();
    }
    
    private function createControllersNodes(){
        $this->setMainNodeController();
        $this->setControllers();
        foreach ($this->controllers as $controllerName) {
            $this->createControllerNodes($controllerName);
        }
    }

    private function setMainNodeController(){
        $this->loadModel('Modulo');
        $this->Modulo->recursive = 0;
        $root = $this->Modulo->find('first',array('conditions'=>array('Modulo.alias'=>'controllers')));
        if (!$root) {
            $this->createAcoMainNode();
            $root = $this->Modulo->find('first',array('conditions'=>array('Modulo.alias'=>'controllers')));
            $this->logOk('Recurso creado para nodo principal controllers' );
        }
        $this->mainNodeController = $root;
    }
    
    private function setControllers(){
        $this->controllers = $this->getAllControllersFromFiles();
        $this->removeAppControllerFromControllers();
    }
    
    private function getAllControllersFromFiles(){
        $controllers = array();
        App::uses('File', 'Utility');
        
        $controllersFresh = App::objects('Controller',null,false);
	        
        foreach ($controllersFresh as $cnt) {
            $controllers[] = str_replace('Controller', '', $cnt);
        }
        return $controllers;        
    }
    
    private function removeAppControllerFromControllers(){
        $appIndex = array_search('App', $this->controllers);
        if ($appIndex !== false){
            unset($this->controllers[$appIndex]);
        }
    }
    
    private function createControllerNodes($controllerName){
        $controllerNode = $this->createNodeFromController($controllerName);     
        $this->createNodeForControllerMethods($controllerNode);
    }
    
    private function createNodeFromController($controllerName){
        $this->loadModel('Modulo');
        $controllerNode = null;
        $node = $this->Modulo->find('first',array('conditions'=>array(
                'Modulo.parent_id' => intval($this->mainNodeController['Modulo']['id']),
                'Modulo.alias'     => $controllerName)));
        if (empty($node)) {
            $this->Modulo->create();
            $this->Modulo->save(array(
                'Modulo'=>array(
                    'parent_id' => intval($this->mainNodeController['Modulo']['id']),
                    'model'     => null, 
                    'foreign_key' => null,
                    'alias'     => $controllerName)));
            $node = $this->Modulo->find('first',array('conditions'=>array(
                'Modulo.parent_id'=>intval($this->mainNodeController['Modulo']['id']),
                'Modulo.alias' => $controllerName)));
            $controllerNode['Aco'] = $node['Modulo'];
            $this->logOk('Recurso creado para ' . $controllerName);
        }else{
            $controllerNode['Aco'] = $node['Modulo'];
        }
        return $controllerNode;
    }
    
    private function createNodeForControllerMethods($controllerNode){
        $this->setControllerMethods($controllerNode['Aco']['alias']);
        foreach ($this->methods as $methodName) {
            $this->createMethodNode($controllerNode, $methodName);
        }
    }
    
    private function setControllerMethods($controllerName = null) {
        App::uses($controllerName . 'Controller', 'Controller');
        unset($this->methods);
        $this->methods   = array();
        $controllerClass = $controllerName . 'Controller';
        
        try {
        	$class           = new ReflectionClass($controllerClass);
        	$publicMethods   = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        	foreach($publicMethods as $publicMethod){
            	$this->methods[] = $publicMethod->getName();
        	}
        	$this->removeBaseMethods();
		}catch (Exception $e){
			CakeLog::write('error', 'Error al obtener las clases y los m�todos al sincronizar permisos. Compruebe que no hay clases duplicadas en ficheros de respaldo en controladores.');
			CakeLog::write('sidera', 'Error al obtener las clases y los m�todos al sincronizar permisos. Compruebe que no hay clases duplicadas en ficheros de respaldo en controladores.');
			die();
		}
    }
   
    private function removeBaseMethods(){
       $baseMethods = $this->getBaseMethods();
       foreach ($this->methods as $k => $method){
            if (in_array($method, $baseMethods)){
                unset($this->methods[$k]);
            }
       }
    }
    
    private function getBaseMethods(){
        $baseMethods   = get_class_methods('Controller');
        $baseMethods[] = 'build_acl';
        return array_unique($baseMethods); 
    }
    
    private function createMethodNode( $controllerNode, $methodName ){
        if ($this->existsMethodNode($controllerNode, $methodName) == false) {
            $this->loadModel('Modulo');
            $this->Modulo->create();
            $this->Modulo->save(array(
                'Modulo'=>array(
                    'parent_id' => $controllerNode['Aco']['id'],
                    'model'     => null, 
                    'foreign_key' => null,
                    'alias'     => $methodName)));
            $this->logOk('Recurso creado para ' . $controllerNode['Aco']['alias'].' => ' . $methodName);
        }
    }
    
    private function existsMethodNode($controllerNode, $methodName){
        $this->loadModel('Modulo');
        $methodNode = $this->Modulo->find('first',array(
            'conditions'=>array(
                'Modulo.parent_id' => $controllerNode['Aco']['id'],
                'Modulo.alias'     => $methodName)));
        if (empty($methodNode)) {
            return false;
        }
        return true;
    }
    
    public function checktosync() {
        $this->setControllers();
        foreach ($this->controllers as $controllerName) {
            $this->setControllerMethods($controllerName);
            $this->checkNodesFromMethods($controllerName);
        }
        $this->checkNodesForMenus();
        $this->setMessage();
    }
    
    private function setMessage(){
        $mensaje = array();
        if(count($this->log)>0){
            $mensaje['sync']='TRUE';            
            $mensaje['message'][]=$this->getLogs();
        }else{
            $mensaje['sync']='FALSE';
        }
        $this->set('mensaje',$mensaje);
    }

    public function sync() {
        $this->createControllersNodes();
        $this->cleanAcosFromNonExistentMethods();
        $this->createAcosForMenus();
        $this->setMessage();
        $this->render('checktosync');
    }
    
    private function createAcosForMenus(){
        $this->createParentMenuAco();
        $this->createMenusAcos();
    }
    
    private function createParentMenuAco(){
        if(!$this->existParentMenuAco()){
            $this->loadModel('Modulo');
            $this->Modulo->create();
            $this->Modulo->save(array(
                'Modulo' => array(
                    'parent_id' => intval($this->mainNodeController['Modulo']['id']), 
                    'model'     => null,
                    'alias'     =>'permisosmenus' )));
        }
    }
    
    private function existParentMenuAco(){
        $parentMenuAco = $this->Modulo->find('all',array('conditions'=>array('Modulo.alias'=>'permisosmenus')));
        if(empty($parentMenuAco)){
            return false;
        }
        return true;
    }
    
    private function createMenusAcos(){
        $menus  = $this->getMenus();
        $parentMenuAcoId = $this->getParentMenuAcoId();
        foreach($menus as $menu){
            $this->createMenuAco($parentMenuAcoId, $menu);
        }
    }
    
    private function getMenus(){
        $this->loadModel('Menu');
        $this->Menu->recursive = 0;
        return $this->Menu->find('all');
    }
    
    private function getParentMenuAcoId(){
        $this->loadModel('Modulo');
        $parentMenuAco = $this->Modulo->find('all',array('conditions'=>array('Modulo.alias'=>'permisosmenus')));
        return intval($parentMenuAco[0]['Modulo']['id']);
    }
    
    private function createMenuAco($parentMenuAcoId, $menu){
        if(!$this->existsMenuAco($menu)){
            $this->loadModel('Modulo');
            $this->Modulo->create();
            $this->Modulo->save(array( 
                'Modulo' => array(
                    'parent_id' => $parentMenuAcoId, 
                    'model' => 'Menu',
                    'foreign_key' => $menu['Menu']['id'] )));
            $this->logOk('Recurso creado para menu '.$menu['Menu']['titulo']);
        }
    }
    
    private function existsMenuAco($menu){
        $this->loadModel('Modulo');
        $menuAco = $this->Modulo->find('all',array('conditions'=>array('Modulo.model' => 'Menu','Modulo.foreign_key' => $menu['Menu']['id'])));
        if(empty($menuAco)){
            return false;
        }
        return true;
    }
    
    private function checkNodesForMenus(){
        $menus = $this->getMenus();
        foreach($menus as $menu){
            if(!$this->existsMenuAco($menu)){
                $this->logFault('No existe recurso para menu '.$menu['Menu']['titulo']);
            }
        }
    }
    
    private function cleanArosAcos(){
        $this->Permiso->deleteAll(array("1=1"), false);
    }
 
    private function createArosAcosForAdmin(){
        $this->Permiso->create();
        $data = array('Permiso'=>array('id'=>1,'aro_id'=>1,'aco_id'=>1,'permitido'=>'1'));
        $this->Permiso->save($data);
    }
     
    public function syncResourcesForController($controllerName) {
        $this->autoRender = false;
        $this->setMainNodeController();
        $this->createControllerNodes($controllerName);
        $this->cleanAcoFromNonExistentMethods($controllerName);
    }

    private function cleanAcoFromNonExistentMethods($controllerName){ 
        $acoParent = $this->getAcoForController($controllerName);
        $this->removeAcoParent($acoParent);
    }
    
    public function cleanAcosFromConstructor($controllerName){ 
        $aco = $this->getAcoForController($controllerName);
        if(count($aco)>0){
            $this->loadModel('Modulo');
            $this->Modulo->recursive = -1;
            $this->Modulo->deleteAll( array( 'Modulo.parent_id' => $aco['Modulo']['id']), false);
            $this->Modulo->deleteAll( array( 'Modulo.alias'     => $aco['Modulo']['alias']), false);
        }
    }
    
    private function getAcoForController($controllerName){
        $this->loadModel('Modulo');
        $this->Modulo->recursive = -1;
        $aco = $this->Modulo->find('all',array(
            'conditions' => array(
                    'Modulo.parent_id' => 1,
                    'Modulo.alias'     => $controllerName
        )));
        return $aco[0];
    }
    
    private function checkNodesFromMethods($controller){
        $this->checkIfNotExistsMainNodeController();
        $this->checkNodeFromController($controller);
        $this->checkNonExistentResourcesForMethods($controller);
        $this->checkNonExistentMethodsForResources($controller);
    }

    private function checkIfNotExistsMainNodeController(){
        $root = $this->aco->node('controllers');
        if (!$root){
            $this->logFault('Falta recurso para nodo principal controllers');
        }
    }
    
    private function checkNodeFromController($controller){
        $controllerNode = $this->aco->node('controllers/' . $controller);
        if (!$controllerNode) {
            $this->logFault('Falta recurso para ' . $controller);
        }
    }
    
    private function logOk($message){
        $this->myLog('<span style="color:#57931E;"><i class="fa fa-ok"></i></span>'.$message.'<br/>');
    }
    
    private function myLog($message){
        $this->log[] = $message;
    }
    
    private function logFault($message){
        $this->myLog('<span style="color:#DF4B4B;"><i class="fa fa-exclamation-sign"></i></span>'.$message.'<br/>');
    }
    
    private function checkNonExistentResourcesForMethods($controller){
        foreach ($this->methods as $method) {
            $methodNode = $this->aco->node('controllers/' . $controller . '/' . $method);
            if (!$methodNode){
                $this->logFault("Falta recurso para ".$controller. " => " . $method);
            }
        }
    }
    
    private function checkNonExistentMethodsForResources($controller){
        $controllerAco = $this->getAcoForController($controller);
        $this->loadModel('Modulo');
        $this->Modulo->recursive = -1;
        $methodsAcos = $this->Modulo->find('all',array('conditions' => array('Modulo.parent_id' => $controllerAco['Modulo']['id'])));
        foreach ($methodsAcos as $methodAco) {
            if (!in_array($methodAco['Modulo']['alias'], $this->methods)){
                $this->logFault("Falta metodo para recurso ".$controller. " => " . $methodAco['Modulo']['alias']);
            }
        }
    }
    
    private function getLogs(){
        if(empty($this->log)){
            $this->logOk("No hay recursos para sincronizar.");
        }
        $mensaje = implode($this->log);
        return $mensaje;
    }
    
    private function cleanAcosFromNonExistentMethods(){ 
        $acosParent = $this->getAcosForAllControllers();
        foreach ($acosParent as $acoParent) {
            $this->removeAcoParent($acoParent);
        }
    }
    
    private function getAcosForAllControllers(){
        $this->loadModel('Modulo');
        $this->Modulo->recursive=-1;
        $acosParent = $this->Modulo->find('all',array('conditions'=>array('Modulo.parent_id'=>1,'Modulo.alias !='=>'permisosmenus')));
        return $acosParent;
    }
    
    private function removeAcoParent($resource){
        if( $this->controllerExists($resource['Modulo']['alias']) ){
            $this->compareResourcesAndClean($resource);
        }else{
            $this->removeOrphanedAcos($resource);
        }
    }
    
    private function controllerExists($controllerName){
        if(empty($this->controllers)) $this->setControllers ();
        if( in_array($controllerName, $this->controllers)){
            return true;
        }
        return false;
    }
    
    private function compareResourcesAndClean($resource){
        $this->loadModel('Modulo');
        $this->setControllerMethods($resource['Modulo']['alias']);
        $acos = $this->Modulo->find('all',array('conditions'=>array('Modulo.parent_id'=>$resource['Modulo']['id'])));
        foreach ($acos as $aco) {
            if( ! in_array($aco['Modulo']['alias'], $this->methods) ){
                $this->Modulo->delete($aco['Modulo']['id']);
                $this->logOk('Recurso '.$aco['Modulo']['alias'].' eliminado al no existir coincidente');
            }
        }
    }
    
    private function removeOrphanedAcos($resource){
        $this->loadModel('Modulo');
        if($resource['Modulo']['id']!=null && $resource['Modulo']['alias'] != null){
            $this->Modulo->deleteAll(array('Modulo.parent_id' => $resource['Modulo']['id']), false);
            $this->Modulo->deleteAll(array('Modulo.alias'     => $resource['Modulo']['alias']), false);
            $this->logOk('Recurso '.$resource['Modulo']['alias'].' y sus acciones eliminados al no existir controlador coincidente');
        }
    }
    
    function getControllerPath($ctrlName = null) {
        $arr = String::tokenize($ctrlName, '/');
        if (count($arr) == 2) {
            return $arr[0] . '.' . $arr[1];
        } else {
            return $arr[0];
        }
    }
    
    public function darPermisoDesdeConstructor($controller,$aro) {
        $this->autoRender = false;
        $this->loadModel('Modulo');
        $this->Modulo->recursive = 0;
        $acos = $this->Modulo->find('all',array('conditions' => array('Parent.alias' => $controller)));
        $this->darPermiso($aro, $acos[0]['Modulo']['parent_id']);
        foreach($acos as $aco) {
            $this->darPermiso($aro, $aco['Modulo']['id']);
        }
        $menuId = $this->getMenuId($controller);
        $acoMenu = $this->getMenuAcoId($menuId);
        $this->darPermiso($aro, $acoMenu);
    }
    
    private function darPermiso($aro,$aco){
        $newData = array('Permiso' => array(
                    'aro_id'    => $aro,
                    'aco_id'    => $aco,
                    'permitido' => 1));
        $this->Permiso->create();
        $this->Permiso->save($newData);
    }
    
    private function getMenuAcoId($menuId){
         $acoMenu = $this->Modulo->find('all',array(
            'conditions' => array(
                'Modulo.model'       => 'Menu',
                'Modulo.foreign_key' => $menuId)));
         return $acoMenu[0]['Modulo']['id'];
    }
    
    private function getMenuId($controller){
        $this->loadModel('Menu');
        $menu = $this->Menu->find('all',array(
            'conditions' => array(
                'Menu.controlador' => $controller)));
        return $menu[0]['Menu']['id'];
    }
    
    public function verifyAcos(){
        die(print_r($this->Acl->Aco->verify()));
    }
    
    public function recoverAcos(){
        die(print_r($this->Acl->Aco->recover()));
    }   
}