<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses("AppController", "Controller");

class HistoricosController extends AppController {

    var    $layout      = "ajax";

    public function beforeFilter(){
       parent::beforeFilter();
        $this->modelo      = "Historico";
        $this->controlador = "historicos";
        $this->modelosAjenos = array();
        $this->modelosAjenosRelacionados = array();
   }
   
    public function index() {
        $modelo       =$this->modelo;
        $controlador  = $this->controlador;
        
        $this->comprobarPaginacion();
        
        $this->Esquema->comprobarSessionModeloYML($modelo);
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $camposModelo = $configModulo[general][campos];
     
        $fieldsForSearch            = $this->Esquema->obtenerCamposABuscarEnIndex($this,$configModulo,$camposModelo,$modelo);
        $this->comprobarFiltroActivo();
        $this->comprobarBusquedaActiva($fieldsForSearch);
        
        $this->paginate["limit"]     = $configModulo["general"]["config"]["registrosPagina"];
        $this->paginate["order"]     = array("$modelo.id" => "DESC");
        
          
        $configModulo["general"]["botones"]["btconfigurar"]=$this->Session->read("CofigurarModulos");
        
        $registros=$this->paginate();
        $this->set(compact("registros","modelo","controlador","configModulo"));
    }

    public function add($obj,$array_datos_antiguos) {
        $usuario=$obj->Session->read('Auth.User.username');                                                                             
        $modulo=$obj->name;                                                               
        $accion=$obj->request->params[action];
        
        if (sizeof($array_datos_antiguos)>0){
             $datos_antiguos = json_encode($array_datos_antiguos);
             //$datos_antiguos= iconv($this->inCharset, $this->outCharset, $datos_antiguos);
        }


        $request_data=array(Historico=>array(
                            'id'            => '',
                            'usuario'       => $usuario,
                            'modulo'        => $modulo,
                            'accion'        => $accion,                                                                                                                
                            'datos_antiguo' => $datos_antiguos
                         
                        )
       );
        
 
        $this->Historico->save($request_data);
    }
}