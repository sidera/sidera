<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt    

 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses("AppController", "Controller");
App::uses("Auditoria", "Model");
App::import('vendor', 'Spyc');

class AuditoriasController extends AppController {

    var $layout = "ajax";

    public function beforeFilter(){
        parent::beforeFilter();
        $this->modelo        = "Auditoria";
        $this->controlador   = "auditorias";
        $this->modelosAjenos = array();
        $this->modelosAjenosRelacionados = array();
    }
   
    public function index() {
        $modelo      = $this->modelo;
        $controlador = $this->controlador;
        
        $this->comprobarPaginacion();
        
        $this->Esquema->comprobarSessionModeloYML($modelo);
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $camposModelo = $configModulo['general']['campos'];
     
        $fieldsForSearch = $this->Esquema->obtenerCamposABuscarEnIndex($this,$configModulo,$camposModelo,$modelo);
        $this->comprobarFiltroActivo();
        $this->comprobarBusquedaActiva($fieldsForSearch);
        
        $this->paginate["limit"] = $configModulo["general"]["config"]["registrosPagina"];
        $this->paginate["order"] = array("$modelo.id" => "DESC");
        
        $configModulo["general"]["botones"]["btconfigurar"]=$this->Session->read("CofigurarModulos");
        
        $registros=$this->paginate();
        $this->set(compact("registros","modelo","controlador","configModulo"));
    }

    public function auditar($obj,$accion=null) {
        $modelo  = $obj->modelo;
        $usuario = $obj->Session->read('Auth.User.username');                                                                             
        $modulo  = $obj->name;                                                               

        if ($accion==null){
           $accion=$obj->request->params[action]; 
        }

        if($obj->$modelo->id > 0){
            $parametros = $obj->$modelo->id; 
        }else{
            $parametros = Spyc::YAMLDump($obj->paginate);
            $parametros = iconv($this->inCharset, $this->outCharset, $parametros);
        }

        $request_data = array( Auditoria => array(
                        'id'         => '',
                        'usuario'    => $usuario,
                        'modulo'     => $modulo,
                        'accion'     => $accion,                                                                                                                
                        'parametros' => $parametros
                    )
        );
        $this->Auditoria->save($request_data);
    }
  
    public function  historico($obj,$accion=null){
        $usuario = $obj->Session->read('Auth.User.username');       
        $modelo  = $obj->modelo;
        $modulo  = $obj->name;                                                               
        $accion  = $obj->request->params[action];
        
        if ($accion==null){
            $accion=$obj->request->params[action]; 
        }
        
        $array_datos_antiguos = $obj->oldData[$modelo];
        $datos_antiguos       = Spyc::YAMLDump($array_datos_antiguos);
        $datos_antiguos       = str_replace("'", " ", $datos_antiguos);

        $request_data = array( Historico => array(
            'id'            => '',
            'usuario'       => $usuario,
            'modulo'        => $modulo,
            'accion'        => $accion,                                                                                                                
            'datos_antiguo' => $datos_antiguos
        ));
        App::import('Model','Historico');
        $this->Historico = new Historico;
        $this->Historico->save($request_data);
    }

    public function edit($id = null) {
        $modelo       = $this->modelo;
        $controlador  = $this->controlador;
        $accion       = "Edit";  
        $schema       = $this->$modelo->schema(true);
        $propiedades  = $this->Esquema->propiedadesModelo($schema);  
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        $configModulo = $this->Esquema->identificarCamposObligatorios($configModulo,$propiedades,"edit");
        $this->Esquema->obtenerDatosModelosAjenos($this);
        $this->request->data = $this->$modelo->read(null, $id);
        $this->set(compact("modelo","propiedades","controlador","accion","configModulo"));
    }
    
    public function update($id = null){
        $modelo            = $this->modelo;
        $controlador       = $this->controlador;
        $this->$modelo->id = $id;
        
        $configModulo = $this->Esquema->leerConfigSession($modelo);
        
        if($this->isAuditoriaActiva($configModulo)) $this->auditoria->auditar($this);      
        if ($this->$modelo->save($this->request->data)) {
            $this->Session->setFlash(MSG_EDITSUCCESS, SUCCESS);
            if($this->isHistoricoActivo($configModulo)){
                $this->auditoria->historico($this);                    
            }
        } else {
            $this->Session->setFlash(MSG_EDITERROR, ERROR);
        }    
        if($this->Session->check("page[$this->modelo]")==TRUE){    
            $this->redirectSidera($controlador,"index/page:".$this->Session->read("page[$this->modelo]"));   
        }else{
            $this->redirectSidera($controlador,"index");        
        }  
    }
		
    public function delete($id = null) {
        $this->autoRender = false;
        $modelo           = $this->modelo;
        $controlador      = $this->controlador;
        $datos            = $this->request->data["id"];
        $exploded_id      = explode(",", $datos);
        
        foreach ($exploded_id as $id) {
            $this->$modelo->id = $id;
            if (!$this->$modelo->exists()) {
                $this->Session->setFlash(MSG_DELETENOTICE, NOTICE);
            }
            $this->$modelo->delete(); 
        }
        $this->Session->setFlash(MSG_DELETESUCCESS, SUCCESS);  
        $this->redirectSidera($controlador,"index");        
    }

    public function getGeneralStats(){
        $this->set('auditorias',$this->Auditoria->getGeneralStats());
    }
}