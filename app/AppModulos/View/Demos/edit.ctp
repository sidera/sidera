<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a href="#"><i class="fa fa-cog fa-lg  "></i></a></li>
            <?php echo $this->element("ElementsVista".DS."Edit".DS."botonGuardarEdit"); ?>
            <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
        </ul>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6">
            <i id="iconoModulo" class="fa fa-edit fa-lg"> </i> 
            <span id="etiquetaModulo"> Modificar registro Demos</span> 
        </div><!--columnaCabecera-->
    </div><!--cabeceraModulo--> 
    
    <div id="cuerpoModulo" class="container-fluid">
        <form action="/sidera/demos/update/<?=$registro["Demo"]["id"]?>" id="DemoEditForm" method="post" accept-charset="iso-8859-1">
            <div id="fila1" class="row-fluid">
                <div id="columna1" class="col-md-6 padded">

                    <div id="nombreDiv" class="form-group">
                        <label>Nombre</label>
                        <input id="nombre" type="text" name="data[Demo][nombre]" class="form-control" value="<?=$registro["Demo"]["nombre"]?>" maxlength="20"  required/>
                    </div><!--nombreDiv-->
            
                    <div id="simpletextDiv" class="form-group">
                        <label>Simpletext</label>
                        <input id="simpletext" type="text" name="data[Demo][simpletext]" class="form-control" value="<?=$registro["Demo"]["simpletext"]?>" maxlength="10"  />
                    </div><!--simpletextDiv-->
            
                    <div id="fechaDiv" class="form-group">
                        <label>Fecha</label>
                        <div class="input-group date" id="fecha">
                            <input type="text" class="form-control" name="data[Demo][fecha]" value="<?=$this->Time->format("d/m/Y",$registro["Demo"]["fecha"])?>" required/>
                            <i class="input-group-addon fa fa-calendar fa-lg"></i>
                        </div>
                    </div><!--fechaDiv-->
            
                    <div id="numeroDiv" class="form-group">
                        <label>Numero</label>
                        <input id="numero" type="text" name="data[Demo][numero]" class="form-control" value="<?=$registro["Demo"]["numero"]?>" maxlength="11" pattern="<?=VALID_NUMERICO?>" title="s�lo n�meros enteros" />
                    </div><!--numeroDiv-->
            
                    <div id="decimalDiv" class="form-group">
                        <label>Decimal</label>
                        <input id="decimal" type="text" name="data[Demo][decimal]" class="form-control" value="<?=$registro["Demo"]["decimal"]?>" maxlength="11" pattern="<?=VALID_DECIMAL?>" title="s�lo n�meros enteros o con 2 decimales" required/>
                    </div><!--decimalDiv-->
            
                    <div id="booleanoDiv" class="form-group">
                        <label>Booleano</label>
                        <input type="hidden" name="data[Demo][booleano]" value="<?=$registro["Demo"]["booleano"]?>" onclick=" $(this).val()==0 ? $(this).val(1) : $(this).val(0) "/>
                        <i class="form-control fa fa-lg fa-check-square-o <?php if (!$registro["Demo"]["booleano"]) echo "fa-square-o"?>" onclick="$(this).toggleClass('fa-square-o').siblings().trigger('click');"></i>
                    </div><!--booleanoDiv-->
            
                    <div id="textoDiv" class="form-group">
                        <label>Texto</label>
                        <input id="texto" type="text" name="data[Demo][texto]" class="form-control" value="<?=$registro["Demo"]["texto"]?>" maxlength="1"  />
                    </div><!--textoDiv-->
            
                    <div id="grupo_idDiv" class="form-group">
                        <label>Grupos</label>
                        <select  id="grupo_id" name="data[Demo][grupo_id]" class="selectpicker" data-live-search="true" required>
                            <option value="" selected>Seleccione una opci�n...</option>
                        <?php foreach ($grupos as $clave => $valor) {
                            if ($clave == $registro["Demo"]["grupo_id"]) $selected="selected"; else $selected="";
                            echo "<option value='".$clave."' ".$selected.">".$valor."</option>";
                        }?>
                        </select>
                    </div><!--grupo_idDiv-->
            
                    <div id="usuario_idDiv" class="form-group">
                        <label>Usuarios</label>
                        <select  id="usuario_id" name="data[Demo][usuario_id]" class="selectpicker" data-live-search="true" >
                            <option value="" selected>Seleccione una opci�n...</option>
                        <?php foreach ($usuarios as $clave => $valor) {
                            if ($clave == $registro["Demo"]["usuario_id"]) $selected="selected"; else $selected="";
                            echo "<option value='".$clave."' ".$selected.">".$valor."</option>";
                        }?>
                        </select>
                    </div><!--usuario_idDiv-->
            
                    <div id="menu_idDiv" class="form-group">
                        <label>Menus</label>
                        <select  id="menu_id" name="data[Demo][menu_id]" class="selectpicker" data-live-search="true" >
                            <option value="" selected>Seleccione una opci�n...</option>
                        <?php foreach ($menus as $clave => $valor) {
                            if ($clave == $registro["Demo"]["menu_id"]) $selected="selected"; else $selected="";
                            echo "<option value='".$clave."' ".$selected.">".$valor."</option>";
                        }?>
                        </select>
                    </div><!--menu_idDiv-->
            
                    <div id="pruebaDiv" class="form-group">
                        <label>Prueba</label>
                        <input id="prueba" type="text" name="data[Demo][prueba]" class="form-control" value="<?=$registro["Demo"]["prueba"]?>" maxlength="2" pattern="<?=VALID_NUMERICO?>" title="s�lo n�meros enteros" />
                    </div><!--pruebaDiv-->
            
                    <div id="datetimeDiv" class="form-group">
                        <label>Datetime</label>
                        <div class="input-group datetime" id="datetime">
                            <input type="text" class="form-control" name="data[Demo][datetime]" value="<?=$this->Time->format("d/m/Y H:i:s",$registro["Demo"]["datetime"])?>" />
                            <i class="input-group-addon fa fa-calendar fa-lg"></i>
                        </div>
                    </div><!--datetimeDiv-->
            
                    <!--nuevosCampos-->
                </div><!--columna1-->
            </div><!--fila1-->  
        </form>
    </div><!--cuerpoModulo-->
    
    <div id="pieModulo" class="row-fluid hide">
        <div id="columnaPie" class="col-md-12 ">
        </div><!--columnaPie-->
    </div><!--pieModulo-->  
</div><!--modulo-->
<script type="text/javascript">
$(document).ready(function() {
    <?php echo $this->element("ElementsVista".DS."Edit".DS."editJs"); ?>
       
    //<!--espacioFuturosControles-->
}); 
</script>