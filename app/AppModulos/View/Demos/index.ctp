<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a href="#"><i class="fa fa-cog fa-lg  "></i></a></li>
            <li><a onclick='$("#cuerpoApp").load("demos/add")' href="#">Nuevo</a></li>
            <?=$this->element("ElementsVista".DS."Index".DS."botonEliminar")?>
            <li><a onclick='$("#cuerpoApp").load("demos/exportarMostrar")' href="#">Exportar</a></li>
            <?=$this->element("ElementsVista".DS."Index".DS."accionEditar")?>
        </ul>
    </div>  
    <div id="columnaBusquedaBarraAccion" class="pull-right col-md-3 col-xs-8 ">
       <?=$this->element("ElementsVista".DS."Index".DS."searchForm",array("model"=>"demos"))?></li>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6 col-xs-6">
            <i id="iconoModulo" class=" fa fa-align-justify fa-lg"></i> 
            <span id="etiquetaModulo">Demos</span> 
        </div><!--columnaCabecera-->
        <div id="columnaPaginacion" class="col-md-6">
            <?=$this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>"Demo"))?>
        </div><!--columnaPaginacion-->
    </div><!--cabeceraModulo--> 
 
    <div id="cuerpoModulo" class="fila row-fluid">  
        <div  class="columna dataTables_wrapper" role="grid" id="DataTables_Table_0_wrapper">
            <table class="elemento table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="check"><i class="fa fa-check-square fa-lg"></i></th>
                        <th data-header="Demo.id" class="orderable"><?=$this->Paginator->sort("id","Id")?></th>
                        <th data-header="Demo.nombre" class="orderable"><?=$this->Paginator->sort("nombre","Nombre")?></th>
                        <th data-header="Demo.simpletext" class="orderable"><?=$this->Paginator->sort("simpletext","Simpletext")?></th>
                        <th data-header="Demo.fecha" class="orderable"><?=$this->Paginator->sort("fecha","Fecha")?></th>
                        <th data-header="Demo.numero" class="orderable"><?=$this->Paginator->sort("numero","Numero")?></th>
                        <th data-header="Demo.decimal" class="orderable"><?=$this->Paginator->sort("decimal","Decimal")?></th>
                        <th data-header="Demo.booleano" class="orderable"><?=$this->Paginator->sort("booleano","Booleano")?></th>
                        <th data-header="Demo.texto" class="orderable"><?=$this->Paginator->sort("texto","Texto")?></th>
                        <th data-header="Demo.grupo_id" class="orderable"><?=$this->Paginator->sort("grupo_id","Grupos")?></th>
                        <th data-header="Demo.usuario_id" class="orderable"><?=$this->Paginator->sort("usuario_id","Usuarios")?></th>
                        <th data-header="Demo.menu_id" class="orderable"><?=$this->Paginator->sort("menu_id","Menus")?></th>
                        <th data-header="Demo.prueba" class="orderable"><?=$this->Paginator->sort("prueba","Prueba")?></th>
                        <th data-header="Demo.datetime" class="orderable"><?=$this->Paginator->sort("datetime","Datetime")?></th>
                        <!--nuevosCamposCabecera-->
                    </tr>
                </thead>
                <tbody class="griddata" id="DemoGridData">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?php echo $registro["Demo"]["id"];?>" class="gradeA" title="Haga clic para editar el registro"> 
                        <td><i value="<?=$registro["Demo"]["id"]?>" class="checkCell fa fa-lg fa-square-o"></i></td>
                                    <td class="id"><?=number_format($registro["Demo"]["id"], 0, ",", ".")?></td>
            <td class="nombre"><?=$registro["Demo"]["nombre"]?></td>
            <td class="simpletext"><?=$registro["Demo"]["simpletext"]?></td>
            <td class="fecha"><?=$this->Time->format("d/m/Y",$registro["Demo"]["fecha"])?></td>
            <td class="numero"><?=number_format($registro["Demo"]["numero"], 0, ",", ".")?></td>
            <td class="decimal"><?=number_format($registro["Demo"]["decimal"], 2, ",", ".")?></td>
            <td class="booleano"><i class="fa <?=($registro["Demo"]["booleano"]) ? "fa-check-square-o" : "fa-square-o" ?>"></td>
            <td class="texto"><?=$registro["Demo"]["texto"]?></td>
            <td class="grupo_id"><?=$registro["Grupo"][$GrupoDisplayField]?></td>
            <td class="usuario_id"><?=$registro["Usuario"][$UsuarioDisplayField]?></td>
            <td class="menu_id"><?=$registro["Menu"][$MenuDisplayField]?></td>
            <td class="prueba"><?=number_format($registro["Demo"]["prueba"], 0, ",", ".")?></td>
            <td class="datetime"><?=$this->Time->format("d/m/Y H:i:s",$registro["Demo"]["datetime"])?></td>
            <!--nuevosCamposCuerpo-->
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div id="pieModulo" class="row-fluid">
        <div id="columnaPie" class="col-md-8 ">
        </div><!--columnaPie-->
        
        <div id="columnaPaginacionPie" class="col-md-4 pull-right">
            <?=$this->element("ElementsVista".DS."Index".DS."registrosPorPagina")?>
        </div><!--columnaPaginacionPie-->     
    </div><!--pieModulo--> 
    
</div><!--modulo--> 
<script type="text/javascript" id="myScript">
$(document).ready(function() {
    <?=$this->element("ElementsVista".DS."Index".DS."indexJs")?>
});
</script>