<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    */
?>	

 <div id="parametros" class="container-fluid span6">
    <div style="position:relative;" class="box bordered">
         <div class="box-header">
                <i class="fa fa-file"></i> Parámetros
         </div>
        <div class="row-fluid">
            <div class="span6 padded">
                <!-- Los parametros deben llevar la clase "param", y el id debe coincidir con el que le llega al informe.-->
                <?php echo $this->Form->input("nombre",array(
						"before"=>"<label>"."Parámetro"."</label>",
						"id"=>"parametro1",
						"label"=>false,
                                                "name"=>"parametro1",
			            "class"=>"input-xlarge param",
			            "placeholder"=>"Parámetro de ejemplo ..."
			            ));?>
                 <?php echo $this->Form->input("nombre",array(
						"before"=>"<label>"."Parámetro"."</label>",
						"id"=>"parametro2",
                                                "type"=>"textarea",
						"label"=>false,
                                                "name"=>"parametro2",
			            "class"=>"input-xlarge param",
			            "placeholder"=>"Parámetro de ejemplo ..."
			            ));?>
                
              
            </div>    
                
        </div>
    </div>
</div>