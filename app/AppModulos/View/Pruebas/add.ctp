<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a href="#"><i class="fa fa-cog fa-lg  "></i></a></li>
            <?php echo $this->element("ElementsVista".DS."Add".DS."botonGuardarAdd"); ?>
            <?php echo $this->element("ElementsVista".DS."botonCancelar"); ?>
        </ul>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6">
            <i id="iconoModulo" class="fa fa-pencil fa-lg"> </i> 
            <span id="etiquetaModulo"> Nuevo registro Pruebas</span> 
        </div><!--columnaCabecera-->
    </div><!--cabeceraModulo--> 
    
    <div id="cuerpoModulo" class="container-fluid">
        <form action="/sidera/pruebas/create/" id="PruebaAddForm" method="post" accept-charset="iso-8859-1">
            <div id="fila1" class="row-fluid">
                <div id="columna1" class="col-md-6 padded">

                    <div id="nombreDiv" class="form-group">
                        <label>Nombre</label>
                        <input id="nombre" type="text" name="data[Prueba][nombre]" class="form-control" maxlength="256"  required/>
                    </div><!--nombreDiv-->
            
                    <div id="descripcionDiv" class="form-group">
                        <label>Descripcion</label>
                        <input id="descripcion" type="text" name="data[Prueba][descripcion]" class="form-control" maxlength="256"  required/>
                    </div><!--descripcionDiv-->
            
                    <!--nuevosCampos-->
                </div><!--columna1-->
            </div><!--fila1-->  
        </form>
    </div><!--cuerpoModulo-->
    
    <div id="pieModulo" class="row-fluid hide">
        <div id="columnaPie" class="col-md-12 ">
        </div><!--columnaPie-->
    </div><!--pieModulo--> 
</div><!--modulo-->
<script type="text/javascript">
$(document).ready(function() {
    <?php echo $this->element("ElementsVista".DS."Add".DS."addJs"); ?>
       
    //<!--espacioFuturosControles-->
}); 
</script>