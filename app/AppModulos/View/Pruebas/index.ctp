<div id="barraAccion" class="row-fluid navbar  navbar-default navbar-fixed-top">
    <?=$this->element("ElementsVista".DS."menuResponsiveBarraAccion")?>
    <div id="columnaBotonesUsuario" class="navbar-left col-md-2 collapse navbar-collapse">
        <ul class="nav navbar-nav" >
            <li><a href="#"><i class="fa fa-cog fa-lg  "></i></a></li>
            <li><a onclick='$("#cuerpoApp").load("pruebas/add")' href="#">Nuevo</a></li>
            <?=$this->element("ElementsVista".DS."Index".DS."botonEliminar")?>
            <li><a onclick='$("#cuerpoApp").load("pruebas/exportarMostrar")' href="#">Exportar</a></li>
            <?=$this->element("ElementsVista".DS."Index".DS."accionEditar")?>
        </ul>
    </div>  
    <div id="columnaBusquedaBarraAccion" class="pull-right col-md-3 col-xs-8 ">
       <?=$this->element("ElementsVista".DS."Index".DS."searchForm",array("model"=>"pruebas"))?></li>
    </div>  
</div><!--barraAccion--> 
 
<?= $this->Element("ElementsVista".DS."flashMessages"); ?>

<div id="modulo" class="container-fluid form">
    <div id="cabeceraModulo" class="row-fluid">
        <div id="columnaCabecera" class="col-md-6 col-xs-6">
            <i id="iconoModulo" class=" fa fa-align-justify fa-lg"></i> 
            <span id="etiquetaModulo">Pruebas</span> 
        </div><!--columnaCabecera-->
        <div id="columnaPaginacion" class="col-md-6">
            <?=$this->element("ElementsVista".DS."Index".DS."pagination",array("model"=>"Prueba"))?>
        </div><!--columnaPaginacion-->
    </div><!--cabeceraModulo--> 
 
    <div id="cuerpoModulo" class="fila row-fluid">  
        <div  class="columna dataTables_wrapper" role="grid" id="DataTables_Table_0_wrapper">
            <table class="elemento table table-striped dataTable ">
                <thead>
                    <tr role="row" title="Haga clic para ordenar">
                        <th class="check"><i class="fa fa-check-square fa-lg"></i></th>
                        <th data-header="Prueba.id" class="orderable"><?=$this->Paginator->sort("id","Id")?></th>
                        <th data-header="Prueba.nombre" class="orderable"><?=$this->Paginator->sort("nombre","Nombre")?></th>
                        <th data-header="Prueba.descripcion" class="orderable"><?=$this->Paginator->sort("descripcion","Descripcion")?></th>
                        <!--nuevosCamposCabecera-->
                    </tr>
                </thead>
                <tbody class="griddata" id="PruebaGridData">
                    <?php foreach ($registros as $registro): ?>
                    <tr itemid="<?php echo $registro["Prueba"]["id"];?>" class="gradeA" title="Haga clic para editar el registro"> 
                        <td><i value="<?=$registro["Prueba"]["id"]?>" class="checkCell fa fa-lg fa-square-o"></i></td>
                                    <td class="id"><?=number_format($registro["Prueba"]["id"], 0, ",", ".")?></td>
            <td class="nombre"><?=$registro["Prueba"]["nombre"]?></td>
            <td class="descripcion"><?=$registro["Prueba"]["descripcion"]?></td>
            <!--nuevosCamposCuerpo-->
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div id="pieModulo" class="row-fluid">
        <div id="columnaPie" class="col-md-8 ">
        </div><!--columnaPie-->
        
        <div id="columnaPaginacionPie" class="col-md-4 pull-right">
            <?=$this->element("ElementsVista".DS."Index".DS."registrosPorPagina")?>
        </div><!--columnaPaginacionPie-->     
    </div><!--pieModulo--> 
    
</div><!--modulo--> 
<script type="text/javascript" id="myScript">
$(document).ready(function() {
    <?=$this->element("ElementsVista".DS."Index".DS."indexJs")?>
});
</script>