<?php
App::uses("AppController", "Controller");

class DemosController extends AppController {

    var    $layout      = "ajax";
    public $modelo      = "Demo";
    public $controlador = "demos";

    public function beforeFilter(){
        parent::beforeFilter();
        $this->modelo        = "Demo";
        $this->controlador   = "demos";
        $this->modelosAjenos = array("Grupo","Usuario","Menu");
        $this->modelosAjenosRelacionados = array();    
    }
    
    public function beforeRender(){
        parent::beforeRender();
        $this->set("modelo",$this->modelo);
        $this->set("controlador",$this->controlador);
    }

    public function index() {
        $this->obtenerDisplayfields();
        $this->paginate["conditions"] = $this->comprobarFiltroActivo();
        $this->paginate["conditions"] = $this->comprobarBusquedaActiva();
        $this->paginate["limit"] = $this->comprobarPaginacion();
        $this->paginate["order"] = array("Demo.id" => "DESC");
        //$this->auditoria->auditar($this);
        $registros = $this->paginate();
        $this->set(compact("registros"));
    }

    public function add() {
        $this->obtenerDatosModelosAjenos();
        $this->checkModal();
        $this->set(compact("Add"));
    }
    
    public function create(){
        $this->Demo->create();
        if ($this->Demo->save($this->request->data)) {
            $this->Session->setFlash(MSG_SAVEDSUCCESS, SUCCESS);
            //$this->auditoria->auditar($this);
        } else {
            $this->Session->setFlash(MSG_SAVEDERROR, ERROR);
        }
        if($this->request->data["formType"]== "modal") {
            $this->autoRender = false;
        } else {
            $this->redirectSidera("demos", "index");
        }    
    }

    public function edit($id = null) {
        $registro = $this->Demo->read(null, $id);
        $this->obtenerDatosModelosAjenos();
        $this->Esquema->obtenerDatosModelosAjenosRelacionados($id,$this);
        $this->checkModal();
        $this->set(compact("Edit","registro"));
    }
    
    public function update($id = null){
        $this->Demo->id = $id;
        //$this->oldData = $this->Demo->read();
        if ($this->Demo->save($this->request->data)) {
            $this->Session->setFlash(MSG_EDITSUCCESS, SUCCESS);
            //$this->auditoria->historico($this);
            //$this->auditoria->auditar($this);
        }else{
            $this->Session->setFlash(MSG_EDITERROR, ERROR);
        }
            
        if ($this->request->data["formType"] == "modal") {
            $this->autoRender = false;
        } else {
            if ($this->Session->check("page[Demo]") == TRUE) {
                $this->redirectSidera("demos", "index/page:" . $this->Session->read("page[Demo]"));
            } else {
                $this->redirectSidera("demos", "index");
            }
        } 
    }

    public function delete($id = null) {
        $this->autoRender = false;
        $datos            = $this->request->data["id"];
        $exploded_id      = explode(",", $datos);
        foreach ($exploded_id as $id) {
            $this->Demo->id=$id;
            //$this->oldData = $this->Demo->read();
            if (!$this->Demo->exists()) {
                $this->Session->setFlash(MSG_DELETENOTICE, NOTICE);
            }
            //$this->auditoria->historico($this);
            //$this->auditoria->auditar($this);
            $this->Demo->delete(); 
        }
        $this->Session->setFlash(MSG_DELETESUCCESS, SUCCESS);  
        if ($this->request->data["formType"] != "modal") {
            $this->redirectSidera("demos", "index");
        }           
    }

}