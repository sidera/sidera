<?php
App::uses("AppModel", "Model");

class Prueba extends AppModel {
    
    var $useDbConfig     = "default";
    public $displayField = "id";

 
    //Relaciones tipo : Un Prueba tiene muchos/muchas "Modelos"	
    var $hasMany = array(
    	
		"Demos_prueba" => array(
			"className"    => "Demos_prueba",
			"foreignKey"   => "prueba_id",
			"dependent"    => false,
			"conditions"   => "",
			"fields"       => "",
			"order"        => "",
			"limit"        => "",
			"offset"       => "",
			"exclusive"    => "",
			"finderQuery"  => "",
			"counterQuery" => ""
		),
        
	);    
 	function beforeSave($options = null) {
 	  parent::beforeSave($options);
    }
}