<?php
App::uses("AppModel", "Model");

class Demo extends AppModel {
    
    var $useDbConfig     = "default";
    public $displayField = "id";

 
    //Relaciones tipo : Un Demo pertenece a un/una "Modelo"	
    public $belongsTo = array(
    	
		"Grupo" => array(
			"className"  => "Grupo",
			"foreignKey" => "grupo_id",
			"conditions" => "",
			"fields"     => "",
			"order"      => ""
		),
        
		"Usuario" => array(
			"className"  => "Usuario",
			"foreignKey" => "usuario_id",
			"conditions" => "",
			"fields"     => "",
			"order"      => ""
		),
        
		"Menu" => array(
			"className"  => "Menu",
			"foreignKey" => "menu_id",
			"conditions" => "",
			"fields"     => "",
			"order"      => ""
		),
        
	);
	 
    //Relaciones tipo : Un Demo tiene muchos/muchas "Modelos"	
    var $hasMany = array(
    	
		"Demos_prueba" => array(
			"className"    => "Demos_prueba",
			"foreignKey"   => "demo_id",
			"dependent"    => false,
			"conditions"   => "",
			"fields"       => "",
			"order"        => "",
			"limit"        => "",
			"offset"       => "",
			"exclusive"    => "",
			"finderQuery"  => "",
			"counterQuery" => ""
		),
        
	);    
 	function beforeSave($options = null) {
 	  parent::beforeSave($options);
    }
}