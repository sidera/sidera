<?php    
//(cofiguracion manual)Conexion DEFAULT de la aplicacion
define("DEFAULT_DATABASE",     "sidera");
define("DEFAULT_HOST",         "127.0.0.1");
define("DEFAULT_LOGIN",        "root");
define("DEFAULT_PASSWORD",     "");
define("DEFAULT_DATASOURCE",   "Database/Mysql");

define("DEFAULT_PREFIX",       "");
define("DEFAULT_PERSISTENT",   false);

//(cofiguracion dinamica)Parametros de la aplicacion
define("APLICACION", "SIDERA 2.5"); 
define("VERSION_MIAPP", "1.0");
define("ORGANISMO", "D.Gral. de Administración Electrónica y T.I."); 
define("CONSEJERIA", "Consejería de Hacienda y Administración Pública");
define("DEBUG", "0");
define("MODO_MANTENIMIENTO", "0");
define("SUBNAVBAR","0");
define("AUDITORIA_ACCESO", "0");
define("REPORT_UNIT", "RiesgosLaborales2");
define("REPORT_SERVER", "");
define("DEFAULT_LOGIN_JASPER","jasperadmin");
define("DEFAULT_PASSWORD_JASPER","jasper");
define("MOSTRAR_MENUSSISTEMA", "0");

//(cofiguracion dinamica)Codificacion de caracteres
define("CODIFICACION_BD", "ISO-8859-1");
define("CODIFICACION_FILES", "UTF-8");
define("INICIO", "pruebas");

//(cofiguracion dinamica)Configuracion redireccion
define("PROTOCOLO","http");             // "https" para preproduccion y produccion
define("RUTA_PROYECTO",   "sidera"); // "00/miapp" para desarrollo, preproduccion y produccion 

//(configuracion dinamica)Correo Electronico
define("EMAIL_HOST","legacy.gobex.es");
define("EMAIL_PORT",25);
define("EMAIL_USER","sidera");
define("EMAIL_PASSWORD","sidera");
define("EMAIL_TRANSPORT","smtp");
define("EMAIL_FROM","sidera@gobex.es");
define("EMAIL_FORMAT","texto");
define("EMAIL_TLS",0);

require_once "appModulosConstants.php";
