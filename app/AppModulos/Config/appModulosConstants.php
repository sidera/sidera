<?php

//Patrones de Validacion en HTML5
define("VALID_ALFABETICO",'[a-zA-Z]+');
define("VALID_NUMERICO",'[0-9]+');
define("VALID_ALFANUMERICO",'[a-zA-Z0-9]+');
define("VALID_DECIMAL",'[0-9]{0,8}(\.[0-9]{1,2})?');//decimales(10,2)
define("VALID_DATE",'pattern="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"');
define("VALID_DATETIME",'pattern="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"');
define("VALID_TIME",'pattern="(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"');