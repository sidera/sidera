USE [SIDERASQL]
GO
ALTER TABLE [dbo].[acos] DROP CONSTRAINT [DF__acos__parent_id__093F5D4E]
GO
ALTER TABLE [dbo].[acos] DROP CONSTRAINT [DF__acos__model__0A338187]
GO
ALTER TABLE [dbo].[acos] DROP CONSTRAINT [DF__acos__foreign_ke__0B27A5C0]
GO
ALTER TABLE [dbo].[acos] DROP CONSTRAINT [DF__acos__alias__0C1BC9F9]
GO
ALTER TABLE [dbo].[acos] DROP CONSTRAINT [DF__acos__lft__0D0FEE32]
GO
ALTER TABLE [dbo].[acos] DROP CONSTRAINT [DF__acos__rght__0E04126B]
GO
ALTER TABLE [dbo].[aros] DROP CONSTRAINT [DF__aros__parent_id__12C8C788]
GO
ALTER TABLE [dbo].[aros] DROP CONSTRAINT [DF__aros__model__13BCEBC1]
GO
ALTER TABLE [dbo].[aros] DROP CONSTRAINT [DF__aros__foreign_ke__14B10FFA]
GO
ALTER TABLE [dbo].[aros] DROP CONSTRAINT [DF__aros__alias__15A53433]
GO
ALTER TABLE [dbo].[aros] DROP CONSTRAINT [DF__aros__lft__1699586C]
GO
ALTER TABLE [dbo].[aros] DROP CONSTRAINT [DF__aros__rght__178D7CA5]
GO
ALTER TABLE [dbo].[aros_acos] DROP CONSTRAINT [DF__aros_acos__creat__1D4655FB]
GO
ALTER TABLE [dbo].[aros_acos] DROP CONSTRAINT [DF__aros_acos__read___1E3A7A34]
GO
ALTER TABLE [dbo].[aros_acos] DROP CONSTRAINT [DF__aros_acos__updat__1F2E9E6D]
GO
ALTER TABLE [dbo].[aros_acos] DROP CONSTRAINT [DF__aros_acos__delet__2022C2A6]
GO
ALTER TABLE [dbo].[auditorias] DROP CONSTRAINT [DF__auditoria__usuar__24E777C3]
GO
ALTER TABLE [dbo].[auditorias] DROP CONSTRAINT [DF__auditoria__modul__25DB9BFC]
GO
ALTER TABLE [dbo].[auditorias] DROP CONSTRAINT [DF__auditoria__accio__26CFC035]
GO
ALTER TABLE [dbo].[eventos] DROP CONSTRAINT [DF__eventos__created__3335971A]
GO
ALTER TABLE [dbo].[eventos] DROP CONSTRAINT [DF__eventos__modifie__3429BB53]
GO
ALTER TABLE [dbo].[filtros] DROP CONSTRAINT [DF__filtros__usuario__38EE7070]
GO
ALTER TABLE [dbo].[filtros] DROP CONSTRAINT [DF__filtros__created__39E294A9]
GO
ALTER TABLE [dbo].[filtros] DROP CONSTRAINT [DF__filtros__modifie__3AD6B8E2]
GO
ALTER TABLE [dbo].[grupos] DROP CONSTRAINT [DF__grupos__descripc__3F9B6DFF]
GO
ALTER TABLE [dbo].[grupos] DROP CONSTRAINT [DF__grupos__created__408F9238]
GO
ALTER TABLE [dbo].[grupos] DROP CONSTRAINT [DF__grupos__modified__4183B671]
GO
ALTER TABLE [dbo].[historicos] DROP CONSTRAINT [DF__historico__usuar__46486B8E]
GO
ALTER TABLE [dbo].[historicos] DROP CONSTRAINT [DF__historico__modul__473C8FC7]
GO
ALTER TABLE [dbo].[historicos] DROP CONSTRAINT [DF__historico__accio__4830B400]
GO
ALTER TABLE [dbo].[informes] DROP CONSTRAINT [DF__informes__inform__4CF5691D]
GO
ALTER TABLE [dbo].[informes] DROP CONSTRAINT [DF__informes__constr__4DE98D56]
GO
ALTER TABLE [dbo].[informes] DROP CONSTRAINT [DF__informes__grupo___4EDDB18F]
GO
ALTER TABLE [dbo].[mensajes] DROP CONSTRAINT [DF__mensajes__usuari__5772F790]
GO
ALTER TABLE [dbo].[menus] DROP CONSTRAINT [DF__menus__menu_id__5C37ACAD]
GO
ALTER TABLE [dbo].[menus] DROP CONSTRAINT [DF__menus__estado__5D2BD0E6]
GO
ALTER TABLE [dbo].[menus] DROP CONSTRAINT [DF__menus__controlad__5E1FF51F]
GO
ALTER TABLE [dbo].[menus] DROP CONSTRAINT [DF__menus__accion__5F141958]
GO
ALTER TABLE [dbo].[menus] DROP CONSTRAINT [DF__menus__orden__60083D91]
GO
ALTER TABLE [dbo].[menus] DROP CONSTRAINT [DF__menus__ruta__60FC61CA]
GO
ALTER TABLE [dbo].[menus] DROP CONSTRAINT [DF__menus__icono__61F08603]
GO
ALTER TABLE [dbo].[usuarios] DROP CONSTRAINT [DF__usuarios__nif__67A95F59]
GO
ALTER TABLE [dbo].[usuarios] DROP CONSTRAINT [DF__usuarios__direcc__689D8392]
GO
ALTER TABLE [dbo].[usuarios] DROP CONSTRAINT [DF__usuarios__unidad__6991A7CB]
GO
ALTER TABLE [dbo].[usuarios] DROP CONSTRAINT [DF__usuarios__puesto__6A85CC04]
GO
ALTER TABLE [dbo].[usuarios] DROP CONSTRAINT [DF__usuarios__email__6B79F03D]
GO
ALTER TABLE [dbo].[usuarios] DROP CONSTRAINT [DF__usuarios__create__6C6E1476]
GO
ALTER TABLE [dbo].[usuarios] DROP CONSTRAINT [DF__usuarios__modifi__6D6238AF]
GO
/****** Object:  Table [dbo].[accesos]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[accesos]
GO
/****** Object:  Table [dbo].[acos]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[acos]
GO
/****** Object:  Table [dbo].[aros]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[aros]
GO
/****** Object:  Table [dbo].[aros_acos]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[aros_acos]
GO
/****** Object:  Table [dbo].[auditorias]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[auditorias]
GO
/****** Object:  Table [dbo].[condiciones]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[condiciones]
GO
/****** Object:  Table [dbo].[eventos]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[eventos]
GO
/****** Object:  Table [dbo].[filtros]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[filtros]
GO
/****** Object:  Table [dbo].[grupos]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[grupos]
GO
/****** Object:  Table [dbo].[historicos]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[historicos]
GO
/****** Object:  Table [dbo].[informes]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[informes]
GO
/****** Object:  Table [dbo].[informestipos]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[informestipos]
GO
/****** Object:  Table [dbo].[mensajes]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[mensajes]
GO
/****** Object:  Table [dbo].[menus]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[menus]
GO
/****** Object:  Table [dbo].[permisostablas]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[permisostablas]
GO
/****** Object:  Table [dbo].[pruebas]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[pruebas]
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 11/25/2016 14:31:35 ******/
DROP TABLE [dbo].[usuarios]
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](255) NOT NULL,
	[password] [varchar](255) NOT NULL,
	[grupo_id] [int] NOT NULL,
	[nombre] [varchar](40) NOT NULL,
	[apellido1] [varchar](30) NOT NULL,
	[apellido2] [varchar](30) NOT NULL,
	[nif] [varchar](9) NULL,
	[direccion_general] [varchar](60) NULL,
	[unidad] [varchar](60) NULL,
	[puesto] [varchar](20) NULL,
	[email] [varchar](120) NULL,
	[created] [datetime] NULL,
	[modified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [username] UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[usuarios] ON
INSERT [dbo].[usuarios] ([id], [username], [password], [grupo_id], [nombre], [apellido1], [apellido2], [nif], [direccion_general], [unidad], [puesto], [email], [created], [modified]) VALUES (1, N'admin', N'd033e22ae348aeb5660fc2140aec35850c4da997', 1, N'Administrador', N'(Soporte Técnico)', N'', N'', N'', N'', N'', N'', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[usuarios] ([id], [username], [password], [grupo_id], [nombre], [apellido1], [apellido2], [nif], [direccion_general], [unidad], [puesto], [email], [created], [modified]) VALUES (2, N'user', N'12dea96fec20593566ab75692c9949596833adc9', 2, N'Usuario', N'Usuario', N'Usuario', N'', NULL, NULL, NULL, N'', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[usuarios] ([id], [username], [password], [grupo_id], [nombre], [apellido1], [apellido2], [nif], [direccion_general], [unidad], [puesto], [email], [created], [modified]) VALUES (3, N'coordinador', N'4c25569915bfcd3c7e2862542137a42db9ab38d1', 3, N'Coordinador', N'Coordinador', N'Coordinador', N'', NULL, NULL, NULL, N'', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[usuarios] OFF
/****** Object:  Table [dbo].[pruebas]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pruebas](
	[alvaroint] [int] NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[alvarotext] [text] NULL,
	[alvarovarchar] [varchar](25) NULL,
	[alvaronumeri] [numeric](18, 5) NULL,
	[alvarodecima] [decimal](10, 2) NULL,
	[alvarodate] [date] NULL,
	[alarodatitim] [datetime] NULL,
	[alvarotime] [time](7) NULL,
	[alvarobiuginteguer] [bigint] NULL,
	[alvarobit] [bit] NULL,
	[alvarotyni] [tinyint] NULL,
 CONSTRAINT [PK_pruebas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[permisostablas]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[permisostablas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[grupo_id] [int] NOT NULL,
	[tabla] [varchar](256) NOT NULL,
	[modified] [datetime] NOT NULL,
	[created] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[menus]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[menus](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [varchar](255) NOT NULL,
	[menu_id] [int] NOT NULL,
	[estado] [varchar](32) NOT NULL,
	[controlador] [varchar](255) NOT NULL,
	[accion] [varchar](255) NOT NULL,
	[orden] [int] NOT NULL,
	[ruta] [varchar](256) NOT NULL,
	[icono] [varchar](256) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[menus] ON
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (1, N'Sistema', 0, N'Activo', N'', N'', 0, N'>Sistema', N'icon-sitemap')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (2, N'Grupos', 6, N'Activo', N'grupos', N'index', 5, N'>Sistema>Administración>Grupos', N'icon-group')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (3, N'Usuarios', 6, N'Activo', N'usuarios', N'index', 6, N'>Sistema>Administración>Usuarios', N'icon-user ')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (4, N'Menus', 5, N'Activo', N'menus', N'index', 3, N'>Sistema>Configuración>Menus', N'icon-list-alt')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (5, N'Configuración', 1, N'Activo', N'', N'', 0, N'>Sistema>Configuración', N'icon-cog')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (6, N'Administración', 1, N'Activo', N'', N'', 1, N'>Sistema>Administración', N'icon-persona')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (7, N'Herramientas', 1, N'Activo', N'', N'', 2, N'>Sistema>Herramientas', N'icon-wrench ')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (8, N'Configuraciones', 5, N'Activo', N'configuraciones', N'edit/1', 0, N'>Sistema>Configuración>Configuraciones', N'icon-cogs')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (10, N'Constructor', 5, N'Activo', N'constructores', N'index', 2, N'>Sistema>Configuración>Constructor', N'icon-magic')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (11, N'Accesos', 7, N'Activo', N'accesos', N'index', 9, N'>Sistema>Herramientas>Accesos', N'icon-key')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (12, N'Permisos', 6, N'Activo', N'permisos', N'index', 4, N'>Sistema>Administración>Permisos', N'icon-lock')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (14, N'Históricos', 7, N'Activo', N'historicos', N'index', 0, N'>Sistema>Herramientas>Históricos', N'icon-geo')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (15, N'Consultas SQL', 7, N'Activo', N'consultas', N'index', 7, N'>Sistema>Herramientas>Consultas SQL', N'icon-bolt')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (16, N'Auditorias', 7, N'Activo', N'auditorias', N'index', 8, N'>Sistema>Herramientas>Auditorias', N' icon-epub')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (27, N'Permisos en tablas', 6, N'Activo', N'grupos', N'indexGruposPermisos', 3, N'>Sistema>Administración>Permisos en tablas', N'icon-lock')
INSERT [dbo].[menus] ([id], [titulo], [menu_id], [estado], [controlador], [accion], [orden], [ruta], [icono]) VALUES (28, N'Pruebas', 0, N'Activo', N'pruebas', N'index', 1, N'>Pruebas', N'NULL')
SET IDENTITY_INSERT [dbo].[menus] OFF
/****** Object:  Table [dbo].[mensajes]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mensajes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[origen] [int] NOT NULL,
	[destino] [int] NOT NULL,
	[asunto] [varchar](256) NOT NULL,
	[texto] [text] NULL,
	[estado] [tinyint] NOT NULL,
	[created] [datetime] NOT NULL,
	[propietario] [int] NOT NULL,
	[usuario] [int] NULL,
	[destinos_texto] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[mensajes] ON
INSERT [dbo].[mensajes] ([id], [origen], [destino], [asunto], [texto], [estado], [created], [propietario], [usuario], [destinos_texto]) VALUES (3, 1, 1, N'Bienvenid@s a SIDERA', N'', 0, CAST(0x0000000000000000 AS DateTime), 1, 0, N'Alvarez Pulido, Luis; ')
INSERT [dbo].[mensajes] ([id], [origen], [destino], [asunto], [texto], [estado], [created], [propietario], [usuario], [destinos_texto]) VALUES (5, 1, 2, N'vfgfg', N'', 0, CAST(0x0000000000000000 AS DateTime), 1, 0, N'Usuario Usuario. Usuario; ')
INSERT [dbo].[mensajes] ([id], [origen], [destino], [asunto], [texto], [estado], [created], [propietario], [usuario], [destinos_texto]) VALUES (6, 1, 2, N'vfgfg', N'', 0, CAST(0x0000000000000000 AS DateTime), 2, 0, N'Usuario Usuario. Usuario; ')
INSERT [dbo].[mensajes] ([id], [origen], [destino], [asunto], [texto], [estado], [created], [propietario], [usuario], [destinos_texto]) VALUES (7, 1, 2, N'dddd', N'', 0, CAST(0x0000000000000000 AS DateTime), 1, NULL, N'Usuario Usuario. Usuario; ')
INSERT [dbo].[mensajes] ([id], [origen], [destino], [asunto], [texto], [estado], [created], [propietario], [usuario], [destinos_texto]) VALUES (8, 1, 2, N'dddd', N'', 0, CAST(0x0000000000000000 AS DateTime), 2, NULL, N'Usuario Usuario. Usuario; ')
SET IDENTITY_INSERT [dbo].[mensajes] OFF
/****** Object:  Table [dbo].[informestipos]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[informestipos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[informestipos] ON
INSERT [dbo].[informestipos] ([id], [nombre]) VALUES (1, N'Listados')
INSERT [dbo].[informestipos] ([id], [nombre]) VALUES (2, N'Cartas')
INSERT [dbo].[informestipos] ([id], [nombre]) VALUES (3, N'Etiquetas')
SET IDENTITY_INSERT [dbo].[informestipos] OFF
/****** Object:  Table [dbo].[informes]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[informes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](250) NOT NULL,
	[report] [varchar](100) NOT NULL,
	[informestipo_id] [int] NULL,
	[constructor_id] [int] NULL,
	[grupo_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[informes] ON
INSERT [dbo].[informes] ([id], [nombre], [report], [informestipo_id], [constructor_id], [grupo_id]) VALUES (1, N'Listado de errores', N'listadoErrores', 1, 0, 0)
SET IDENTITY_INSERT [dbo].[informes] OFF
/****** Object:  Table [dbo].[historicos]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[historicos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [varchar](256) NULL,
	[modulo] [varchar](256) NULL,
	[accion] [varchar](11) NULL,
	[datos_antiguo] [varchar](max) NULL,
	[created] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[grupos]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[grupos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[descripcion] [varchar](250) NULL,
	[created] [datetime] NULL,
	[modified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[grupos] ON
INSERT [dbo].[grupos] ([id], [nombre], [descripcion], [created], [modified]) VALUES (1, N'Administradores', N'', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[grupos] ([id], [nombre], [descripcion], [created], [modified]) VALUES (2, N'Usuarios', N'', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[grupos] ([id], [nombre], [descripcion], [created], [modified]) VALUES (3, N'Coordinadores', N'', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[grupos] OFF
/****** Object:  Table [dbo].[filtros]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[filtros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modelo] [varchar](256) NOT NULL,
	[titulo] [varchar](256) NOT NULL,
	[tipo_condicion] [varchar](256) NOT NULL,
	[usuario_id] [int] NULL,
	[created] [datetime] NULL,
	[modified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[eventos]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[eventos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](255) NOT NULL,
	[description] [varchar](150) NOT NULL,
	[startevent] [datetime] NOT NULL,
	[endevent] [datetime] NOT NULL,
	[allday] [tinyint] NOT NULL,
	[created] [datetime] NULL,
	[modified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[condiciones]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[condiciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[campo] [varchar](256) NOT NULL,
	[comparador] [varchar](256) NOT NULL,
	[contenido] [varchar](256) NOT NULL,
	[filtro_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[auditorias]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[auditorias](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [varchar](256) NULL,
	[modulo] [varchar](256) NULL,
	[accion] [varchar](11) NULL,
	[parametros] [varchar](max) NULL,
	[created] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[aros_acos]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aros_acos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[aro_id] [int] NOT NULL,
	[aco_id] [int] NOT NULL,
	[create_] [varchar](2) NOT NULL,
	[read_] [varchar](2) NOT NULL,
	[update_] [varchar](2) NOT NULL,
	[delete_] [varchar](2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [ARO_ACO_KEY] UNIQUE NONCLUSTERED 
(
	[aro_id] ASC,
	[aco_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[aros_acos] ON
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (1, 1, 1, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (2, 3, 79, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (3, 3, 78, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (4, 3, 83, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (5, 3, 85, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (6, 3, 84, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (7, 3, 86, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (8, 3, 81, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (9, 3, 87, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (10, 3, 82, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (11, 3, 100, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (12, 3, 101, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (13, 3, 129, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (15, 3, 131, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (16, 3, 130, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (20, 3, 136, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (21, 3, 127, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (26, 3, 167, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (27, 3, 168, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (28, 3, 182, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (29, 3, 178, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (31, 3, 186, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (33, 3, 183, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (34, 3, 184, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (35, 3, 179, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (38, 3, 187, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (39, 3, 193, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (40, 3, 194, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (43, 3, 180, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (44, 3, 192, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (46, 3, 206, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (48, 3, 215, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (51, 3, 214, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (52, 3, 216, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (129, 5, 67, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (131, 5, 69, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (132, 5, 70, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (133, 5, 71, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (139, 5, 77, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (140, 5, 78, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (141, 5, 79, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (142, 5, 81, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (143, 5, 82, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (144, 5, 83, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (145, 5, 84, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (146, 5, 85, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (147, 5, 86, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (148, 5, 87, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (149, 5, 89, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (151, 5, 91, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (152, 5, 92, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (153, 5, 93, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (159, 5, 99, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (160, 5, 100, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (161, 5, 101, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (162, 5, 127, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (164, 5, 129, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (165, 5, 130, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (166, 5, 131, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (171, 5, 136, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (172, 5, 167, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (173, 5, 168, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (177, 5, 178, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (178, 5, 179, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (179, 5, 180, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (181, 5, 182, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (182, 5, 183, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (183, 5, 184, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (185, 5, 186, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (186, 5, 187, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (191, 5, 192, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (192, 5, 193, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (193, 5, 194, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (195, 5, 214, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (196, 5, 215, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (197, 5, 216, N'1', N'1', N'1', N'1')
INSERT [dbo].[aros_acos] ([id], [aro_id], [aco_id], [create_], [read_], [update_], [delete_]) VALUES (202, 5, 206, N'1', N'1', N'1', N'1')
SET IDENTITY_INSERT [dbo].[aros_acos] OFF
/****** Object:  Table [dbo].[aros]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parent_id] [int] NULL,
	[model] [varchar](255) NULL,
	[foreign_key] [int] NULL,
	[alias] [varchar](255) NULL,
	[lft] [int] NULL,
	[rght] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[aros] ON
INSERT [dbo].[aros] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1, NULL, N'Grupo', 1, NULL, 1, 2)
INSERT [dbo].[aros] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (2, NULL, N'Usuario', 1, NULL, 2, 1)
INSERT [dbo].[aros] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (3, NULL, N'Grupo', 2, NULL, 3, 6)
INSERT [dbo].[aros] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (4, 3, N'Usuario', 2, NULL, 4, 5)
INSERT [dbo].[aros] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (5, NULL, N'Grupo', 3, NULL, 7, 10)
INSERT [dbo].[aros] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (6, 5, N'Usuario', 3, NULL, 8, 9)
SET IDENTITY_INSERT [dbo].[aros] OFF
/****** Object:  Table [dbo].[acos]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[acos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[parent_id] [int] NULL,
	[model] [varchar](255) NULL,
	[foreign_key] [int] NULL,
	[alias] [varchar](255) NULL,
	[lft] [int] NULL,
	[rght] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[acos] ON
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1, NULL, NULL, NULL, N'controllers', 1, 3442)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (2, 1, NULL, NULL, N'Accesos', 2, 27)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (3, 2, NULL, NULL, N'index', 3, 4)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (5, 2, NULL, NULL, N'add', 5, 6)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (6, 2, NULL, NULL, N'edit', 7, 8)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (7, 2, NULL, NULL, N'delete', 9, 10)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (13, 2, NULL, NULL, N'registrosPorPagina', 11, 12)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (14, 1, NULL, NULL, N'Auditorias', 28, 55)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (15, 14, NULL, NULL, N'index', 29, 30)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (19, 14, NULL, NULL, N'edit', 31, 32)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (20, 14, NULL, NULL, N'delete', 33, 34)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (25, 14, NULL, NULL, N'registrosPorPagina', 35, 36)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (26, 14, NULL, NULL, N'getGeneralStats', 37, 38)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (35, 1, NULL, NULL, N'Configuraciones', 84, 105)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (36, 35, NULL, NULL, N'edit', 85, 86)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (37, 35, NULL, NULL, N'borrarCache', 87, 88)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (44, 1, NULL, NULL, N'Constructores', 106, 135)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (45, 44, NULL, NULL, N'index', 107, 108)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (46, 44, NULL, NULL, N'add', 109, 110)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (47, 44, NULL, NULL, N'cargar', 111, 112)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (49, 44, NULL, NULL, N'delete', 113, 114)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (52, 1, NULL, NULL, N'Consultas', 136, 167)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (53, 52, NULL, NULL, N'index', 137, 138)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (54, 52, NULL, NULL, N'lista', 139, 140)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (55, 52, NULL, NULL, N'exportar', 141, 142)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (56, 52, NULL, NULL, N'nombreTablas', 143, 144)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (57, 52, NULL, NULL, N'nombreColumnas', 145, 146)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (58, 52, NULL, NULL, N'comprobarTipoConexion', 147, 148)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (59, 52, NULL, NULL, N'obtenerTablasConsulta', 149, 150)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (60, 52, NULL, NULL, N'cargarConexion', 151, 152)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (61, 52, NULL, NULL, N'montarTablaResultado', 153, 154)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (66, 1, NULL, NULL, N'Eventos', 168, 197)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (67, 66, NULL, NULL, N'index', 169, 170)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (69, 66, NULL, NULL, N'add', 171, 172)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (70, 66, NULL, NULL, N'edit', 173, 174)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (71, 66, NULL, NULL, N'delete', 175, 176)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (77, 66, NULL, NULL, N'registrosPorPagina', 177, 178)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (78, 66, NULL, NULL, N'getEventsForDateRange', 179, 180)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (79, 66, NULL, NULL, N'getCalendar', 181, 182)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (80, 1, NULL, NULL, N'Filtros', 198, 227)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (81, 80, NULL, NULL, N'index', 199, 200)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (82, 80, NULL, NULL, N'view', 201, 202)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (83, 80, NULL, NULL, N'add', 203, 204)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (84, 80, NULL, NULL, N'edit', 205, 206)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (85, 80, NULL, NULL, N'delete', 207, 208)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (86, 80, NULL, NULL, N'filtro', 209, 210)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (87, 80, NULL, NULL, N'leerCondicionesFiltro', 211, 212)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (102, 1, NULL, NULL, N'Grupos', 258, 283)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (103, 102, NULL, NULL, N'index', 259, 260)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (105, 102, NULL, NULL, N'add', 261, 262)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (106, 102, NULL, NULL, N'edit', 263, 264)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (107, 102, NULL, NULL, N'delete', 265, 266)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (113, 102, NULL, NULL, N'registrosPorPagina', 267, 268)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (114, 1, NULL, NULL, N'Historicos', 284, 301)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (115, 114, NULL, NULL, N'index', 285, 286)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (117, 114, NULL, NULL, N'add', 287, 288)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (125, 114, NULL, NULL, N'registrosPorPagina', 289, 290)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (126, 1, NULL, NULL, N'Informes', 302, 329)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (127, 126, NULL, NULL, N'index', 303, 304)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (129, 126, NULL, NULL, N'add', 305, 306)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (130, 126, NULL, NULL, N'edit', 307, 308)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (131, 126, NULL, NULL, N'delete', 309, 310)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (136, 126, NULL, NULL, N'imprimir', 311, 312)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (137, 1, NULL, NULL, N'Informestipos', 330, 355)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (138, 137, NULL, NULL, N'index', 331, 332)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (140, 137, NULL, NULL, N'add', 333, 334)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (141, 137, NULL, NULL, N'edit', 335, 336)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (142, 137, NULL, NULL, N'delete', 337, 338)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (147, 1, NULL, NULL, N'Logmessages', 356, 375)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (148, 147, NULL, NULL, N'getErrorLogMessages', 357, 358)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (177, 1, NULL, NULL, N'Mensajes', 430, 469)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (178, 177, NULL, NULL, N'comprobarMensajesNuevos', 431, 432)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (179, 177, NULL, NULL, N'enviados', 433, 434)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (180, 177, NULL, NULL, N'recibidos', 435, 436)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (182, 177, NULL, NULL, N'add', 437, 438)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (183, 177, NULL, NULL, N'edit_enviados', 439, 440)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (184, 177, NULL, NULL, N'edit_recibidos', 441, 442)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (186, 177, NULL, NULL, N'delete', 443, 444)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (187, 177, NULL, NULL, N'filtro', 445, 446)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (192, 177, NULL, NULL, N'registrosPorPagina', 447, 448)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (193, 177, NULL, NULL, N'getLastMessages', 449, 450)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (194, 177, NULL, NULL, N'getNotReadMessagesCounter', 451, 452)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (196, 1, NULL, NULL, N'Menus', 470, 497)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (197, 196, NULL, NULL, N'index', 471, 472)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (199, 196, NULL, NULL, N'add', 473, 474)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (200, 196, NULL, NULL, N'edit', 475, 476)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (201, 196, NULL, NULL, N'delete', 477, 478)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (202, 196, NULL, NULL, N'draw', 479, 480)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (203, 196, NULL, NULL, N'registrosPorPagina', 481, 482)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (205, 1, NULL, NULL, N'Pages', 498, 515)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (206, 205, NULL, NULL, N'display', 499, 500)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (207, 205, NULL, NULL, N'maintenance', 501, 502)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (213, 1, NULL, NULL, N'Pdfs', 516, 535)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (214, 213, NULL, NULL, N'index', 517, 518)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (215, 213, NULL, NULL, N'array2table', 519, 520)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (216, 213, NULL, NULL, N'styles', 521, 522)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (221, 1, NULL, NULL, N'Permisos', 536, 575)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (222, 221, NULL, NULL, N'index', 537, 538)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (223, 221, NULL, NULL, N'add', 539, 540)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (224, 221, NULL, NULL, N'edit', 541, 542)
GO
print 'Processed 100 total records'
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (225, 221, NULL, NULL, N'delete', 543, 544)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (229, 221, NULL, NULL, N'checktosync', 545, 546)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (230, 221, NULL, NULL, N'sync', 547, 548)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (247, 221, NULL, NULL, N'cleanAcosFromConstructor', 549, 550)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (252, 221, NULL, NULL, N'darPermisoDesdeConstructor', 551, 552)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (257, 1, NULL, NULL, N'Usuarios', 576, 609)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (258, 257, NULL, NULL, N'index', 577, 578)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (260, 257, NULL, NULL, N'add', 579, 580)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (261, 257, NULL, NULL, N'edit', 581, 582)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (262, 257, NULL, NULL, N'delete', 583, 584)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (267, 257, NULL, NULL, N'login', 585, 586)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (268, 257, NULL, NULL, N'logout', 587, 588)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (270, 257, NULL, NULL, N'registrosPorPagina', 589, 590)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (271, 1, NULL, NULL, N'permisosmenus', 610, 859)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (272, 271, N'Menu', 1, NULL, 611, 612)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (273, 271, N'Menu', 2, NULL, 613, 614)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (274, 271, N'Menu', 3, NULL, 615, 616)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (275, 271, N'Menu', 4, NULL, 617, 618)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (276, 271, N'Menu', 5, NULL, 619, 620)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (277, 271, N'Menu', 6, NULL, 621, 622)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (278, 271, N'Menu', 7, NULL, 623, 624)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (279, 271, N'Menu', 8, NULL, 625, 626)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (280, 271, N'Menu', 9, NULL, 627, 628)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (281, 271, N'Menu', 10, NULL, 629, 630)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (282, 271, N'Menu', 11, NULL, 631, 632)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (283, 271, N'Menu', 12, NULL, 633, 634)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (284, 271, N'Menu', 13, NULL, 635, 636)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (285, 271, N'Menu', 14, NULL, 637, 638)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (286, 271, N'Menu', 15, NULL, 639, 640)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (287, 271, N'Menu', 16, NULL, 641, 642)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (288, 271, N'Menu', 17, NULL, 643, 644)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (290, 44, NULL, NULL, N'tablasDisponibles', 115, 116)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (291, 44, NULL, NULL, N'indexMasivo', 117, 118)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (292, 2, NULL, NULL, N'create', 13, 14)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (293, 2, NULL, NULL, N'update', 15, 16)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (294, 2, NULL, NULL, N'exportarMostrar', 17, 18)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (295, 2, NULL, NULL, N'exportarEjecutar', 19, 20)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (296, 2, NULL, NULL, N'configurarModulo', 21, 22)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (297, 2, NULL, NULL, N'configurarModuloGuardar', 23, 24)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (299, 2, NULL, NULL, N'js', 25, 26)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (341, 14, NULL, NULL, N'auditar', 39, 40)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (342, 14, NULL, NULL, N'historico', 41, 42)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (343, 14, NULL, NULL, N'update', 43, 44)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (344, 14, NULL, NULL, N'exportarMostrar', 45, 46)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (345, 14, NULL, NULL, N'exportarEjecutar', 47, 48)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (346, 14, NULL, NULL, N'configurarModulo', 49, 50)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (347, 14, NULL, NULL, N'configurarModuloGuardar', 51, 52)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (349, 14, NULL, NULL, N'js', 53, 54)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (443, 35, NULL, NULL, N'js', 89, 90)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (447, 35, NULL, NULL, N'registrosPorPagina', 91, 92)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (455, 35, NULL, NULL, N'exportarEjecutar', 93, 94)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (456, 35, NULL, NULL, N'exportarMostrar', 95, 96)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (457, 35, NULL, NULL, N'configurarModulo', 97, 98)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (458, 35, NULL, NULL, N'configurarModuloGuardar', 99, 100)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (492, 44, NULL, NULL, N'js', 119, 120)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (496, 44, NULL, NULL, N'registrosPorPagina', 121, 122)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (504, 44, NULL, NULL, N'exportarEjecutar', 123, 124)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (505, 44, NULL, NULL, N'exportarMostrar', 125, 126)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (506, 44, NULL, NULL, N'configurarModulo', 127, 128)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (507, 44, NULL, NULL, N'configurarModuloGuardar', 129, 130)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (541, 52, NULL, NULL, N'js', 155, 156)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (545, 52, NULL, NULL, N'registrosPorPagina', 157, 158)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (553, 52, NULL, NULL, N'exportarEjecutar', 159, 160)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (554, 52, NULL, NULL, N'exportarMostrar', 161, 162)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (555, 52, NULL, NULL, N'configurarModulo', 163, 164)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (556, 52, NULL, NULL, N'configurarModuloGuardar', 165, 166)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (589, 66, NULL, NULL, N'create', 183, 184)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (590, 66, NULL, NULL, N'update', 185, 186)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (591, 66, NULL, NULL, N'exportarMostrar', 187, 188)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (592, 66, NULL, NULL, N'exportarEjecutar', 189, 190)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (593, 66, NULL, NULL, N'configurarModulo', 191, 192)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (594, 66, NULL, NULL, N'configurarModuloGuardar', 193, 194)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (596, 66, NULL, NULL, N'js', 195, 196)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (639, 80, NULL, NULL, N'js', 213, 214)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (643, 80, NULL, NULL, N'registrosPorPagina', 215, 216)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (650, 80, NULL, NULL, N'exportarEjecutar', 217, 218)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (651, 80, NULL, NULL, N'exportarMostrar', 219, 220)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (652, 80, NULL, NULL, N'configurarModulo', 221, 222)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (653, 80, NULL, NULL, N'configurarModuloGuardar', 223, 224)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (735, 102, NULL, NULL, N'create', 269, 270)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (736, 102, NULL, NULL, N'update', 271, 272)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (737, 102, NULL, NULL, N'exportarMostrar', 273, 274)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (738, 102, NULL, NULL, N'exportarEjecutar', 275, 276)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (739, 102, NULL, NULL, N'configurarModulo', 277, 278)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (740, 102, NULL, NULL, N'configurarModuloGuardar', 279, 280)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (742, 102, NULL, NULL, N'js', 281, 282)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (784, 114, NULL, NULL, N'exportarMostrar', 291, 292)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (785, 114, NULL, NULL, N'exportarEjecutar', 293, 294)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (786, 114, NULL, NULL, N'configurarModulo', 295, 296)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (787, 114, NULL, NULL, N'configurarModuloGuardar', 297, 298)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (789, 114, NULL, NULL, N'js', 299, 300)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (831, 126, NULL, NULL, N'create', 313, 314)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (832, 126, NULL, NULL, N'update', 315, 316)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (833, 126, NULL, NULL, N'registrosPorPagina', 317, 318)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (834, 126, NULL, NULL, N'exportarMostrar', 319, 320)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (835, 126, NULL, NULL, N'exportarEjecutar', 321, 322)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (836, 126, NULL, NULL, N'configurarModulo', 323, 324)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (837, 126, NULL, NULL, N'configurarModuloGuardar', 325, 326)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (839, 126, NULL, NULL, N'js', 327, 328)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (881, 137, NULL, NULL, N'create', 339, 340)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (882, 137, NULL, NULL, N'update', 341, 342)
GO
print 'Processed 200 total records'
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (883, 137, NULL, NULL, N'registrosPorPagina', 343, 344)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (884, 137, NULL, NULL, N'exportarMostrar', 345, 346)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (885, 137, NULL, NULL, N'exportarEjecutar', 347, 348)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (886, 137, NULL, NULL, N'configurarModulo', 349, 350)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (887, 137, NULL, NULL, N'configurarModuloGuardar', 351, 352)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (889, 137, NULL, NULL, N'js', 353, 354)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (932, 147, NULL, NULL, N'js', 359, 360)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (936, 147, NULL, NULL, N'registrosPorPagina', 361, 362)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (944, 147, NULL, NULL, N'exportarEjecutar', 363, 364)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (945, 147, NULL, NULL, N'exportarMostrar', 365, 366)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (946, 147, NULL, NULL, N'configurarModulo', 367, 368)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (947, 147, NULL, NULL, N'configurarModuloGuardar', 369, 370)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1079, 177, NULL, NULL, N'enviar', 453, 454)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1080, 177, NULL, NULL, N'responderEnviados', 455, 456)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1081, 177, NULL, NULL, N'responderRecibidos', 457, 458)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1082, 177, NULL, NULL, N'exportarMostrar', 459, 460)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1083, 177, NULL, NULL, N'exportarEjecutar', 461, 462)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1084, 177, NULL, NULL, N'configurarModulo', 463, 464)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1085, 177, NULL, NULL, N'configurarModuloGuardar', 465, 466)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1087, 177, NULL, NULL, N'js', 467, 468)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1129, 196, NULL, NULL, N'create', 483, 484)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1130, 196, NULL, NULL, N'update', 485, 486)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1131, 196, NULL, NULL, N'configurarModulo', 487, 488)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1132, 196, NULL, NULL, N'configurarModuloGuardar', 489, 490)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1134, 196, NULL, NULL, N'js', 491, 492)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1145, 196, NULL, NULL, N'exportarEjecutar', 493, 494)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1146, 196, NULL, NULL, N'exportarMostrar', 495, 496)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1180, 205, NULL, NULL, N'js', 503, 504)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1184, 205, NULL, NULL, N'registrosPorPagina', 505, 506)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1192, 205, NULL, NULL, N'exportarEjecutar', 507, 508)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1193, 205, NULL, NULL, N'exportarMostrar', 509, 510)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1194, 205, NULL, NULL, N'configurarModulo', 511, 512)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1195, 205, NULL, NULL, N'configurarModuloGuardar', 513, 514)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1229, 213, NULL, NULL, N'js', 523, 524)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1233, 213, NULL, NULL, N'registrosPorPagina', 525, 526)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1241, 213, NULL, NULL, N'exportarEjecutar', 527, 528)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1242, 213, NULL, NULL, N'exportarMostrar', 529, 530)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1243, 213, NULL, NULL, N'configurarModulo', 531, 532)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1244, 213, NULL, NULL, N'configurarModuloGuardar', 533, 534)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1277, 221, NULL, NULL, N'exportarMostrar', 553, 554)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1278, 221, NULL, NULL, N'exportarEjecutar', 555, 556)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1280, 221, NULL, NULL, N'js', 557, 558)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1281, 221, NULL, NULL, N'verifyAcos', 559, 560)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1282, 221, NULL, NULL, N'recoverAcos', 561, 562)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1284, 221, NULL, NULL, N'registrosPorPagina', 563, 564)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1291, 221, NULL, NULL, N'configurarModulo', 565, 566)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1292, 221, NULL, NULL, N'configurarModuloGuardar', 567, 568)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1326, 257, NULL, NULL, N'create', 591, 592)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1327, 257, NULL, NULL, N'update', 593, 594)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1328, 257, NULL, NULL, N'exportarMostrar', 595, 596)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1329, 257, NULL, NULL, N'exportarEjecutar', 597, 598)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1330, 257, NULL, NULL, N'configurarModulo', 599, 600)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1331, 257, NULL, NULL, N'configurarModuloGuardar', 601, 602)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1332, 257, NULL, NULL, N'js', 603, 604)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1374, 1, NULL, NULL, N'Combos', 860, 881)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1375, 1374, NULL, NULL, N'crearCodigoCombosRelacionados', 861, 862)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1376, 1374, NULL, NULL, N'cargarComboRelacionado', 863, 864)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1377, 1374, NULL, NULL, N'cargarComboRelacionadoAvanzado', 865, 866)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1378, 1374, NULL, NULL, N'js', 867, 868)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1379, 1374, NULL, NULL, N'registrosPorPagina', 869, 870)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1380, 1374, NULL, NULL, N'exportarEjecutar', 871, 872)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1381, 1374, NULL, NULL, N'exportarMostrar', 873, 874)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1382, 1374, NULL, NULL, N'configurarModulo', 875, 876)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1383, 1374, NULL, NULL, N'configurarModuloGuardar', 877, 878)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1384, 44, NULL, NULL, N'addMasivo', 131, 132)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1385, 1, NULL, NULL, N'Instalador', 882, 899)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1386, 1385, NULL, NULL, N'instalar', 883, 884)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1387, 1385, NULL, NULL, N'finalizarInstalacion', 885, 886)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1388, 1385, NULL, NULL, N'js', 887, 888)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1389, 1385, NULL, NULL, N'registrosPorPagina', 889, 890)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1390, 1385, NULL, NULL, N'exportarEjecutar', 891, 892)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1391, 1385, NULL, NULL, N'exportarMostrar', 893, 894)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1392, 1385, NULL, NULL, N'configurarModulo', 895, 896)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1393, 1385, NULL, NULL, N'configurarModuloGuardar', 897, 898)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1394, 221, NULL, NULL, N'resetSync', 569, 570)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1395, 221, NULL, NULL, N'syncResourcesForController', 571, 572)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1396, 221, NULL, NULL, N'getControllerPath', 573, 574)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1397, 1374, NULL, NULL, N'crearCodigoComboModal', 879, 880)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1398, 35, NULL, NULL, N'guardarConfig', 101, 102)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1399, 35, NULL, NULL, N'cargarConfigYML', 103, 104)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1400, 44, NULL, NULL, N'create', 133, 134)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1401, 1, NULL, NULL, N'Elfinderfiles', 926, 941)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1402, 1401, NULL, NULL, N'vista', 927, 928)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1403, 1401, NULL, NULL, N'js', 929, 930)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1404, 1401, NULL, NULL, N'registrosPorPagina', 931, 932)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1405, 1401, NULL, NULL, N'exportarEjecutar', 933, 934)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1406, 1401, NULL, NULL, N'exportarMostrar', 935, 936)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1407, 1401, NULL, NULL, N'configurarModulo', 937, 938)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1408, 1401, NULL, NULL, N'configurarModuloGuardar', 939, 940)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1409, 147, NULL, NULL, N'mostrarLogs', 371, 372)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1410, 147, NULL, NULL, N'borrarLogs', 373, 374)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1411, 257, NULL, NULL, N'pass', 605, 606)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1412, 257, NULL, NULL, N'updatePass', 607, 608)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1426, 80, NULL, NULL, N'limpiarTodosFiltroActivo', 225, 226)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1455, 271, N'Menu', 27, N'NULL', 849, 850)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1456, 271, N'Menu', 28, NULL, 851, 852)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1457, 1, NULL, NULL, N'Pruebas', 3322, 3351)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1458, 1457, NULL, NULL, N'index', 3323, 3324)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1459, 1457, NULL, NULL, N'add', 3325, 3326)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1460, 1457, NULL, NULL, N'create', 3327, 3328)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1461, 1457, NULL, NULL, N'edit', 3329, 3330)
GO
print 'Processed 300 total records'
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1462, 1457, NULL, NULL, N'update', 3331, 3332)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1463, 1457, NULL, NULL, N'delete', 3333, 3334)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1464, 1457, NULL, NULL, N'indexModal', 3335, 3336)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1465, 1457, NULL, NULL, N'cargarFormModal', 3337, 3338)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1466, 1457, NULL, NULL, N'js', 3339, 3340)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1467, 1457, NULL, NULL, N'registrosPorPagina', 3341, 3342)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1468, 1457, NULL, NULL, N'exportarEjecutar', 3343, 3344)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1469, 1457, NULL, NULL, N'exportarMostrar', 3345, 3346)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1470, 1457, NULL, NULL, N'configurarModulo', 3347, 3348)
INSERT [dbo].[acos] ([id], [parent_id], [model], [foreign_key], [alias], [lft], [rght]) VALUES (1471, 1457, NULL, NULL, N'configurarModuloGuardar', 3349, 3350)
SET IDENTITY_INSERT [dbo].[acos] OFF
/****** Object:  Table [dbo].[accesos]    Script Date: 11/25/2016 14:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[accesos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](255) NOT NULL,
	[clave] [varchar](255) NOT NULL,
	[ip] [varchar](15) NOT NULL,
	[exito] [varchar](32) NOT NULL,
	[created] [datetime] NOT NULL,
	[modified] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[accesos] ON
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (1, N'admin', N'', N'172.20.103.230', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (2, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (3, N'admin', N'', N'172.20.103.230', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (4, N'admin', N'', N'172.20.103.230', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (5, N'admin', N'', N'172.20.103.230', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (6, N'admin', N'', N'172.20.103.230', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (7, N'admin', N'', N'172.20.103.230', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (8, N'admin', N'', N'172.20.103.230', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (9, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (10, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (11, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (12, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (13, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (14, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (15, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (16, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (17, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (18, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (19, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (20, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (21, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (22, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (23, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (24, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (25, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (26, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (27, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (28, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (29, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (30, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (31, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (32, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (33, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (34, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (35, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (36, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (37, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (38, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (39, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (40, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (41, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (42, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (43, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (44, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (45, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (46, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (47, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (48, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (49, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (50, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (51, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (52, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (53, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (54, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (55, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (56, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (57, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (58, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (59, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (60, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (61, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (62, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (63, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (64, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (65, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (66, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (67, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (68, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (69, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (70, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (71, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (72, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (73, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (74, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (75, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (76, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (77, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (78, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (79, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (80, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (81, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (82, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (83, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (84, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (85, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (86, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (87, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (88, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (89, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (90, N'adminm', N'admin', N'127.0.0.1', N'ERROR', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (91, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (92, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (93, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (94, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (95, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (96, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (97, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (98, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (99, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (100, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
GO
print 'Processed 100 total records'
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (101, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (102, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (103, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (104, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (105, N'admin', N'', N'127.0.0.1', N'OK', CAST(0x0000000000000000 AS DateTime), CAST(0x0000000000000000 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (106, N'admin', N'', N'172.20.102.215', N'OK', CAST(0x0000A6C900DFD8B0 AS DateTime), CAST(0x0000A6C900DFD8B0 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (107, N'admin', N'', N'172.20.102.215', N'OK', CAST(0x0000A6C900E0B050 AS DateTime), CAST(0x0000A6C900E0B050 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (108, N'admin', N'', N'172.20.102.215', N'OK', CAST(0x0000A6C900E59188 AS DateTime), CAST(0x0000A6C900E59188 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (109, N'admin', N'', N'172.20.102.215', N'OK', CAST(0x0000A6C900E70504 AS DateTime), CAST(0x0000A6C900E70504 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (110, N'admin', N'', N'172.20.102.215', N'OK', CAST(0x0000A6C900E72700 AS DateTime), CAST(0x0000A6C900E72700 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (111, N'admin', N'', N'172.20.102.215', N'OK', CAST(0x0000A6C900E75F40 AS DateTime), CAST(0x0000A6C900E75F40 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (112, N'admin', N'', N'172.20.102.215', N'OK', CAST(0x0000A6C900E79780 AS DateTime), CAST(0x0000A6C900E79780 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (113, N'admin', N'', N'172.20.102.215', N'OK', CAST(0x0000A6C900E81D18 AS DateTime), CAST(0x0000A6C900E81D18 AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (114, N'admin', N'', N'172.20.102.215', N'OK', CAST(0x0000A6C900EBA94C AS DateTime), CAST(0x0000A6C900EBA94C AS DateTime))
INSERT [dbo].[accesos] ([id], [username], [clave], [ip], [exito], [created], [modified]) VALUES (115, N'admin', N'', N'172.20.102.215', N'OK', CAST(0x0000A6C900EC2B60 AS DateTime), CAST(0x0000A6C900EC2B60 AS DateTime))
SET IDENTITY_INSERT [dbo].[accesos] OFF
/****** Object:  Default [DF__acos__parent_id__093F5D4E]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[acos] ADD  DEFAULT (NULL) FOR [parent_id]
GO
/****** Object:  Default [DF__acos__model__0A338187]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[acos] ADD  DEFAULT (NULL) FOR [model]
GO
/****** Object:  Default [DF__acos__foreign_ke__0B27A5C0]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[acos] ADD  DEFAULT (NULL) FOR [foreign_key]
GO
/****** Object:  Default [DF__acos__alias__0C1BC9F9]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[acos] ADD  DEFAULT (NULL) FOR [alias]
GO
/****** Object:  Default [DF__acos__lft__0D0FEE32]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[acos] ADD  DEFAULT (NULL) FOR [lft]
GO
/****** Object:  Default [DF__acos__rght__0E04126B]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[acos] ADD  DEFAULT (NULL) FOR [rght]
GO
/****** Object:  Default [DF__aros__parent_id__12C8C788]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[aros] ADD  DEFAULT (NULL) FOR [parent_id]
GO
/****** Object:  Default [DF__aros__model__13BCEBC1]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[aros] ADD  DEFAULT (NULL) FOR [model]
GO
/****** Object:  Default [DF__aros__foreign_ke__14B10FFA]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[aros] ADD  DEFAULT (NULL) FOR [foreign_key]
GO
/****** Object:  Default [DF__aros__alias__15A53433]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[aros] ADD  DEFAULT (NULL) FOR [alias]
GO
/****** Object:  Default [DF__aros__lft__1699586C]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[aros] ADD  DEFAULT (NULL) FOR [lft]
GO
/****** Object:  Default [DF__aros__rght__178D7CA5]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[aros] ADD  DEFAULT (NULL) FOR [rght]
GO
/****** Object:  Default [DF__aros_acos__creat__1D4655FB]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[aros_acos] ADD  DEFAULT ('0') FOR [create_]
GO
/****** Object:  Default [DF__aros_acos__read___1E3A7A34]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[aros_acos] ADD  DEFAULT ('0') FOR [read_]
GO
/****** Object:  Default [DF__aros_acos__updat__1F2E9E6D]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[aros_acos] ADD  DEFAULT ('0') FOR [update_]
GO
/****** Object:  Default [DF__aros_acos__delet__2022C2A6]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[aros_acos] ADD  DEFAULT ('0') FOR [delete_]
GO
/****** Object:  Default [DF__auditoria__usuar__24E777C3]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[auditorias] ADD  DEFAULT (NULL) FOR [usuario]
GO
/****** Object:  Default [DF__auditoria__modul__25DB9BFC]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[auditorias] ADD  DEFAULT (NULL) FOR [modulo]
GO
/****** Object:  Default [DF__auditoria__accio__26CFC035]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[auditorias] ADD  DEFAULT (NULL) FOR [accion]
GO
/****** Object:  Default [DF__eventos__created__3335971A]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[eventos] ADD  DEFAULT (NULL) FOR [created]
GO
/****** Object:  Default [DF__eventos__modifie__3429BB53]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[eventos] ADD  DEFAULT (NULL) FOR [modified]
GO
/****** Object:  Default [DF__filtros__usuario__38EE7070]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[filtros] ADD  DEFAULT (NULL) FOR [usuario_id]
GO
/****** Object:  Default [DF__filtros__created__39E294A9]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[filtros] ADD  DEFAULT (NULL) FOR [created]
GO
/****** Object:  Default [DF__filtros__modifie__3AD6B8E2]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[filtros] ADD  DEFAULT (NULL) FOR [modified]
GO
/****** Object:  Default [DF__grupos__descripc__3F9B6DFF]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[grupos] ADD  DEFAULT (NULL) FOR [descripcion]
GO
/****** Object:  Default [DF__grupos__created__408F9238]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[grupos] ADD  DEFAULT (NULL) FOR [created]
GO
/****** Object:  Default [DF__grupos__modified__4183B671]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[grupos] ADD  DEFAULT (NULL) FOR [modified]
GO
/****** Object:  Default [DF__historico__usuar__46486B8E]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[historicos] ADD  DEFAULT (NULL) FOR [usuario]
GO
/****** Object:  Default [DF__historico__modul__473C8FC7]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[historicos] ADD  DEFAULT (NULL) FOR [modulo]
GO
/****** Object:  Default [DF__historico__accio__4830B400]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[historicos] ADD  DEFAULT (NULL) FOR [accion]
GO
/****** Object:  Default [DF__informes__inform__4CF5691D]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[informes] ADD  DEFAULT (NULL) FOR [informestipo_id]
GO
/****** Object:  Default [DF__informes__constr__4DE98D56]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[informes] ADD  DEFAULT ('0') FOR [constructor_id]
GO
/****** Object:  Default [DF__informes__grupo___4EDDB18F]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[informes] ADD  DEFAULT ('4') FOR [grupo_id]
GO
/****** Object:  Default [DF__mensajes__usuari__5772F790]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[mensajes] ADD  DEFAULT (NULL) FOR [usuario]
GO
/****** Object:  Default [DF__menus__menu_id__5C37ACAD]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[menus] ADD  DEFAULT ('0') FOR [menu_id]
GO
/****** Object:  Default [DF__menus__estado__5D2BD0E6]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[menus] ADD  DEFAULT ('Activo') FOR [estado]
GO
/****** Object:  Default [DF__menus__controlad__5E1FF51F]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[menus] ADD  DEFAULT ('') FOR [controlador]
GO
/****** Object:  Default [DF__menus__accion__5F141958]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[menus] ADD  DEFAULT ('') FOR [accion]
GO
/****** Object:  Default [DF__menus__orden__60083D91]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[menus] ADD  DEFAULT ('0') FOR [orden]
GO
/****** Object:  Default [DF__menus__ruta__60FC61CA]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[menus] ADD  DEFAULT ('') FOR [ruta]
GO
/****** Object:  Default [DF__menus__icono__61F08603]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[menus] ADD  DEFAULT (NULL) FOR [icono]
GO
/****** Object:  Default [DF__usuarios__nif__67A95F59]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[usuarios] ADD  DEFAULT (NULL) FOR [nif]
GO
/****** Object:  Default [DF__usuarios__direcc__689D8392]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[usuarios] ADD  DEFAULT (NULL) FOR [direccion_general]
GO
/****** Object:  Default [DF__usuarios__unidad__6991A7CB]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[usuarios] ADD  DEFAULT (NULL) FOR [unidad]
GO
/****** Object:  Default [DF__usuarios__puesto__6A85CC04]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[usuarios] ADD  DEFAULT (NULL) FOR [puesto]
GO
/****** Object:  Default [DF__usuarios__email__6B79F03D]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[usuarios] ADD  DEFAULT (NULL) FOR [email]
GO
/****** Object:  Default [DF__usuarios__create__6C6E1476]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[usuarios] ADD  DEFAULT (NULL) FOR [created]
GO
/****** Object:  Default [DF__usuarios__modifi__6D6238AF]    Script Date: 11/25/2016 14:31:35 ******/
ALTER TABLE [dbo].[usuarios] ADD  DEFAULT (NULL) FOR [modified]
GO
