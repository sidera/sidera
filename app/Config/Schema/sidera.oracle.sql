--------------------------------------------------------
-- Archivo creado  - lunes-noviembre-18-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ACCESOS
--------------------------------------------------------

  CREATE TABLE "ACCESOS" 
   (	"ID" NUMBER, 
	"USERNAME" VARCHAR2(255 BYTE), 
	"CLAVE" VARCHAR2(255 BYTE), 
	"IP" VARCHAR2(255 BYTE), 
	"EXITO" VARCHAR2(20 BYTE), 
	"CREATED" TIMESTAMP (6), 
	"MODIFIED" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table ACOS
--------------------------------------------------------

  CREATE TABLE "ACOS" 
   (	"ID" NUMBER, 
	"PARENT_ID" NUMBER, 
	"MODEL" VARCHAR2(255 BYTE), 
	"FOREIGN_KEY" NUMBER, 
	"ALIAS" VARCHAR2(255 BYTE), 
	"LFT" NUMBER, 
	"RGHT" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table AROS
--------------------------------------------------------

  CREATE TABLE "AROS" 
   (	"ID" NUMBER, 
	"PARENT_ID" NUMBER, 
	"MODEL" VARCHAR2(255 BYTE), 
	"FOREIGN_KEY" NUMBER, 
	"ALIAS" VARCHAR2(255 BYTE), 
	"LFT" NUMBER, 
	"RGHT" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table AROS_ACOS
--------------------------------------------------------

  CREATE TABLE "AROS_ACOS" 
   (	"ID" NUMBER, 
	"ARO_ID" NUMBER, 
	"ACO_ID" NUMBER, 
	"CREATE_" NUMBER, 
	"READ_" NUMBER, 
	"UPDATE_" NUMBER, 
	"DELETE_" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table AUDITORIAS
--------------------------------------------------------

  CREATE TABLE "AUDITORIAS" 
   (	"ID" NUMBER, 
	"USUARIO" VARCHAR2(255 BYTE), 
	"MODULO" VARCHAR2(255 BYTE), 
	"ACCION" VARCHAR2(255 BYTE), 
	"PARAMETROS" VARCHAR2(512 BYTE), 
	"CREATED" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table CONDICIONES
--------------------------------------------------------

  CREATE TABLE "CONDICIONES" 
   (	"ID" NUMBER, 
	"CAMPO" VARCHAR2(255 BYTE), 
	"COMPARADOR" VARCHAR2(255 BYTE), 
	"CONTENIDO" VARCHAR2(255 BYTE), 
	"FILTRO_ID" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table DEMOS
--------------------------------------------------------

  CREATE TABLE "DEMOS" 
   (	"ID" NUMBER, 
	"NOMBRE" VARCHAR2(255 BYTE), 
	"FECHA" DATE, 
	"TIME_HORA" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table EVENTOS
--------------------------------------------------------

  CREATE TABLE "EVENTOS" 
   (	"ID" NUMBER, 
	"TITLE" VARCHAR2(20 BYTE), 
	"STARTEVENT" TIMESTAMP (6), 
	"ENDEVENT" TIMESTAMP (6), 
	"ALLDAY" NUMBER(1,0), 
	"CREATED" TIMESTAMP (6), 
	"MODIFIED" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table FILTROS
--------------------------------------------------------

  CREATE TABLE "FILTROS" 
   ("ID" NUMBER, 
	"MODELO" VARCHAR2(255 BYTE), 
	"TITULO" VARCHAR2(255 BYTE), 
	"TIPO_CONDICION" VARCHAR2(255 BYTE),
        "USUARIO_ID" NUMBER, 
	"CREATED" TIMESTAMP (6), 
	"MODIFIED" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table GESTORES
--------------------------------------------------------

  CREATE TABLE "GESTORES" 
   (	"ID" NUMBER, 
	"NOMBRE_COMPLETO" VARCHAR2(255 BYTE), 
	"TELEFONO" VARCHAR2(9 BYTE), 
	"EXTENSION_TELEFONICA" VARCHAR2(9 BYTE), 
	"FAX" VARCHAR2(9 BYTE), 
	"CARGO" VARCHAR2(255 BYTE), 
	"ORDEN" NUMBER(3,0), 
	"GESTOR_INFORMATICO" NUMBER(1,0), 
	"EMAIL" VARCHAR2(255 BYTE), 
	"UBICACION" VARCHAR2(255 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table GRUPOS
--------------------------------------------------------

  CREATE TABLE "GRUPOS" 
   (	"ID" NUMBER, 
	"NOMBRE" VARCHAR2(255 BYTE), 
	"DESCRIPCION" VARCHAR2(255 BYTE), 
	"CREATED" TIMESTAMP (6), 
	"MODIFIED" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table HISTORICOS
--------------------------------------------------------

  CREATE TABLE "HISTORICOS" 
   (	"ID" NUMBER, 
	"USUARIO" VARCHAR2(255 BYTE), 
	"MODULO" VARCHAR2(255 BYTE), 
	"ACCION" VARCHAR2(255 BYTE), 
	"DATOS_ANTIGUO" VARCHAR2(255 BYTE), 
	"CREATED" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table INFORMES
--------------------------------------------------------

  CREATE TABLE "INFORMES" 
   (	"ID" NUMBER, 
	"NOMBRE" VARCHAR2(255 BYTE), 
	"REPORT" VARCHAR2(255 BYTE), 
	"INFORMESTIPO_ID" NUMBER, 
	"CONSTRUCTOR_ID" NUMBER, 
	"GRUPO_ID" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table INFORMESTIPOS
--------------------------------------------------------

  CREATE TABLE "INFORMESTIPOS" 
   (	"ID" NUMBER, 
	"NOMBRE" VARCHAR2(255 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table LOGS
--------------------------------------------------------

  CREATE TABLE "LOGS" 
   (	"ID" NUMBER, 
	"VERSION" VARCHAR2(255 BYTE), 
	"SUB_VERSION" NUMBER, 
	"SUB_SUB_VERSION" NUMBER, 
	"CAMBIO" VARCHAR2(255 BYTE), 
	"DESCRIPCION" VARCHAR2(255 BYTE), 
	"SEVERIDAD" VARCHAR2(255 BYTE), 
	"FECHA" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table MANUALES
--------------------------------------------------------

  CREATE TABLE "MANUALES" 
   (	"ID" NUMBER, 
	"TITULO" VARCHAR2(255 BYTE), 
	"CAPITULO" NUMBER, 
	"VERSION" VARCHAR2(255 BYTE), 
	"RUTA_VIDEO" VARCHAR2(255 BYTE), 
	"DESCRIPCION" VARCHAR2(255 BYTE), 
	"FECHA_ACTUALIZACION" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table MENSAJES
--------------------------------------------------------

  CREATE TABLE "MENSAJES" 
   (	"ID" NUMBER, 
	"ORIGEN" NUMBER, 
	"DESTINO" NUMBER, 
	"ASUNTO" VARCHAR2(255 BYTE), 
	"TEXTO" VARCHAR2(512 BYTE), 
	"ESTADO" NUMBER(1,0), 
	"CREATED" TIMESTAMP (6) DEFAULT current_timestamp, 
	"PROPIETARIO" NUMBER, 
	"USUARIO" NUMBER, 
	"DESTINOS_TEXTO" VARCHAR2(255 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table MENUS
--------------------------------------------------------

  CREATE TABLE "MENUS" 
   (	"ID" NUMBER, 
	"TITULO" VARCHAR2(255 BYTE), 
	"MENU_ID" NUMBER, 
	"ESTADO" VARCHAR2(255 BYTE), 
	"CONTROLADOR" VARCHAR2(255 BYTE), 
	"ACCION" VARCHAR2(20 BYTE), 
	"ORDEN" NUMBER, 
	"RUTA" VARCHAR2(255 BYTE), 
	"ICONO" VARCHAR2(255 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table PRUEBAMASIVOS
--------------------------------------------------------

  CREATE TABLE "PRUEBAMASIVOS" 
   (	"ID" NUMBER, 
	"NOMBRE" VARCHAR2(20 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Table SIDERAS
--------------------------------------------------------

  CREATE TABLE "SIDERAS" 
   (	"ID" NUMBER(11,0), 
	"ENTERO" NUMBER(11,0), 
	"REAL" NUMBER(7,2), 
	"FECHA" DATE, 
	"TEXTO" VARCHAR2(7 CHAR), 
	"BOOL_EANO" NUMBER(*,0), 
	"DATETIME" TIMESTAMP (6), 
	"TIME_HORA" TIMESTAMP (6), 
	"COLUMN1" NUMBER(11,0), 
	"DNI" VARCHAR2(9 BYTE), 
	"CC_BANCO" NUMBER(4,0), 
	"CC_SUCURSAL" NUMBER(4,0), 
	"CC_DC" NUMBER(2,0), 
	"CC_CUENTA" NUMBER(10,0)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) ;
--------------------------------------------------------
--  DDL for Table USUARIOS
--------------------------------------------------------

  CREATE TABLE "USUARIOS" 
   (	"ID" NUMBER, 
	"USERNAME" VARCHAR2(255 BYTE), 
	"PASSWORD" VARCHAR2(255 BYTE), 
	"GRUPO_ID" NUMBER, 
	"NOMBRE" VARCHAR2(255 BYTE), 
	"APELLIDO1" VARCHAR2(255 BYTE), 
	"APELLIDO2" VARCHAR2(255 BYTE), 
	"NIF" VARCHAR2(9 BYTE), 
	"DIRECCION_GENERAL" VARCHAR2(255 BYTE), 
	"UNIDAD" VARCHAR2(255 BYTE), 
	"PUESTO" VARCHAR2(255 BYTE), 
	"EMAIL" VARCHAR2(255 BYTE), 
	"CREATED" TIMESTAMP (6), 
	"MODIFIED" TIMESTAMP (6)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
REM INSERTING into ACOS
SET DEFINE OFF;
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9529','9528',null,null,'index','3','4');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9530','9528',null,null,'add','5','6');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9531','9528',null,null,'create','7','8');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9532','9528',null,null,'edit','9','10');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9533','9528',null,null,'update','11','12');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9534','9528',null,null,'delete','13','14');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9535','9528',null,null,'js','15','16');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9536','9528',null,null,'registrosPorPagina','17','18');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9537','9528',null,null,'exportarEjecutar','19','20');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9538','9528',null,null,'exportarMostrar','21','22');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9539','9528',null,null,'configurarModulo','23','24');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9540','9528',null,null,'configurarModuloGuardar','25','26');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9541','1',null,null,'Auditorias','28','55');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9542','9541',null,null,'index','29','30');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9543','9541',null,null,'auditar','31','32');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9544','9541',null,null,'historico','33','34');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9545','9541',null,null,'edit','35','36');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9546','9541',null,null,'update','37','38');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9547','9541',null,null,'delete','39','40');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9548','9541',null,null,'getGeneralStats','41','42');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9549','9541',null,null,'js','43','44');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9550','9541',null,null,'registrosPorPagina','45','46');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9551','9541',null,null,'exportarEjecutar','47','48');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9552','9541',null,null,'exportarMostrar','49','50');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9553','9541',null,null,'configurarModulo','51','52');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9554','9541',null,null,'configurarModuloGuardar','53','54');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9555','1',null,null,'Bdconexiones','56','83');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9556','9555',null,null,'index','57','58');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9557','9555',null,null,'add','59','60');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9558','9555',null,null,'create','61','62');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9559','9555',null,null,'edit','63','64');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9560','9555',null,null,'update','65','66');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9561','9555',null,null,'delete','67','68');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9562','9555',null,null,'configurarModulo','69','70');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9563','9555',null,null,'checkDbConnection','71','72');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9564','9555',null,null,'js','73','74');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9565','9555',null,null,'registrosPorPagina','75','76');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9566','9555',null,null,'exportarEjecutar','77','78');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9567','9555',null,null,'exportarMostrar','79','80');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9568','9555',null,null,'configurarModuloGuardar','81','82');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9569','1',null,null,'Combos','84','103');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9570','9569',null,null,'crearCodigoCombosRelacionados','85','86');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9571','9569',null,null,'cargarComboRelacionado','87','88');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9572','9569',null,null,'cargarComboRelacionadoAvanzado','89','90');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9573','9569',null,null,'js','91','92');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9574','9569',null,null,'registrosPorPagina','93','94');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9575','9569',null,null,'exportarEjecutar','95','96');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9576','9569',null,null,'exportarMostrar','97','98');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9577','9569',null,null,'configurarModulo','99','100');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9578','9569',null,null,'configurarModuloGuardar','101','102');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9579','1',null,null,'Configuraciones','104','125');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9580','9579',null,null,'edit','105','106');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9581','9579',null,null,'borrarCache','107','108');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9582','9579',null,null,'mostrarLogs','109','110');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9583','9579',null,null,'borrarLogs','111','112');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9584','9579',null,null,'js','113','114');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9585','9579',null,null,'registrosPorPagina','115','116');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9586','9579',null,null,'exportarEjecutar','117','118');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9587','9579',null,null,'exportarMostrar','119','120');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9588','9579',null,null,'configurarModulo','121','122');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9589','9579',null,null,'configurarModuloGuardar','123','124');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9590','1',null,null,'Constructores','126','155');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9591','9590',null,null,'index','127','128');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9592','9590',null,null,'add','129','130');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9593','9590',null,null,'cargar','131','132');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9594','9590',null,null,'cargarConexion','133','134');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9595','9590',null,null,'tablasDisponibles','135','136');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9596','9590',null,null,'delete','137','138');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9597','9590',null,null,'indexMasivo','139','140');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9598','9590',null,null,'addMasivo','141','142');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9599','9590',null,null,'js','143','144');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9600','9590',null,null,'registrosPorPagina','145','146');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9601','9590',null,null,'exportarEjecutar','147','148');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9602','9590',null,null,'exportarMostrar','149','150');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9603','9590',null,null,'configurarModulo','151','152');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9604','9590',null,null,'configurarModuloGuardar','153','154');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9605','1',null,null,'Consultas','156','187');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9606','9605',null,null,'index','157','158');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9607','9605',null,null,'lista','159','160');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9608','9605',null,null,'exportar','161','162');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9609','9605',null,null,'nombreTablas','163','164');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9610','9605',null,null,'nombreColumnas','165','166');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9611','9605',null,null,'comprobarTipoConexion','167','168');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9612','9605',null,null,'obtenerTablasConsulta','169','170');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9613','9605',null,null,'cargarConexion','171','172');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9614','9605',null,null,'montarTablaResultado','173','174');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9615','9605',null,null,'js','175','176');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9616','9605',null,null,'registrosPorPagina','177','178');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9617','9605',null,null,'exportarEjecutar','179','180');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9618','9605',null,null,'exportarMostrar','181','182');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9619','9605',null,null,'configurarModulo','183','184');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9620','9605',null,null,'configurarModuloGuardar','185','186');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9621','1',null,null,'Eventos','188','217');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9622','9621',null,null,'index','189','190');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9623','9621',null,null,'add','191','192');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9624','9621',null,null,'create','193','194');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9625','9621',null,null,'edit','195','196');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9626','9621',null,null,'update','197','198');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9627','9621',null,null,'delete','199','200');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9628','9621',null,null,'getEventsForDateRange','201','202');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9629','9621',null,null,'getCalendar','203','204');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9630','9621',null,null,'js','205','206');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9631','9621',null,null,'registrosPorPagina','207','208');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9632','9621',null,null,'exportarEjecutar','209','210');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9633','9621',null,null,'exportarMostrar','211','212');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9634','9621',null,null,'configurarModulo','213','214');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9635','9621',null,null,'configurarModuloGuardar','215','216');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9636','1',null,null,'Filtros','218','245');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9637','9636',null,null,'index','219','220');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9638','9636',null,null,'view','221','222');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9639','9636',null,null,'add','223','224');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9640','9636',null,null,'edit','225','226');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9641','9636',null,null,'delete','227','228');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9642','9636',null,null,'filtro','229','230');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9643','9636',null,null,'leerCondicionesFiltro','231','232');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9644','9636',null,null,'js','233','234');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9645','9636',null,null,'registrosPorPagina','235','236');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9646','9636',null,null,'exportarEjecutar','237','238');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9647','9636',null,null,'exportarMostrar','239','240');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9648','9636',null,null,'configurarModulo','241','242');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9649','9636',null,null,'configurarModuloGuardar','243','244');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9650','1',null,null,'Gestores','246','275');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9651','9650',null,null,'index','247','248');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9652','9650',null,null,'create','249','250');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9653','9650',null,null,'add','251','252');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9654','9650',null,null,'edit','253','254');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9655','9650',null,null,'update','255','256');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9656','9650',null,null,'delete','257','258');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9657','9650',null,null,'getGestores','259','260');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9658','9650',null,null,'getSoporteTecnico','261','262');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9659','9650',null,null,'js','263','264');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9660','9650',null,null,'registrosPorPagina','265','266');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('1',null,null,null,'controllers','1','938');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9661','9650',null,null,'exportarEjecutar','267','268');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9662','9650',null,null,'exportarMostrar','269','270');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9528','1',null,null,'Accesos','2','27');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9663','9650',null,null,'configurarModulo','271','272');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9664','9650',null,null,'configurarModuloGuardar','273','274');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9665','1',null,null,'Grupos','276','301');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9666','9665',null,null,'index','277','278');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9667','9665',null,null,'add','279','280');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9668','9665',null,null,'create','281','282');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9669','9665',null,null,'edit','283','284');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9670','9665',null,null,'update','285','286');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9671','9665',null,null,'delete','287','288');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9672','9665',null,null,'js','289','290');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9673','9665',null,null,'registrosPorPagina','291','292');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9674','9665',null,null,'exportarEjecutar','293','294');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9675','9665',null,null,'exportarMostrar','295','296');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9676','9665',null,null,'configurarModulo','297','298');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9677','9665',null,null,'configurarModuloGuardar','299','300');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9678','1',null,null,'Historicos','302','319');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9679','9678',null,null,'index','303','304');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9680','9678',null,null,'add','305','306');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9681','9678',null,null,'js','307','308');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9682','9678',null,null,'registrosPorPagina','309','310');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9683','9678',null,null,'exportarEjecutar','311','312');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9684','9678',null,null,'exportarMostrar','313','314');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9685','9678',null,null,'configurarModulo','315','316');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9686','9678',null,null,'configurarModuloGuardar','317','318');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9687','1',null,null,'Informes','320','347');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9688','9687',null,null,'index','321','322');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9689','9687',null,null,'add','323','324');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9690','9687',null,null,'create','325','326');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9691','9687',null,null,'edit','327','328');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9692','9687',null,null,'update','329','330');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9693','9687',null,null,'delete','331','332');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9694','9687',null,null,'imprimir','333','334');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9695','9687',null,null,'js','335','336');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9696','9687',null,null,'registrosPorPagina','337','338');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9697','9687',null,null,'exportarEjecutar','339','340');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9698','9687',null,null,'exportarMostrar','341','342');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9699','9687',null,null,'configurarModulo','343','344');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9700','9687',null,null,'configurarModuloGuardar','345','346');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9701','1',null,null,'Informestipos','348','373');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9702','9701',null,null,'index','349','350');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9703','9701',null,null,'add','351','352');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9704','9701',null,null,'create','353','354');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9705','9701',null,null,'edit','355','356');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9706','9701',null,null,'update','357','358');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9707','9701',null,null,'delete','359','360');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9708','9701',null,null,'js','361','362');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9709','9701',null,null,'registrosPorPagina','363','364');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9710','9701',null,null,'exportarEjecutar','365','366');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9711','9701',null,null,'exportarMostrar','367','368');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9712','9701',null,null,'configurarModulo','369','370');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9713','9701',null,null,'configurarModuloGuardar','371','372');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9714','1',null,null,'Logmessages','374','389');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9715','9714',null,null,'getErrorLogMessages','375','376');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9716','9714',null,null,'js','377','378');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9717','9714',null,null,'registrosPorPagina','379','380');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9718','9714',null,null,'exportarEjecutar','381','382');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9719','9714',null,null,'exportarMostrar','383','384');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9720','9714',null,null,'configurarModulo','385','386');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9721','9714',null,null,'configurarModuloGuardar','387','388');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9722','1',null,null,'Logs','390','415');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9723','9722',null,null,'index','391','392');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9724','9722',null,null,'add','393','394');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9725','9722',null,null,'create','395','396');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9726','9722',null,null,'edit','397','398');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9727','9722',null,null,'update','399','400');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9728','9722',null,null,'delete','401','402');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9729','9722',null,null,'js','403','404');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9730','9722',null,null,'registrosPorPagina','405','406');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9731','9722',null,null,'exportarEjecutar','407','408');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9732','9722',null,null,'exportarMostrar','409','410');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9733','9722',null,null,'configurarModulo','411','412');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9734','9722',null,null,'configurarModuloGuardar','413','414');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9735','1',null,null,'Manuales','416','443');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9737','9735',null,null,'view','419','420');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9738','9735',null,null,'add','421','422');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9739','9735',null,null,'create','423','424');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9740','9735',null,null,'edit','425','426');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9741','9735',null,null,'update','427','428');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9742','9735',null,null,'delete','429','430');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9743','9735',null,null,'js','431','432');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9744','9735',null,null,'registrosPorPagina','433','434');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9745','9735',null,null,'exportarEjecutar','435','436');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9746','9735',null,null,'exportarMostrar','437','438');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9747','9735',null,null,'configurarModulo','439','440');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9748','9735',null,null,'configurarModuloGuardar','441','442');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9749','1',null,null,'Mensajes','444','483');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9750','9749',null,null,'comprobarMensajesNuevos','445','446');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9751','9749',null,null,'enviados','447','448');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9752','9749',null,null,'recibidos','449','450');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9753','9749',null,null,'add','451','452');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9754','9749',null,null,'enviar','453','454');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9755','9749',null,null,'edit_enviados','455','456');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9756','9749',null,null,'responderEnviados','457','458');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9757','9749',null,null,'edit_recibidos','459','460');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9758','9749',null,null,'responderRecibidos','461','462');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9759','9749',null,null,'delete','463','464');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9760','9749',null,null,'getLastMessages','465','466');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9761','9749',null,null,'getNotReadMessagesCounter','467','468');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9762','9749',null,null,'registrosPorPagina','469','470');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9763','9749',null,null,'filtro','471','472');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9764','9749',null,null,'exportarMostrar','473','474');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9765','9749',null,null,'exportarEjecutar','475','476');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9766','9749',null,null,'configurarModulo','477','478');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9767','9749',null,null,'configurarModuloGuardar','479','480');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9768','9749',null,null,'js','481','482');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9769','1',null,null,'Menus','484','511');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9770','9769',null,null,'index','485','486');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9771','9769',null,null,'add','487','488');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9772','9769',null,null,'create','489','490');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9773','9769',null,null,'edit','491','492');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9774','9769',null,null,'update','493','494');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9775','9769',null,null,'delete','495','496');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9776','9769',null,null,'draw','497','498');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9777','9769',null,null,'js','499','500');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9778','9769',null,null,'registrosPorPagina','501','502');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9779','9769',null,null,'exportarEjecutar','503','504');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9780','9769',null,null,'exportarMostrar','505','506');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9781','9769',null,null,'configurarModulo','507','508');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9782','9769',null,null,'configurarModuloGuardar','509','510');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9783','1',null,null,'Pages','512','529');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9784','9783',null,null,'display','513','514');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9785','9783',null,null,'maintenance','515','516');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9786','9783',null,null,'js','517','518');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9787','9783',null,null,'registrosPorPagina','519','520');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9788','9783',null,null,'exportarEjecutar','521','522');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9789','9783',null,null,'exportarMostrar','523','524');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9790','9783',null,null,'configurarModulo','525','526');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9791','9783',null,null,'configurarModuloGuardar','527','528');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9792','1',null,null,'Pdfs','530','549');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9793','9792',null,null,'index','531','532');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9794','9792',null,null,'array2table','533','534');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9795','9792',null,null,'styles','535','536');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9796','9792',null,null,'js','537','538');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9797','9792',null,null,'registrosPorPagina','539','540');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9798','9792',null,null,'exportarEjecutar','541','542');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9799','9792',null,null,'exportarMostrar','543','544');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9800','9792',null,null,'configurarModulo','545','546');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9801','9792',null,null,'configurarModuloGuardar','547','548');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9802','1',null,null,'Permisos','550','589');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9803','9802',null,null,'index','551','552');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9804','9802',null,null,'add','553','554');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9805','9802',null,null,'edit','555','556');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9806','9802',null,null,'delete','557','558');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9807','9802',null,null,'resetSync','559','560');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9808','9802',null,null,'checktosync','561','562');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9809','9802',null,null,'sync','563','564');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9810','9802',null,null,'syncResourcesForController','565','566');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9811','9802',null,null,'cleanAcosFromConstructor','567','568');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9812','9802',null,null,'getControllerPath','569','570');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9813','9802',null,null,'darPermisoDesdeConstructor','571','572');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9814','9802',null,null,'verifyAcos','573','574');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9815','9802',null,null,'recoverAcos','575','576');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9816','9802',null,null,'js','577','578');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9817','9802',null,null,'registrosPorPagina','579','580');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9818','9802',null,null,'exportarEjecutar','581','582');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9819','9802',null,null,'exportarMostrar','583','584');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9820','9802',null,null,'configurarModulo','585','586');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9821','9802',null,null,'configurarModuloGuardar','587','588');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9822','1',null,null,'Usuarios','590','619');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9823','9822',null,null,'index','591','592');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9824','9822',null,null,'add','593','594');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9825','9822',null,null,'create','595','596');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9826','9822',null,null,'edit','597','598');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9827','9822',null,null,'update','599','600');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9828','9822',null,null,'delete','601','602');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9829','9822',null,null,'login','603','604');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9830','9822',null,null,'logout','605','606');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9831','9822',null,null,'js','607','608');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9832','9822',null,null,'registrosPorPagina','609','610');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9833','9822',null,null,'exportarEjecutar','611','612');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9834','9822',null,null,'exportarMostrar','613','614');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9835','9822',null,null,'configurarModulo','615','616');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9836','9822',null,null,'configurarModuloGuardar','617','618');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9837','1',null,null,'permisosmenus','620','677');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9838','9837','Menu','1',null,'621','622');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9839','9837','Menu','2',null,'623','624');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9840','9837','Menu','3',null,'625','626');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9841','9837','Menu','4',null,'627','628');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9842','9837','Menu','5',null,'629','630');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9843','9837','Menu','6',null,'631','632');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9844','9837','Menu','7',null,'633','634');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9845','9837','Menu','8',null,'635','636');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9846','9837','Menu','9',null,'637','638');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9847','9837','Menu','10',null,'639','640');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9848','9837','Menu','11',null,'641','642');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9849','9837','Menu','12',null,'643','644');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9850','9837','Menu','13',null,'645','646');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9851','9837','Menu','14',null,'647','648');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9852','9837','Menu','15',null,'649','650');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9853','9837','Menu','16',null,'651','652');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9854','9837','Menu','17',null,'653','654');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9855','9837','Menu','18',null,'655','656');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9900','9837','Menu','108',null,'667','668');
Insert into ACOS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('9736','9735',null,null,'index','417','418');
REM INSERTING into AROS
SET DEFINE OFF;
Insert into AROS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('1',null,'Grupo','1',null,'1','8');
Insert into AROS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('2',null,'Usuario','1',null,'8','1');
Insert into AROS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('3',null,'Grupo','2',null,'9','12');
Insert into AROS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('4','3','Usuario','2',null,'10','11');
Insert into AROS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('5',null,'Grupo','3',null,'13','16');
Insert into AROS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('6','5','Usuario','3',null,'14','15');
Insert into AROS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('16','1','Usuario','10',null,'6','7');
Insert into AROS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('14','1','Usuario','8',null,'2','3');
Insert into AROS (ID,PARENT_ID,MODEL,FOREIGN_KEY,ALIAS,LFT,RGHT) values ('15','1','Usuario','9',null,'4','5');
REM INSERTING into AROS_ACOS
SET DEFINE OFF;
Insert into AROS_ACOS (ID,ARO_ID,ACO_ID,CREATE_,READ_,UPDATE_,DELETE_) values ('1','1','1','1','1','1','1');
SET DEFINE OFF;
Insert into GRUPOS (ID,NOMBRE,DESCRIPCION,CREATED,MODIFIED) values ('1','Administradores',null,to_timestamp('08/01/13 08:42:32,000000000','DD/MM/RR HH24:MI:SS,FF'),to_timestamp('04/02/13 10:13:47,000000000','DD/MM/RR HH24:MI:SS,FF'));
Insert into GRUPOS (ID,NOMBRE,DESCRIPCION,CREATED,MODIFIED) values ('2','Usuarios',null,to_timestamp('24/09/13 12:00:59,000000000','DD/MM/RR HH24:MI:SS,FF'),to_timestamp('14/11/13 13:09:26,000000000','DD/MM/RR HH24:MI:SS,FF'));
Insert into GRUPOS (ID,NOMBRE,DESCRIPCION,CREATED,MODIFIED) values ('3','Coordinadores',null,to_timestamp('24/09/13 12:08:06,000000000','DD/MM/RR HH24:MI:SS,FF'),to_timestamp('24/09/13 12:08:06,000000000','DD/MM/RR HH24:MI:SS,FF'));
REM INSERTING into INFORMESTIPOS
SET DEFINE OFF;
Insert into INFORMESTIPOS (ID,NOMBRE) values ('1','Listados');
Insert into INFORMESTIPOS (ID,NOMBRE) values ('2','Cartas');
Insert into INFORMESTIPOS (ID,NOMBRE) values ('3','Etiquetas');
REM INSERTING into LOGS
SET DEFINE OFF;
REM INSERTING into MANUALES
SET DEFINE OFF;
REM INSERTING into MENSAJES
SET DEFINE OFF;
REM INSERTING into MENUS
SET DEFINE OFF;
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('1','Sistema','0','Activo',null,null,'0','>Sistema','fa fa-sitemap');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('2','Grupos','6','Activo','grupos','index','5','>Sistema>Administraci?n>Grupos','fa fa-group');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('3','Usuarios','6','Activo','usuarios','index','6','>Sistema>Administraci?n>Usuarios','fa fa-user ');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('4','Menus','5','Activo','menus','index','3','>Sistema>Configuraci?n>Menus','fa fa-list-alt');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('5','Configuraci?n','1','Activo',null,null,'0','>Sistema>Configuraci?n','fa fa-cog');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('6','Administraci?n','1','Activo',null,null,'1','>Sistema>Administraci?n','fa fa-persona');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('7','Herramientas','1','Activo',null,null,'2','>Sistema>Herramientas','fa fa-wrench ');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('8','Configuraciones','5','Activo','configuraciones','edit/1','0','>Sistema>Configuraci?n>Configuraciones','fa fa-cogs');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('9','Conexiones de BD','5','Activo','bdconexiones','index','1','>Sistema>Configuraci?n>Conexiones de BD','fa fa-link');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('10','Constructor','5','Activo','constructores','index','2','>Sistema>Configuraci?n>Constructor','fa fa-magic');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('11','Accesos','6','Activo','accesos','index','0','>Sistema>Administraci?n>Accesos',null);
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('12','Permisos','6','Activo','permisos','index','4','>Sistema>Administraci?n>Permisos','fa fa-lock');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('13','Logs','7','Activo','logs','index','0','>Sistema>Herramientas>Logs',null);
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('14','Hist?ricos','7','Activo','historicos','index','0','>Sistema>Herramientas>Hist?ricos',null);
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('15','Consultas SQL','7','Activo','consultas','index','7','>Sistema>Herramientas>Consultas SQL','fa fa-bolt');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('16','Auditorias','7','Activo','auditorias','index','8','>Sistema>Herramientas>Auditorias',' fa fa-epub');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('17','Responsables','7','Activo','gestores','index','9','>Sistema>Herramientas>Responsables','fa fa-group');
Insert into MENUS (ID,TITULO,MENU_ID,ESTADO,CONTROLADOR,ACCION,ORDEN,RUTA,ICONO) values ('18','Informes','0','Activo','informes','index','20','>Informes',null);
REM INSERTING into PRUEBAMASIVOS
SET DEFINE OFF;
REM INSERTING into SIDERAS
SET DEFINE OFF;
Insert into SIDERAS (ID,ENTERO,REAL,FECHA,TEXTO,BOOL_EANO,DATETIME,TIME_HORA,COLUMN1,DNI,CC_BANCO,CC_SUCURSAL,CC_DC,CC_CUENTA) values ('43','2','2',to_date('14/11/13','DD/MM/RR'),'2','0',to_timestamp('01/01/70 00:00:00,000000000','DD/MM/RR HH24:MI:SS,FF'),to_timestamp('01/01/70 09:55:00,000000000','DD/MM/RR HH24:MI:SS,FF'),'2',null,null,null,null,null);
Insert into SIDERAS (ID,ENTERO,REAL,FECHA,TEXTO,BOOL_EANO,DATETIME,TIME_HORA,COLUMN1,DNI,CC_BANCO,CC_SUCURSAL,CC_DC,CC_CUENTA) values ('11','98','9,8',to_date('05/09/13','DD/MM/RR'),'98','0',to_timestamp('07/08/13 13:14:15,000000000','DD/MM/RR HH24:MI:SS,FF'),to_timestamp('01/01/00 13:34:54,000000000','DD/MM/RR HH24:MI:SS,FF'),'98',null,null,null,null,null);
Insert into SIDERAS (ID,ENTERO,REAL,FECHA,TEXTO,BOOL_EANO,DATETIME,TIME_HORA,COLUMN1,DNI,CC_BANCO,CC_SUCURSAL,CC_DC,CC_CUENTA) values ('22','3','0',to_date('01/09/13','DD/MM/RR'),'3','0',to_timestamp('06/08/13 13:06:11,000000000','DD/MM/RR HH24:MI:SS,FF'),to_timestamp('01/01/00 13:34:45,000000000','DD/MM/RR HH24:MI:SS,FF'),'33',null,null,null,null,null);
Insert into SIDERAS (ID,ENTERO,REAL,FECHA,TEXTO,BOOL_EANO,DATETIME,TIME_HORA,COLUMN1,DNI,CC_BANCO,CC_SUCURSAL,CC_DC,CC_CUENTA) values ('42','11','11111,22',to_date('01/09/13','DD/MM/RR'),'11','0',to_timestamp('03/09/13 00:00:00,000000000','DD/MM/RR HH24:MI:SS,FF'),to_timestamp('01/01/00 13:34:18,000000000','DD/MM/RR HH24:MI:SS,FF'),'11','71633453E',null,'388',null,null);
REM INSERTING into USUARIOS
SET DEFINE OFF;
Insert into USUARIOS (ID,USERNAME,PASSWORD,GRUPO_ID,NOMBRE,APELLIDO1,APELLIDO2,NIF,DIRECCION_GENERAL,UNIDAD,PUESTO,EMAIL,CREATED,MODIFIED) values ('1','admin','d033e22ae348aeb5660fc2140aec35850c4da997','1','Administrador','(Soporte T?cnico)',null,null,null,null,null,null,to_timestamp('08/01/13 08:43:29,000000000','DD/MM/RR HH24:MI:SS,FF'),to_timestamp('08/01/13 08:43:29,000000000','DD/MM/RR HH24:MI:SS,FF'));
Insert into USUARIOS (ID,USERNAME,PASSWORD,GRUPO_ID,NOMBRE,APELLIDO1,APELLIDO2,NIF,DIRECCION_GENERAL,UNIDAD,PUESTO,EMAIL,CREATED,MODIFIED) values ('2','user','12dea96fec20593566ab75692c9949596833adc9','2','Usuaria','Usuaria','Usuaria',null,null,null,null,null,to_timestamp('24/09/13 12:02:09,000000000','DD/MM/RR HH24:MI:SS,FF'),to_timestamp('02/10/13 15:05:11,000000000','DD/MM/RR HH24:MI:SS,FF'));
Insert into USUARIOS (ID,USERNAME,PASSWORD,GRUPO_ID,NOMBRE,APELLIDO1,APELLIDO2,NIF,DIRECCION_GENERAL,UNIDAD,PUESTO,EMAIL,CREATED,MODIFIED) values ('3','coordinadora','4c25569915bfcd3c7e2862542137a42db9ab38d1','3','Coordinadora','Coordinadora','Coordinadora',null,null,null,null,null,to_timestamp('24/09/13 12:08:37,000000000','DD/MM/RR HH24:MI:SS,FF'),to_timestamp('02/10/13 15:04:57,000000000','DD/MM/RR HH24:MI:SS,FF'));
--------------------------------------------------------
--  DDL for Index ACCESOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ACCESOS_PK" ON "ACCESOS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index ACOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ACOS_PK" ON "ACOS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index AUDITORIAS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUDITORIAS_PK" ON "AUDITORIAS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index CONDICIONES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CONDICIONES_PK" ON "CONDICIONES" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index DEMOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "DEMOS_PK" ON "DEMOS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index FILTROS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "FILTROS_PK" ON "FILTROS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index GESTORES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "GESTORES_PK" ON "GESTORES" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index GRUPOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRUPOS_PK" ON "GRUPOS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index HISTORICOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "HISTORICOS_PK" ON "HISTORICOS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index INFORMES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "INFORMES_PK" ON "INFORMES" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index INFORMESTIPOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "INFORMESTIPOS_PK" ON "INFORMESTIPOS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index LOGS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LOGS_PK" ON "LOGS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index MANUALES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MANUALES_PK" ON "MANUALES" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index MENSAJES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MENSAJES_PK" ON "MENSAJES" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index MENUS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MENUS_PK" ON "MENUS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index PRUEBAMASIVOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PRUEBAMASIVOS_PK" ON "PRUEBAMASIVOS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  DDL for Index SIDERA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SIDERA_PK" ON "SIDERAS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
--------------------------------------------------------
--  DDL for Index USUARIOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "USUARIOS_PK" ON "USUARIOS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
   ;
--------------------------------------------------------
--  Constraints for Table ACCESOS
--------------------------------------------------------

  ALTER TABLE "ACCESOS" ADD CONSTRAINT "ACCESOS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "ACCESOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ACOS
--------------------------------------------------------

  ALTER TABLE "ACOS" ADD CONSTRAINT "ACOS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "ACOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table AROS
--------------------------------------------------------

  ALTER TABLE "AROS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table AROS_ACOS
--------------------------------------------------------

  ALTER TABLE "AROS_ACOS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "AROS_ACOS" MODIFY ("ARO_ID" NOT NULL ENABLE);
 
  ALTER TABLE "AROS_ACOS" MODIFY ("ACO_ID" NOT NULL ENABLE);
 
  ALTER TABLE "AROS_ACOS" MODIFY ("CREATE_" NOT NULL ENABLE);
 
  ALTER TABLE "AROS_ACOS" MODIFY ("READ_" NOT NULL ENABLE);
 
  ALTER TABLE "AROS_ACOS" MODIFY ("UPDATE_" NOT NULL ENABLE);
 
  ALTER TABLE "AROS_ACOS" MODIFY ("DELETE_" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table AUDITORIAS
--------------------------------------------------------

  ALTER TABLE "AUDITORIAS" ADD CONSTRAINT "AUDITORIAS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "AUDITORIAS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CONDICIONES
--------------------------------------------------------

  ALTER TABLE "CONDICIONES" ADD CONSTRAINT "CONDICIONES_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "CONDICIONES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table DEMOS
--------------------------------------------------------

  ALTER TABLE "DEMOS" ADD CONSTRAINT "DEMOS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "DEMOS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "DEMOS" MODIFY ("NOMBRE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EVENTOS
--------------------------------------------------------

  ALTER TABLE "EVENTOS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "EVENTOS" MODIFY ("TITLE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table FILTROS
--------------------------------------------------------

  ALTER TABLE "FILTROS" ADD CONSTRAINT "FILTROS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "FILTROS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "FILTROS" MODIFY ("MODELO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table GESTORES
--------------------------------------------------------

  ALTER TABLE "GESTORES" ADD CONSTRAINT "GESTORES_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "GESTORES" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "GESTORES" MODIFY ("NOMBRE_COMPLETO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table GRUPOS
--------------------------------------------------------

  ALTER TABLE "GRUPOS" ADD CONSTRAINT "GRUPOS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "GRUPOS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "GRUPOS" MODIFY ("NOMBRE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table HISTORICOS
--------------------------------------------------------

  ALTER TABLE "HISTORICOS" ADD CONSTRAINT "HISTORICOS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "HISTORICOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table INFORMES
--------------------------------------------------------

  ALTER TABLE "INFORMES" ADD CONSTRAINT "INFORMES_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "INFORMES" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "INFORMES" MODIFY ("NOMBRE" NOT NULL ENABLE);
 
  ALTER TABLE "INFORMES" MODIFY ("REPORT" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table INFORMESTIPOS
--------------------------------------------------------

  ALTER TABLE "INFORMESTIPOS" ADD CONSTRAINT "INFORMESTIPOS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "INFORMESTIPOS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "INFORMESTIPOS" MODIFY ("NOMBRE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table LOGS
--------------------------------------------------------

  ALTER TABLE "LOGS" ADD CONSTRAINT "LOGS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "LOGS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MANUALES
--------------------------------------------------------

  ALTER TABLE "MANUALES" ADD CONSTRAINT "MANUALES_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "MANUALES" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "MANUALES" MODIFY ("TITULO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MENSAJES
--------------------------------------------------------

  ALTER TABLE "MENSAJES" ADD CONSTRAINT "MENSAJES_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "MENSAJES" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "MENSAJES" MODIFY ("ORIGEN" NOT NULL ENABLE);
 
  ALTER TABLE "MENSAJES" MODIFY ("DESTINO" NOT NULL ENABLE);
 
  ALTER TABLE "MENSAJES" MODIFY ("ASUNTO" NOT NULL ENABLE);
 
  ALTER TABLE "MENSAJES" MODIFY ("CREATED" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MENUS
--------------------------------------------------------

  ALTER TABLE "MENUS" ADD CONSTRAINT "MENUS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "MENUS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "MENUS" MODIFY ("TITULO" NOT NULL ENABLE);
 
  ALTER TABLE "MENUS" MODIFY ("MENU_ID" NOT NULL ENABLE);
 
  ALTER TABLE "MENUS" MODIFY ("ESTADO" NOT NULL ENABLE);
 
  ALTER TABLE "MENUS" MODIFY ("ORDEN" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PRUEBAMASIVOS
--------------------------------------------------------

  ALTER TABLE "PRUEBAMASIVOS" ADD CONSTRAINT "PRUEBAMASIVOS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
 
  ALTER TABLE "PRUEBAMASIVOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SIDERAS
--------------------------------------------------------

  ALTER TABLE "SIDERAS" ADD CONSTRAINT "SIDERA_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
--------------------------------------------------------
--  Constraints for Table USUARIOS
--------------------------------------------------------

  ALTER TABLE "USUARIOS" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "USUARIOS" MODIFY ("USERNAME" NOT NULL ENABLE);
 
  ALTER TABLE "USUARIOS" MODIFY ("GRUPO_ID" NOT NULL ENABLE);
 
  ALTER TABLE "USUARIOS" ADD CONSTRAINT "USUARIOS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT);
  
  
  --------------------------------------------------------
--  DDL for Sequence ACCESOS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ACCESOS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 134 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ACOS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "ACOS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 9996 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence AROS_ACOS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "AROS_ACOS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 63 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence AROS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "AROS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 17 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence AUDITORIAS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "AUDITORIAS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 3 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence CONDICIONES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "CONDICIONES_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence DEMOS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "DEMOS_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 224 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence EVENTOS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "EVENTOS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence FILTROS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "FILTROS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence GESTORES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GESTORES_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 19 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence GRUPOS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRUPOS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence HISTORICOS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "HISTORICOS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence INFORMES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "INFORMES_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence INFORMESTIPOS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "INFORMESTIPOS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 4 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence LOGS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "LOGS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence MANUALES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "MANUALES_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence MENSAJES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "MENSAJES_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence MENUS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "MENUS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 113 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PRUEBAMASIVOS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "PRUEBAMASIVOS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SIDERAS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SIDERAS_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence USUARIOS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "USUARIOS_SEQ"  MINVALUE 1 MAXVALUE 99999999999999 INCREMENT BY 1 START WITH 11 NOCACHE  NOORDER  NOCYCLE ;
   
--------------------------------------------------------
--  DDL for Trigger PK_ACCESOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_ACCESOS_TRIGGER" before insert on "ACCESOS"    for each row begin     if inserting then       if :NEW."ID" is null then          select ACCESOS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_ACCESOS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_ACOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_ACOS_TRIGGER" before insert on "ACOS"    for each row begin     if inserting then       if :NEW."ID" is null then          select ACOS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_ACOS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_AROS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_AROS_TRIGGER" before insert on "AROS"    for each row begin     if inserting then       if :NEW."ID" is null then          select AROS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_AROS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_AROS_ACOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_AROS_ACOS_TRIGGER" before insert on "AROS_ACOS"    for each row begin     if inserting then       if :NEW."ID" is null then          select AROS_ACOS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_AROS_ACOS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_AUDITORIAS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_AUDITORIAS_TRIGGER" before insert on "AUDITORIAS"    for each row begin     if inserting then       if :NEW."ID" is null then          select AUDITORIAS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_AUDITORIAS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_CONDICIONES_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_CONDICIONES_TRIGGER" before insert on "CONDICIONES"    for each row begin     if inserting then       if :NEW."ID" is null then          select CONDICIONES_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_CONDICIONES_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_DEMOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_DEMOS_TRIGGER" 
   before insert on "DEMOS" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select DEMOS_SEQ.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;
/
ALTER TRIGGER "PK_DEMOS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_EVENTOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_EVENTOS_TRIGGER" before insert on "EVENTOS"    for each row begin     if inserting then       if :NEW."ID" is null then          select EVENTOS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_EVENTOS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_FILTROS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_FILTROS_TRIGGER" before insert on "FILTROS"    for each row begin     if inserting then       if :NEW."ID" is null then          select FILTROS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_FILTROS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_GESTORES_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_GESTORES_TRIGGER" before insert on "GESTORES"    for each row begin     if inserting then       if :NEW."ID" is null then          select GESTORES_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_GESTORES_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_GRUPOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_GRUPOS_TRIGGER" before insert on "GRUPOS"    for each row begin     if inserting then       if :NEW."ID" is null then          select GRUPOS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_GRUPOS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_HISTORICOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_HISTORICOS_TRIGGER" before insert on "HISTORICOS"    for each row begin     if inserting then       if :NEW."ID" is null then          select HISTORICOS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_HISTORICOS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_INFORMES_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_INFORMES_TRIGGER" before insert on "INFORMES"    for each row begin     if inserting then       if :NEW."ID" is null then          select INFORMES_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_INFORMES_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_INFORMESTIPOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_INFORMESTIPOS_TRIGGER" before insert on "INFORMESTIPOS"    for each row begin     if inserting then       if :NEW."ID" is null then          select INFORMESTIPOS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_INFORMESTIPOS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_LOGS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_LOGS_TRIGGER" before insert on "LOGS"    for each row begin     if inserting then       if :NEW."ID" is null then          select LOGS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_LOGS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_MANUALES_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_MANUALES_TRIGGER" before insert on "MANUALES"    for each row begin     if inserting then       if :NEW."ID" is null then          select MANUALES_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_MANUALES_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_MENSAJES_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_MENSAJES_TRIGGER" before insert on "MENSAJES"    for each row begin     if inserting then       if :NEW."ID" is null then          select MENSAJES_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_MENSAJES_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_MENUS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_MENUS_TRIGGER" before insert on "MENUS"    for each row begin     if inserting then       if :NEW."ID" is null then          select MENUS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_MENUS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_PRUEBAMASIVOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_PRUEBAMASIVOS_TRIGGER" before insert on "PRUEBAMASIVOS"    for each row begin     if inserting then       if :NEW."ID" is null then          select PRUEBAMASIVOS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_PRUEBAMASIVOS_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PK_USUARIOS_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PK_USUARIOS_TRIGGER" before insert on "USUARIOS"    for each row begin     if inserting then       if :NEW."ID" is null then          select USUARIOS_SEQ.nextval into :NEW."ID" from dual;       end if;    end if; end;
/
ALTER TRIGGER "PK_USUARIOS_TRIGGER" ENABLE;
