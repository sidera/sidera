<?php
/*
 * SIDERA : Sistema de Desarrollo Rapido.
 * Copyright (C) 2014 
 * 
 * Organization: 
 *           Junta de Extremadura
 * Autors:
 *		Francisco Gonzalez Lozano
 *		Jesus Arance Calvo
 *		Javier Mateos Caballero				
 *
 * This file is part of SIDERA, licensed under The MIT License
 * For full copyright and license information, please see the app/lib/LICENSE.txt
    
 * @since         SIDERA 2.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//(cofiguracion manual) Tablas Privadas del Sistema
define("TABLAS_SISTEMA",serialize( array(
    'menus','usuarios','filtros','condiciones','grupos','aros','acos','aros_acos','informes',
    'informestipos','auditorias','mensajes','accesos','eventos','historicos')));

//(cofiguracion manual)Message types
define("SUCCESS",              "success");
define("ERROR",                "error");
define("NOTICE",               "notice");
define("WARNING",              "warning");

//(cofiguracion manual)Message strings
define("MSG_SAVEDSUCCESS",     "El registro ha sido creado correctamente.");
define("MSG_SAVEDERROR",       "No pudo crearse el registro. Int�ntelo de nuevo.");
define("MSG_EDITSUCCESS",      "El registro ha sido guardado correctamente.");
define("MSG_EDITERROR",        "No pudo guardarse el registro. Int�ntelo de nuevo.");
define("MSG_EDITNOTICE",       "Registro invalido, int�nntelo de nuevo.");
define("MSG_DELETESUCCESS",    "El registro ha sido eliminado correctamente.");
define("MSG_DELETERROR",       "No pudo eliminarse el registro. Int�ntelo de nuevo.");
define("MSG_DELETENOTICE",     "Registro invalido, int�ntelo de nuevo.");
define("MSG_VIEWNOTICE",       "Registro invalido, int�ntelo de nuevo.");
define("MSG_IMPORTFILEERROR",  "El tipo de archivo no es v�lido. Int�ntelo de nuevo.");
define("MSG_IMPORTSIZEERROR",  "El tama�o m�ximo permitido son 15MB. Int�ntelo de nuevo.");
define("MSG_IMPORTSUCCESS",    "El archivo ha sido importado correctamente.");
define("MSG_IMPORTERROR",      "No pudo importarse el archivo. Int�ntelo de nuevo.");
define("MSG_CONFIGSUCCESS",    "Configuraci�n guardada con �xito.");

define ("ACTIVO",    "1"); 
define ("INACTIVO",  "0"); 

define ("ADMINISTRADOR","1");

define ("REGISTROS_PAGINA", "10");
define ("VERSION","2.5");

