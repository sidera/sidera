<?php
   /*
    * SIDERA : Sistema de Desarrollo Rapido.
    * Copyright (C) 2014 
    * 
    * Organization: 
    *           Junta de Extremadura
    * Autors:
    *		Francisco Gonzalez Lozano
    *		Jesus Arance Calvo
    *		Javier Mateos Caballero				
    *
    * This file is part of SIDERA, licensed under The MIT License
    * For full copyright and license information, please see the app/lib/LICENSE.txt
    

    * @since         SIDERA 2.1
    * @license       http://www.opensource.org/licenses/mit-license.php MIT License
    *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * In this file you set up your database connection details.
 *
 * @package       cake.config
 */
/**
 * Database configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * datasource => The name of a supported datasource; valid options are as follows:
 *		Database/Mysql 		- MySQL 4 & 5,
 *		Database/Sqlite		- SQLite (PHP5 only),
 *		Database/Postgres	- PostgreSQL 7 and higher,
 *		Database/Sqlserver	- Microsoft SQL Server 2005 and higher
 *
 * You can add custom database datasources (or override existing datasources) by adding the
 * appropriate file to app/Model/Datasource/Database.  Datasources should be named "MyDatasource.php",
 *
 *
 * persistent => true / false
 * Determines whether or not the database should use a persistent connection
 *
 * host =>
 * the host you connect to the database. To add a socket or port number, use "port" => #
 *
 * prefix =>
 * Uses the given prefix for all the tables in this database.  This setting can be overridden
 * on a per-table basis with the Model::$tablePrefix property.
 *
 * schema =>
 * For Postgres specifies which schema you would like to use the tables in. Postgres defaults to "public".
 *
 * encoding =>
 * For MySQL, Postgres specifies the character encoding to use when connecting to the
 * database. Uses database default not specified.
 *
 * unix_socket =>
 * For MySQL to connect via socket specify the `unix_socket` parameter instead of `host` and `port`
 */
require_once "sideraConstants.php";
  
class DATABASE_CONFIG {
    //La configuracion de la conexion $default esta en el archivo sideraConstants.php
    public $default = array(
	"database"   => DEFAULT_DATABASE,
        "host"       => DEFAULT_HOST,
	"login"      => DEFAULT_LOGIN,
	"password"   => DEFAULT_PASSWORD,
	"datasource" => DEFAULT_DATASOURCE,
        "prefix"     => DEFAULT_PREFIX,
	"persistent" => DEFAULT_PERSISTENT,
    );
    
    
    //La configuracion de la conexion $default esta en el archivo sideraConstants.php
    function __construct() {
        if (DEFAULT_DATASOURCE == "Database/Sqlserver") {
            $this->default = array(
                "database" => DEFAULT_DATABASE,
                "host" => DEFAULT_HOST,
                "login" => DEFAULT_LOGIN,
                "password" => DEFAULT_PASSWORD,
                "datasource" => DEFAULT_DATASOURCE,
                "prefix" => DEFAULT_PREFIX,
                "encoding" => PDO::SQLSRV_ENCODING_SYSTEM, //Esto se pone para qeu mande bien los caracteres de acentos y demas cuando nos traemos los dartos del servidor
                "persistent" => DEFAULT_PERSISTENT,
                "schema" => "dbo",
            );
        }
        if (DEFAULT_DATASOURCE == "Database/Sybase") {
            $this->default = array(
                "database" => DEFAULT_DATABASE,
                "host" => DEFAULT_HOST,
                "login" => DEFAULT_LOGIN,
                "password" => DEFAULT_PASSWORD,
                "datasource" => DEFAULT_DATASOURCE,
                "prefix" => DEFAULT_PREFIX,
                //"encoding" => PDO::SQLSRV_ENCODING_SYSTEM, //Esto se pone para qeu mande bien los caracteres de acentos y demas cuando nos traemos los dartos del servidor
                "persistent" => DEFAULT_PERSISTENT,
                "schema" => "dbo",
            );
        }
    }
}