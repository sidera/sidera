<?php

App::uses('AuthComponent', 'Controller/Component');

class UsuarioFixture extends CakeTestFixture {

    public $fields = array(
        'id'        => array('type' => 'integer',  'null' => false, 'default' => null, 'length'  => 10, 'key' => 'primary'),
        'username'  => array('type' => 'string',   'null' => false, 'default' => null, 'key'     => 'unique', 'collate' => 'latin1_spanish_ci', 'charset' => 'latin1'),
        'password'  => array('type' => 'string',   'null' => false, 'default' => null, 'collate' => 'latin1_spanish_ci', 'charset' => 'latin1'),
        'grupo_id'  => array('type' => 'integer',  'null' => false, 'default' => null),
        'nombre'    => array('type' => 'string',   'null' => false, 'default' => null, 'length' => 40, 'collate' => 'latin1_spanish_ci', 'charset' => 'latin1'),
        'apellido1' => array('type' => 'string',   'null' => false, 'default' => null, 'length' => 30, 'collate' => 'latin1_spanish_ci', 'charset' => 'latin1'),
        'apellido2' => array('type' => 'string',   'null' => false, 'default' => null, 'length' => 30, 'collate' => 'latin1_spanish_ci', 'charset' => 'latin1'),
        'nif'       => array('type' => 'string',   'null' => true,  'default' => null, 'length' => 9, 'collate' => 'latin1_spanish_ci', 'charset' => 'latin1'),
        'direccion_general' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 60, 'collate' => 'latin1_spanish_ci', 'charset' => 'latin1'),
        'unidad'    => array('type' => 'string',   'null' => true,  'default' => null, 'length' => 60, 'collate' => 'latin1_spanish_ci', 'charset' => 'latin1'),
        'puesto'    => array('type' => 'string',   'null' => true,  'default' => null, 'length' => 20, 'collate' => 'latin1_spanish_ci', 'charset' => 'latin1'),
        'email'     => array('type' => 'string',   'null' => true,  'default' => null, 'length' => 120, 'collate' => 'latin1_spanish_ci', 'charset' => 'latin1'),
        'created'   => array('type' => 'datetime', 'null' => true,  'default' => null),
        'modified'  => array('type' => 'datetime', 'null' => true,  'default' => null),
        'indexes'   => array(
            'PRIMARY'  => array('column' => 'id', 'unique' => 1),
            'username' => array('column' => 'username', 'unique' => 1)
        ),
        'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_spanish_ci', 'engine' => 'InnoDB')
    );

    public function init(){
        $this->records = array(
            array(
                'id'        => 1,
                'username'  => 'admin',
                'password'  => AuthComponent::password('admin'),
                'grupo_id'  => 1,
                'nombre'    => 'Administrador',
                'apellido1' => '',
                'apellido2' => '',
                'nif'       => '000000X',
                'direccion_general' => 'Lorem ipsum dolor sit amet',
                'unidad'    => 'Lorem ipsum dolor sit amet',
                'puesto'    => 'Lorem ipsum dolor ',
                'email'     => 'Lorem ipsum dolor sit amet',
                'created'   => '2013-10-22 08:46:44',
                'modified'  => '2013-10-22 08:46:44'
            ),
            array(
                'id'        => 2,
                'username'  => 'user',
                'password'  => AuthComponent::password('user'),
                'grupo_id'  => 2,
                'nombre'    => 'Usuario',
                'apellido1' => '',
                'apellido2' => '',
                'nif'       => '11111111X',
                'direccion_general' => 'Lorem ipsum dolor sit amet',
                'unidad'    => 'Lorem ipsum dolor sit amet',
                'puesto'    => 'Lorem ipsum dolor ',
                'email'     => 'Lorem ipsum dolor sit amet',
                'created'   => '2013-10-22 08:46:44',
                'modified'  => '2013-10-22 08:46:44'
            ),
        );
        parent::init();
    }
}
