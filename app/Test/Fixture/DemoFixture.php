<?php
/**
 * DemoFixture
 *
 */
class DemoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'simpletext' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'fecha' => array('type' => 'date', 'null' => false, 'default' => null),
		'numero' => array('type' => 'integer', 'null' => false, 'default' => null),
		'decimal' => array('type' => 'float', 'null' => false, 'default' => null, 'length' => '10,2'),
		'booleano' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'texto' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'grupo_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'usuario_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'menu_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'prueba' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2),
		'datetime' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'nombre' => 'Lorem ipsum dolor sit amet',
			'simpletext' => 'Lorem ip',
			'fecha' => '2013-10-18',
			'numero' => 1,
			'decimal' => 1,
			'booleano' => 1,
			'texto' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'grupo_id' => 1,
			'usuario_id' => 1,
			'menu_id' => 1,
			'prueba' => 1,
			'datetime' => '2013-10-18 13:58:20'
		),
	);

}
