<?php

App::uses("DemosController", "Controller");

class DemosControllerTest extends ControllerTestCase {
    
    public $fixtures = array('app.demo');
    
    public function testIndex() {
        $this->generate('Demos');   
        $this->testAction('/demos/index');
        $registros = $this->vars['registros'];
        debug($registros);
    }
    
    /*
    public $demos;
    
    public function setUp() {
        $this->demos = $this->generate('Demos', array(
            'components' => array(
                'Session','Auth' => array('user')
            )
        ));
    }
   
    public function testIndexFromAdminUser() {
        $this->demos->Auth->staticExpects($this->any())
            ->method('user')
            ->will($this->returnValue(array('id'=>1,'group_id'=>1)));
        
        $this->testAction('/demos/index',array('return'=>'view'));
        $this->assertNotEqual(null, $this->view);

    }	
    */
   
}
