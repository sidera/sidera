<?php

App::uses("UsuariosController", "Controller");

class UsuariosControllerTest extends ControllerTestCase {
    
    public $fixtures = array('app.usuario');
     
    public function testIndexFromNotLoggedInUser() {
        $this->generate('Usuarios', array(
            'components' => array('Auth' => array('user'))
        ));
        $this->controller->Auth->staticExpects($this->any())
            ->method('user')
            ->will($this->returnValue(null));
        
        $this->testAction('/usuarios/index', array('method' => 'get'));
        $this->assertContains('/usuarios/login', $this->headers['Location']);
    }

    public function testIndexFromLoggedInUser() {
        $this->generate('Usuarios', array(
            'components' => array(
                'Acl'  => array('check'),
                'Auth' => array('user')
            )
        ));

        $this->controller->Auth->staticExpects($this->any())
            ->method('user')
            ->will($this->returnValue(array('id'=>1,'grupo_id'=>1)));
              
        $this->testAction('/usuarios/index',array('method' => 'get'));
        $this->assertNotContains('/usuarios/login', $this->headers['Location']);
    }

    public function testLogInForm() {
        $this->testAction('/usuarios/login',array('method'=>'GET','return'=>'view'));
        $this->assertRegExp('/name="form_login"/', $this->view);
    }
    
    public function testLogInEmptyUser() {
        $this->generate('Usuarios', array(
            'components' => array('Auth' => array('user'))
        ));
        $data = array(
            'Usuario' => array(
                'username' => '',
                'password' => ''
            )
        );
        $this->testAction('/usuarios/login',array('data' => $data,'method'=>'POST','return'=>'view'));
        $this->assertRegExp('/name="form_login"/', $this->view);
    }
    /*
    public function testLogInCorrectUser() {
        $this->generate('Usuarios', array(
            'components' => array('Auth' => array('user'))
        ));
        
        $this->controller->Auth->staticExpects($this->any())
            ->method('user')
            ->will($this->returnValue(array('id'=>1,'grupo_id'=>1)));
        
        $data = array(
            'Usuario' => array(
                'username' => 'admin',
                'password' => 'admin'
            )
        );
        $this->testAction('/usuarios/login',array('data' => $data,'return'=>'view','method'=>'POST'));
        debug($this->headers['Location']);
        //$this->assertContains('http://localhost/sidera/', $this->headers['Location']);
    }*/
  
    public function testLogout() {
        $this->generate('Usuarios', array(
            'components' => array('Auth' => array('user'))
        ));
        $this->controller->logout();
        $this->testAction('/usuarios/index',array('method'=>'get'));
        $this->assertContains('/usuarios/login', $this->headers['Location']);
    }
    
    
    public function testAddForm(){
        $this->generate('Usuarios', array(
            'components' => array('Auth' => array('user'))
        ));
        $this->controller->Auth->staticExpects($this->any())
            ->method('user')
            ->will($this->returnValue(array('id'=>1,'grupo_id'=>1)));
        
        $this->testAction('/usuarios/add',array('method'=>'GET','return'=>'view'));
        $this->assertRegExp('/id="UsuarioAddForm"/', $this->view); 
    }
    /*
    //public function testAddWithoutData(){
 
    //}
    
    //public function testAddWithUncompleteData(){}
    
    public function testAddWithCompleteData(){
        
        $this->generate('Usuarios', array(            
            'components' => array('Session', 'Auth' => array('user'))
        ));
        
        $this->controller->Auth->staticExpects($this->any())
            ->method('user')
            ->will($this->returnValue(array('id'=>1,'group_id'=>1)));
        
        
        $data = array('Usuario' => array(
                'id'=>3,
                'username'  => 'prueba',
                'password'  => 'prueba',
                'grupo_id'  => 2,
                'nombre'    => 'prueba',
            ));
        
        $this->testAction('/usuarios/add',array( 'data' => $data, 'method'=>'POST'));
        
        $this->assertContains('http://app/posts', $this->headers['Location']);
        $this->assertEquals(4, $this->controller->Post->find('count'));
        
        //$usuarios->Usuario->read(null,2);
        //debug($usuarios->data);
        
    }
*/
}
